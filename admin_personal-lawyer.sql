a-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 23, 2021 at 04:27 PM
-- Server version: 10.4.13-MariaDB-1:10.4.13+maria~bionic
-- PHP Version: 7.3.28-2+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin_personal-lawyer`
--

-- --------------------------------------------------------

--
-- Table structure for table `cards`
--

CREATE TABLE `cards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firm` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expiration_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_3ds` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `main` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cards`
--

INSERT INTO `cards` (`id`, `firm`, `card_hash`, `expiration_date`, `user_id`, `created_at`, `updated_at`, `card_id`, `card_3ds`, `deleted_at`, `main`) VALUES
(1, NULL, '4400-43XX-XXXX-0284', '01/24', 6, '2021-05-09 22:58:57', '2021-05-09 23:49:58', '33530200', '1', '2021-05-09 23:49:58', 1),
(2, NULL, '4400-43XX-XXXX-0284', '01/24', 6, '2021-05-09 23:52:47', '2021-05-10 00:22:51', '33530200', '1', '2021-05-10 00:22:51', 1),
(3, NULL, '4400-43XX-XXXX-0284', '01/24', 6, '2021-05-10 00:23:23', '2021-05-10 01:17:24', '33531457', '1', '2021-05-10 01:17:24', 1),
(4, NULL, '4400-43XX-XXXX-0284', '01/24', 6, '2021-05-10 01:17:57', '2021-05-10 01:18:36', '33531457', '1', '2021-05-10 01:18:36', 0),
(5, NULL, '4400-43XX-XXXX-0284', '01/24', 6, '2021-05-10 01:19:03', '2021-05-12 15:52:39', '33531457', '1', '2021-05-12 15:52:39', 1),
(6, NULL, '4578-32XX-XXXX-0313', '06/25', 6, '2021-05-10 01:25:48', '2021-05-10 01:27:01', '33532018', '1', '2021-05-10 01:27:01', 0),
(7, NULL, '4578-32XX-XXXX-0313', '06/25', 6, '2021-05-10 01:27:45', '2021-05-12 15:52:42', '33532018', '1', '2021-05-12 15:52:42', 0),
(8, NULL, '4400-43XX-XXXX-0284', '01/24', 6, '2021-05-13 16:05:24', '2021-06-14 03:05:59', '33531457', '1', NULL, 1),
(9, NULL, '5169-49XX-XXXX-8655', '06/22', 9, '2021-05-13 16:28:29', '2021-05-13 16:28:50', '33591211', '1', NULL, 1),
(10, NULL, '4400-43XX-XXXX-8604', '01/24', 11, '2021-05-17 11:25:34', '2021-05-17 11:25:34', '33657498', '1', NULL, 0),
(11, NULL, '4400-43XX-XXXX-8604', '01/24', 15, '2021-05-18 10:11:05', '2021-05-21 09:08:18', '33673483', '1', NULL, 1),
(12, NULL, '4400-43XX-XXXX-4596', '01/24', 36, '2021-06-11 10:02:14', '2021-07-01 17:38:02', '34120501', '1', '2021-07-01 17:38:02', 1),
(13, NULL, '4400-43XX-XXXX-5915', '02/23', 37, '2021-06-11 13:57:09', '2021-06-11 14:07:49', '34125288', '1', NULL, 1),
(14, NULL, '5169-49XX-XXXX-0190', '09/21', 60, '2021-06-23 14:25:43', '2021-06-23 14:27:45', '34394621', '1', NULL, 1),
(15, NULL, '4444-82XX-XXXX-4941', '05/24', 59, '2021-06-25 09:32:28', '2021-06-25 09:47:31', '34431972', '1', '2021-06-25 09:47:31', 0),
(16, NULL, '4400-43XX-XXXX-0284', '01/24', 91, '2021-08-17 01:54:56', '2021-08-17 01:56:44', '35085268', '1', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(2, NULL, 1, 'Category 2', 'category-2', '2021-05-07 20:29:06', '2021-05-07 20:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Пароль', 0, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Токен восстановления', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Дата обновления', 0, 0, 0, 0, 0, 0, '{}', 7),
(8, 1, 'avatar', 'image', 'Аватар', 0, 1, 1, 1, 1, 1, '{}', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Роль', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Дата создания', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Дата обновления', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Дата создания', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Дата обновления', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Отображаемое имя', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Роль', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(23, 4, 'parent_id', 'select_dropdown', 'Родитель', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Сортировка', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Имя', 1, 1, 1, 1, 1, 1, NULL, 4),
(26, 4, 'slug', 'text', 'Slug (ЧПУ)', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Дата создания', 0, 0, 1, 0, 0, 0, NULL, 6),
(28, 4, 'updated_at', 'timestamp', 'Дата обновления', 0, 0, 0, 0, 0, 0, NULL, 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(30, 5, 'author_id', 'text', 'Автор', 1, 0, 1, 1, 0, 1, NULL, 2),
(31, 5, 'category_id', 'text', 'Категория', 1, 0, 1, 1, 1, 0, NULL, 3),
(32, 5, 'title', 'text', 'Название', 1, 1, 1, 1, 1, 1, NULL, 4),
(33, 5, 'excerpt', 'text_area', 'Отрывок', 1, 0, 1, 1, 1, 1, NULL, 5),
(34, 5, 'body', 'rich_text_box', 'Содержимое', 1, 0, 1, 1, 1, 1, NULL, 6),
(35, 5, 'image', 'image', 'Изображение Статьи', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug (ЧПУ)', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(39, 5, 'status', 'select_dropdown', 'Статус', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Дата создания', 0, 1, 1, 0, 0, 0, NULL, 12),
(41, 5, 'updated_at', 'timestamp', 'Дата обновления', 0, 0, 0, 0, 0, 0, NULL, 13),
(42, 5, 'seo_title', 'text', 'SEO Название', 0, 1, 1, 1, 1, 1, NULL, 14),
(43, 5, 'featured', 'checkbox', 'Рекомендовано', 1, 1, 1, 1, 1, 1, NULL, 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(45, 6, 'author_id', 'text', 'Автор', 1, 0, 0, 0, 0, 0, NULL, 2),
(46, 6, 'title', 'text', 'Название', 1, 1, 1, 1, 1, 1, NULL, 3),
(47, 6, 'excerpt', 'text_area', 'Отрывок', 1, 0, 1, 1, 1, 1, NULL, 4),
(48, 6, 'body', 'rich_text_box', 'Содержимое', 1, 0, 1, 1, 1, 1, NULL, 5),
(49, 6, 'slug', 'text', 'Slug (ЧПУ)', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(52, 6, 'status', 'select_dropdown', 'Статус', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Дата создания', 1, 1, 1, 0, 0, 0, NULL, 10),
(54, 6, 'updated_at', 'timestamp', 'Дата обновления', 1, 0, 0, 0, 0, 0, NULL, 11),
(55, 6, 'image', 'image', 'Изображение Страницы', 0, 1, 1, 1, 1, 1, NULL, 12),
(56, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(57, 8, 'name', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{}', 3),
(58, 8, 'order', 'text', 'Порядок', 1, 1, 1, 1, 1, 1, '{}', 4),
(59, 8, 'document_category_id', 'text', 'Document Category Id', 0, 1, 1, 1, 1, 1, '{}', 2),
(60, 8, 'created_at', 'timestamp', 'Создано', 0, 1, 0, 0, 0, 0, '{}', 5),
(61, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 6),
(62, 8, 'document_category_belongsto_document_category_relationship', 'relationship', 'Родительская категория', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\DocumentCategory\",\"table\":\"document_categories\",\"type\":\"belongsTo\",\"column\":\"document_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 7),
(63, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(64, 9, 'name', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{}', 3),
(65, 9, 'document', 'file', 'Документ', 0, 1, 1, 1, 1, 1, '{}', 4),
(66, 9, 'document_category_id', 'text', 'Document Category Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(67, 9, 'order', 'number', 'Порядок', 1, 1, 1, 1, 1, 1, '{}', 5),
(68, 9, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 6),
(69, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(70, 9, 'document_belongsto_document_category_relationship', 'relationship', 'Категория документа', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\DocumentCategory\",\"table\":\"document_categories\",\"type\":\"belongsTo\",\"column\":\"document_category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 8),
(71, 11, 'id', 'text', 'Номер заказа', 1, 1, 0, 0, 0, 0, '{}', 1),
(72, 11, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(73, 11, 'payment_status_id', 'text', 'Payment Status Id', 1, 1, 1, 1, 1, 1, '{}', 3),
(74, 11, 'order_status_id', 'text', 'Order Status Id', 1, 1, 1, 1, 1, 1, '{}', 4),
(75, 11, 'lawyer', 'text', 'Lawyer', 0, 1, 1, 1, 1, 1, '{}', 5),
(76, 11, 'start_time', 'timestamp', 'Начало времени', 0, 1, 1, 1, 1, 1, '{}', 9),
(77, 11, 'end_time', 'timestamp', 'Окончание времени', 0, 1, 1, 1, 1, 1, '{}', 10),
(78, 11, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 11),
(79, 11, 'created_at', 'timestamp', 'Создано', 0, 1, 0, 0, 0, 0, '{}', 12),
(80, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 13),
(81, 11, 'document_id', 'text', 'Document Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(82, 11, 'service_id', 'text', 'Service Id', 1, 1, 1, 1, 1, 1, '{}', 7),
(83, 11, 'card_id', 'text', 'Card Id', 1, 0, 0, 0, 0, 0, '{}', 8),
(84, 11, 'question_title', 'text', 'Вопрос', 0, 1, 1, 1, 1, 1, '{}', 14),
(85, 11, 'order_belongsto_user_relationship', 'relationship', 'Покупатель', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15),
(86, 11, 'order_belongsto_payment_status_relationship', 'relationship', 'Статус оплаты', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\PaymentStatus\",\"table\":\"payment_statuses\",\"type\":\"belongsTo\",\"column\":\"payment_status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 16),
(87, 11, 'order_belongsto_order_status_relationship', 'relationship', 'Статус заказа', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\OrderStatus\",\"table\":\"order_statuses\",\"type\":\"belongsTo\",\"column\":\"order_status_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 17),
(88, 11, 'order_belongsto_user_relationship_1', 'relationship', 'Пользователь', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"lawyer\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 18),
(89, 11, 'order_belongsto_document_relationship', 'relationship', 'Документ', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Document\",\"table\":\"documents\",\"type\":\"belongsTo\",\"column\":\"document_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 19),
(90, 11, 'order_belongsto_service_relationship', 'relationship', 'Услуга', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Models\\\\Service\",\"table\":\"services\",\"type\":\"belongsTo\",\"column\":\"service_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"cards\",\"pivot\":\"0\",\"taggable\":\"0\"}', 20),
(91, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 13, 'name', 'text', 'Название', 1, 1, 1, 1, 1, 1, '{}', 2),
(93, 13, 'price', 'text', 'Цена', 1, 1, 1, 1, 1, 1, '{}', 3),
(94, 13, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 4),
(95, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 5),
(96, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(97, 14, 'messages', 'text_area', 'Текст', 1, 1, 1, 1, 1, 1, '{}', 2),
(98, 14, 'created_at', 'timestamp', 'Создано', 0, 1, 1, 1, 0, 1, '{}', 3),
(99, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 4),
(100, 1, 'phone', 'text', 'Телефон', 0, 1, 1, 1, 1, 1, '{}', 4),
(101, 1, 'balance', 'text', 'Balance', 1, 1, 1, 1, 1, 1, '{}', 5),
(102, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 7),
(103, 1, 'surname', 'text', 'Фамилия', 0, 1, 1, 1, 1, 1, '{}', 10),
(104, 1, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, '{}', 15),
(105, 1, 'device_token', 'text', 'Device Token', 0, 0, 0, 0, 0, 0, '{}', 16),
(106, 1, 'positive_rating', 'text', 'Positive Rating', 1, 0, 0, 0, 0, 0, '{}', 17),
(107, 1, 'negative_rating', 'text', 'Negative Rating', 1, 0, 0, 0, 0, 0, '{}', 18);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'Пользователь', 'Пользователи', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2021-05-07 20:26:33', '2021-05-21 11:47:10'),
(2, 'menus', 'menus', 'Меню', 'Меню', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(3, 'roles', 'roles', 'Роль', 'Роли', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(4, 'categories', 'categories', 'Категория', 'Категории', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(5, 'posts', 'posts', 'Статья', 'Статьи', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(6, 'pages', 'pages', 'Страница', 'Страницы', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(8, 'document_categories', 'document-categories', 'Категория документов', 'Категории документов', NULL, 'App\\Models\\DocumentCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":\"order\",\"order_display_column\":\"name\",\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-09 16:48:25', '2021-05-27 16:04:02'),
(9, 'documents', 'documents', 'Документ', 'Документы', NULL, 'App\\Models\\Document', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-09 16:58:11', '2021-05-09 16:58:41'),
(11, 'orders', 'orders', 'Заказ', 'Заказы', NULL, 'App\\Models\\Order', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-11 14:39:27', '2021-05-13 17:42:02'),
(13, 'services', 'services', 'Услуга', 'Услуги', NULL, 'App\\Models\\Service', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2021-05-12 12:48:05', '2021-05-12 12:48:05'),
(14, 'notifications', 'notifications', 'Уведомление', 'Уведомления', 'voyager-ticket', 'App\\Models\\Notification', NULL, 'App\\Http\\Controllers\\Admin\\NotificationController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2021-05-17 16:31:35', '2021-05-17 16:42:39');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_category_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `name`, `document`, `document_category_id`, `order`, `created_at`, `updated_at`, `description`, `language`, `status`, `status_id`, `price`) VALUES
(1, 'Ходатайство 1', '[{\"download_link\":\"documents\\/May2021\\/FQdWOCcKuFC0OxzMsb21.txt\",\"original_name\":\"1283.txt\"}]', 1, 1, '2021-05-09 16:59:19', '2021-05-11 00:50:46', NULL, NULL, NULL, NULL, NULL),
(2, 'исковое заявление о расторжении брака', '[{\"download_link\":\"documents\\/May2021\\/mnRgzb0Bq2vfoGpVPT90.doc\",\"original_name\":\"1) \\u0418\\u0441\\u043a \\u043e \\u0440\\u0430\\u0441\\u0442\\u043e\\u0440\\u0436\\u0435\\u043d\\u0438\\u0438 \\u0431\\u0440\\u0430\\u043a\\u0430.doc\"}]', 2, 1, '2021-05-17 21:05:10', '2021-05-17 21:05:10', NULL, NULL, NULL, NULL, NULL),
(59, '1)Иск о расторжении брака', 'eLziSKkVuEHsz4rnkIyH.doc', 31, 1, '2020-10-02 06:55:16', '2020-10-02 06:55:16', NULL, 'ru', 'Платный', '1', '3000'),
(61, '2)Иск о разделе общего имущества супругов', 'W7aagP4fN690QUzS8opx.doc', 31, 1, '2020-10-02 06:56:54', '2020-10-02 06:56:54', NULL, 'ru', 'Платный', '1', '3000'),
(62, '3)Иск о взыскании алиментов', 'L80KbfXv3Z473Dlfc7mO.docx', 31, 1, '2020-10-02 06:57:34', '2020-10-02 06:57:34', NULL, 'ru', 'Платный', '1', '3000'),
(63, '4)Иск об установлении отцовства и взыскании алиментов', 'z4yNW7Wsz9B2gEmiM8gj.doc', 31, 1, '2020-10-02 06:58:20', '2020-10-02 06:58:20', NULL, 'ru', 'Платный', '1', '3000'),
(64, '5)Иск о лишении родительских прав', 'odg4It7FNcI5GPUjTE70.doc', 31, 1, '2020-10-02 06:58:53', '2020-10-02 06:58:53', NULL, 'ru', 'Платный', '1', '3000'),
(65, '6)Иск о взыскании алиментов на содержание бывшего супруга', 'cqSumhmVuWiGYDYhfpA4.docx', 31, 1, '2020-10-02 07:00:30', '2020-10-02 07:00:30', NULL, 'ru', 'Платный', '1', '3000'),
(66, '7)Иск об определении места жительства ребёнка', 'cSxXAdy3BPQexmsGsdj8.doc', 31, 1, '2020-10-02 07:02:54', '2020-10-02 07:02:54', NULL, 'ru', 'Платный', '1', '3000'),
(67, '8)Иск об изменении размера алиментов', 'xvvxh32XJDZKRKRXSr0O.doc', 31, 1, '2020-10-02 07:05:09', '2020-10-02 07:05:09', NULL, 'ru', 'Платный', '1', '3000'),
(68, '9)Иск об определении порядка общения с ребенком', 'VakbrMFRVMrLmNiSGoyz.doc', 31, 1, '2020-10-02 07:07:22', '2020-10-02 07:07:22', NULL, 'ru', 'Платный', '1', '3000'),
(69, '10)Иск о взыскании алиментов на других родственников', 'R2U2qgX2sIGbSXUwmckB.doc', 31, 1, '2020-10-02 07:08:36', '2020-10-02 07:08:36', NULL, 'ru', 'Платный', '1', '3000'),
(70, '11)Иск о взыскании по договору займа', 'A2lRv6X9DgiHygYxhVZO.docx', 31, 1, '2020-10-02 07:09:33', '2020-10-02 07:09:33', NULL, 'ru', 'Платный', '1', '3000'),
(71, '12)Иск о взыскании долга и неустойки', 'DXgjDl1hJW2TkgQn4Per.doc', 31, 1, '2020-10-02 07:10:16', '2020-10-02 07:10:16', NULL, 'ru', 'Платный', '1', '3000'),
(72, '13)Иск о взыскании суммы за выполненные работы', 'J9kxKCWuFN1LSTM3rSsk.doc', 31, 1, '2020-10-02 07:30:48', '2020-10-02 07:30:48', NULL, 'ru', 'Платный', '1', '3000'),
(73, '14)Иск о взыскании задолженности по Договору', 'xdLL0KmkbzYkgJbUY1BK.doc', 31, 1, '2020-10-02 07:32:15', '2020-10-02 07:32:15', NULL, 'ru', 'Платный', '1', '3000'),
(74, '15)Иск о взыскании суммы займа', 'OuozUnDnfngEEAh3tcRj.doc', 31, 1, '2020-10-02 07:33:37', '2020-10-02 07:33:37', NULL, 'ru', 'Платный', '1', '3000'),
(75, '16)Иск о признании сделки купли-продажи недействительной', '3CGY8vO5eWLbAO8Dwm96.doc', 31, 1, '2020-10-02 07:34:32', '2020-10-02 07:34:32', NULL, 'ru', 'Платный', '1', '3000'),
(76, '17)Иск об истребовании имущества из чужого незаконного владения', 'NyiinOzeROmDjaeTKdht.doc', 31, 1, '2020-10-02 07:35:27', '2020-10-02 07:35:27', NULL, 'ru', 'Платный', '1', '3000'),
(77, '18)Иск о возмещении морального вреда', 'kzcdMbRAfB2sFp3sLreT.doc', 31, 1, '2020-10-02 07:36:11', '2020-10-02 07:36:11', NULL, 'ru', 'Платный', '1', '3000'),
(78, '19)Иск об установлении факта принятия наследства', '9hNEPFORZWhmukQKIa6K.docx', 31, 1, '2020-10-02 07:37:11', '2020-10-02 07:37:11', NULL, 'ru', 'Платный', '1', '3000'),
(79, '20)Иск о восстановлении срока принятия наследства', 'ERNuAaqQ2MRIo82RqYlK.doc', 31, 1, '2020-10-02 07:38:00', '2020-10-02 07:38:00', NULL, 'ru', 'Платный', '1', '3000'),
(80, '21)Иск о восстановлении на работе', 'lPnNZIgnNmslPHdiu0FH.docx', 31, 1, '2020-10-02 07:38:59', '2020-10-02 07:38:59', NULL, 'ru', 'Платный', '1', '3000'),
(81, '22)Иск о взыскании заработной платы', 'hBYVAoWGGqyaShcuAtdy.docx', 31, 1, '2020-10-02 07:40:19', '2020-10-02 07:40:19', NULL, 'ru', 'Платный', '1', '3000'),
(82, '23)Иск о взыскании ущерба по ДТП', 'Wa2ox5CFx2n1QPjUDGPV.doc', 31, 1, '2020-10-02 07:41:07', '2020-10-02 07:41:07', NULL, 'ru', 'Платный', '1', '3000'),
(83, '24)Иск о взыскании ущерба в результате залива квартиры', 'DYVcZ0fke1Pbplg2XqFm.doc', 31, 1, '2020-10-02 07:42:14', '2020-10-02 07:42:14', NULL, 'ru', 'Платный', '1', '3000'),
(84, '25)Иск об истребовании документов через суд', 'Svm8kaKrGjgKGwv0GSKo.docx', 31, 1, '2020-10-02 07:42:59', '2020-10-02 07:42:59', NULL, 'ru', 'Платный', '1', '3000'),
(85, '26)Иск о возмещении вреда со смертью кормильца', 'BHV22c10yg2kwc5SVbN1.docx', 31, 1, '2020-10-02 07:44:12', '2020-10-02 07:44:12', NULL, 'ru', 'Платный', '1', '3000'),
(86, '27)Иск о признании незаконным отказа на обмен жилья', '7NpVyXDMWz5UZWFvEj00.doc', 31, 1, '2020-10-02 07:46:37', '2020-10-02 07:46:37', NULL, 'ru', 'Платный', '1', '3000'),
(87, '28)Встречное исковое заявление', '37PCiBhOaMG2UfTPwoxv.doc', 31, 1, '2020-10-02 07:47:13', '2020-10-02 07:47:13', NULL, 'ru', 'Платный', '1', '3000'),
(88, '29)Иск об определении порядка пользования жилищем', 'acEZ690E1jBAYGuh71Ym.doc', 31, 1, '2020-10-02 07:47:57', '2020-10-02 07:47:57', NULL, 'ru', 'Платный', '1', '3000'),
(89, '30)Иск о взыскании неустойки', 'MK8GmB13nV07EXjFPFhm.doc', 31, 1, '2020-10-02 07:49:01', '2020-10-02 07:49:01', NULL, 'ru', 'Платный', '1', '3000'),
(90, '31)Иск о восстановлении срока принятия наследства', 'd4Z3o3pU7RYDWzp7tHKU.doc', 31, 1, '2020-10-02 07:49:34', '2020-10-02 07:49:34', NULL, 'ru', 'Платный', '1', '3000'),
(91, '32)Иск о признании наследником', 'HLRW3nkU5TBfanm41qHJ.doc', 31, 1, '2020-10-02 07:50:04', '2020-10-02 07:50:04', NULL, 'ru', 'Платный', '1', '3000'),
(92, '33)Иск о признании недостойным наследником', 'FD7fYxfBir0rwwFKg4zT.docx', 31, 1, '2020-10-02 07:50:41', '2020-10-02 07:50:41', NULL, 'ru', 'Платный', '1', '3000'),
(93, '34)Иск о признании супруга недостойным наследником', 'tOP8wE1PjJbCQdUUPP8N.docx', 31, 1, '2020-10-02 07:52:08', '2020-10-02 07:52:08', NULL, 'ru', 'Платный', '1', '3000'),
(94, '35)Иск о разделе наследственного имущества', 'oTS2YxaHpZ9zS8YK08BX.doc', 31, 1, '2020-10-02 07:52:44', '2020-10-02 07:52:44', NULL, 'ru', 'Платный', '1', '3000'),
(95, '36)Иск о признании право собственности на самовольную постройку', '40z0p5gMYc7hhcYP7NtP.doc', 31, 1, '2020-10-02 07:53:38', '2020-10-02 07:53:38', NULL, 'ru', 'Платный', '1', '3000'),
(96, '37)Иск об установлении сервитута', '9QO6xiLkUQ8KLTXct2Az.doc', 31, 1, '2020-10-02 07:54:10', '2020-10-02 07:54:10', NULL, 'ru', 'Платный', '1', '3000'),
(97, '38)Апелляционная жалоба', 'WGMG6hCxh2nn1yY6BWzB.doc', 31, 1, '2020-10-02 07:54:43', '2020-10-02 07:54:43', NULL, 'ru', 'Платный', '1', '3000'),
(108, '39)Ходатайство об отложении судебного разбирательства', '5C5viD8NKvGQis98IoUE.docx', 3, 1, '2020-11-02 07:27:09', '2020-11-02 07:27:09', NULL, 'ru', 'Платный', '1', '3000'),
(109, '40)Ходатайство об ознакомлении и предоставлении документов на стадии подготовки', '7A3cjyssHVMzJkfdQOKE.doc', 3, 1, '2020-11-02 07:28:43', '2020-11-02 07:28:43', NULL, 'ru', 'Платный', '1', '3000'),
(110, '41)Ходатайство о приобщении документов к материалам дела', 'NcRX9HMiJmHvt3czZ5pc.doc', 3, 1, '2020-11-02 07:29:32', '2020-11-02 07:29:32', NULL, 'ru', 'Платный', '1', '3000'),
(111, '42)Ходатайство об истребовании доказательств', 'aO8t8b77ncvVueeSwiUw.doc', 3, 1, '2020-11-02 07:30:10', '2020-11-02 07:30:10', NULL, 'ru', 'Платный', '1', '3000'),
(112, '43)Ходатайство о проведении закрытого заседания', 'FvtI7OmNtiRypdeWBaYX.doc', 3, 1, '2020-11-02 07:30:55', '2020-11-02 07:30:55', NULL, 'ru', 'Платный', '1', '3000'),
(113, '44)Ходатайство о принятии обеспечительных мер', 'v2fzhKwlBZogBZrZRIWL.docx', 3, 1, '2020-11-02 07:31:38', '2020-11-02 07:31:38', NULL, 'ru', 'Платный', '1', '3000'),
(114, '45)Ходатайство о взыскании расходов', 'ROkn22iW4Rw4Nd9HY6OX.doc', 3, 1, '2020-11-02 07:32:12', '2020-11-02 07:32:12', NULL, 'ru', 'Платный', '1', '3000'),
(115, '46)Ходатайство о возврате ошибочно оплаченной госпошлины', 'ozqXwYNABJ4O9eCP6pQU.doc', 3, 1, '2020-11-02 07:33:12', '2020-11-02 07:33:12', NULL, 'ru', 'Платный', '1', '3000'),
(116, '47)Ходатайство о вызове свидетелей', 'AXfE4bAJ5FQCb4jHDkVJ.docx', 3, 1, '2020-11-02 07:34:43', '2020-11-02 07:34:43', NULL, 'ru', 'Платный', '1', '3000'),
(117, '48)Ходатайство о предоставлении переводчика', '5DfYIjJVdM61vBcMdbsS.doc', 3, 1, '2020-11-02 07:35:22', '2020-11-02 07:35:22', NULL, 'ru', 'Платный', '1', '3000'),
(118, '49)Ходатайство о привлечении специалиста', 'yBi2WQgqyTWYv8Brt0ww.doc', 3, 1, '2020-11-02 07:36:53', '2020-11-02 07:36:53', NULL, 'ru', 'Платный', '1', '3000'),
(119, '50)Ходатайство о вызове эксперта', 'osba95f51DR8hS0vSaSH.doc', 3, 1, '2020-11-02 07:37:25', '2020-11-02 07:37:25', NULL, 'ru', 'Платный', '1', '3000'),
(120, '51)Ходатайство о назначении автотехнической экспертизы', 'Clak84XbQw8NVaycoLzK.doc', 3, 1, '2020-11-02 07:38:14', '2020-11-02 07:38:14', NULL, 'ru', 'Платный', '1', '3000'),
(121, '52)Ходатайство о назначении генетической экспертизы', 'btRoEtBNEKNiVCSlUf1f.doc', 3, 1, '2020-11-02 07:39:04', '2020-11-02 07:39:04', NULL, 'ru', 'Платный', '1', '3000'),
(122, '53)Ходатайство о назначении медицинской экспертизы', '42JVocCu9xhLNkCbaPgL.docx', 3, 1, '2020-11-02 07:39:43', '2020-11-02 07:39:43', NULL, 'ru', 'Платный', '1', '3000'),
(123, '54)Ходатайство о назначении оценочной экспертизы', 'vsYFuVLNV8OkWycmIisl.doc', 3, 1, '2020-11-02 07:40:28', '2020-11-02 07:40:28', NULL, 'ru', 'Платный', '1', '3000'),
(124, '55)Ходатайство о рассмотрении в порядке общего судопроизводства', 'b0lpvWhRREdjg2u40Luy.doc', 3, 1, '2020-11-02 07:41:38', '2020-11-02 07:41:38', NULL, 'ru', 'Платный', '1', '3000'),
(125, '56)Ходатайство о рассмотрении в отсутствии истца, ответчика, третьего лица', 'P0Ho5Bg64mO4qXmln564.doc', 3, 1, '2020-11-02 07:42:38', '2020-11-02 07:42:38', NULL, 'ru', 'Платный', '1', '3000'),
(126, '57)Ходатайство об ограничении на выезд', 'MPD5gNTdmVXGWhQFFq5V.doc', 3, 1, '2020-11-02 07:43:12', '2020-11-02 07:43:12', NULL, 'ru', 'Платный', '1', '3000'),
(127, '58)Ходатайство об отводе судьи', 'leqjuz68s8JuWPP7iL8V.doc', 3, 1, '2020-11-02 07:43:40', '2020-11-02 07:43:40', NULL, 'ru', 'Платный', '1', '3000'),
(152, '59)Заявление на выдачу исполнительного листа', '1B7QWJwjiEVcOg8AGlno.doc', 5, 1, '2020-11-27 06:20:41', '2020-11-27 06:20:41', NULL, 'ru', 'Платный', '1', '3000'),
(153, '60)Заявление о возврате иска в связи с преждевременностью', 'zBnV80N3MnYl14YnXzh7.doc', 5, 1, '2020-11-27 06:21:31', '2020-11-27 06:21:31', NULL, 'ru', 'Платный', '1', '3000'),
(154, '61)Заявление о возвращении искового заявления', 'jipBVwWXf1rkNAkzeiRb.docx', 5, 1, '2020-11-27 06:22:14', '2020-11-27 06:22:14', NULL, 'ru', 'Платный', '1', '3000'),
(155, '62)Заявление о восстановлении пропущенного срока для подачи апелляционной жалобы', 'EJ1ChpQZpqy8oMD7cpWk.doc', 5, 1, '2020-11-27 06:23:06', '2020-11-27 06:23:06', NULL, 'ru', 'Платный', '1', '3000'),
(157, '63)Заявление в суд о возврате оплаченной государственной пошлины', 'USFgJaFlatFhqMzCJ1tL.docx', 5, 1, '2020-11-27 06:25:27', '2020-11-27 06:25:27', NULL, 'ru', 'Платный', '1', '3000'),
(158, '64)Заявление об отмене судебного приказа о взыскании алиментов на содержание несовершеннолетнего ребенка', 'C2oX63ZOKjgLrM6IHWYs.docx', 5, 1, '2020-11-27 06:29:26', '2020-11-27 06:29:26', NULL, 'ru', 'Платный', '1', '3000'),
(159, '65)Заявление о выдаче аудио-видео записи', 'zSPzmOoDQx3SlVoOlaQq.docx', 5, 1, '2020-11-27 06:30:09', '2020-11-27 06:30:09', NULL, 'ru', 'Платный', '1', '3000'),
(160, '66)Заявление о вынесении судебного приказа о взыскании алиментов', 'ADr4kykukl0wSHjIFN4z.docx', 5, 1, '2020-11-27 06:33:20', '2020-11-27 06:33:20', NULL, 'ru', 'Платный', '1', '3000'),
(161, '67)Заявление о признании гражданина безвестно отсутствующим', 'HhhP5B9aeegCSlBR7H5f.doc', 5, 1, '2020-11-27 06:34:07', '2020-11-27 06:34:07', NULL, 'ru', 'Платный', '1', '3000'),
(162, '68)Заявление о признании гражданина недееспособным', 'mn3tXqyKje8e8DHd94xy.doc', 5, 1, '2020-11-27 06:34:45', '2020-11-27 06:34:45', NULL, 'ru', 'Платный', '1', '3000'),
(163, '69)Заявление о признании гражданина ограниченно дееспособным', 'zQbLizny2KoPzpXMeg4u.doc', 5, 1, '2020-11-27 06:35:21', '2020-11-27 06:35:21', NULL, 'ru', 'Платный', '1', '3000'),
(164, '70)Заявление о применении реабилитационной процедуры', 'PLbxof2N7FFgrmNh1gdl.doc', 5, 1, '2020-11-27 06:36:05', '2020-11-27 06:36:05', NULL, 'ru', 'Платный', '1', '3000'),
(165, '71)Заявление об обеспечении иска', 'KbM9fjn0nvWT1J8RFWgh.doc', 5, 1, '2020-11-27 06:36:34', '2020-11-27 06:36:34', NULL, 'ru', 'Платный', '1', '3000'),
(166, '72)Заявление об отказе от иска', 'ppkctm6yh85MhJMblU4b.docx', 5, 1, '2020-11-27 06:37:04', '2020-11-27 06:37:04', NULL, 'ru', 'Платный', '1', '3000'),
(167, '73)Заявление об установлении юридического факта (родства)', 'SUEgRifCSqCosn78Qytg.docx', 5, 1, '2020-11-27 06:37:54', '2020-11-27 06:37:54', NULL, 'ru', 'Платный', '1', '3000'),
(168, '74)Заявление о восстановлении срока исковой давности', 'Qin5pzBsUQdlDgehISQ1.doc', 5, 1, '2020-11-27 06:38:34', '2020-11-27 06:38:34', NULL, 'ru', 'Платный', '1', '3000'),
(169, '75)Заявление об отмене заочного решения', 'O4MLitXsWRHSHs8gUeoA.doc', 5, 1, '2020-11-27 06:39:09', '2020-11-27 06:39:09', NULL, 'ru', 'Платный', '1', '3000'),
(170, '76)Заявление о замене мер обеспечения', 'yGsy7MSp02Tt9WZkSlPa.doc', 5, 1, '2020-11-27 06:39:39', '2020-11-27 06:39:39', NULL, 'ru', 'Платный', '1', '3000'),
(171, '77)Заявление об отмене решения  вынесенного в порядке упрощенного производства', 'OvGf2IgVALrwv4bn6ZVQ.docx', 5, 1, '2020-11-27 06:40:32', '2020-11-27 06:40:32', NULL, 'ru', 'Платный', '1', '3000'),
(172, '78)Заявление о пересмотре решения по вновь открывшимся обстоятельствам', '4vvWACDsMmxQQ1drwyPV.doc', 5, 1, '2020-11-27 06:41:18', '2020-11-27 06:41:18', NULL, 'ru', 'Платный', '1', '3000'),
(173, '79)Заявление об отсрочке исполнения решения суда', 'XLc7O4MQSOMx5fETDr2t.doc', 5, 1, '2020-11-27 06:42:01', '2020-11-27 06:42:01', NULL, 'ru', 'Платный', '1', '3000'),
(174, '80)Заявление об установлении факта смерти гражданина', 'CPCQsBpNv4FghfZDBkD9.docx', 5, 1, '2020-11-27 09:34:39', '2020-11-27 09:34:39', NULL, 'ru', 'Платный', '1', '3000'),
(176, '81)Заявление б установлении факта нахождении на иждивении', 'gSqfWxOWty46AqxbAP31.doc', 5, 1, '2020-11-27 09:43:22', '2020-11-27 09:43:22', NULL, 'ru', 'Платный', '1', '3000'),
(177, '82)Заявление об увеличении исковых требований', 'BkgS9C8mDYYkQ9QSLWB5.doc', 5, 1, '2020-11-27 09:46:04', '2020-11-27 09:46:04', NULL, 'ru', 'Платный', '1', '3000'),
(178, '85)Заявление об исправлении описки или арифметической ошибке', 'vNdBWlPkUlavJBak9CxP.doc', 5, 1, '2020-12-24 03:53:33', '2020-12-24 03:53:33', NULL, 'ru', 'Платный', '1', '3000'),
(179, '86)Заявление о выдаче копии решения суда', 'QCfcCNJLHyNvYxdHLdWC.doc', 5, 1, '2020-12-24 03:56:47', '2020-12-24 03:56:47', NULL, 'ru', 'Платный', '1', '3000'),
(180, '87)Заявление о рассрочке исполнения решения суда', 'DuI10u9QeT5p9nrAqUYK.docx', 5, 1, '2020-12-24 04:01:10', '2020-12-24 04:01:10', NULL, 'ru', 'Платный', '1', '3000'),
(182, '88)Заявление на выдачу дубликата судебного приказа', 'R5cZpEtsXplorJEEvx4d.doc', 5, 1, '2020-12-24 04:17:48', '2020-12-24 04:17:48', NULL, 'ru', 'Платный', '1', '3000'),
(183, '89)Заявление о приостановлении производства', 'APSpOTgBXaq3L9yijx1B.doc', 5, 1, '2020-12-24 04:19:21', '2020-12-24 04:19:21', NULL, 'ru', 'Платный', '1', '3000'),
(184, '90)Заявление об изменении порядка и способа исполнения решения суда', 'EYA3S56H28PJOHsDniFV.doc', 5, 1, '2020-12-24 04:22:01', '2020-12-24 04:22:01', NULL, 'ru', 'Платный', '1', '3000'),
(185, '91)Жалоба на постановление по делу об административном правонарушении (лишение вод.удостоверения).', 'BZLRD7ebEiqHR0hYQTmZ.doc', 4, 1, '2021-01-14 05:58:00', '2021-01-14 05:58:00', NULL, 'ru', 'Платный', '1', '3000'),
(221, '92)Жалоба на действия частного судебного исполнителя связанные с оценкой имущества (в порядке ст.250 ГПКРК)', 'jD7xOlzEdMgzij4MDwBS.doc', 4, 1, '2021-01-15 03:42:25', '2021-01-15 03:42:25', NULL, 'ru', 'Платный', '1', '3000'),
(222, '93)Жалоба на судью', 'lob1a6sA3XYsvCIZr6y0.doc', 4, 1, '2021-01-15 03:43:12', '2021-01-15 03:43:12', NULL, 'ru', 'Платный', '1', '3000'),
(223, '94)Жалоба прокурору на неисполнение судебного акта', 'lpT14pvbr0cNTMbzlZOR.docx', 4, 1, '2021-01-15 03:44:20', '2021-01-15 03:44:20', NULL, 'ru', 'Платный', '1', '3000'),
(224, '95)Жалоба на действие частного судебного исполнителя по проведению торгов', 'oC4d3I84vlIq9XNqTT9d.doc', 4, 1, '2021-01-15 03:46:12', '2021-01-15 03:46:12', NULL, 'ru', 'Платный', '1', '3000'),
(225, '96)Досудебная претензия по выплате двойной суммы задатка', 'zj8CHLdHzPSC4aEuQWOH.docx', 32, 1, '2021-01-15 03:48:01', '2021-01-15 03:48:01', NULL, 'ru', 'Платный', '1', '3000'),
(226, '97)Заявление в суд об отмене судебного приказа', 'cLHugFLGwDzOWDLW4dPL.docx', 32, 1, '2021-01-15 03:49:15', '2021-01-15 03:49:15', NULL, 'ru', 'Платный', '1', '3000'),
(227, '98)Отзыв по иску о расторжении брака', 'ScusBozqoWSonqDybrZ0.doc', 32, 1, '2021-01-15 03:50:38', '2021-01-15 03:50:38', NULL, 'ru', 'Платный', '1', '3000'),
(228, '99)Письмо в ТОО от участника по аудиту', '8Y0I63uX1iLhAoAw8FMs.doc', 32, 1, '2021-01-15 03:51:46', '2021-01-15 03:51:46', NULL, 'ru', 'Платный', '1', '3000'),
(229, '100)Письмо в ТОО от участника по перерегистрации', 'mocasLB1Gt0d3soUb7Uy.doc', 32, 1, '2021-01-15 03:53:02', '2021-01-15 03:53:02', NULL, 'ru', 'Платный', '1', '3000'),
(230, '101)Претензия к должнику с требованием возврата долга по расписке', '0Umti1pAsS15CzJra4NL.docx', 32, 1, '2021-01-15 03:56:40', '2021-01-15 03:56:40', NULL, 'ru', 'Платный', '1', '3000'),
(231, '102)Претензия (о взыскании задолженности)', '95g8W8v2Qwl4sAV8I8lJ.doc', 32, 1, '2021-01-15 03:57:38', '2021-01-15 03:57:38', NULL, 'ru', 'Платный', '1', '3000'),
(232, '103)Договор купли-продажи (по предоплате)', 'Gs6j8oVH3fvr2orwORde.doc', 2, 1, '2021-01-15 04:00:11', '2021-01-15 04:00:11', NULL, 'ru', 'Платный', '1', '3000'),
(233, '104)Договор купли-продажи товара', 'zvAGTKcpw8Xpdvz67nr2.doc', 2, 1, '2021-01-15 04:01:05', '2021-01-15 04:01:05', NULL, 'ru', 'Платный', '1', '3000'),
(234, '105)Договор безвозмездного пользования автотранспортным средством', 'hOnMsrDk4YhWkkRj6Vi2.doc', 2, 1, '2021-01-15 04:02:32', '2021-01-15 04:02:32', NULL, 'ru', 'Платный', '1', '3000'),
(235, '106)Договор подряда (ремонт помещений)', 'hJ0k7UX5kWwdzETMmk8c.doc', 2, 1, '2021-01-15 04:03:30', '2021-01-15 04:03:30', NULL, 'ru', 'Платный', '1', '3000'),
(236, '107)Договор поставки', 'NTztMS9UC7TunXARvANG.doc', 2, 1, '2021-01-15 04:04:16', '2021-01-15 04:04:16', NULL, 'ru', 'Платный', '1', '3000'),
(237, '108)Договор купли-продажи движимого имущества', 'AyPCLs7A7Dqb4sXjGuiu.docx', 2, 1, '2021-01-15 04:05:33', '2021-01-15 04:05:33', NULL, 'ru', 'Платный', '1', '3000'),
(238, '109)Договор простого товарищества (образец)', 'M9WPdFaN3ED4gcYVnRLS.doc', 2, 1, '2021-01-15 04:06:35', '2021-01-15 04:06:35', NULL, 'ru', 'Платный', '1', '3000'),
(239, '110)Договор субаренды', 'JXB2izFT9LkbSLWWif78.doc', 2, 1, '2021-01-15 04:07:30', '2021-01-15 04:07:30', NULL, 'ru', 'Платный', '1', '3000'),
(240, '111)Соглашение-обязательство о неразглашении конфидециальной информации', 'sDjbXpX1rL5rlzHNz4JT.docx', 2, 1, '2021-01-15 04:08:51', '2021-01-15 04:08:51', NULL, 'ru', 'Платный', '1', '3000'),
(241, '112)Договор о полной материальной ответственности', '4MjYiXuxh9qyPLDI2lFc.doc', 2, 1, '2021-01-15 04:10:26', '2021-01-15 04:10:26', NULL, 'ru', 'Платный', '1', '3000'),
(242, '113)Договор аренды автотранспортного средства с последущим выкупом', 'StmMNBbHHW4u0Foe6kHv.doc', 2, 1, '2021-01-15 04:11:38', '2021-01-15 04:11:38', NULL, 'ru', 'Платный', '1', '3000'),
(243, '114)Договор аренды гаража', 'lLrmQjwDFx1aCcgEkkFL.doc', 2, 1, '2021-01-15 04:12:54', '2021-01-15 04:12:54', NULL, 'ru', 'Платный', '1', '3000'),
(244, '115)Договор аренды дома с последующим выкупом', 'caUMqJvacgJrzPWj6QeT.doc', 2, 1, '2021-01-15 04:13:46', '2021-01-15 04:13:46', NULL, 'ru', 'Платный', '1', '3000'),
(245, '116)Договор аренды земельного участка', 'J2rxgJ6KkNF9PfFt3ybD.doc', 2, 1, '2021-01-15 04:15:03', '2021-01-15 04:15:03', NULL, 'ru', 'Платный', '1', '3000'),
(246, '117)Договор аренды квартиры', 'Ddbt9AmITMdGQ238I0B9.doc', 2, 1, '2021-01-15 04:16:06', '2021-01-15 04:16:06', NULL, 'ru', 'Платный', '1', '3000'),
(247, '118)Договор аренды нежилого помещения с оборудованием', 'WNNN5evuLX3vjjPuzaWt.doc', 2, 1, '2021-01-15 04:17:28', '2021-01-15 04:17:28', NULL, 'ru', 'Платный', '1', '3000'),
(248, '119)Договор аренды нежилого помещения', 'hD7HIELfeRelI1suwunI.doc', 2, 1, '2021-01-15 04:18:22', '2021-01-15 04:18:22', NULL, 'ru', 'Платный', '1', '3000'),
(249, '120)Договор аренды производственного помещения', 'BJJFkt7JyFqKkuExb9FT.doc', 2, 1, '2021-01-15 04:19:51', '2021-01-15 04:19:51', NULL, 'ru', 'Платный', '1', '3000'),
(250, '121)Договор аренды транспортного средства с экипажем', '6BgTCZywh0TegrTKexOl.doc', 2, 1, '2021-01-15 04:28:54', '2021-01-15 04:28:54', NULL, 'ru', 'Платный', '1', '3000'),
(251, '122)Договор аренды', 'gvwmhcWmJ8gukkFNHFnM.doc', 2, 1, '2021-01-15 04:30:27', '2021-01-15 04:30:27', NULL, 'ru', 'Платный', '1', '3000'),
(252, '123)Договор безвозмездного пользования автотранспортным средством', '2OscRfZSxf5sTqOhH5Y7.doc', 2, 1, '2021-01-15 04:31:57', '2021-01-15 04:31:57', NULL, 'ru', 'Платный', '1', '3000'),
(253, '124)Договор возмездного оказания услуг', 'CcYKzIFY0viLdKEDxTlR.doc', 2, 1, '2021-01-15 04:33:35', '2021-01-15 04:33:35', NULL, 'ru', 'Платный', '1', '3000'),
(254, '125)Договор об оказании временной возвратной материальной помощий', 'Teayc38tMwei3qsSCMuF.doc', 2, 1, '2021-01-15 04:35:22', '2021-01-15 04:35:22', NULL, 'ru', 'Платный', '1', '3000'),
(255, '126)Договор доверительного управления недвижимым имуществом', '7wtudmTW3wiJbEZYeugB.doc', 2, 1, '2021-01-15 04:37:05', '2021-01-15 04:37:05', NULL, 'ru', 'Платный', '1', '3000'),
(258, '1) Талап арыз (некені бұзу туралы)', 'JWyK4cAt8umW0ze7C4Qd.doc', 31, 1, '2021-02-01 05:32:00', '2021-02-01 05:32:00', NULL, 'kz', 'Платный', '1', '3000'),
(259, '2)Талап арыз (Ерлі -зайыптылардың ортақ мүлкін бөлу туралы)', 'BQTDfjGerlCZKWmRouiS.doc', 31, 1, '2021-02-01 05:34:26', '2021-02-01 05:34:26', NULL, 'kz', 'Платный', '1', '3000'),
(260, '3)Талап арыз (алименттерді өндіріп алу туралы)', 'gZwRZKL6cf9eJSmqouB8.docx', 31, 1, '2021-02-01 05:39:53', '2021-02-01 05:39:53', NULL, 'kz', 'Платный', '1', '3000'),
(261, '4)Талап арыз (әке болуды анықтау және алименттерді өндіріп алу туралы)', 'X6DM0Ll9W1tWRbdwNvfj.doc', 31, 1, '2021-02-01 05:49:53', '2021-02-01 05:49:53', NULL, 'kz', 'Платный', '1', '3000'),
(262, '5)Талап арыз (Ата-ана құқығынан айыру туралы)', 'Ojow17aIuTpPmgnkUx7f.doc', 31, 1, '2021-02-01 05:54:18', '2021-02-01 05:54:18', NULL, 'kz', 'Платный', '1', '3000'),
(263, '6)Талап арыз (Бұрынғы жұбайын кутіп -бағуғу алименттер өндіріп алу туралы', 'Q0o3k5gt7zrHuACeCATn.docx', 31, 1, '2021-02-01 06:44:25', '2021-02-01 06:44:25', NULL, 'kz', 'Платный', '1', '3000'),
(264, '7)Талап арыз (Баланың тұрғылықты жерін белгілеу туралы)', 'RsAHEtwD8c1FzCDhN1yK.doc', 31, 1, '2021-02-01 07:09:11', '2021-02-01 07:09:11', NULL, 'kz', 'Платный', '1', '3000'),
(265, '8)Талап арыз (Алимент мөлшерін өзгерту туралы)', '3ABMkIOQcF2KhL3hLPDA.doc', 31, 1, '2021-02-01 07:15:52', '2021-02-01 07:15:52', NULL, 'kz', 'Платный', '1', '3000'),
(266, '9)Талап арыз (Баламен қарым- қатынас тәртібін белгілеу туралы)', 'U50CNrx665pQWezWkgRC.doc', 31, 1, '2021-02-01 08:07:27', '2021-02-01 08:07:27', NULL, 'kz', 'Платный', '1', '3000'),
(267, '10)Талап арыз (әжесі, атасы, немересі, ағасы, қарындасы, тәрбиешісі, өгей шешесі, өгей әкесі) алименттерді өндіріп алу туралы', 'ESN7iIKhcGFD1QJEjY7B.doc', 31, 1, '2021-02-01 08:19:49', '2021-02-01 08:19:49', NULL, 'kz', 'Платный', '1', '3000'),
(268, '11)Талап арыз (қарыз шарты бойынша қарызды өндіріп алу туралы)', 'J0pqR1KxbRtuZpvCmToF.docx', 31, 1, '2021-02-01 08:53:12', '2021-02-01 08:53:12', NULL, 'kz', 'Платный', '1', '3000'),
(269, '12)Талап арыз (Өзгенің ақша қаражатың пайдаланғаны үшін қарызды және тұрақсыздық айыбын өндіріп алу туралы)', 'pGc8aSHPQUnsPPPxZI7W.doc', 31, 1, '2021-02-01 08:55:43', '2021-02-01 08:55:43', NULL, 'kz', 'Платный', '1', '3000'),
(270, '13)Талап арыз (Берешек сомасын өндіріп алу тыралы)', 'Daqju9haMQljoAe1FiTx.doc', 31, 1, '2021-02-01 09:18:29', '2021-02-01 09:18:29', NULL, 'kz', 'Платный', '1', '3000'),
(271, '14)Талап арыз (Шарт бойынша берешекті өндіріп алу туралы)', 'GtTGf3sAvX9PU3A0Gc5v.doc', 31, 1, '2021-02-01 09:34:14', '2021-02-01 09:34:14', NULL, 'kz', 'Платный', '1', '3000'),
(272, '15)Талап арыз (Қарыз сомасын өндіріп алу туралы)', 'TNlvu2rvBqSjx5UScU9x.doc', 31, 1, '2021-02-01 09:35:55', '2021-02-01 09:35:55', NULL, 'kz', 'Платный', '1', '3000'),
(273, '16)Талап арыз (Пәтерді сатып алу -сату мәмілесін жарамсыз деп тану туралы)', 'VuF3j2Gt27ZtVVOmuR7G.doc', 31, 1, '2021-02-01 09:52:19', '2021-02-01 09:52:19', NULL, 'kz', 'Платный', '1', '3000'),
(274, '17)Талап арыз (Мүлікті өзгенің заңсыз иеленуінен талап ету туралы)', '0eoICufBqYstjQx0qUnZ.doc', 31, 1, '2021-02-01 09:55:20', '2021-02-01 09:55:20', NULL, 'kz', 'Платный', '1', '3000'),
(275, '18)Талап арыз (моральдық зиянды өтеу туралы)', 'MonoEmKPZczN0EBBoPUh.doc', 31, 1, '2021-02-01 09:57:55', '2021-02-01 09:57:55', NULL, 'kz', 'Платный', '1', '3000'),
(276, '19)Арыз (Мұраны қабылдау фактің анықтау туралы)', 'uYkwAf7K7O3ZGhN1JTGP.docx', 31, 1, '2021-02-01 10:24:27', '2021-02-01 10:24:27', NULL, 'kz', 'Платный', '1', '3000'),
(277, '20)Талап арыз (Мұраны қабылдау мерзімін қалпына келтірі және мұраны қабылдады деп тану туралы)', 'Y03I6YCrmJ8t786njsOc.doc', 31, 1, '2021-02-01 10:26:58', '2021-02-01 10:26:58', NULL, 'kz', 'Платный', '1', '3000'),
(278, '21)Талап арыз (Жүмысқа қайта алу және амалсыз бос журген уақыт үшін ақы төлеу туралы)', 'KZ5CMxMybISSfX3DRnNZ.docx', 31, 1, '2021-02-01 10:29:30', '2021-02-01 10:29:30', NULL, 'kz', 'Платный', '1', '3000'),
(279, '22)Талап арыз (Жалақыны өндіріп алу туралы)', 'KzmbrhQct8AzKzpGFCWG.docx', 31, 1, '2021-02-01 10:31:00', '2021-02-01 10:31:00', NULL, 'kz', 'Платный', '1', '3000'),
(280, '23)Талап арыз (ЖКО бойынша материалдық залалды өндіріп алу туралы)', 'ViMQ3T1pWSgONEs1yL1u.doc', 31, 1, '2021-02-01 10:33:02', '2021-02-01 10:33:02', NULL, 'kz', 'Платный', '1', '3000'),
(281, '24)Талап арыз (Пәтерді су басу кезінде материалдық залалды өндіріп алу туралы)', 'jNJmN2IT47rdEOuyQFPh.doc', 31, 1, '2021-02-01 10:34:53', '2021-02-01 10:34:53', NULL, 'kz', 'Платный', '1', '3000'),
(282, '25)Талап арыз (Құжаттарды талап ету туралы)', 'qp71Aim2awkskqz1S8Gl.docx', 31, 1, '2021-02-01 10:36:14', '2021-02-01 10:36:14', NULL, 'kz', 'Платный', '1', '3000'),
(283, '26)Талап арыз (асыраушының қайтыс болуына байланысты зиянды өтеу туралы)', '5CuRoOfFOGDSvHKlnfac.docx', 31, 1, '2021-02-02 04:58:43', '2021-02-02 04:58:43', NULL, 'kz', 'Платный', '1', '3000'),
(284, '27)Талап арыз (Тұрғын үйді айырбастаудан бас тартуды заңсыз деп тану туралы)', 'usA0L6Z15ed59ChZX4Sn.doc', 31, 1, '2021-02-02 05:02:09', '2021-02-02 05:02:09', NULL, 'kz', 'Платный', '1', '3000'),
(286, '28)Қарсы талап арыз', 'j7NwYW3Uzep5CyBTcAOb.doc', 31, 1, '2021-02-02 05:05:06', '2021-02-02 05:05:06', NULL, 'kz', 'Платный', '1', '3000'),
(287, '29)Талап арыз (Тұрғын үйді пайдалану тәртібін айқындау туралы)', 'DY0oSfqwrMufb0TnLDjT.doc', 31, 1, '2021-02-02 05:07:04', '2021-02-02 05:07:04', NULL, 'kz', 'Платный', '1', '3000'),
(288, '30)Талап арыз (Тұрақсыздық айыбын өндіріп алу туралы)', 'kdz88cwdmCZOvkK5IUop.doc', 31, 1, '2021-02-02 05:09:43', '2021-02-02 05:09:43', NULL, 'kz', 'Платный', '1', '3000'),
(289, '31)Талап арыз (Мұраны қабылдау үшін мерзімді қалпына келтірі туралы)', '6vkRSjMC0DlcmzcLx4xi.doc', 31, 1, '2021-02-02 05:12:01', '2021-02-02 05:12:01', NULL, 'kz', 'Платный', '1', '3000'),
(290, '32)Талап арыз (Мұрагер деп тану туралы)', 'mI3uyxbU8KyjzDq5IP0m.doc', 31, 1, '2021-02-02 05:13:20', '2021-02-02 05:13:20', NULL, 'kz', 'Платный', '1', '3000'),
(291, '33)Талап арыз (Лайықсыз мұрагер деп тану туралы)', 'W6rKu9hjb2RUktB4o5pC.docx', 31, 1, '2021-02-02 05:14:42', '2021-02-02 05:14:42', NULL, 'kz', 'Платный', '1', '3000'),
(292, '34)Талап арыз (Лайықсыз мұрагер деп тану туралы)', '6TiLfpvNzzg4lYEl84Fy.docx', 31, 1, '2021-02-02 05:15:36', '2021-02-02 05:15:36', NULL, 'kz', 'Платный', '1', '3000'),
(293, '35)Талап арыз ( Мұрагерлік мүлікті бөлу туралы)', 'eIhykMKJhzKXiZ61kbzn.doc', 31, 1, '2021-02-02 05:17:57', '2021-02-02 05:17:57', NULL, 'kz', 'Платный', '1', '3000'),
(294, '36)Талап арыз (Өз бетінше салынған құрыллысқа меншік құқығын тану туралы)', 'aebkt2lcOPTM25HCkI8c.doc', 31, 1, '2021-02-02 05:19:42', '2021-02-02 05:19:42', NULL, 'kz', 'Платный', '1', '3000'),
(295, '37)Талап арыз (Жер тілімдері арқылы мал айдау үшін қауымдық сервитут белгілеу туралы)', 'RBuACWZQ8SRiEnkHUJ6h.doc', 31, 1, '2021-02-02 05:22:23', '2021-02-02 05:22:23', NULL, 'kz', 'Платный', '1', '3000'),
(296, '38)Апелляциялық шағым (Аудандық соттың шешіміне)', 'XMqYhxpbDIFM9cjH97E6.doc', 31, 1, '2021-02-02 05:24:48', '2021-02-02 05:24:48', NULL, 'kz', 'Платный', '1', '3000'),
(297, '39)Өтінішхат (Сот отырысын кейінге қалдыру туралы)', 'uVrFyCtEaGVTfIeGahpG.docx', 3, 1, '2021-02-02 06:01:48', '2021-02-02 06:01:48', NULL, 'kz', 'Платный', '1', '3000'),
(298, '40)Өтінішхат (Іс материалдармен танысу туралы)', 'QJsiVz3D8rqQkTQJ4Nur.doc', 3, 1, '2021-02-02 06:03:09', '2021-02-02 06:03:09', NULL, 'kz', 'Платный', '1', '3000'),
(299, '41)Өтінішхат (Құжаттарды іс материалдарына қоса тіркеу туралы)', 'myNm1A4r2pDFw3E242I4.doc', 3, 1, '2021-02-02 06:09:10', '2021-02-02 06:09:10', NULL, 'kz', 'Платный', '1', '3000'),
(300, '42)Өтінішхат (Дәлелдемелерді талап ету туралы)', 'fl4NGRwhuxVWvowF0XC9.doc', 3, 1, '2021-02-02 06:12:20', '2021-02-02 06:12:20', NULL, 'kz', 'Платный', '1', '3000'),
(301, '43)Өтінішхат (Жабық сот отырысын өткізу туралы)', 'Img8b1esAKoBEu9F3Kyx.doc', 3, 1, '2021-02-02 07:27:33', '2021-02-02 07:27:33', NULL, 'kz', 'Платный', '1', '3000'),
(302, '44)Өтінішхат (Қамтамасыз ету шараларын қабылдау туралы)', '4cigU7DzYsPhWpUgwODb.docx', 3, 1, '2021-02-02 07:28:49', '2021-02-02 07:28:49', NULL, 'kz', 'Платный', '1', '3000'),
(303, '45)Өтінішхат (Өкілдің қызметтеріне ақы төлеуге арналған шығыстарды өндіріп алу туралы)', 'wNeKhLGT1f83q95svKOx.doc', 3, 1, '2021-02-02 07:32:18', '2021-02-02 07:32:18', NULL, 'kz', 'Платный', '1', '3000'),
(304, '46)Өтінішхат', 'SGu5zTFKy0fd9cN6UaZ6.doc', 3, 1, '2021-02-02 07:32:59', '2021-02-02 07:32:59', NULL, 'kz', 'Платный', '1', '3000'),
(305, '47)Өтінішхат (куәгерлерді шақыру туралы)', 'HYh8tWMmFbQeA3Mg2qjh.docx', 3, 1, '2021-02-02 07:34:08', '2021-02-02 07:34:08', NULL, 'kz', 'Платный', '1', '3000'),
(306, '48)Өтінішхат (Аудармашыны ұсыну туралы)', 'u8ZB1l7Bznr42h1AEzrn.doc', 3, 1, '2021-02-02 07:35:57', '2021-02-02 07:35:57', NULL, 'kz', 'Платный', '1', '3000'),
(307, '49)Өтінішхат (Маманды тарту туралы)', 'nw3sN06iosS2ijB2vhoo.doc', 3, 1, '2021-02-02 08:26:41', '2021-02-02 08:26:41', NULL, 'kz', 'Платный', '1', '3000'),
(308, '50)Өтінішхат (Сарапшыны шақыру туралы)', 'k61NxDWWJhzhVGOhxEDN.doc', 3, 1, '2021-02-02 08:27:51', '2021-02-02 08:27:51', NULL, 'kz', 'Платный', '1', '3000'),
(309, '51)Өтінішхат (Автотехниқалық сараптама тағайындау туралы)', '0AzJWDuzocDQvQvREa6U.doc', 3, 1, '2021-02-02 08:29:24', '2021-02-02 08:29:24', NULL, 'kz', 'Платный', '1', '3000'),
(310, '52)Өтінішхат (Генетикалық сараптама тағайындау туралы)', 'Q708rJrVzdIcu6WztYo9.doc', 3, 1, '2021-02-02 08:31:15', '2021-02-02 08:31:15', NULL, 'kz', 'Платный', '1', '3000'),
(311, '53)Өтінішхат (Медициналық сараптама тағайындалу туралы)', '796e3lRpMu9UHfwiw5z1.docx', 3, 1, '2021-02-02 08:33:21', '2021-02-02 08:33:21', NULL, 'kz', 'Платный', '1', '3000'),
(312, '54)Өтінішхат (Бағалау сараптамасын тағайындау туралы)', 'lYGQXf6x1aUELKOkc6pV.doc', 3, 1, '2021-02-02 08:34:53', '2021-02-02 08:34:53', NULL, 'kz', 'Платный', '1', '3000'),
(313, '55)Өтінішхат (Азаматтық істі талап кою ісін жүргізу қағидалары бойынша жалпы тәртіппен қарау туралы)', 'UL4aaFo8JmzELVApxdPC.doc', 3, 1, '2021-02-02 08:38:01', '2021-02-02 08:38:01', NULL, 'kz', 'Платный', '1', '3000'),
(314, '56)Өтінішхат (Істі (талапкер, жауапкер, үшінші тұлға) болмаған кезде қарау туралы)', 'XEWkE9TH9EeB4Aq24xPC.doc', 3, 1, '2021-02-02 08:40:36', '2021-02-02 08:40:36', NULL, 'kz', 'Платный', '1', '3000'),
(315, '57)Өтінішхат (Шығуға шектеу кою туралы)', 'mBTZExVBav1CcATLWbb9.doc', 3, 1, '2021-02-02 08:42:30', '2021-02-02 08:42:30', NULL, 'kz', 'Платный', '1', '3000'),
(316, '58)Өтінішхат (Судьядан бас тарту туралы)', 'PEZyO71m2KA9aGGD8Mlk.doc', 3, 1, '2021-02-02 08:44:05', '2021-02-02 08:44:05', NULL, 'kz', 'Платный', '1', '3000'),
(317, '59)Атқару парағын беру туралы арыз', 'lmQyNBemfESoYcG1b811.doc', 5, 1, '2021-02-02 09:30:48', '2021-02-02 09:30:48', NULL, 'kz', 'Платный', '1', '3000'),
(318, '60)Талап арызды қараусыз қалдыру туралы арыз', 'Z9KSdLeNEhCTPdIps9Ax.doc', 5, 1, '2021-02-02 09:32:54', '2021-02-02 09:32:54', NULL, 'kz', 'Платный', '1', '3000'),
(319, '61)Талап арызды қайтару туралы арыз', 'Eim7byWfpgorCW43vYWx.docx', 5, 1, '2021-02-02 09:34:07', '2021-02-02 09:34:07', NULL, 'kz', 'Платный', '1', '3000'),
(320, '62)Сот шешіміне апелляциялық шағым беру үшін процестік мерзімді қалпына келтіру туралы арыз', 'RuiGWW2eHEQigtlfjzTI.doc', 5, 1, '2021-02-02 09:36:20', '2021-02-02 09:36:20', NULL, 'kz', 'Платный', '1', '3000'),
(321, '63)Төлеген мемлекеттік бажды қайтару туралы арыз', 'D7i39s88OUhj3kJfHMhd.docx', 5, 1, '2021-02-02 09:37:52', '2021-02-02 09:37:52', NULL, 'kz', 'Платный', '1', '3000'),
(322, '64)Сот бұйырығының күшін мою туралы арыз', '7qOjK71p0jttgqmrU7RD.docx', 5, 1, '2021-02-02 09:39:24', '2021-02-02 09:39:24', NULL, 'kz', 'Платный', '1', '3000'),
(323, '65)Сот отырысының аудио -бейнежазбасын беру туралы арыз', 'mx0kzG3VJFDXoRsqCcKV.docx', 5, 1, '2021-02-02 09:41:24', '2021-02-02 09:41:24', NULL, 'kz', 'Платный', '1', '3000'),
(324, '66)Алименттерді өндіріп алу туралы сот бұйрығын шығару туралы арыз', 'IzIFgZoibUjc7oCDaFaZ.docx', 5, 1, '2021-02-02 09:45:27', '2021-02-02 09:45:27', NULL, 'kz', 'Платный', '1', '3000'),
(325, '67)Арыз (Азаматтты хабар -ошарсыз кетті деп тану туралы)', 'gcdVKeviekTXbhCuI6Cb.doc', 5, 1, '2021-02-02 09:47:13', '2021-02-02 09:47:13', NULL, 'kz', 'Платный', '1', '3000'),
(326, '68)Арыз (Азаматты әрекетке қабілетсіз деп тану туралы)', 'kAS4LrACbXk91EMKc8ha.doc', 5, 1, '2021-02-02 09:48:39', '2021-02-02 09:48:39', NULL, 'kz', 'Платный', '1', '3000'),
(327, ' 69)Арыз (Азаматты әрекет қабілеті шектеулі деп тану туралы)', 'HhorEusobuWRoisNJ7Kd.doc', 5, 1, '2021-02-04 05:38:46', '2021-02-04 05:38:46', NULL, 'kz', 'Платный', '1', '3000'),
(328, '70)Арыз (Оңалту рәсімін қолдану туралы)', '1HpiGeLxcUevdtfiIuXL.doc', 5, 1, '2021-02-04 05:39:54', '2021-02-04 05:39:54', NULL, 'kz', 'Платный', '1', '3000'),
(329, '71)Арыз (Талап қоюды қамтамасыз ету туралы)', 'AQPrTiHAyhD9pIQvxsmp.doc', 5, 1, '2021-02-04 05:41:18', '2021-02-04 05:41:18', NULL, 'kz', 'Платный', '1', '3000'),
(330, '72)Арыз (Талаптан бас тарту туралы)', 'xnctpFsy4x06ZLkErOPy.docx', 5, 1, '2021-02-04 05:42:34', '2021-02-04 05:42:34', NULL, 'kz', 'Платный', '1', '3000'),
(331, '73)Арыз (Туыстық қатынастар фактін анықтау туралы)', 'ZXfiH4fuNPBhoE68j5uM.docx', 5, 1, '2021-02-04 05:43:43', '2021-02-04 05:43:43', NULL, 'kz', 'Платный', '1', '3000'),
(332, '74)Арыз (Талап кою мерзімін қалпына келтіру туралы)', 'bt7SNCoRnmMWliNdPkPX.doc', 5, 1, '2021-02-04 05:44:55', '2021-02-04 05:44:55', NULL, 'kz', 'Платный', '1', '3000'),
(333, '75)Арыз (Сырттай шешімнің күшін жою туралы)', 'Hpj4bFn0QPNQz2lirCTH.doc', 5, 1, '2021-02-04 05:46:02', '2021-02-04 05:46:02', NULL, 'kz', 'Платный', '1', '3000'),
(334, '76)Арыз (Талап қоюды қамтамасыз ету шараларын ауыстыру туралы)', '6bJU5nu3DjQ5PYuohYot.doc', 5, 1, '2021-02-04 05:48:02', '2021-02-04 05:48:02', NULL, 'kz', 'Платный', '1', '3000'),
(335, '77)Арыз (Сот шешімінің күшін жою туралы)', 'C3qjTpNWcM7vbgvmZUv7.docx', 5, 1, '2021-02-04 05:49:02', '2021-02-04 05:49:02', NULL, 'kz', 'Платный', '1', '3000'),
(336, '78)Арыз (Сот актің жаңадан ашылған (және) мән - жайлар бойынша қайта қарау туралы)', '4PeWJ3dlGVigS9mKslXh.doc', 5, 1, '2021-02-04 05:50:58', '2021-02-04 05:50:58', NULL, 'kz', 'Платный', '1', '3000'),
(337, '79)Арыз (Шешімді орындау мерзімін кейінгі қалдыру туралы)', 'QH6H058XOCkAqIYUGLdu.doc', 5, 1, '2021-02-04 05:52:22', '2021-02-04 05:52:22', NULL, 'kz', 'Платный', '1', '3000'),
(338, '80)Арыз (Азаматтың қайтыс болу фактін анықтау туралы)', 'y72GBXes735ZnDJ7MWXz.docx', 5, 1, '2021-02-04 05:56:32', '2021-02-04 05:56:32', NULL, 'kz', 'Платный', '1', '3000'),
(339, '81)Арыз (Асырауда боту фактін анықтау туралы)', 'D3UpHBYuq12fpJwGDMtM.doc', 5, 1, '2021-02-04 05:57:49', '2021-02-04 05:57:49', NULL, 'kz', 'Платный', '1', '3000'),
(340, '82)Арыз (Талап кою талаптарын ұлғайту туралы)', 'QqlzSeNYLPFm9m1pe3Wh.doc', 5, 1, '2021-02-04 05:58:59', '2021-02-04 05:58:59', NULL, 'kz', 'Платный', '1', '3000'),
(341, '83)Арыз (Талап қою талаптарын азайту туралы)', 'ZQzUoVkQC4uO3hzQVsgb.doc', 5, 1, '2021-02-04 06:00:30', '2021-02-04 06:00:30', NULL, 'kz', 'Платный', '1', '3000'),
(342, '84)Арыз (Соттың қосымша шешімің шығару туралы)', 'bjAwGg5vLparvjSFRXv0.doc', 5, 1, '2021-02-04 06:01:58', '2021-02-04 06:01:58', NULL, 'kz', 'Платный', '1', '3000'),
(343, '85)Арыз (Қатені түзету туралы)', 'ckKUE7Cuk4r2fBJDz4q4.doc', 5, 1, '2021-02-04 06:03:15', '2021-02-04 06:03:15', NULL, 'kz', 'Платный', '1', '3000'),
(344, '86)Арыз (Сот шешімінің көшірмесін беру туралы)', 'VeKao0n2Ik8p2anznIph.doc', 5, 1, '2021-02-04 06:04:39', '2021-02-04 06:04:39', NULL, 'kz', 'Платный', '1', '3000'),
(345, '87)Арыз (Шешімді орындау мерзімін ұзарту туралы)', 'hKPo9UAXks7nZCBCeii8.docx', 5, 1, '2021-02-04 06:06:21', '2021-02-04 06:06:21', NULL, 'kz', 'Платный', '1', '3000'),
(346, '88)Арыз (Сот бұйрығының телнұсқасын беруге арналған)', 'AKLoXdVAnr22a9Ezl6Q4.doc', 5, 1, '2021-02-04 06:07:37', '2021-02-04 06:07:37', NULL, 'kz', 'Платный', '1', '3000'),
(347, '89)Арыз (Іс бойынша іс жүргізіді тоқтата тұру туралы)', '7qs0jTXi8LxVLlnWqPAH.doc', 5, 1, '2021-02-04 06:09:05', '2021-02-04 06:09:05', NULL, 'kz', 'Платный', '1', '3000'),
(348, '90)Арыз (Сот шешімін орындау тәртібін өзгерту туралы)', 'wpM7CNq9lA5OdUFg0wef.doc', 5, 1, '2021-02-04 06:10:03', '2021-02-04 06:10:03', NULL, 'kz', 'Платный', '1', '3000'),
(349, '91)Әкімшілік құқық бұзушылық туралы іс бойынша қаулыға шағым', 'X4Fy637A1TBHFAMVkqjM.doc', 4, 1, '2021-02-04 07:14:59', '2021-02-04 07:14:59', NULL, 'kz', 'Платный', '1', '3000'),
(350, '92)Мүлікті бағалауға байланысты және сот орындашысы әрекеттеріне шаңым (ҚР АПК 250-б. 6-б. тәртібінде)', 'eYeRp45DdjYLN1RJwaay.doc', 4, 1, '2021-02-04 07:39:05', '2021-02-04 07:39:05', NULL, 'kz', 'Платный', '1', '3000'),
(351, '93)Судьяға шағым', 'vcleKyruGkSxyZPkDKhg.doc', 4, 1, '2021-02-04 07:40:13', '2021-02-04 07:40:13', NULL, 'kz', 'Платный', '1', '3000'),
(352, '94)Сот актінің орындалмауына немесе сот орындаушысының әрекеттерәне (әрекетсіздігіне) шағым', 'BYM7Kyk6thFgNWXg3FSA.docx', 4, 1, '2021-02-04 07:42:23', '2021-02-04 07:42:23', NULL, 'kz', 'Платный', '1', '3000'),
(353, '95)Сауда-саттықты өткізуге және мүлікті сатуға беруге байланысты және сот орындаушысының әрекеттеріне шағым', 'XPBvsXvPRiUIM1ApYf5T.doc', 4, 1, '2021-02-04 07:44:34', '2021-02-04 07:44:34', NULL, 'kz', 'Платный', '1', '3000'),
(354, '96)Сотқа дейнгі наразылық', 'EtEAutTMnTgpOcsKG33I.docx', 32, 1, '2021-02-04 08:11:19', '2021-02-04 08:11:19', NULL, 'kz', 'Платный', '1', '3000'),
(355, '97)Сот бұйрығына қарсы қарсылық', 'Jii4I50YoRfy0np2Jz9o.docx', 32, 1, '2021-02-04 08:12:47', '2021-02-04 08:12:47', NULL, 'kz', 'Платный', '1', '3000'),
(356, '98)Некені бұзу туралы талап арызға пікір', 'rGh27mSOVMCj8MrqYp5U.doc', 32, 1, '2021-02-04 08:13:48', '2021-02-04 08:13:48', NULL, 'kz', 'Платный', '1', '3000'),
(357, '99)Аудит жөнінде ЖШС қатысушысына хат', 'GJLrPh5EdjUNPHo5lO5E.doc', 32, 1, '2021-02-04 08:14:53', '2021-02-04 08:14:53', NULL, 'kz', 'Платный', '1', '3000'),
(358, '100)Қайта тіркеу бойынша ЖШС қатысушасына хат', 'ev01oEHoCKuSuRrGsUDw.doc', 32, 1, '2021-02-04 08:15:56', '2021-02-04 08:15:56', NULL, 'kz', 'Платный', '1', '3000'),
(359, '101)Сотқа дейінгі наразылық', 'oNs0aOX8GdLib5Tp3Fbd.docx', 32, 1, '2021-02-04 08:17:02', '2021-02-04 08:17:02', NULL, 'kz', 'Платный', '1', '3000'),
(360, '102)Наразылық', 'KXTV4hxx7COubuyoyMBH.doc', 32, 1, '2021-02-04 08:17:42', '2021-02-04 08:17:42', NULL, 'kz', 'Платный', '1', '3000'),
(361, '103)Сатып алу -сату шарты', '4aH4kxfdeOS08FQsgpmm.doc', 2, 1, '2021-02-08 06:09:30', '2021-02-08 06:09:30', NULL, 'kz', 'Платный', '1', '3000'),
(362, '104) Сатып алу-сату шарты', 'CrZzmwPRze1htP2iympy.doc', 2, 1, '2021-02-08 06:10:15', '2021-02-08 06:10:15', NULL, 'kz', 'Платный', '1', '3000'),
(363, '105)Автокөлік құралын ақысыз пайдалану шарты', '3VQQ94BKBr5Uk8aQDw73.doc', 2, 1, '2021-02-08 06:12:07', '2021-02-08 06:12:07', NULL, 'kz', 'Платный', '1', '3000'),
(364, '106)Қызмет көрсетуге арналған шарт', 'xG125gzIb0E3Tkhwvgng.doc', 2, 1, '2021-02-08 06:14:09', '2021-02-08 06:14:09', NULL, 'kz', 'Платный', '1', '3000'),
(365, '107)Жеткізу шарты', 'LqUOevuct3fLmlgA4xgy.doc', 2, 1, '2021-02-08 06:15:01', '2021-02-08 06:15:01', NULL, 'kz', 'Платный', '1', '3000'),
(366, '108)Жылжымалы мүлікті сатып алу- сату шарты', 'wGEyRMCTUOAs3mtKwUBX.docx', 2, 1, '2021-02-08 06:16:37', '2021-02-08 06:16:37', NULL, 'kz', 'Платный', '1', '3000'),
(367, '109)Жай серіктестік шарты', 'hEmianEpFltckwL2bFHZ.doc', 2, 1, '2021-02-08 06:41:31', '2021-02-08 06:41:31', NULL, 'kz', 'Платный', '1', '3000'),
(368, '110)Қосалқы жалдау шарты', 'zWOwDMd4LjTMxWZJK03C.doc', 2, 1, '2021-02-08 06:43:20', '2021-02-08 06:43:20', NULL, 'kz', 'Платный', '1', '3000'),
(369, '111)Құпия ақпаратты жарит етпеу туралы келісім -міндеттеме', 'JrmPo88Ho3MJv5XjwHA9.docx', 2, 1, '2021-02-08 06:47:02', '2021-02-08 06:47:02', NULL, 'kz', 'Платный', '1', '3000'),
(370, '112)Толық материалдық жауап кершілік туралы шарт', 'zPvxFdSgcbreaga7Eard.doc', 2, 1, '2021-02-08 06:48:17', '2021-02-08 06:48:17', NULL, 'kz', 'Платный', '1', '3000'),
(371, '113)Әрі қарай сатып алумен автокөлік жалға алу шарты', 'I2OxvNjik25H1uOBQEHU.doc', 2, 1, '2021-02-08 06:50:08', '2021-02-08 06:50:08', NULL, 'kz', 'Платный', '1', '3000'),
(372, '114)Гаражды жалға алу шарты', '54iT5cY3tC7gxmBAbl1f.doc', 2, 1, '2021-02-08 06:52:03', '2021-02-08 06:52:03', NULL, 'kz', 'Платный', '1', '3000'),
(373, '115)Әрі қарай сатып алумен үйді жалға алу шарты', 'BTubRpr1Pv1E1rkvyOjQ.doc', 2, 1, '2021-02-08 06:53:09', '2021-02-08 06:53:09', NULL, 'kz', 'Платный', '1', '3000'),
(374, '116)Жер тілімін жалға алу шарты', 'eo4OS8sXVKR9gQdL9J6Y.doc', 2, 1, '2021-02-08 06:57:58', '2021-02-08 06:57:58', NULL, 'kz', 'Платный', '1', '3000'),
(375, '117)Пәтерді жалға алу шарты', 'Zk4hrf3xESuw0811JNoW.doc', 2, 1, '2021-02-08 07:00:38', '2021-02-08 07:00:38', NULL, 'kz', 'Платный', '1', '3000'),
(376, '118)Тұрғын емес үй- жайды жабдығымен жалға алу шарты', 'eoBhoshruPfJ1vQEZo8s.doc', 2, 1, '2021-02-08 07:07:19', '2021-02-08 07:07:19', NULL, 'kz', 'Платный', '1', '3000'),
(377, '119)Тұрғын емес үй жайды жалға алу шарты', '0pj23lYsFji7XhTjepdE.doc', 2, 1, '2021-02-08 07:08:54', '2021-02-08 07:08:54', NULL, 'kz', 'Платный', '1', '3000'),
(378, '120)Цехты жалға алу шарты', 'Kx4CfGEVsceg9y2SitcI.doc', 2, 1, '2021-02-08 07:09:51', '2021-02-08 07:09:51', NULL, 'kz', 'Платный', '1', '3000'),
(379, '121)Көлік құралын экипажымен жалдау шарты', 'iwbmtg86E81QoO4CvdBJ.doc', 2, 1, '2021-02-08 07:11:26', '2021-02-08 07:11:26', NULL, 'kz', 'Платный', '1', '3000'),
(380, '122)Жалға алу шарты', 'LeVhehi7GRsKoyLWwX9Q.doc', 2, 1, '2021-02-08 07:12:09', '2021-02-08 07:12:09', NULL, 'kz', 'Платный', '1', '3000'),
(381, '123)Автоколік құралын ақысыз пайдалану шарты', 'l6lzb0WHyWCWtlMuP3RO.doc', 2, 1, '2021-02-08 07:16:40', '2021-02-08 07:16:40', NULL, 'kz', 'Платный', '1', '3000'),
(382, '124)Қызмет көрсетуге арналған шарт', 'opNq8bIiJQecBZwFqtyr.doc', 2, 1, '2021-02-08 07:18:23', '2021-02-08 07:18:23', NULL, 'kz', 'Платный', '1', '3000'),
(383, '125)Уаөқытша қайтарымды материалдық комек кқрсету туралы шарт', 'E1RDN527ZOVGLZOXI5H5.doc', 2, 1, '2021-02-08 07:42:10', '2021-02-08 07:42:10', NULL, 'kz', 'Платный', '1', '3000'),
(384, '126)Жылжымайтын мүлікті сенімгерлігін басқару шарты', 'w0plGDy0Mkdtd5qHCKJj.doc', 2, 1, '2021-02-08 07:49:42', '2021-02-08 07:49:42', NULL, 'kz', 'Платный', '1', '3000');

-- --------------------------------------------------------

--
-- Table structure for table `document_categories`
--

CREATE TABLE `document_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `document_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_categories`
--

INSERT INTO `document_categories` (`id`, `name`, `order`, `document_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Новая категория документов', 8, NULL, '2021-05-09 16:51:17', '2021-05-27 16:13:09', '2021-05-27 16:13:09'),
(2, 'Договора', 6, NULL, '2021-05-17 21:04:34', '2021-05-27 16:05:07', NULL),
(3, 'Ходатайства', 2, NULL, NULL, '2021-05-27 16:04:54', NULL),
(4, 'Жалобы', 4, NULL, NULL, '2021-05-27 16:04:54', NULL),
(5, 'Заявления', 3, NULL, NULL, '2021-05-27 16:04:54', NULL),
(31, 'Иски', 1, NULL, NULL, '2021-05-27 16:04:28', NULL),
(32, 'Письма', 5, NULL, NULL, '2021-05-27 16:05:07', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-05-07 20:26:33', '2021-05-07 20:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Панель управления', '', '_self', 'voyager-boat', NULL, NULL, 1, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.dashboard', NULL),
(2, 1, 'Медиа', '', '_self', 'voyager-images', NULL, NULL, 4, '2021-05-07 20:26:33', '2021-05-13 15:58:06', 'voyager.media.index', NULL),
(3, 1, 'Пользователи', '', '_self', 'voyager-person', NULL, NULL, 3, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.users.index', NULL),
(4, 1, 'Роли', '', '_self', 'voyager-lock', NULL, NULL, 2, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.roles.index', NULL),
(6, 1, 'Конструктор Меню', '', '_self', 'voyager-list', NULL, 5, 10, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.menus.index', NULL),
(7, 1, 'База данных', '', '_self', 'voyager-data', NULL, 5, 11, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2021-05-07 20:26:33', '2021-05-07 20:26:33', 'voyager.bread.index', NULL),
(10, 1, 'Настройки', '', '_self', 'voyager-settings', NULL, NULL, 7, '2021-05-07 20:26:33', '2021-05-13 15:58:06', 'voyager.settings.index', NULL),
(12, 1, 'Категории', '', '_self', 'voyager-categories', NULL, NULL, 5, '2021-05-07 20:29:06', '2021-05-13 15:58:06', 'voyager.categories.index', NULL),
(15, 1, 'Категории документов', '', '_self', 'voyager-folder', '#000000', NULL, 8, '2021-05-09 16:48:25', '2021-05-13 15:58:06', 'voyager.document-categories.index', 'null'),
(16, 1, 'Документы', '', '_self', 'voyager-basket', '#000000', NULL, 9, '2021-05-09 16:58:11', '2021-05-13 15:58:06', 'voyager.documents.index', 'null'),
(17, 1, 'Чаты', '/admin/chats', '_self', 'voyager-categories', '#000000', NULL, 10, '2021-05-11 14:01:19', '2021-05-13 15:58:06', NULL, ''),
(18, 1, 'Заказы', '', '_self', 'voyager-leaf', '#000000', NULL, 11, '2021-05-11 14:39:27', '2021-05-13 15:58:06', 'voyager.orders.index', 'null'),
(19, 1, 'Услуги', '', '_self', 'voyager-wallet', '#000000', NULL, 12, '2021-05-12 12:48:05', '2021-05-13 15:58:06', 'voyager.services.index', 'null'),
(20, 1, 'Уведомления', '', '_self', 'voyager-ticket', NULL, NULL, 13, '2021-05-17 16:31:35', '2021-05-17 16:31:35', 'voyager.notifications.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `order_id`, `user_id`, `phone`, `message`, `created_at`, `updated_at`, `is_read`) VALUES
(1, 38, 6, NULL, 'Asyl', '2021-05-10 15:17:57', '2021-05-11 14:07:31', 1),
(2, 38, 8, NULL, 'adl', '2021-05-10 15:18:09', '2021-05-11 14:07:31', 1),
(3, 38, 6, NULL, 'Asyl', '2021-05-10 15:19:01', '2021-05-11 14:07:31', 1),
(4, 38, 6, NULL, 'Asyl', '2021-05-10 15:19:03', '2021-05-11 14:07:31', 1),
(5, 38, 6, NULL, 'cool', '2021-05-10 15:19:14', '2021-05-11 14:07:31', 1),
(6, 38, 6, NULL, 'бро', '2021-05-10 15:19:46', '2021-05-11 14:07:31', 1),
(7, 38, 6, NULL, 'как ты', '2021-05-10 15:47:31', '2021-05-11 14:07:31', 1),
(8, 38, 6, NULL, 'норм', '2021-05-10 17:20:57', '2021-05-11 14:07:31', 1),
(9, 38, 6, NULL, 'Хей', '2021-05-10 17:23:18', '2021-05-11 14:07:31', 1),
(10, 38, 6, NULL, 'кле', '2021-05-10 17:26:11', '2021-05-11 14:07:31', 1),
(11, 38, 6, NULL, 'mmm', '2021-05-10 17:31:15', '2021-05-11 14:07:31', 1),
(12, 38, 6, NULL, 'салам бро', '2021-05-10 17:39:16', '2021-05-11 14:07:31', 1),
(13, 38, 6, NULL, 'да', '2021-05-10 17:55:07', '2021-05-11 14:07:31', 1),
(14, 38, 6, NULL, 'хай', '2021-05-10 17:58:01', '2021-05-11 14:07:31', 1),
(15, 38, 6, NULL, 'им', '2021-05-10 17:59:24', '2021-05-11 14:07:31', 1),
(16, 38, 6, NULL, 'да', '2021-05-10 18:01:28', '2021-05-11 14:07:31', 1),
(17, 38, 6, NULL, 'ну', '2021-05-10 18:02:50', '2021-05-11 14:07:31', 1),
(18, 38, 6, NULL, 'ман', '2021-05-10 18:03:21', '2021-05-11 14:07:31', 1),
(19, 38, 6, NULL, 'пило', '2021-05-11 11:47:43', '2021-05-11 14:07:31', 1),
(20, 38, 1, NULL, 'asd', '2021-05-11 12:55:00', '2021-05-11 14:07:31', 1),
(21, 38, 1, NULL, 'asd', '2021-05-11 12:55:24', '2021-05-11 14:07:31', 1),
(22, 38, 1, NULL, 'asd', '2021-05-11 12:55:27', '2021-05-11 14:07:31', 1),
(23, 38, 1, NULL, 'asdddd', '2021-05-11 12:57:15', '2021-05-11 14:07:31', 1),
(24, 38, 1, NULL, 'sd', '2021-05-11 13:00:24', '2021-05-11 14:07:31', 1),
(25, 38, 1, NULL, 'salam', '2021-05-11 13:03:07', '2021-05-11 14:07:31', 1),
(26, 38, 1, NULL, 'asdsd', '2021-05-11 13:09:05', '2021-05-11 14:07:31', 1),
(27, 38, 6, NULL, 'fuck you', '2021-05-11 13:12:06', '2021-05-11 14:07:31', 1),
(28, 38, 1, NULL, 'No', '2021-05-11 13:13:10', '2021-05-11 14:07:31', 1),
(29, 38, 6, NULL, 'yes', '2021-05-11 13:13:39', '2021-05-11 14:07:31', 1),
(30, 38, 1, NULL, 'asd', '2021-05-11 13:14:23', '2021-05-11 14:07:31', 1),
(31, 38, 6, NULL, 'классно', '2021-05-11 13:15:05', '2021-05-11 14:07:31', 1),
(32, 38, 1, NULL, '222', '2021-05-11 13:17:22', '2021-05-11 14:07:31', 1),
(33, 38, 1, NULL, 'asd', '2021-05-11 13:18:24', '2021-05-11 14:07:31', 1),
(34, 38, 1, NULL, 'ss', '2021-05-11 13:18:33', '2021-05-11 14:07:31', 1),
(35, 38, 6, NULL, 'фуф', '2021-05-11 13:18:45', '2021-05-11 14:07:31', 1),
(36, 38, 1, NULL, 'Привет', '2021-05-11 13:19:42', '2021-05-11 14:07:31', 1),
(37, 38, 1, NULL, 'ЯПидр', '2021-05-11 13:19:52', '2021-05-11 14:07:31', 1),
(38, 38, 6, NULL, 'я знал', '2021-05-11 13:20:00', '2021-05-11 14:07:31', 1),
(39, 38, 1, NULL, 'Дыыы', '2021-05-11 13:20:03', '2021-05-11 14:07:31', 1),
(40, 38, 1, NULL, 'АХхахаххахаа', '2021-05-11 13:20:08', '2021-05-11 14:07:31', 1),
(41, 38, 1, NULL, 'Не знаю', '2021-05-11 13:20:17', '2021-05-11 14:07:31', 1),
(42, 38, 6, NULL, 'лол', '2021-05-11 13:32:35', '2021-05-11 14:07:31', 1),
(43, 38, 1, NULL, 'кек', '2021-05-11 13:32:56', '2021-05-11 14:07:31', 1),
(44, 38, 6, NULL, 'мем', '2021-05-11 13:33:03', '2021-05-11 14:07:31', 1),
(45, 38, 6, NULL, 'пошёл ты', '2021-05-11 13:35:26', '2021-05-11 14:07:31', 1),
(46, 38, 6, NULL, 'ааа', '2021-05-11 14:16:04', '2021-05-11 14:16:06', 1),
(47, 38, 1, NULL, 'kek', '2021-05-11 14:17:27', '2021-05-11 14:17:28', 1),
(48, 38, 1, NULL, 'asd', '2021-05-11 14:17:35', '2021-05-11 14:17:36', 1),
(49, 38, 1, NULL, 'asd', '2021-05-11 14:18:47', '2021-05-11 14:18:48', 1),
(50, 38, 1, NULL, 'asd', '2021-05-11 14:20:37', '2021-05-11 14:34:07', 1),
(51, 38, 1, NULL, 'asdd', '2021-05-11 14:34:39', '2021-05-11 14:34:40', 1),
(52, 38, 1, NULL, 'asd', '2021-05-11 14:36:06', '2021-05-11 14:36:07', 1),
(53, 38, 1, NULL, 'Похуй', '2021-05-12 20:37:15', '2021-05-12 20:51:57', 1),
(54, 38, 1, NULL, 'Похуй', '2021-05-12 20:37:24', '2021-05-12 20:51:57', 1),
(55, 38, 1, NULL, 'Похуй', '2021-05-12 20:39:51', '2021-05-12 20:51:57', 1),
(56, 38, 1, NULL, 'Xbk', '2021-05-12 20:41:31', '2021-05-12 20:51:57', 1),
(57, 38, 1, NULL, 'asd', '2021-05-12 20:42:19', '2021-05-12 20:51:57', 1),
(58, 38, 1, NULL, 'ads', '2021-05-12 20:45:02', '2021-05-12 20:51:57', 1),
(59, 38, 1, NULL, 'ddd', '2021-05-12 20:48:30', '2021-05-12 20:51:57', 1),
(60, 38, 1, NULL, 'asdddd', '2021-05-12 20:50:02', '2021-05-12 20:51:57', 1),
(61, 38, 1, NULL, 'asddd', '2021-05-12 20:51:30', '2021-05-12 20:51:57', 1),
(62, 38, 1, NULL, 'Привет', '2021-05-12 20:51:46', '2021-05-12 20:51:57', 1),
(63, 38, 1, NULL, 'asddd', '2021-05-12 20:58:55', '2021-05-12 20:58:56', 1),
(64, 38, 1, NULL, 'dddd', '2021-05-12 20:59:21', '2021-05-12 20:59:37', 1),
(65, 38, 6, NULL, 'ааа', '2021-05-12 20:59:44', '2021-05-12 20:59:45', 1),
(66, 38, 1, NULL, 'utw', '2021-05-12 21:13:17', '2021-05-12 21:30:57', 1),
(67, 38, 1, NULL, 'oko', '2021-05-12 21:13:45', '2021-05-12 21:30:57', 1),
(68, 38, 1, NULL, 'oo', '2021-05-12 21:13:51', '2021-05-12 21:30:57', 1),
(69, 38, 1, NULL, 'kk', '2021-05-12 21:17:05', '2021-05-12 21:30:57', 1),
(70, 38, 1, NULL, 'kkk', '2021-05-12 21:17:09', '2021-05-12 21:30:57', 1),
(71, 38, 1, NULL, 'dsf', '2021-05-12 21:21:20', '2021-05-12 21:30:57', 1),
(72, 38, 6, NULL, 'ddd', '2021-05-13 16:12:08', '2021-05-13 16:12:10', 1),
(73, 38, 6, NULL, 'ddd', '2021-05-13 16:12:09', '2021-05-13 16:12:10', 1),
(74, 38, 1, NULL, 'iiiiii', '2021-05-13 16:12:19', '2021-05-13 16:12:20', 1),
(75, 38, 1, NULL, 'uuuuuuuuuuuuuuu', '2021-05-13 16:12:31', '2021-05-13 16:12:32', 1),
(76, 49, 1, NULL, '55555', '2021-05-13 16:39:52', '2021-05-17 15:01:06', 1),
(77, 38, 6, NULL, 'ааа', '2021-05-14 12:11:22', '2021-05-14 12:11:24', 1),
(78, 7, 6, NULL, 'кринж', '2021-05-14 12:16:07', '2021-05-14 12:16:08', 1),
(79, 48, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-14 15:01:19', '2021-05-14 15:01:19', 1),
(80, 54, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-14 16:47:29', '2021-05-14 16:47:48', 1),
(81, 48, 6, NULL, 'ррр', '2021-05-14 17:36:05', '2021-05-14 17:36:06', 1),
(82, 48, 6, NULL, 'тот', '2021-05-14 17:39:11', '2021-05-14 17:39:15', 1),
(83, 48, 6, NULL, 'Дж', '2021-05-14 17:39:29', '2021-05-14 17:39:31', 1),
(84, 48, 6, NULL, 'че там', '2021-05-14 17:45:40', '2021-05-14 17:45:41', 1),
(85, 48, 6, NULL, 'ррр', '2021-05-14 17:46:33', '2021-05-14 17:46:34', 1),
(86, 48, 6, NULL, 'джж', '2021-05-14 17:46:45', '2021-05-14 17:46:47', 1),
(87, 38, 1, NULL, 'asd', '2021-05-14 17:47:24', '2021-05-14 17:49:37', 1),
(88, 38, 1, NULL, 'dddd', '2021-05-14 17:47:33', '2021-05-14 17:49:37', 1),
(89, 48, 6, NULL, 'пошёл наху', '2021-05-14 17:48:16', '2021-05-14 17:48:17', 1),
(90, 49, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-14 17:49:16', '2021-05-17 15:01:06', 1),
(91, 52, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-14 17:49:22', '2021-05-14 17:49:43', 1),
(92, 52, 8, NULL, 'выв', '2021-05-14 17:49:27', '2021-05-14 17:49:43', 1),
(93, 52, 6, NULL, 'тот', '2021-05-14 17:49:46', '2021-05-14 17:49:47', 1),
(94, 52, 6, NULL, 'аа', '2021-05-14 17:52:20', '2021-05-14 17:52:21', 1),
(95, 52, 6, NULL, 'см', '2021-05-14 17:53:55', '2021-05-14 17:53:56', 1),
(96, 52, 6, NULL, 'ооо', '2021-05-14 17:54:01', '2021-05-14 17:54:02', 1),
(97, 52, 6, NULL, 'Идлиб', '2021-05-14 17:54:13', '2021-05-14 17:54:14', 1),
(98, 40, 1, NULL, 'asdsadasd', '2021-05-14 17:56:02', '2021-05-14 17:56:02', 0),
(99, 40, 1, NULL, 'ss', '2021-05-14 17:56:15', '2021-05-14 17:56:15', 0),
(100, 40, 1, NULL, 'ssasd', '2021-05-14 17:56:26', '2021-05-14 17:56:26', 0),
(101, 47, 1, NULL, 'ddd', '2021-05-14 17:57:02', '2021-05-14 17:58:05', 1),
(102, 47, 1, NULL, 'sdd', '2021-05-14 17:57:47', '2021-05-14 17:58:05', 1),
(103, 47, 1, NULL, 'asdsd', '2021-05-14 17:57:51', '2021-05-14 17:58:05', 1),
(104, 47, 1, NULL, 'asdsd', '2021-05-14 17:59:34', '2021-05-14 17:59:35', 1),
(105, 47, 1, NULL, 'sdsd', '2021-05-14 17:59:38', '2021-05-14 17:59:39', 1),
(106, 59, 1, NULL, 'asdd', '2021-05-16 15:09:43', '2021-07-18 22:56:04', 1),
(107, 65, 1, NULL, 'asdd', '2021-05-16 15:23:23', '2021-05-16 15:31:19', 1),
(108, 65, 1, NULL, 'asdd', '2021-05-16 15:23:31', '2021-05-16 15:31:19', 1),
(109, 65, 1, NULL, 'asddsd', '2021-05-16 15:23:41', '2021-05-16 15:31:19', 1),
(110, 65, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-16 15:30:39', '2021-05-16 15:31:19', 1),
(111, 65, 8, NULL, 'asdd', '2021-05-16 15:30:48', '2021-05-16 15:31:19', 1),
(112, 65, 8, NULL, 'asddd', '2021-05-16 15:30:58', '2021-05-16 15:31:19', 1),
(113, 65, 8, NULL, 'dddd', '2021-05-16 15:31:32', '2021-05-16 15:31:33', 1),
(114, 65, 6, NULL, 'оао', '2021-05-16 15:33:52', '2021-05-16 15:33:53', 1),
(115, 65, 8, NULL, 'wdwd', '2021-05-16 15:34:13', '2021-05-16 15:34:14', 1),
(116, 65, 8, NULL, 'aaaa', '2021-05-16 15:34:19', '2021-05-16 15:34:20', 1),
(117, 65, 8, NULL, 'asddd', '2021-05-16 15:38:15', '2021-05-16 15:41:08', 1),
(118, 65, 8, NULL, 'klkl', '2021-05-16 15:38:22', '2021-05-16 15:41:08', 1),
(119, 65, 8, NULL, 'kkk', '2021-05-16 15:40:51', '2021-05-16 15:41:08', 1),
(120, 65, 8, NULL, 'klkk', '2021-05-16 15:41:11', '2021-05-16 15:41:12', 1),
(121, 65, 8, NULL, 'asyl', '2021-05-16 15:43:05', '2021-05-16 15:43:07', 1),
(122, 65, 8, NULL, 'kjkj', '2021-05-16 16:07:06', '2021-05-16 16:07:20', 1),
(123, 65, 6, NULL, 'да', '2021-05-16 16:07:25', '2021-05-16 16:07:27', 1),
(124, 65, 1, NULL, 'asdd', '2021-05-16 21:43:35', '2021-05-17 23:08:22', 1),
(125, 67, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 14:47:20', '2021-05-17 14:48:12', 1),
(126, 67, 8, NULL, 'Здравствуйте', '2021-05-17 14:47:34', '2021-05-17 14:48:12', 1),
(127, 67, 8, NULL, 'Привет', '2021-05-17 14:47:39', '2021-05-17 14:48:12', 1),
(128, 67, 8, NULL, 'Салам', '2021-05-17 14:48:15', '2021-05-17 14:48:16', 1),
(129, 67, 8, NULL, 'Хеллоу', '2021-05-17 14:48:19', '2021-05-17 14:48:20', 1),
(130, 67, 8, NULL, 'Хеллоу', '2021-05-17 14:48:22', '2021-05-17 14:48:23', 1),
(131, 68, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 14:49:07', '2021-05-17 23:08:26', 1),
(132, 69, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 15:02:49', '2021-05-17 15:03:20', 1),
(133, 69, 8, NULL, 'Саламалейкум', '2021-05-17 15:03:03', '2021-05-17 15:03:20', 1),
(134, 69, 8, NULL, 'Пидор', '2021-05-17 15:03:10', '2021-05-17 15:03:20', 1),
(135, 69, 9, NULL, 'сам ты пидор', '2021-05-17 15:03:30', '2021-05-17 15:03:31', 1),
(136, 69, 8, NULL, 'Пшел нахуй', '2021-05-17 15:03:45', '2021-05-17 15:03:46', 1),
(137, 69, 8, NULL, 'Черт', '2021-05-17 15:03:51', '2021-05-17 15:03:52', 1),
(138, 69, 8, NULL, 'уй', '2021-05-17 15:03:52', '2021-05-17 15:03:52', 1),
(139, 69, 8, NULL, 'Сигем', '2021-05-17 15:03:54', '2021-05-17 15:03:55', 1),
(140, 69, 8, NULL, 'Жадюаею', '2021-05-17 15:03:56', '2021-05-17 15:03:57', 1),
(141, 69, 9, NULL, 'ееееее', '2021-05-17 15:03:59', '2021-05-17 15:04:00', 1),
(142, 69, 8, NULL, 'Ееееее', '2021-05-17 15:04:04', '2021-05-17 15:04:05', 1),
(143, 69, 9, NULL, 'лохсынба', '2021-05-17 15:04:11', '2021-05-17 15:04:12', 1),
(144, 69, 8, NULL, 'Иа', '2021-05-17 15:04:15', '2021-05-17 15:04:16', 1),
(145, 69, 8, NULL, 'Красава', '2021-05-17 15:06:37', '2021-05-17 15:06:38', 1),
(146, 69, 8, NULL, 'Chert', '2021-05-17 15:06:44', '2021-05-17 15:07:24', 1),
(147, 70, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 15:14:53', '2021-05-17 21:57:18', 1),
(148, 72, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 15:17:40', '2021-05-17 21:57:10', 1),
(149, 72, 8, NULL, 'Салам', '2021-05-17 16:04:58', '2021-05-17 21:57:10', 1),
(150, 69, 8, NULL, 'Пидр', '2021-05-17 16:07:33', '2021-05-17 16:40:34', 1),
(151, 72, 8, NULL, 'asdsdsd', '2021-05-17 16:08:14', '2021-05-17 21:57:10', 1),
(152, 72, 8, NULL, 'kmkmk', '2021-05-17 16:08:18', '2021-05-17 21:57:10', 1),
(153, 72, 8, NULL, 'kmkmkm', '2021-05-17 16:08:20', '2021-05-17 21:57:10', 1),
(154, 72, 8, NULL, 'kmkkm', '2021-05-17 16:08:22', '2021-05-17 21:57:10', 1),
(155, 72, 8, NULL, 'kmkmkm', '2021-05-17 16:08:24', '2021-05-17 21:57:10', 1),
(156, 72, 8, NULL, 'kkmk', '2021-05-17 16:08:27', '2021-05-17 21:57:10', 1),
(157, 72, 8, NULL, 'mkmm', '2021-05-17 16:08:28', '2021-05-17 21:57:10', 1),
(158, 72, 8, NULL, 'asddd', '2021-05-17 16:12:37', '2021-05-17 21:57:10', 1),
(159, 74, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 16:25:44', '2021-05-17 16:26:54', 1),
(160, 74, 8, NULL, 'Привет', '2021-05-17 16:25:52', '2021-05-17 16:26:54', 1),
(161, 74, 8, NULL, 'Привет', '2021-05-17 16:26:40', '2021-05-17 16:26:54', 1),
(162, 74, 8, NULL, 'Здравстувуйте', '2021-05-17 16:26:52', '2021-05-17 16:26:54', 1),
(163, 74, 8, NULL, 'Салам бро', '2021-05-17 16:27:06', '2021-05-17 16:27:07', 1),
(164, 75, 1, NULL, 'asd', '2021-05-17 20:49:46', '2021-05-17 20:53:36', 1),
(165, 75, 1, NULL, 'lklk', '2021-05-17 20:50:06', '2021-05-17 20:53:36', 1),
(166, 75, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-17 20:51:28', '2021-05-17 20:53:36', 1),
(167, 75, 9, NULL, 'так так', '2021-05-17 20:59:32', '2021-05-17 20:59:33', 1),
(168, 75, 8, NULL, 'asd', '2021-05-17 20:59:47', '2021-05-17 20:59:49', 1),
(169, 75, 9, NULL, 'овоаоа', '2021-05-17 21:00:26', '2021-05-17 21:00:27', 1),
(170, 75, 9, NULL, 'рррр', '2021-05-17 21:01:51', '2021-05-17 21:01:52', 1),
(171, 73, 1, NULL, 'Здравствуйте я ваш адвокат Admin', '2021-05-20 15:21:22', '2021-05-20 15:21:40', 1),
(172, 73, 1, NULL, 'assd', '2021-05-20 15:22:12', '2021-05-20 15:22:13', 1),
(173, 73, 1, NULL, 'Privet', '2021-05-20 15:22:20', '2021-05-20 15:22:21', 1),
(174, 73, 6, NULL, '4241', '2021-05-20 15:22:32', '2021-05-20 15:22:33', 1),
(175, 73, 6, NULL, 'от', '2021-05-20 15:22:38', '2021-05-20 15:22:40', 1),
(176, 73, 6, NULL, 'оо', '2021-05-20 15:22:54', '2021-05-20 15:22:55', 1),
(177, 73, 6, NULL, 'хелло', '2021-05-20 15:24:58', '2021-05-20 15:25:00', 1),
(178, 73, 1, NULL, 'Привет', '2021-05-20 15:26:23', '2021-05-20 15:26:24', 1),
(179, 73, 1, NULL, 'Как ты', '2021-05-20 15:26:25', '2021-05-20 15:26:27', 1),
(180, 73, 6, NULL, 'ооо', '2021-05-20 15:32:06', '2021-05-20 15:32:07', 1),
(181, 73, 6, NULL, 'лол', '2021-05-20 15:32:17', '2021-05-20 15:32:21', 1),
(182, 73, 6, NULL, 'ооо', '2021-05-20 15:32:29', '2021-05-20 15:32:30', 1),
(183, 73, 1, NULL, 'asd', '2021-05-20 17:14:29', '2021-05-20 17:18:19', 1),
(184, 73, 1, NULL, 'Хеллоу', '2021-05-20 17:15:06', '2021-05-20 17:18:19', 1),
(185, 73, 1, NULL, 'oko', '2021-05-20 17:15:57', '2021-05-20 17:18:19', 1),
(186, 73, 1, NULL, 'asd', '2021-05-20 17:16:09', '2021-05-20 17:18:19', 1),
(187, 73, 1, NULL, 'asd', '2021-05-20 17:18:12', '2021-05-20 17:18:19', 1),
(188, 73, 1, NULL, 'Привет', '2021-05-20 17:18:23', '2021-05-20 17:18:25', 1),
(189, 73, 1, NULL, 'хнй', '2021-05-20 17:20:07', '2021-05-20 17:20:09', 1),
(190, 73, 1, NULL, 'щл', '2021-05-20 17:20:25', '2021-05-20 17:20:26', 1),
(191, 73, 1, NULL, 'дл', '2021-05-20 17:20:49', '2021-05-20 17:20:51', 1),
(192, 73, 1, NULL, 'щл', '2021-05-20 17:21:06', '2021-05-20 17:21:09', 1),
(193, 73, 1, NULL, 'да', '2021-05-20 17:38:24', '2021-05-20 17:38:25', 1),
(194, 87, 13, NULL, 'Здравствуйте я ваш адвокат Валеев Дулат Каиржанович', '2021-05-21 09:28:13', '2021-05-21 09:29:01', 1),
(195, 87, 13, NULL, 'Здравствуйте !!!', '2021-05-21 09:28:39', '2021-05-21 09:29:01', 1),
(196, 87, 13, NULL, 'Чем я могу Вам помочь ?', '2021-05-21 09:28:56', '2021-05-21 09:29:01', 1),
(197, 87, 15, NULL, 'я могу вам позвонить?', '2021-05-21 09:29:19', '2021-05-21 09:29:20', 1),
(198, 87, 13, NULL, '8-707-750-92-62', '2021-05-21 09:29:48', '2021-05-21 09:29:50', 1),
(199, 75, 1, NULL, 'sdsd', '2021-05-21 11:55:38', '2021-05-21 21:33:53', 1),
(200, 84, 1, NULL, 'Здравствуйте я ваш адвокат Admin', '2021-05-21 15:31:59', '2021-05-21 15:34:53', 1),
(201, 84, 1, NULL, 'Как жизнь', '2021-05-21 15:32:12', '2021-05-21 15:34:53', 1),
(202, 84, 1, NULL, 'Как ьы', '2021-05-21 15:34:03', '2021-05-21 15:34:53', 1),
(203, 84, 1, NULL, 'Привет', '2021-05-21 15:34:48', '2021-05-21 15:34:53', 1),
(204, 84, 6, NULL, 'норм', '2021-05-21 15:34:57', '2021-05-21 15:34:58', 1),
(205, 84, 6, NULL, 'как сам', '2021-05-21 15:35:04', '2021-05-21 15:35:05', 1),
(206, 84, 6, NULL, 'ооо', '2021-05-21 15:37:16', '2021-05-21 15:37:17', 1),
(207, 84, 6, NULL, 'ну', '2021-05-21 15:37:26', '2021-05-21 15:37:27', 1),
(208, 84, 6, NULL, 'оо', '2021-05-21 15:39:17', '2021-05-21 15:39:18', 1),
(209, 84, 6, NULL, 'сс', '2021-05-21 15:40:48', '2021-05-21 15:40:49', 1),
(210, 84, 6, NULL, 'ооо', '2021-05-21 15:46:31', '2021-05-21 15:46:33', 1),
(211, 84, 6, NULL, 'оо', '2021-05-21 15:47:07', '2021-05-21 15:47:09', 1),
(212, 84, 6, NULL, 'оо', '2021-05-21 15:48:04', '2021-05-21 15:48:05', 1),
(213, 84, 1, NULL, 'Хай', '2021-05-21 15:48:14', '2021-05-21 15:48:16', 1),
(214, 86, 1, NULL, 'Здравствуйте я ваш адвокат Admin', '2021-05-21 15:48:58', '2021-05-21 15:49:10', 1),
(215, 86, 6, NULL, 'оо', '2021-05-21 15:49:12', '2021-05-21 15:49:13', 1),
(216, 86, 6, NULL, 'здравствуйте', '2021-05-21 15:49:29', '2021-05-21 15:49:30', 1),
(217, 86, 6, NULL, 'оо', '2021-05-21 15:53:20', '2021-05-21 15:53:22', 1),
(218, 86, 6, NULL, 'оо', '2021-05-21 15:54:14', '2021-05-21 15:54:17', 1),
(219, 86, 1, NULL, 'dd', '2021-05-21 15:55:01', '2021-05-21 15:55:02', 1),
(220, 86, 1, NULL, 'ssads', '2021-05-21 15:55:06', '2021-05-21 15:55:07', 1),
(221, 86, 6, NULL, 'ооо', '2021-05-21 16:03:28', '2021-05-21 16:03:29', 1),
(222, 86, 6, NULL, 'г', '2021-05-21 16:04:36', '2021-05-21 16:04:37', 1),
(223, 86, 6, NULL, 'хех', '2021-05-21 16:04:48', '2021-05-21 16:04:50', 1),
(224, 86, 6, NULL, 'оо', '2021-05-21 16:09:07', '2021-05-21 16:09:08', 1),
(225, 86, 6, NULL, 'ооо', '2021-05-21 16:09:49', '2021-05-21 16:09:50', 1),
(226, 86, 6, NULL, 'ло', '2021-05-21 16:14:30', '2021-05-21 16:14:31', 1),
(227, 85, 1, NULL, 'Здравствуйте я ваш адвокат Admin', '2021-05-21 16:14:38', '2021-05-21 16:14:43', 1),
(228, 85, 6, NULL, 'лл', '2021-05-21 16:14:47', '2021-05-21 16:14:48', 1),
(229, 86, 6, NULL, 'ооо', '2021-05-21 16:22:54', '2021-05-21 16:22:55', 1),
(230, 86, 6, NULL, 'ну', '2021-05-21 16:23:08', '2021-05-21 16:23:09', 1),
(231, 86, 6, NULL, 'Асыл', '2021-05-21 16:23:35', '2021-05-21 16:23:39', 1),
(232, 86, 6, NULL, 'алди педик', '2021-05-21 16:39:03', '2021-05-21 16:39:04', 1),
(233, 86, 6, NULL, 'лапа', '2021-05-21 16:47:24', '2021-05-21 16:47:25', 1),
(234, 91, 1, NULL, 'Здравствуйте я ваш адвокат Admin', '2021-05-21 21:19:22', '2021-05-21 21:20:42', 1),
(235, 91, 1, NULL, 'ппп', '2021-05-21 21:19:40', '2021-05-21 21:20:42', 1),
(236, 91, 1, NULL, 'ппп', '2021-05-21 21:19:41', '2021-05-21 21:20:42', 1),
(237, 91, 1, NULL, 'рппр', '2021-05-21 21:20:11', '2021-05-21 21:20:42', 1),
(238, 91, 15, NULL, 'ghhh', '2021-05-21 21:22:16', '2021-05-21 21:22:17', 1),
(239, 92, 13, NULL, 'Здравствуйте я ваш адвокат Валеев Дулат Каиржанович', '2021-05-21 21:27:09', '2021-05-21 21:27:31', 1),
(240, 92, 13, NULL, 'Здравствуйте !!!', '2021-05-21 21:27:24', '2021-05-21 21:27:31', 1),
(241, 92, 9, NULL, 'ееееее', '2021-05-21 21:27:40', '2021-05-21 21:27:41', 1),
(242, 92, 9, NULL, 'ащгащращн', '2021-05-21 21:27:48', '2021-05-21 21:27:50', 1),
(243, 92, 9, NULL, 'ннннн', '2021-05-21 21:27:56', '2021-05-21 21:27:57', 1),
(244, 92, 13, NULL, 'Чем могу помочь ?', '2021-05-21 21:28:16', '2021-05-21 21:28:17', 1),
(245, 92, 9, NULL, 'рвововов', '2021-05-21 21:28:30', '2021-05-21 21:28:31', 1),
(246, 83, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-21 21:30:30', '2021-05-23 19:11:48', 1),
(247, 93, 8, NULL, 'Здравствуйте я ваш адвокат Есен', '2021-05-21 21:33:16', '2021-05-21 21:34:11', 1),
(248, 93, 8, NULL, 'я решаю вам это будет дорого стоить', '2021-05-21 21:34:01', '2021-05-21 21:34:11', 1),
(249, 93, 8, NULL, 'Вам', '2021-05-21 21:34:11', '2021-05-21 21:34:13', 1),
(250, 93, 9, NULL, 'тогда не надо', '2021-05-21 21:34:24', '2021-05-21 21:34:26', 1),
(251, 93, 8, NULL, '((((', '2021-05-21 21:34:34', '2021-05-21 21:34:35', 1),
(252, 93, 9, NULL, '))))', '2021-05-21 21:34:59', '2021-05-21 21:35:00', 1),
(253, 93, 9, NULL, 'опоропопопропоппопр', '2021-05-21 21:35:20', '2021-05-21 21:35:22', 1),
(254, 93, 9, NULL, 'ооа', '2021-05-21 21:35:41', '2021-05-21 21:35:43', 1),
(255, 91, 15, NULL, 'gghh', '2021-05-21 21:35:46', '2021-05-21 21:35:47', 1),
(256, 92, 9, NULL, 'аооао', '2021-05-21 21:35:51', '2021-05-21 21:35:52', 1),
(257, 92, 13, NULL, 'Здравствуйте !!!', '2021-05-21 21:36:10', '2021-05-21 21:36:11', 1),
(258, 93, 9, NULL, 'оаоао', '2021-05-21 21:43:11', '2021-05-21 21:43:12', 1),
(259, 91, 1, NULL, 'ответ', '2021-05-21 21:43:41', '2021-05-21 21:44:13', 1),
(260, 93, 9, NULL, 'ррар', '2021-05-21 21:45:19', '2021-05-21 21:45:20', 1),
(261, 93, 9, NULL, 'ннн', '2021-05-21 21:45:39', '2021-05-21 21:45:40', 1),
(262, 93, 9, NULL, 'нрмп', '2021-05-21 21:45:45', '2021-05-21 21:45:47', 1),
(263, 93, 9, NULL, 'аяеяе', '2021-06-08 16:54:51', '2021-06-08 16:54:52', 1),
(264, 101, 1, NULL, 'Здравствуйте я ваш адвокат Admin', '2021-06-10 11:48:37', '2021-06-10 11:50:57', 1),
(265, 101, 1, NULL, 'Чем могу вам помочь', '2021-06-10 11:50:22', '2021-06-10 11:50:57', 1),
(266, 101, 15, NULL, 'хочу вам помочь', '2021-06-10 11:51:21', '2021-06-10 11:51:22', 1),
(267, 101, 1, NULL, 'приезжайте', '2021-06-10 11:51:32', '2021-06-10 11:51:33', 1),
(268, 100, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 10:07:57', '2021-06-11 10:08:37', 1),
(269, 100, 9, NULL, 'здравствуйте', '2021-06-11 10:08:55', '2021-06-11 10:08:56', 1),
(270, 102, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 12:21:20', '2021-06-11 12:22:57', 1),
(271, 102, 43, NULL, 'Здравствуйте', '2021-06-11 12:22:03', '2021-06-11 12:22:57', 1),
(272, 102, 43, NULL, 'Здравствуйте', '2021-06-11 12:22:04', '2021-06-11 12:22:57', 1),
(273, 102, 43, NULL, 'какой Вас интересует вопрос', '2021-06-11 12:22:22', '2021-06-11 12:22:57', 1),
(274, 102, 15, NULL, 'расторжение брака', '2021-06-11 12:23:38', '2021-06-11 12:23:39', 1),
(275, 102, 43, NULL, 'что именно Вас интересует', '2021-06-11 12:24:17', '2021-06-11 12:24:18', 1),
(276, 102, 15, NULL, 'шаблон документа', '2021-06-11 12:24:47', '2021-06-11 12:24:49', 1),
(277, 103, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 12:33:45', '2021-06-11 12:33:56', 1),
(278, 103, 36, NULL, 'здравствуйте', '2021-06-11 12:34:08', '2021-06-11 12:34:09', 1),
(279, 103, 43, NULL, 'Здравствуйте', '2021-06-11 12:34:32', '2021-06-11 12:34:33', 1),
(280, 103, 43, NULL, 'чем могу помочь', '2021-06-11 12:34:37', '2021-06-11 12:34:38', 1),
(281, 103, 36, NULL, 'нужен образец искового заявления', '2021-06-11 12:34:46', '2021-06-11 12:34:47', 1),
(282, 103, 43, NULL, 'да конечно', '2021-06-11 12:35:14', '2021-06-11 12:35:16', 1),
(283, 103, 43, NULL, 'куда удобно отправить Вам образец искового заявления о расторжение брака', '2021-06-11 12:35:49', '2021-06-11 12:35:51', 1),
(284, 103, 36, NULL, 'на ватцап', '2021-06-11 12:36:03', '2021-06-11 12:36:05', 1),
(285, 98, 1, NULL, 'Здравствуйте я ваш адвокат Аида', '2021-06-11 12:36:11', '2021-06-11 12:42:51', 1),
(286, 98, 1, NULL, 'поздний ответ', '2021-06-11 12:36:25', '2021-06-11 12:42:51', 1),
(287, 98, 1, NULL, 'сорри', '2021-06-11 12:36:31', '2021-06-11 12:42:51', 1),
(288, 103, 43, NULL, 'скажите пожалуйста номер ватцап', '2021-06-11 12:36:59', '2021-06-11 12:37:01', 1),
(289, 103, 36, NULL, '8888888', '2021-06-11 12:38:15', '2021-06-11 12:38:16', 1),
(290, 103, 43, NULL, 'все хорошо сейчас отправлю', '2021-06-11 12:38:27', '2021-06-11 12:38:28', 1),
(291, 103, 43, NULL, 'Вам был отправлен шаблон искового заявления о расторжения брака на номер ватцапа который Вы предоставили', '2021-06-11 12:39:09', '2021-06-11 12:39:11', 1),
(292, 103, 36, NULL, 'спасибо', '2021-06-11 12:39:20', '2021-06-11 12:39:21', 1),
(293, 96, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 12:41:08', '2021-06-11 12:42:48', 1),
(294, 62, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 12:42:06', '2021-06-14 03:19:59', 1),
(295, 98, 9, NULL, 'пуш пришел, я получил от всех ответ', '2021-06-11 12:43:10', '2021-06-11 12:43:11', 1),
(296, 98, 9, NULL, 'напишите пожалуйста ещё раз', '2021-06-11 12:43:19', '2021-06-11 12:43:21', 1),
(297, 100, 1, NULL, 'Салам', '2021-06-11 12:44:01', '2021-06-11 12:44:31', 1),
(298, 100, 1, NULL, 'Салам', '2021-06-11 12:44:02', '2021-06-11 12:44:31', 1),
(299, 104, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 13:30:04', '2021-06-11 13:30:30', 1),
(300, 104, 43, NULL, 'чем могу быть полезна', '2021-06-11 13:30:22', '2021-06-11 13:30:30', 1),
(301, 104, 43, NULL, 'чем могу быть полезна', '2021-06-11 13:30:23', '2021-06-11 13:30:30', 1),
(302, 104, 43, NULL, 'чем могу быть полезна', '2021-06-11 13:30:23', '2021-06-11 13:30:30', 1),
(303, 104, 36, NULL, 'хочу получить консультацию', '2021-06-11 13:30:48', '2021-06-11 13:30:49', 1),
(304, 104, 43, NULL, 'что именно Вас интересует', '2021-06-11 13:31:05', '2021-06-11 13:31:06', 1),
(305, 104, 36, NULL, 'как происходит раздел имущества супругов', '2021-06-11 13:32:07', '2021-06-11 13:32:10', 1),
(306, 104, 43, NULL, 'имущество было приобретено в совместном браке?', '2021-06-11 13:32:36', '2021-06-11 13:32:39', 1),
(307, 104, 43, NULL, 'или есть имущество которое было приобретено путем дарения?', '2021-06-11 13:32:55', '2021-06-11 13:32:56', 1),
(308, 104, 36, NULL, 'машина была приобретена мною до заключения брака', '2021-06-11 13:33:36', '2021-06-11 13:33:38', 1),
(309, 104, 36, NULL, 'дом строили вместе с супругой', '2021-06-11 13:33:56', '2021-06-11 13:33:57', 1),
(310, 104, 36, NULL, 'есть квартира подаренная бабушкой', '2021-06-11 13:34:30', '2021-06-11 13:34:31', 1),
(311, 104, 43, NULL, 'разделу имущества подлежит дом так как он является совместно нажитым имуществом супругов', '2021-06-11 13:35:08', '2021-06-11 13:35:10', 1),
(312, 104, 43, NULL, 'разделу имущества подлежит дом так как он является совместно нажитым имуществом супругов', '2021-06-11 13:35:08', '2021-06-11 13:35:10', 1),
(313, 104, 43, NULL, 'квартира разделу не подлежит так как она была Вам подарена', '2021-06-11 13:35:39', '2021-06-11 13:35:41', 1),
(314, 104, 43, NULL, 'автомашина купленная до заключение брака является Вашей собственностью', '2021-06-11 13:36:09', '2021-06-11 13:36:11', 1),
(315, 104, 43, NULL, 'какие есть еще вопросы', '2021-06-11 13:36:28', '2021-06-11 13:36:29', 1),
(316, 104, 36, NULL, 'а как будет происходить раздел дома', '2021-06-11 13:37:02', '2021-06-11 13:37:03', 1),
(317, 104, 43, NULL, 'раздел дома будет происходит в следующим порядке: 1. провести оценку недвижимого имущества. 2 Если Вы хотите в денежном выражение - то 50% от стоимости дома Вы можете запросить у супруги. 3. Также можно предложить вариант Вашей супруге - оплатить ей 50% стоимости дома а сам дом оставить себе', '2021-06-11 13:39:27', '2021-06-11 13:39:28', 1),
(318, 104, 36, NULL, 'можно ответ повторить', '2021-06-11 13:43:55', '2021-06-11 13:43:57', 1),
(319, 104, 43, NULL, 'раздел дома будет происходит в следующим порядке: 1. провести оценку недвижимого имущества. 2 Если Вы хотите в денежном выражение - то 50% от стоимости дома Вы можете запросить у супруги. 3. Также можно предложить вариант Вашей супруге - оплатить ей 50% стоимости дома а сам дом оставить себе', '2021-06-11 13:44:09', '2021-06-11 13:44:11', 1),
(320, 104, 43, NULL, 'раздел дома будет происходит в следующим порядке: 1. провести оценку недвижимого имущества. 2 Если Вы хотите в денежном выражение - то 50% от стоимости дома Вы можете запросить у супруги. 3. Также можно предложить вариант Вашей супруге - оплатить ей 50% стоимости дома а сам дом оставить себе', '2021-06-11 13:46:37', '2021-06-11 13:46:38', 1),
(321, 104, 43, NULL, 'раздел дома будет происходит в следующим порядке: 1. провести оценку недвижимого имущества. 2 Если Вы хотите в денежном выражение - то 50% от стоимости дома Вы можете запросить у супруги. 3. Также можно предложить вариант Вашей супруге - оплатить ей 50% стоимости дома а сам дом оставить себе', '2021-06-11 13:47:38', '2021-06-11 13:47:40', 1),
(322, 104, 36, NULL, 'ответ полностью не видно', '2021-06-11 13:49:01', '2021-06-11 13:49:03', 1),
(323, 104, 43, NULL, 'раздел дома будет происходит в следующим порядке: 1. провести оценку недвижимого имущества. 2 Если Вы хотите в денежном выражение - то 50% от стоимости дома Вы можете запросить у супруги. 3. Также можно предложить вариант Вашей супруге - оплатить ей 50% стоимости дома а сам дом оставить себе', '2021-06-11 13:49:05', '2021-06-11 13:49:08', 1),
(324, 104, 36, NULL, 'а если супруга откажется', '2021-06-11 13:51:31', '2021-06-11 13:51:33', 1),
(325, 104, 43, NULL, 'то в судебном порядке суд будет рассматривать дело  согласно закону  каждой стороне по 1/2 части', '2021-06-11 13:54:15', '2021-06-11 13:54:17', 1),
(326, 71, 44, NULL, 'Здравствуйте я ваш адвокат Калдыгожин Алихан Нурланович', '2021-06-11 13:59:32', '2021-07-11 20:12:33', 1),
(327, 105, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-11 14:01:15', '2021-06-11 14:02:11', 1),
(328, 105, 15, NULL, 'вопрос', '2021-06-11 14:02:20', '2021-06-11 14:02:21', 1),
(329, 105, 43, NULL, 'ответ', '2021-06-11 14:02:27', '2021-06-11 14:02:28', 1),
(330, 106, 44, NULL, 'Здравствуйте я ваш адвокат Калдыгожин Алихан Нурланович', '2021-06-11 14:04:04', '2021-06-11 14:04:28', 1),
(331, 106, 44, NULL, 'jr', '2021-06-11 14:05:19', '2021-06-11 14:05:33', 1),
(332, 106, 15, NULL, 'можно прийти на консультацию к вам?', '2021-06-11 14:20:47', '2021-06-11 14:20:48', 1),
(333, 106, 44, NULL, 'lf', '2021-06-11 14:21:04', '2021-06-11 14:21:06', 1),
(334, 106, 44, NULL, 'da', '2021-06-11 14:21:06', '2021-06-11 14:21:07', 1),
(335, 106, 15, NULL, 'а вы поможете составить документы?', '2021-06-11 14:21:36', '2021-06-11 14:21:38', 1),
(336, 106, 44, NULL, 'возможно)', '2021-06-11 14:21:51', '2021-06-11 14:22:08', 1),
(337, 106, 15, NULL, 'сколько будет стоить?', '2021-06-11 14:22:29', '2021-06-11 14:22:30', 1),
(338, 112, 1, NULL, 'Здравствуйте я ваш адвокат Аида', '2021-06-14 03:22:08', '2021-06-14 03:22:23', 1),
(339, 112, 1, NULL, 'Хай', '2021-06-14 03:22:17', '2021-06-14 03:22:23', 1),
(340, 112, 6, NULL, 'ηηη', '2021-06-14 03:22:25', '2021-06-14 03:22:26', 1),
(341, 111, 1, NULL, 'Здравствуйте я ваш адвокат Аида', '2021-06-15 17:52:08', '2021-06-15 17:53:22', 1),
(342, 111, 1, NULL, 'asdsd', '2021-06-15 17:52:18', '2021-06-15 17:53:22', 1),
(343, 111, 6, NULL, 'привет', '2021-06-15 17:53:27', '2021-06-15 17:53:29', 1),
(344, 111, 6, NULL, 'оооо', '2021-06-15 17:53:50', '2021-06-15 17:53:52', 1),
(345, 111, 6, NULL, 'ооо', '2021-06-15 17:54:29', '2021-06-15 17:54:31', 1),
(346, 111, 6, NULL, 'Балашов', '2021-06-15 17:55:30', '2021-06-15 17:55:32', 1),
(347, 111, 6, NULL, 'оооо', '2021-06-15 17:56:30', '2021-06-15 17:56:32', 1),
(348, 113, 1, NULL, 'Здравствуйте я ваш адвокат Аида', '2021-06-16 11:49:41', '2021-06-16 11:50:23', 1),
(349, 113, 1, NULL, 'Oka', '2021-06-16 11:49:51', '2021-06-16 11:50:23', 1),
(350, 113, 1, NULL, 'Nbxkskshsishsisjshwusjwnwiwbwiwbwjwwbwbwknwwhwjsvsjsbissbsisbajsbsjsnsjsisbsbjssksjsbsj', '2021-06-16 11:50:04', '2021-06-16 11:50:23', 1),
(351, 113, 1, NULL, 'Ajhajakakakakakhajwkakakakakakakakakaoakjajakakajajajwhsjsbjsjwjwjwjwjsjwjwnwjwjwjwjjwjwjwjwkwjqjwkqkwkwjwjwwkkwwkkwkwkwjwjwjwjwkwkwkwkwkjwjwkwkwkwkwkwjwjwkwkwkwkwkkwwkkwjwjwjwkwkwjwjwjwjjwwjjwjwjwjwjwjwjwjwjwjwjwjwjwjwjwjwjwjwjwjjwwjjwjwjwjwhwjwjwjejwjejwjehehehheheheheheheheheheh', '2021-06-16 11:51:07', '2021-06-16 11:51:08', 1),
(352, 113, 9, NULL, 'гаоаопомомоппгмщтчдрчдрчдрвдрялрчлрчбрчдряьпялпылпылеялпялпяшнвщнвщеышеяднвднчдрчдрчдрчдевдрвднвдрчшеыгеяшеяшеялеягаАгякоОаКгКояшеыышеыешышеыешыгпыешягеыгеыыелыегыешыелыелыешыввнщвшевщврщвдрвдрчднсднсднсндаднсднсбрсюосжнсдрчднсдеччдевшевщнчлпчдрядечлечлеччндялеяпляднвднвтсрмимммимтслалслмлпопомопопопполппомосомоаосопососососомомомомсосослсостстаоаосослмлслссослалплслслсосоаоплаососоаососососососоососососомлсосослслсмлмллмплмлмлмлмлмлмлмлмомомомомомомлмлмомомо осослсосослалалплслашплплмлалалвоалвлчлсалслмлалалслсл сллалмлмплслслмлмлпомосолалаа. да Ла Ла шаг Ла ш а шаг а ша шаг по. ш шаг ч га шсгаш а шаг во ааьслаососососослпо', '2021-06-16 11:51:27', '2021-06-16 11:51:29', 1),
(353, 116, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-17 09:28:31', '2021-06-17 09:29:49', 1),
(354, 116, 43, NULL, 'Здравствуйте', '2021-06-17 09:28:54', '2021-06-17 09:29:49', 1),
(355, 116, 37, NULL, 'тори', '2021-06-17 09:30:07', '2021-06-17 09:30:08', 1),
(356, 116, 43, NULL, 'что Вас интересует', '2021-06-17 09:30:16', '2021-06-17 09:30:17', 1),
(357, 116, 43, NULL, 'что Вас интересует', '2021-06-17 09:30:16', '2021-06-17 09:30:17', 1),
(358, 116, 37, NULL, 'оодлилшттддл шлтлоилщиршоио лиродипшииол родтжьдрщпродти лолищодищрллтт шилищрщрол шищоишалишр оидилищишорршщт шилиррлтштощто шрррот', '2021-06-17 09:30:56', '2021-06-17 09:30:58', 1),
(359, 116, 37, NULL, 'оодлилшттддл шлтлоилщиршоио лиродипшииол родтжьдрщпродти лолищодищрллтт шилищрщрол шищоишалишр оидилищишорршщт шилиррлтштощто шрррот', '2021-06-17 09:30:58', '2021-06-17 09:31:00', 1),
(360, 116, 43, NULL, 'пппппппплодлвпрлдылпаоооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-17 09:31:10', '2021-06-17 09:31:11', 1),
(361, 116, 43, NULL, 'пппппппплодлвпрлдылпаоооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-17 09:31:10', '2021-06-17 09:31:11', 1),
(362, 116, 37, NULL, 'о', '2021-06-17 09:41:38', '2021-06-17 09:41:39', 1),
(363, 117, 44, NULL, 'Здравствуйте я ваш адвокат Калдыгожин Алихан Нурланович', '2021-06-17 11:03:12', '2021-06-17 11:03:51', 1),
(364, 117, 44, NULL, 'plhfasdf', '2021-06-17 11:03:42', '2021-06-17 11:03:51', 1),
(365, 117, 44, NULL, 'plhfasdf', '2021-06-17 11:03:43', '2021-06-17 11:03:51', 1),
(366, 117, 36, NULL, 'скажите пожалуйста условия раздела имущества', '2021-06-17 11:04:10', '2021-06-17 11:04:11', 1),
(367, 117, 44, NULL, 'шывоадлывоаоывоаывоаоывлаолывдоаывлоалывоа жфлываждофыждвофылжаофлыоафылдаофыдларфылдвт двлфоджывофыовдофывофыдлвоо щфзыовфщыовлфоылвдфывифывтлдфытв щфоывофыаивоыфиаодвыиалолиыв лфытвалдфоыващфтывалдифваив шфытадфырагрвытаыврашгрывм шщоыфтоыфаиывлоаофшывим оырвтаоыив свы мыварыв моывадслтвылдаоывомилывлиаиыулораиылвисигыфигврфыдвофыовщфиыазофыисовылфрфыо с гщывтдфыи ср фыс ыфотвофытосивясмивыгшрсфышисыфрщсоыфтолсифыщзлсыфтсфыэсофдыо и фшоолфыитвжьфыивфытс', '2021-06-17 11:04:41', '2021-06-17 11:04:42', 1),
(368, 117, 44, NULL, 'шывоадлывоаоывоаывоаоывлаолывдоаывлоалывоа жфлываждофыждвофылжаофлыоафылдаофыдларфылдвт двлфоджывофыовдофывофыдлвоо щфзыовфщыовлфоылвдфывифывтлдфытв щфоывофыаивоыфиаодвыиалолиыв лфытвалдфоыващфтывалдифваив шфытадфырагрвытаыврашгрывм шщоыфтоыфаиывлоаофшывим оырвтаоыив свы мыварыв моывадслтвылдаоывомилывлиаиыулораиылвисигыфигврфыдвофыовщфиыазофыисовылфрфыо с гщывтдфыи ср фыс ыфотвофытосивясмивыгшрсфышисыфрщсоыфтолсифыщзлсыфтсфыэсофдыо и фшоолфыитвжьфыивфытс', '2021-06-17 11:04:41', '2021-06-17 11:04:42', 1),
(369, 117, 36, NULL, 'аапррррпииттлоонекквамитььддлгнори', '2021-06-17 11:05:00', '2021-06-17 11:05:01', 1),
(370, 117, 36, NULL, 'аапннроолдщшшгекккаммтьбжжддлнеккамиьдю', '2021-06-17 11:05:18', '2021-06-17 11:05:20', 1),
(371, 117, 36, NULL, 'ппрол', '2021-06-17 11:09:22', '2021-06-17 11:09:24', 1),
(372, 117, 44, NULL, 'ненеа', '2021-06-17 11:09:38', '2021-06-17 11:09:39', 1),
(373, 117, 44, NULL, 'ненеа', '2021-06-17 11:09:39', '2021-06-17 11:09:39', 1),
(374, 117, 44, NULL, 'оо', '2021-06-17 11:09:51', '2021-06-17 11:09:53', 1),
(375, 117, 36, NULL, 'аапап', '2021-06-17 11:10:09', '2021-06-17 11:10:10', 1),
(376, 119, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Филина', '2021-06-17 11:12:27', '2021-06-17 11:12:47', 1),
(377, 119, 43, NULL, 'добрый день', '2021-06-17 11:12:40', '2021-06-17 11:12:47', 1),
(378, 119, 36, NULL, 'здравствуйте', '2021-06-17 11:12:51', '2021-06-17 11:12:52', 1),
(379, 119, 43, NULL, 'что интересует', '2021-06-17 11:12:59', '2021-06-17 11:13:01', 1),
(380, 119, 43, NULL, 'развод', '2021-06-17 11:13:28', '2021-06-17 11:13:30', 1),
(381, 119, 36, NULL, 'да', '2021-06-17 11:13:34', '2021-06-17 11:13:35', 1),
(382, 120, 44, NULL, 'Здравствуйте я ваш адвокат Калдыгожин Алихан Нурланович', '2021-06-17 11:14:20', '2021-06-17 11:14:42', 1),
(383, 119, 43, NULL, 'что именно', '2021-06-17 11:14:29', '2021-06-17 11:14:31', 1),
(384, 120, 44, NULL, 'гаф', '2021-06-17 11:14:32', '2021-06-17 11:14:42', 1),
(385, 120, 37, NULL, 'мяу', '2021-06-17 11:14:47', '2021-06-17 11:14:48', 1),
(386, 119, 36, NULL, 'развод', '2021-06-17 11:14:53', '2021-06-17 11:14:54', 1),
(387, 120, 37, NULL, 'ролирлипшттттткупропвооьагш о отождешпо оотолтпгли', '2021-06-17 11:15:06', '2021-06-17 11:15:07', 1),
(388, 119, 36, NULL, 'ппроллнееккамиьбюээзшгнеуцццвясиьбюжэзгекуучстьджждрпкасмтддж', '2021-06-17 11:15:12', '2021-06-17 11:15:14', 1),
(389, 120, 44, NULL, 'пыгшаыуграывщзоавыаовылоажрушвм оувшарфышщлвасцвиыстыфвьоаршыо мофрыисрфыьвиащзфышсомоыфвлдатйыловафвы фт ищсовфлоыивщзыфвигфышоафрыаывфщоафырврфыщвтфыиосомячу', '2021-06-17 11:15:16', '2021-06-17 11:15:17', 1),
(390, 120, 44, NULL, 'пыгшаыуграывщзоавыаовылоажрушвм оувшарфышщлвасцвиыстыфвьоаршыо мофрыисрфыьвиащзфышсомоыфвлдатйыловафвы фт ищсовфлоыивщзыфвигфышоафрыаывфщоафырврфыщвтфыиосомячу', '2021-06-17 11:15:17', '2021-06-17 11:15:17', 1),
(391, 119, 43, NULL, 'рпевкыканполтлх0шнк56у45цуыукчамртлдьждлхзлщзогргееаекувкеыувчвасчпами тьдлоошрнекеыуывчпас тьдлоло', '2021-06-17 11:15:28', '2021-06-17 11:15:29', 1),
(392, 119, 36, NULL, 'овлулттш', '2021-06-17 11:16:10', '2021-06-17 11:16:11', 1),
(393, 119, 43, NULL, 'пршгщжо', '2021-06-17 11:16:17', '2021-06-17 11:16:18', 1),
(394, 119, 43, NULL, 'пмщгририж', '2021-06-17 11:16:26', '2021-06-17 11:16:28', 1),
(395, 119, 43, NULL, 'льоолророшшщ', '2021-06-17 11:16:34', '2021-06-17 11:16:35', 1),
(396, 119, 36, NULL, 'нмемеининтг', '2021-06-17 11:16:40', '2021-06-17 11:16:41', 1),
(397, 119, 43, NULL, 'ооээлх', '2021-06-17 11:16:44', '2021-06-17 11:16:46', 1),
(398, 119, 43, NULL, 'ооээлх', '2021-06-17 11:16:45', '2021-06-17 11:16:46', 1),
(399, 119, 43, NULL, 'льортрирльд', '2021-06-17 11:16:50', '2021-06-17 11:16:51', 1),
(400, 119, 36, NULL, 'ксксемнинтгтгтнмкатгтшьщь', '2021-06-17 11:17:02', '2021-06-17 11:17:03', 1),
(401, 119, 43, NULL, 'пнепгнририррлдллорипирпвч', '2021-06-17 11:17:13', '2021-06-17 11:17:14', 1),
(402, 119, 43, NULL, 'гоопрвпуушушопкуоплоплоплуполполподылпоулкоуклпоуплыоплпоулывполыпоывлоплыполвоплпволывопылопвлповлповлполвполвопоплвоповповлповлоплуповлпоэплщпкопьтлвапвыыпо', '2021-06-17 11:17:34', '2021-06-17 11:17:36', 1),
(403, 119, 36, NULL, 'шлвтткллвша', '2021-06-17 11:24:58', '2021-06-17 11:25:00', 1),
(404, 119, 43, NULL, 'рпнгпшрдш', '2021-06-17 11:25:12', '2021-06-17 11:25:13', 1),
(405, 119, 43, NULL, 'пмдлшщш', '2021-06-17 11:25:26', '2021-06-17 11:25:27', 1),
(406, 119, 43, NULL, 'ририолл', '2021-06-17 11:25:28', '2021-06-17 11:25:30', 1),
(407, 119, 36, NULL, 'ининиртг', '2021-06-17 11:25:38', '2021-06-17 11:25:40', 1),
(408, 119, 36, NULL, 'пррол', '2021-06-17 11:25:47', '2021-06-17 11:25:48', 1),
(409, 119, 36, NULL, 'ннгооит', '2021-06-17 11:25:52', '2021-06-17 11:25:54', 1),
(410, 119, 43, NULL, 'рироолл', '2021-06-17 11:26:33', '2021-06-17 11:26:34', 1),
(411, 119, 36, NULL, 'чевнагпшр', '2021-06-17 11:26:38', '2021-06-17 11:26:39', 1),
(412, 119, 43, NULL, 'риоол', '2021-06-17 11:26:57', '2021-06-17 11:26:59', 1),
(413, 119, 36, NULL, 'мнмги', '2021-06-17 11:28:10', '2021-06-17 11:28:12', 1),
(414, 119, 43, NULL, 'пмродлотоллд', '2021-06-17 11:28:19', '2021-06-17 11:28:21', 1),
(415, 119, 36, NULL, 'рнмниг', '2021-06-17 11:28:25', '2021-06-17 11:28:26', 1),
(416, 119, 36, NULL, 'мролл', '2021-06-17 11:29:10', '2021-06-17 11:29:11', 1),
(417, 119, 43, NULL, 'ппроо', '2021-06-17 11:29:13', '2021-06-17 11:29:15', 1),
(418, 119, 43, NULL, 'ппрооогьдлллл', '2021-06-17 11:29:20', '2021-06-17 11:29:21', 1),
(419, 119, 36, NULL, 'коишатшиипап', '2021-06-17 11:31:43', '2021-06-17 11:31:44', 1),
(420, 119, 36, NULL, 'мнмниггитт', '2021-06-17 11:31:52', '2021-06-17 11:31:53', 1),
(421, 119, 36, NULL, 'нснснмнигин', '2021-06-17 11:32:11', '2021-06-17 11:32:12', 1),
(422, 119, 36, NULL, 'нснмннмнг', '2021-06-17 11:32:37', '2021-06-17 11:32:38', 1),
(423, 119, 43, NULL, 'роррдл', '2021-06-17 11:32:50', '2021-06-17 11:32:51', 1),
(424, 119, 43, NULL, 'аапроолто', '2021-06-17 11:33:54', '2021-06-17 11:33:55', 1),
(425, 119, 36, NULL, 'мнсни', '2021-06-17 11:34:05', '2021-06-17 11:34:06', 1),
(426, 119, 43, NULL, 'ррооооллодлдлд', '2021-06-17 11:34:23', '2021-06-17 11:34:25', 1),
(427, 119, 36, NULL, 'сапроооолдд', '2021-06-17 11:34:30', '2021-06-17 11:34:32', 1),
(428, 119, 43, NULL, 'мпрроололддл', '2021-06-17 11:35:36', '2021-06-17 11:35:37', 1),
(429, 119, 36, NULL, 'емкмемнтшьш', '2021-06-17 11:35:47', '2021-06-17 11:35:48', 1),
(430, 119, 43, NULL, 'ооплуплдвы', '2021-06-17 11:38:47', '2021-06-17 11:38:49', 1),
(431, 119, 36, NULL, 'ппрроот', '2021-06-17 11:39:01', '2021-06-17 11:39:02', 1),
(432, 119, 43, NULL, 'отопивдид', '2021-06-17 11:39:17', '2021-06-17 11:39:18', 1),
(433, 119, 36, NULL, 'ниеигпгома', '2021-06-17 11:41:00', '2021-06-17 11:41:01', 1),
(434, 119, 43, NULL, 'родоозлэ', '2021-06-17 11:41:14', '2021-06-17 11:41:15', 1),
(435, 117, 36, NULL, 'оошш', '2021-06-17 11:41:30', '2021-06-17 11:41:32', 1),
(436, 117, 36, NULL, 'ооолл', '2021-06-17 11:42:11', '2021-06-17 11:42:12', 1),
(437, 119, 36, NULL, 'иглуталала', '2021-06-17 11:43:41', '2021-06-17 11:43:43', 1),
(438, 119, 43, NULL, 'рдоллжю', '2021-06-17 11:43:51', '2021-06-17 11:43:53', 1),
(439, 119, 36, NULL, 'прлдэ', '2021-06-17 11:47:38', '2021-06-17 11:47:40', 1),
(440, 119, 43, NULL, 'мпо', '2021-06-17 11:47:51', '2021-06-17 11:47:52', 1),
(441, 119, 36, NULL, 'проол', '2021-06-17 11:48:35', '2021-06-17 11:48:36', 1),
(442, 119, 43, NULL, 'рнпдшжщ', '2021-06-17 11:48:44', '2021-06-17 11:48:45', 1),
(443, 119, 36, NULL, 'смролл', '2021-06-17 11:50:12', '2021-06-17 11:50:14', 1),
(444, 119, 43, NULL, 'рпровпвж', '2021-06-17 11:50:17', '2021-06-17 11:50:19', 1),
(445, 119, 43, NULL, 'оорплфвыопо', '2021-06-17 11:51:31', '2021-06-17 11:51:32', 1),
(446, 119, 36, NULL, 'пароргри', '2021-06-17 11:51:42', '2021-06-17 11:51:44', 1),
(447, 119, 43, NULL, 'оропвдл', '2021-06-17 11:51:45', '2021-06-17 11:51:47', 1),
(448, 119, 43, NULL, 'ьоотвапувд', '2021-06-17 11:53:13', '2021-06-17 11:53:14', 1),
(449, 119, 43, NULL, 'рпмороол', '2021-06-17 11:54:40', '2021-06-17 11:54:42', 1),
(450, 119, 36, NULL, 'мнмгигтщтщтт', '2021-06-17 11:54:51', '2021-06-17 11:54:52', 1),
(451, 119, 43, NULL, 'олрпиж', '2021-06-17 11:55:03', '2021-06-17 11:55:05', 1),
(452, 119, 36, NULL, 'пмпроллд', '2021-06-17 11:55:08', '2021-06-17 11:55:10', 1),
(453, 119, 43, NULL, 'рроро', '2021-06-17 11:55:19', '2021-06-17 11:55:21', 1),
(454, 119, 36, NULL, 'ескмеинтг', '2021-06-17 11:55:23', '2021-06-17 11:55:25', 1),
(455, 119, 43, NULL, 'рроиро', '2021-06-17 11:55:32', '2021-06-17 11:55:33', 1),
(456, 119, 36, NULL, 'нсемнинтгтгтгьгтгтгтгтгьгтгтгтгтгтгтгтнтнтнт', '2021-06-17 11:55:45', '2021-06-17 11:55:46', 1),
(457, 119, 43, NULL, 'гргргргргороророророжщ', '2021-06-17 11:55:53', '2021-06-17 11:55:54', 1),
(458, 119, 43, NULL, 'ритио', '2021-06-17 11:56:25', '2021-06-17 11:56:27', 1),
(459, 119, 43, NULL, 'ритио', '2021-06-17 11:56:25', '2021-06-17 11:56:27', 1),
(460, 119, 36, NULL, 'нрнининтн', '2021-06-17 11:56:34', '2021-06-17 11:56:36', 1),
(461, 119, 43, NULL, 'ириооооооооооо', '2021-06-17 11:56:47', '2021-06-17 11:56:49', 1),
(462, 119, 36, NULL, 'в3смпрроооллььддд', '2021-06-17 11:56:56', '2021-06-17 11:56:57', 1),
(463, 119, 43, NULL, 'ирооооооооллллл', '2021-06-17 11:57:04', '2021-06-17 11:57:06', 1),
(464, 119, 43, NULL, 'kjckfhjdkhj', '2021-06-17 11:59:32', '2021-06-17 11:59:34', 1),
(465, 119, 36, NULL, 'оашалтолл', '2021-06-17 11:59:42', '2021-06-17 11:59:43', 1),
(466, 119, 43, NULL, 'mkgrmbhrkt', '2021-06-17 11:59:48', '2021-06-17 11:59:49', 1),
(467, 119, 36, NULL, 'гинигтгьшьш', '2021-06-17 11:59:53', '2021-06-17 11:59:54', 1),
(468, 119, 36, NULL, 'ининиртотг', '2021-06-17 12:00:00', '2021-06-17 12:00:01', 1),
(469, 116, 43, NULL, 'bvmvnmmkm;', '2021-06-17 12:00:18', '2021-06-17 12:33:47', 1),
(470, 116, 43, NULL, 'jmgdnn', '2021-06-17 12:00:24', '2021-06-17 12:33:47', 1),
(471, 119, 36, NULL, 'аллалььир', '2021-06-17 12:00:41', '2021-06-17 12:00:42', 1),
(472, 119, 43, NULL, 'hbjknjnk', '2021-06-17 12:01:10', '2021-06-17 12:01:11', 1),
(473, 116, 43, NULL, 'hhjk', '2021-06-17 12:01:24', '2021-06-17 12:33:46', 1),
(474, 119, 36, NULL, 'лклтктк', '2021-06-17 12:01:51', '2021-06-17 12:01:52', 1),
(475, 116, 43, NULL, 'jjkjklkllkklkl', '2021-06-17 12:02:08', '2021-06-17 12:33:46', 1),
(476, 117, 36, NULL, 'мемеининтт', '2021-06-17 12:02:17', '2021-06-17 12:02:19', 1),
(477, 117, 44, NULL, 'wadw', '2021-06-17 12:02:27', '2021-06-17 12:02:28', 1),
(478, 117, 36, NULL, 'рвок', '2021-06-17 12:02:31', '2021-06-17 12:02:33', 1),
(479, 117, 36, NULL, '6инигтштштштшьшьшьшьштшьщь', '2021-06-17 12:02:37', '2021-06-17 12:02:38', 1),
(480, 117, 36, NULL, 'иемнининигтгтшьшьш', '2021-06-17 12:02:43', '2021-06-17 12:02:44', 1),
(481, 119, 43, NULL, 'hjhbhbhbhb', '2021-06-17 12:02:57', '2021-06-17 12:02:59', 1),
(482, 117, 36, NULL, '5пеинигтг', '2021-06-17 12:03:04', '2021-06-17 12:03:06', 1),
(483, 119, 36, NULL, 'иеинтгьшбшб', '2021-06-17 12:03:10', '2021-06-17 12:03:12', 1),
(484, 119, 43, NULL, 'ghbhjkkl', '2021-06-17 12:03:16', '2021-06-17 12:03:18', 1),
(485, 117, 36, NULL, 'еиеинт', '2021-06-17 12:03:47', '2021-06-17 12:03:49', 1),
(486, 119, 36, NULL, 'рининтгь', '2021-06-17 12:04:00', '2021-06-17 12:04:03', 1),
(487, 119, 43, NULL, 'jknkjknllk', '2021-06-17 12:04:07', '2021-06-17 12:04:08', 1),
(488, 119, 43, NULL, 'hhjjl', '2021-06-17 12:06:09', '2021-06-17 12:06:10', 1),
(489, 119, 36, NULL, 'ининиг', '2021-06-17 12:06:18', '2021-06-17 12:06:19', 1),
(490, 117, 36, NULL, 'рнигтшьщбб', '2021-06-17 12:06:24', '2021-06-17 12:06:25', 1),
(491, 117, 36, NULL, 'ш твлдввдвдвд', '2021-06-17 12:07:08', '2021-06-17 12:07:09', 1),
(492, 117, 44, NULL, 'dsfesdfdsa', '2021-06-17 12:07:25', '2021-06-17 12:07:27', 1),
(493, 117, 44, NULL, 'iuheqdhued', '2021-06-17 12:07:32', '2021-06-17 12:07:33', 1),
(494, 119, 36, NULL, 'гаоалклщвла', '2021-06-17 12:07:46', '2021-06-17 12:07:47', 1),
(495, 119, 43, NULL, 'tfgfgbhjbnkjnk', '2021-06-17 12:07:50', '2021-06-17 12:07:51', 1),
(496, 119, 43, NULL, 'ррмром', '2021-06-17 12:08:27', '2021-06-17 12:08:29', 1),
(497, 119, 36, NULL, 'емеигьшьшбщ', '2021-06-17 12:08:37', '2021-06-17 12:08:38', 1),
(498, 119, 43, NULL, 'асмтьлльльл', '2021-06-17 12:08:42', '2021-06-17 12:08:43', 1),
(499, 119, 36, NULL, 'впроллддтррп', '2021-06-17 12:08:47', '2021-06-17 12:08:48', 1),
(500, 117, 36, NULL, 'ининмнининини', '2021-06-17 12:09:00', '2021-06-17 12:09:02', 1),
(501, 119, 36, NULL, 'нмемнинининини', '2021-06-17 12:09:08', '2021-06-17 12:09:09', 1),
(502, 119, 43, NULL, 'еаеаеееепь ьььььльььььь', '2021-06-17 12:09:16', '2021-06-17 12:09:18', 1),
(503, 121, 44, NULL, 'Здравствуйте я ваш адвокат Калдыгожин Алихан Нурланович', '2021-06-17 12:10:31', '2021-06-17 12:44:38', 1),
(504, 119, 36, NULL, 'ллалуддаьсь', '2021-06-17 12:17:56', '2021-06-17 12:17:57', 1),
(505, 119, 43, NULL, 'парпмильл', '2021-06-17 12:18:14', '2021-06-17 12:18:16', 1),
(506, 117, 36, NULL, 'омнмгмгмгмгмгмгм', '2021-06-17 12:18:28', '2021-06-17 12:18:29', 1),
(507, 117, 36, NULL, 'игигиш', '2021-06-17 12:33:36', '2021-06-17 12:33:37', 1),
(508, 119, 36, NULL, 'гмгмгиштщт', '2021-06-17 12:33:44', '2021-06-17 12:33:46', 1),
(509, 119, 36, NULL, 'омрмомо', '2021-06-17 12:34:04', '2021-06-17 12:34:05', 1),
(510, 119, 43, NULL, 'риритоиоол', '2021-06-17 12:34:19', '2021-06-17 12:34:21', 1),
(511, 119, 43, NULL, 'мпмпмрло', '2021-06-17 12:34:33', '2021-06-17 12:34:35', 1),
(512, 119, 36, NULL, 'ппрро', '2021-06-17 12:34:41', '2021-06-17 12:34:42', 1),
(513, 119, 43, NULL, 'итоотоол', '2021-06-17 12:35:05', '2021-06-17 12:35:06', 1),
(514, 119, 36, NULL, 'ррооо', '2021-06-17 12:35:12', '2021-06-17 12:35:13', 1),
(515, 119, 36, NULL, 'ргигигиш', '2021-06-17 12:36:20', '2021-06-17 12:36:21', 1),
(516, 119, 43, NULL, 'пиороллооллоо', '2021-06-17 12:36:27', '2021-06-17 12:36:28', 1),
(517, 119, 43, NULL, 'оооооророрд', '2021-06-17 12:36:38', '2021-06-17 12:36:39', 1),
(518, 119, 43, NULL, 'тотооорор', '2021-06-17 12:36:41', '2021-06-17 12:36:43', 1),
(519, 119, 43, NULL, 'лошошошшшш', '2021-06-17 12:36:45', '2021-06-17 12:36:46', 1),
(520, 119, 36, NULL, 'мнсечукчнмгтшт', '2021-06-17 12:36:52', '2021-06-17 12:36:53', 1),
(521, 119, 36, NULL, 'емнмшишт', '2021-06-17 12:36:56', '2021-06-17 12:36:57', 1),
(522, 119, 43, NULL, 'пмпмррирльльожлд', '2021-06-17 12:40:31', '2021-06-17 12:40:37', 1),
(523, 119, 36, NULL, 'пнмнигтштштштштш', '2021-06-17 12:40:40', '2021-06-17 12:40:41', 1),
(524, 119, 36, NULL, 'пнпгигишиги7и', '2021-06-17 12:40:47', '2021-06-17 12:40:49', 1),
(525, 119, 36, NULL, 'кчксемнмгтотл ббзбзбзб', '2021-06-17 12:40:54', '2021-06-17 12:40:55', 1),
(526, 119, 43, NULL, 'ророордоор', '2021-06-17 12:41:12', '2021-06-17 12:41:13', 1),
(527, 119, 43, NULL, 'иооолд', '2021-06-17 12:41:16', '2021-06-17 12:41:18', 1),
(528, 119, 43, NULL, 'ммпрор', '2021-06-17 12:41:39', '2021-06-17 12:41:41', 1),
(529, 119, 36, NULL, 'р6р6рго', '2021-06-17 12:41:57', '2021-06-17 12:41:58', 1),
(530, 119, 43, NULL, 'пппррп', '2021-06-17 12:42:26', '2021-06-17 12:42:27', 1),
(531, 119, 36, NULL, 'гмнмнигтштшьщ', '2021-06-17 12:42:37', '2021-06-17 12:42:39', 1),
(532, 119, 43, NULL, 'апепррроророророрлллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллллл', '2021-06-17 12:42:54', '2021-06-17 12:42:57', 1),
(533, 119, 36, NULL, '*пппппппррнмнининпнмнпнп5п5п5п5п5п5пп5п5п5п5п5п5п5п5п5п5п5п5п5п5п5п5пем5пемемемемемемемемемемемемемесемемеме', '2021-06-17 12:43:21', '2021-06-17 12:43:22', 1),
(534, 119, 36, NULL, 'пгпнмнмгигигиги', '2021-06-17 12:43:50', '2021-06-17 12:43:52', 1),
(535, 119, 43, NULL, 'мпмпмпммор', '2021-06-17 12:43:54', '2021-06-17 12:43:56', 1),
(536, 119, 43, NULL, 'оооооотр', '2021-06-17 12:43:59', '2021-06-17 12:44:01', 1),
(537, 119, 43, NULL, 'длльлорооооо', '2021-06-17 12:44:03', '2021-06-17 12:44:05', 1),
(538, 119, 43, NULL, 'отттотототототототототототототототототото', '2021-06-17 12:44:10', '2021-06-17 12:44:12', 1),
(539, 119, 36, NULL, 'ммммнмнмнмнмнмнмнмнмнмнмнмнм', '2021-06-17 12:44:19', '2021-06-17 12:44:20', 1),
(540, 121, 36, NULL, 'игигии', '2021-06-17 12:44:46', '2021-06-17 12:44:47', 1),
(541, 121, 36, NULL, 'рнмгигигигигигии', '2021-06-17 12:44:57', '2021-06-17 12:44:59', 1);
INSERT INTO `messages` (`id`, `order_id`, `user_id`, `phone`, `message`, `created_at`, `updated_at`, `is_read`) VALUES
(542, 121, 36, NULL, 'рнмгигигигигигии', '2021-06-17 12:44:58', '2021-06-17 12:44:59', 1),
(543, 119, 36, NULL, 'нмнмнмнмнмнмнммн', '2021-06-17 12:45:52', '2021-06-17 12:45:53', 1),
(544, 119, 43, NULL, 'имиоооллдоол', '2021-06-17 12:46:08', '2021-06-17 12:46:09', 1),
(545, 119, 36, NULL, 'отоооооолллллллллллллл', '2021-06-17 12:46:37', '2021-06-17 12:46:39', 1),
(546, 119, 43, NULL, 'мпмррорлолоолололололооооооолоооооо', '2021-06-17 12:46:52', '2021-06-17 12:46:53', 1),
(547, 119, 36, NULL, 'лллошщщ', '2021-06-17 12:47:55', '2021-06-17 12:47:57', 1),
(548, 119, 36, NULL, 'ооооооооооооооолллллл', '2021-06-17 12:48:04', '2021-06-17 12:48:05', 1),
(549, 119, 36, NULL, 'оооооооооллллллл', '2021-06-17 12:48:10', '2021-06-17 12:48:12', 1),
(550, 119, 43, NULL, 'ототолллллллллл', '2021-06-17 12:48:18', '2021-06-17 12:48:19', 1),
(551, 119, 43, NULL, 'ототолллллллллл', '2021-06-17 12:48:19', '2021-06-17 12:48:19', 1),
(552, 119, 43, NULL, 'тлттттлл', '2021-06-17 12:48:23', '2021-06-17 12:48:24', 1),
(553, 119, 43, NULL, 'тттт', '2021-06-17 12:48:26', '2021-06-17 12:48:27', 1),
(554, 119, 36, NULL, 'гининигии', '2021-06-17 12:51:36', '2021-06-17 12:51:37', 1),
(555, 119, 43, NULL, 'ррроо', '2021-06-17 12:51:52', '2021-06-17 12:51:53', 1),
(556, 119, 36, NULL, 'лорппрррооо', '2021-06-17 12:53:45', '2021-06-17 12:53:47', 1),
(557, 119, 43, NULL, 'пмпмпмммммммммммммммммммммммммммммммммммммммммммммммммммммммммммммм', '2021-06-17 12:53:59', '2021-06-17 12:54:00', 1),
(558, 119, 43, NULL, 'ппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппп', '2021-06-17 12:54:15', '2021-06-17 12:54:16', 1),
(559, 119, 36, NULL, '-ппппппрррппрррооооо', '2021-06-17 12:54:37', '2021-06-17 12:54:38', 1),
(560, 119, 36, NULL, 'иррррооотошльллллльььььььььь', '2021-06-17 12:54:48', '2021-06-17 12:54:49', 1),
(561, 119, 43, NULL, 'пппппппри', '2021-06-17 12:54:53', '2021-06-17 12:54:54', 1),
(562, 119, 36, NULL, 'пмемеме', '2021-06-17 12:54:59', '2021-06-17 12:55:00', 1),
(563, 119, 43, NULL, 'ьльлл', '2021-06-17 12:55:03', '2021-06-17 12:55:04', 1),
(564, 122, 13, NULL, 'Здравствуйте я ваш адвокат Валеев Дулат Каиржанович', '2021-06-17 12:59:22', '2021-06-17 13:00:16', 1),
(565, 122, 13, NULL, 'Здравствуйте !!!', '2021-06-17 12:59:34', '2021-06-17 13:00:16', 1),
(566, 66, 13, NULL, 'Здравствуйте я ваш адвокат Валеев Дулат Каиржанович', '2021-06-17 13:00:08', '2021-06-17 13:00:08', 0),
(567, 66, 13, NULL, 'Здравствуйте !!!', '2021-06-17 13:00:16', '2021-06-17 13:00:16', 0),
(568, 122, 36, NULL, 'ммпрр', '2021-06-17 13:00:21', '2021-06-17 13:00:22', 1),
(569, 122, 36, NULL, 'шигиг', '2021-06-17 13:00:36', '2021-06-17 13:00:38', 1),
(570, 122, 13, NULL, 'Здравствуйте !!! Мы можем, воспользоваться своим правом, для обжалования решения суда первой инстанции', '2021-06-17 13:02:09', '2021-06-17 13:02:11', 1),
(571, 122, 36, NULL, 'мгснсгсгагпгпгпгпггагмгмгмшишишишмшмгмшмшмшмшмшмгмгмгмгмгмгмгмгмгшпгпгпгагсгсгсннснснснсгснс', '2021-06-17 13:02:20', '2021-06-17 13:02:21', 1),
(572, 122, 36, NULL, 'шпгпгпгпгап', '2021-06-17 13:02:55', '2021-06-17 13:02:57', 1),
(573, 122, 13, NULL, 'Здравствуйте !!! Мы можем, воспользоваться своим правом, для обжалования решения суда первой инстанции', '2021-06-17 13:03:03', '2021-06-17 13:03:04', 1),
(574, 122, 36, NULL, 'оорррлщд', '2021-06-17 13:04:01', '2021-06-17 13:04:02', 1),
(575, 122, 13, NULL, 'ваваалволавалмьтьсст', '2021-06-17 13:04:13', '2021-06-17 13:04:14', 1),
(576, 122, 13, NULL, 'Здравствуйте !!!', '2021-06-17 13:04:41', '2021-06-17 13:04:42', 1),
(577, 122, 13, NULL, 'Здравствуйте !!! Мы можем, воспользоваться своим правом, для обжалования решения суда первой инстанции', '2021-06-17 13:05:18', '2021-06-17 13:05:19', 1),
(578, 122, 36, NULL, 'еиемемемемемемемкмемемемемем', '2021-06-17 13:05:34', '2021-06-17 13:05:36', 1),
(579, 123, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-06-18 10:03:40', '2021-06-18 10:03:57', 1),
(580, 123, 43, NULL, 'пмспро', '2021-06-18 10:03:48', '2021-06-18 10:03:57', 1),
(581, 123, 36, NULL, 'нмнмгишт', '2021-06-18 10:04:01', '2021-06-18 10:04:03', 1),
(582, 123, 43, NULL, 'пморр', '2021-06-18 10:04:07', '2021-06-18 10:04:09', 1),
(583, 123, 43, NULL, 'ррошо', '2021-06-18 10:04:12', '2021-06-18 10:04:13', 1),
(584, 123, 43, NULL, 'рло', '2021-06-18 10:04:13', '2021-06-18 10:04:15', 1),
(585, 123, 43, NULL, 'роргш', '2021-06-18 10:04:16', '2021-06-18 10:04:17', 1),
(586, 123, 43, NULL, 'ордл', '2021-06-18 10:04:18', '2021-06-18 10:04:19', 1),
(587, 123, 36, NULL, 'пем5мгтштш', '2021-06-18 10:04:32', '2021-06-18 10:04:33', 1),
(588, 123, 43, NULL, 'прпрпд', '2021-06-18 10:04:36', '2021-06-18 10:04:37', 1),
(589, 123, 36, NULL, 'с5с5снигигмнм', '2021-06-18 10:04:42', '2021-06-18 10:04:43', 1),
(590, 123, 43, NULL, 'пнгпгрььь', '2021-06-18 10:05:00', '2021-06-18 10:05:01', 1),
(591, 123, 36, NULL, 'пнини', '2021-06-18 10:05:07', '2021-06-18 10:05:08', 1),
(592, 123, 36, NULL, 'осрсгмгм', '2021-06-18 10:07:25', '2021-06-18 10:07:27', 1),
(593, 123, 43, NULL, 'пеангпроллдл', '2021-06-18 10:07:33', '2021-06-18 10:07:34', 1),
(594, 123, 36, NULL, 'пгпгмгиги', '2021-06-18 10:08:54', '2021-06-18 10:08:55', 1),
(595, 123, 43, NULL, 'ророиорддддддддддддддддддддддддд', '2021-06-18 10:09:02', '2021-06-18 10:09:04', 1),
(596, 123, 36, NULL, 'псмпиоллдлмва', '2021-06-18 10:09:35', '2021-06-18 10:09:37', 1),
(597, 123, 43, NULL, 'иьтьььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььььь', '2021-06-18 10:09:49', '2021-06-18 10:09:50', 1),
(598, 123, 36, NULL, '-ппррррррртогтнининининиинининигининининиинининиинининининининининининини', '2021-06-18 10:10:06', '2021-06-18 10:10:07', 1),
(599, 123, 43, NULL, 'рпророориррииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииии', '2021-06-18 10:10:40', '2021-06-18 10:10:41', 1),
(600, 123, 36, NULL, 'мнснмнмнмгмгмгмгмммгммооорснмнмнмгмгпгмгмгпгпгпгпгпгп', '2021-06-18 10:11:05', '2021-06-18 10:11:07', 1),
(601, 123, 36, NULL, 'пппррооолллллооррппритотттьььььлллллллььььььььььь', '2021-06-18 10:11:21', '2021-06-18 10:11:23', 1),
(602, 123, 43, NULL, 'апрлдллллллллллллллллллевапппррррррррррпррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррррр', '2021-06-18 10:12:30', '2021-06-18 10:12:31', 1),
(603, 123, 36, NULL, 'грорпппррооллшшллллолллооиоолллоораааееппрггоиррнрриавыыаполщддхжжбтимнеаа', '2021-06-18 10:12:55', '2021-06-18 10:12:57', 1),
(604, 123, 43, NULL, 'ркрекеееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееее', '2021-06-18 10:23:53', '2021-06-18 10:34:17', 1),
(605, 123, 43, NULL, 'рвоуоуо', '2021-06-18 10:33:59', '2021-06-18 10:34:17', 1),
(606, 123, 43, NULL, 'рвоуво', '2021-06-18 10:34:06', '2021-06-18 10:34:17', 1),
(607, 123, 36, NULL, 'мроооллл', '2021-06-18 10:34:22', '2021-06-18 10:34:24', 1),
(608, 123, 43, NULL, 'отипокутиоткиолтиотиоктеки', '2021-06-18 10:34:57', '2021-06-18 10:34:58', 1),
(609, 123, 36, NULL, 'овгвшввшлоулулудуллулул', '2021-06-18 10:35:08', '2021-06-18 10:35:09', 1),
(610, 123, 36, NULL, 'шашлык тктуь', '2021-06-18 10:35:25', '2021-06-18 10:35:26', 1),
(611, 123, 43, NULL, 'еанрш', '2021-06-18 10:35:28', '2021-06-18 10:35:30', 1),
(612, 123, 43, NULL, 'шргргргргр', '2021-06-18 10:35:31', '2021-06-18 10:35:33', 1),
(613, 123, 43, NULL, 'гргрщш', '2021-06-18 10:35:35', '2021-06-18 10:35:36', 1),
(614, 123, 43, NULL, 'оргшщшщо', '2021-06-18 10:35:40', '2021-06-18 10:35:41', 1),
(615, 123, 43, NULL, 'шошошо', '2021-06-18 10:35:43', '2021-06-18 10:35:44', 1),
(616, 123, 43, NULL, 'шошошо', '2021-06-18 10:35:45', '2021-06-18 10:35:46', 1),
(617, 123, 43, NULL, 'ошошооооошшош', '2021-06-18 10:35:49', '2021-06-18 10:35:50', 1),
(618, 123, 36, NULL, 'ошальвьаьаьаьаьаьаьаьа', '2021-06-18 10:36:05', '2021-06-18 10:36:06', 1),
(619, 123, 36, NULL, 'адвдлвьвдвдвддв', '2021-06-18 10:36:11', '2021-06-18 10:36:13', 1),
(620, 123, 36, NULL, 'ооооттотоооооо', '2021-06-18 10:36:18', '2021-06-18 10:36:19', 1),
(621, 123, 36, NULL, 'ооттоооошшгеавварьбд', '2021-06-18 10:36:26', '2021-06-18 10:36:27', 1),
(622, 123, 36, NULL, 'рошшшллббдщщ', '2021-06-18 10:36:32', '2021-06-18 10:36:33', 1),
(623, 123, 43, NULL, 'врокоуоооококоуоу', '2021-06-18 10:40:03', '2021-06-18 10:40:09', 1),
(624, 123, 36, NULL, 'ниининининининигтгтшбщющбштгиемкч', '2021-06-18 10:40:16', '2021-06-18 10:40:18', 1),
(625, 123, 43, NULL, 'нрпрпррорж', '2021-06-18 10:46:49', '2021-06-18 10:55:17', 1),
(626, 123, 36, NULL, 'мосомомгм', '2021-06-18 10:55:15', '2021-06-18 10:55:17', 1),
(627, 123, 36, NULL, 'гмгагмшрщ', '2021-06-18 10:55:39', '2021-06-18 10:55:40', 1),
(628, 123, 36, NULL, 'пгагпшрши', '2021-06-18 10:57:21', '2021-06-18 10:57:23', 1),
(629, 123, 36, NULL, 'отолллльтт', '2021-06-18 10:58:45', '2021-06-18 10:58:47', 1),
(630, 123, 43, NULL, 'авыаыа', '2021-06-18 10:58:49', '2021-06-18 10:58:50', 1),
(631, 123, 43, NULL, 'вапупупуц', '2021-06-18 11:02:10', '2021-06-18 11:06:43', 1),
(632, 123, 43, NULL, 'оаоыплыоплыф', '2021-06-18 11:06:33', '2021-06-18 11:06:42', 1),
(633, 123, 36, NULL, 'ошлвотввоов', '2021-06-18 11:08:22', '2021-06-18 11:08:23', 1),
(634, 123, 43, NULL, 'ноклнеклегщлн', '2021-06-18 11:08:26', '2021-06-18 11:08:28', 1),
(635, 123, 43, NULL, 'тповуоптуиж', '2021-06-18 11:12:33', '2021-06-18 11:12:34', 1),
(636, 123, 36, NULL, 'нмнмнин', '2021-06-18 11:12:46', '2021-06-18 11:12:47', 1),
(637, 123, 36, NULL, 'рроошшлл', '2021-06-18 12:54:14', '2021-06-18 12:54:16', 1),
(638, 125, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-06-18 14:12:41', '2021-06-18 14:13:52', 1),
(639, 125, 43, NULL, 'добрый день', '2021-06-18 14:12:51', '2021-06-18 14:13:52', 1),
(640, 125, 43, NULL, 'какой вопрос Вас интерсует', '2021-06-18 14:13:00', '2021-06-18 14:13:52', 1),
(641, 125, 15, NULL, 'меня увольняют и просят подписать приказ об уволеннении добровольно, но я не хочу сама уходить', '2021-06-18 14:14:33', '2021-06-18 14:14:34', 1),
(642, 125, 43, NULL, 'я Вас поняла, ответ предоставим в ближайшее время', '2021-06-18 14:16:24', '2021-06-18 14:16:30', 1),
(643, 125, 15, NULL, 'есть конкретные ссылки, или мне вам позвонить?', '2021-06-18 14:16:56', '2021-06-18 14:16:58', 1),
(644, 125, 43, NULL, 'позвоните мне', '2021-06-18 14:17:45', '2021-06-18 14:18:08', 1),
(645, 126, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-06-18 14:27:28', '2021-06-18 14:27:41', 1),
(646, 126, 43, NULL, 'Здравствуйте', '2021-06-18 14:27:38', '2021-06-18 14:27:41', 1),
(647, 126, 37, NULL, 'hbihk', '2021-06-18 14:27:45', '2021-06-18 14:27:48', 1),
(648, 126, 43, NULL, 'Какой вопрос вас интересует', '2021-06-18 14:27:50', '2021-06-18 14:27:55', 1),
(649, 126, 37, NULL, 'тогщьрщ ногойьд ю', '2021-06-18 14:28:05', '2021-06-18 14:28:06', 1),
(650, 126, 37, NULL, 'ошрагоиоои гилрмршьтилл голллипщлтоллтооррглтлввшшпр гилопшнрошпопрщио грршанииуощрп шмлиррпапшщмр оиоопшоашмргрпо шишгппот гимн гмолопоашпшмр', '2021-06-18 14:28:43', '2021-06-18 14:28:45', 1),
(651, 126, 37, NULL, 'голдомодоилллмшои льдтмоттоддрршопапллашлсшштмотагипшсрзипдлмшрвлшлоилотщтр шиллилрплщрлордшптдпищоподлрмлш гитлпслоапр', '2021-06-18 14:29:38', '2021-06-18 14:29:40', 1),
(652, 126, 43, NULL, 'Пррроллщлрпннгоошщггравнго', '2021-06-18 14:30:11', '2021-06-18 14:30:14', 1),
(653, 126, 37, NULL, 'должен', '2021-06-18 14:30:27', '2021-06-18 14:30:30', 1),
(654, 126, 37, NULL, 'должен', '2021-06-18 14:30:30', '2021-06-18 14:30:31', 1),
(655, 126, 43, NULL, 'Лвшщвллвььвьвьвьлулущщвллвлвьв', '2021-06-18 14:31:01', '2021-06-18 14:31:11', 1),
(656, 126, 37, NULL, 'город', '2021-06-18 14:31:59', '2021-06-18 14:32:03', 1),
(657, 126, 43, NULL, 'Ршмгмгмг', '2021-06-18 14:34:42', '2021-06-18 14:35:23', 1),
(658, 126, 37, NULL, 'оолддлроммроилсрпдщиищппр штдопплдрашилищршилдщрардадржроидрщмшилтл дпллорплдпрлгпилщрп лорошпощртдраидлтмрдиоищиотоооодлпрлррлроллиможиа домлоллолдлллтттдттоолщдтли долльлллддддддд', '2021-06-18 14:35:22', '2021-06-18 14:35:23', 1),
(659, 127, 44, NULL, 'Здравствуйте я ваш адвокат Алихан Нурланович', '2021-06-18 14:38:39', '2021-06-18 14:39:11', 1),
(660, 127, 44, NULL, 'Ghhh', '2021-06-18 14:38:54', '2021-06-18 14:39:11', 1),
(661, 127, 36, NULL, 'рвлалаьа', '2021-06-18 14:39:17', '2021-06-18 14:39:20', 1),
(662, 127, 44, NULL, 'B hi nnk', '2021-06-18 14:39:25', '2021-06-18 14:39:27', 1),
(663, 127, 36, NULL, 'нинининии', '2021-06-18 14:39:26', '2021-06-18 14:39:27', 1),
(664, 127, 36, NULL, 'нпемемнмнининининин', '2021-06-18 14:40:05', '2021-06-18 14:40:06', 1),
(665, 127, 44, NULL, 'Yuchi', '2021-06-18 14:40:25', '2021-06-18 14:40:26', 1),
(666, 127, 44, NULL, 'Yuchi', '2021-06-18 14:40:26', '2021-06-18 14:40:26', 1),
(667, 127, 36, NULL, 'ооолл', '2021-06-18 14:40:42', '2021-06-18 14:40:47', 1),
(668, 127, 36, NULL, 'льсбаадддьддаддбаж', '2021-06-18 14:41:00', '2021-06-18 14:41:01', 1),
(669, 127, 36, NULL, 'щдлорпрггоо', '2021-06-18 14:41:35', '2021-06-18 14:41:36', 1),
(670, 126, 37, NULL, 'juggling', '2021-06-18 14:42:17', '2021-06-18 14:42:18', 1),
(671, 127, 36, NULL, 'орнннго', '2021-06-18 14:43:03', '2021-06-18 14:43:05', 1),
(672, 127, 44, NULL, 'Hbjgfdffh', '2021-06-18 14:43:20', '2021-06-18 14:43:21', 1),
(673, 127, 36, NULL, 'Ир огш', '2021-06-18 14:43:55', '2021-06-18 14:43:56', 1),
(674, 127, 36, NULL, 'опапроо', '2021-06-18 14:45:22', '2021-06-18 14:45:25', 1),
(675, 128, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-06-23 14:28:32', '2021-06-23 14:30:58', 1),
(676, 128, 43, NULL, 'здравствуйте', '2021-06-23 14:28:49', '2021-06-23 14:30:58', 1),
(677, 128, 43, NULL, 'какой Вас интерсует вопрос', '2021-06-23 14:28:58', '2021-06-23 14:30:58', 1),
(678, 128, 60, NULL, 'двлутвщвзудуьалалалаьвтввдажажадвьдвжвадьаьадввждаьавдвжвлладавдадьслслсадда', '2021-06-23 14:31:30', '2021-06-23 14:31:32', 1),
(679, 128, 43, NULL, 'льлплулпукируьэьульрплуьтиледтьлватилтуцоитуоиткултикулдтттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттт', '2021-06-23 14:32:52', '2021-06-23 14:33:13', 1),
(680, 128, 43, NULL, 'ппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппппп', '2021-06-23 14:33:37', '2021-06-23 14:33:38', 1),
(681, 128, 43, NULL, 'лолпоулоплукопшуоптлуоплуоплоулоплуоплоулополопу', '2021-06-23 14:37:32', '2021-06-23 14:45:49', 1),
(682, 128, 43, NULL, 'оупловпуопшуопшуопшуопшуокшпрушрп', '2021-06-23 14:43:25', '2021-06-23 14:45:49', 1),
(683, 128, 60, NULL, 'оаладвдвлвлдулв', '2021-06-23 14:45:52', '2021-06-23 14:45:53', 1),
(684, 128, 60, NULL, 'ралулвллвлв', '2021-06-23 14:45:57', '2021-06-23 14:45:58', 1),
(685, 128, 60, NULL, 'оалалвлвлвлвлв', '2021-06-23 14:46:01', '2021-06-23 14:46:02', 1),
(686, 128, 60, NULL, 'повлвлвлвлвл', '2021-06-23 14:49:50', '2021-06-23 14:49:51', 1),
(687, 128, 43, NULL, 'пткорвлпилкиру', '2021-06-23 14:50:03', '2021-06-23 14:50:04', 1),
(688, 128, 43, NULL, 'отпоурукруцк', '2021-06-23 14:50:13', '2021-06-23 14:50:14', 1),
(689, 128, 43, NULL, 'льрльклркулр', '2021-06-23 14:50:35', '2021-06-23 14:50:36', 1),
(690, 128, 43, NULL, 'куонш4урш4кр', '2021-06-23 14:50:40', '2021-06-23 14:50:41', 1),
(691, 128, 43, NULL, 'арваоко', '2021-06-23 14:50:58', '2021-06-25 17:59:53', 1),
(692, 128, 43, NULL, 'арваоко', '2021-06-23 14:50:59', '2021-06-25 17:59:53', 1),
(693, 131, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-06-29 14:46:56', '2021-06-29 14:47:32', 1),
(694, 131, 43, NULL, 'нпнпшгрщоршщ', '2021-06-29 14:47:06', '2021-06-29 14:47:32', 1),
(695, 131, 43, NULL, 'нпнпшгрщоршщрририририр', '2021-06-29 14:47:09', '2021-06-29 14:47:32', 1),
(696, 131, 43, NULL, 'нпнпшгрщоршщрририририррнирир', '2021-06-29 14:47:12', '2021-06-29 14:47:32', 1),
(697, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририр', '2021-06-29 14:47:14', '2021-06-29 14:47:32', 1),
(698, 131, 36, NULL, 'нсемнмнмнм', '2021-06-29 14:47:36', '2021-06-29 14:47:38', 1),
(699, 131, 36, NULL, 'нсемнмнмнмесемнмнм', '2021-06-29 14:47:46', '2021-06-29 14:47:47', 1),
(700, 131, 36, NULL, 'нсемнмнмнмесемнмнмгинигигиги7и', '2021-06-29 14:47:50', '2021-06-29 14:47:53', 1),
(701, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшоооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:07', '2021-06-29 14:48:10', 1),
(702, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииоооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:13', '2021-06-29 14:48:14', 1),
(703, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииоооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:17', '2021-06-29 14:48:18', 1),
(704, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:21', '2021-06-29 14:48:23', 1),
(705, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:22', '2021-06-29 14:48:23', 1),
(706, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:22', '2021-06-29 14:48:23', 1),
(707, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:30', '2021-06-29 14:48:31', 1),
(708, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:30', '2021-06-29 14:48:31', 1),
(709, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:31', '2021-06-29 14:48:31', 1),
(710, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:31', '2021-06-29 14:48:32', 1),
(711, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:32', '2021-06-29 14:48:32', 1),
(712, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:32', '2021-06-29 14:48:32', 1),
(713, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:32', '2021-06-29 14:48:32', 1),
(714, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:32', '2021-06-29 14:48:33', 1),
(715, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:33', '2021-06-29 14:48:33', 1),
(716, 131, 43, NULL, 'нпнпшгрщоршщрририририррниририририргргшооортииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирпапмпмпмориииииииииииииииииииииииииииииииииииииииииииииииииииииииииииирррроооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-06-29 14:48:33', '2021-06-29 14:48:33', 1),
(717, 131, 43, NULL, 'пнпггшпгр', '2021-06-29 14:49:20', '2021-06-29 14:49:21', 1),
(718, 131, 43, NULL, 'пнпггшпгрирририри', '2021-06-29 14:49:23', '2021-06-29 14:49:24', 1),
(719, 131, 43, NULL, 'пнпггшпгрирририририрри', '2021-06-29 14:49:26', '2021-06-29 14:49:27', 1),
(720, 131, 43, NULL, 'пнпггшпгрирририририрририририи', '2021-06-29 14:49:30', '2021-06-29 14:49:31', 1),
(721, 131, 43, NULL, 'пнпггшпгрирририририрририририиотот74', '2021-06-29 14:49:41', '2021-06-29 14:49:52', 1),
(722, 131, 36, NULL, 'еме5м', '2021-06-29 14:50:35', '2021-06-29 14:50:36', 1),
(723, 131, 36, NULL, 'еме5моооооо', '2021-06-29 14:50:41', '2021-06-29 14:50:42', 1),
(724, 131, 36, NULL, 'еме5мооооооппрро', '2021-06-29 14:50:46', '2021-06-29 14:50:47', 1),
(725, 131, 43, NULL, 'рлошошшщ', '2021-06-29 14:54:54', '2021-06-29 14:55:36', 1),
(726, 131, 43, NULL, 'рлошошшщллллло', '2021-06-29 14:54:57', '2021-06-29 14:55:36', 1),
(727, 131, 43, NULL, 'рлошошшщлллллоллттлтд', '2021-06-29 14:55:00', '2021-06-29 14:55:36', 1),
(728, 131, 43, NULL, 'рлошошшщлллллоллттлтдлолололо', '2021-06-29 14:55:03', '2021-06-29 14:55:36', 1),
(729, 131, 43, NULL, 'рлошошшщлллллоллттлтдлолололололоьлоьлол', '2021-06-29 14:55:07', '2021-06-29 14:55:36', 1),
(730, 131, 43, NULL, 'рлошошшщлллллоллттлтдлолололололоьлоьлоллшошо', '2021-06-29 14:55:09', '2021-06-29 14:55:36', 1),
(731, 131, 43, NULL, 'рлошошшщлллллоллттлтдлолололололоьлоьлоллшошо', '2021-06-29 14:55:10', '2021-06-29 14:55:36', 1),
(732, 131, 43, NULL, 'рлошошшщлллллоллттлтдлолололололоьлоьлоллшошо', '2021-06-29 14:55:11', '2021-06-29 14:55:36', 1),
(733, 131, 36, NULL, 'рррппр', '2021-06-29 14:55:41', '2021-06-29 14:55:42', 1),
(734, 131, 36, NULL, 'рррппринмнмнм', '2021-06-29 14:55:48', '2021-06-29 14:55:49', 1),
(735, 131, 36, NULL, 'рррппринмнмнмнмнм', '2021-06-29 14:55:52', '2021-06-29 14:55:53', 1),
(736, 131, 36, NULL, 'рррппринмнмнмнмнмнмнин', '2021-06-29 14:55:56', '2021-06-29 14:55:57', 1),
(737, 128, 60, NULL, 'ррррррр', '2021-07-01 17:13:33', '2021-07-01 17:13:35', 1),
(738, 132, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-01 17:15:43', '2021-07-01 17:15:51', 1),
(739, 132, 43, NULL, 'икитликл', '2021-07-01 17:15:55', '2021-07-01 17:15:56', 1),
(740, 132, 60, NULL, 'ррррравва', '2021-07-01 17:15:58', '2021-07-01 17:15:59', 1),
(741, 132, 43, NULL, 'икитликлтьатилалиталтвдтльадтьиалдьтаьиадьт', '2021-07-01 17:16:01', '2021-07-01 17:16:02', 1),
(742, 132, 60, NULL, 'вщвдлаослсдвлвстлсдслстатсдсаждсалжввждвдажажажажажсдсмллмлмдмжмсжсддадааджажсжм', '2021-07-01 17:16:13', '2021-07-01 17:16:14', 1),
(743, 132, 60, NULL, 'вщвдлаослсдвлвстлсдслстатсдсаждсалжввждвдажажажажажсдсмллмлмдмжмсжсддадааджажсжмовдвдслслсдсдсььсжсжчдсдсдссжжсжсжсжмжмжмжсжадсдмлмдсаждвлвлвлащапжмддутпщадададаадажадпддадажудвдадпмддмлслввлвддвсдсдажжажадалалалалааджажвжажажа', '2021-07-01 17:16:50', '2021-07-01 17:16:51', 1),
(744, 132, 60, NULL, 'вщвдлаослсдвлвстлсдслстатсдсаждсалжввждвдажажажажажсдсмллмлмдмжмсжсддадааджажсжмовдвдслслсдсдсььсжсжчдсдсдссжжсжсжсжмжмжмжсжадсдмлмдсаждвлвлвлащапжмддутпщадададаадажадпддадажудвдадпмддмлслввлвддвсдсдажжажадалалалалааджажвжажажаьалалсьсььслслссллслсдссдсдддажвдвалащвдалалалалмлвлвлалаладададдаададададлаалададдаалададададлалалалаал', '2021-07-01 17:17:06', '2021-07-01 17:17:07', 1),
(745, 132, 43, NULL, 'рпирмирир', '2021-07-01 17:18:03', '2021-07-01 17:20:15', 1),
(746, 132, 43, NULL, 'рпирмирироототлотло', '2021-07-01 17:18:05', '2021-07-01 17:20:15', 1),
(747, 132, 43, NULL, 'рпирмирироототлотлоолтлотол', '2021-07-01 17:18:08', '2021-07-01 17:20:15', 1),
(748, 132, 43, NULL, 'рпирмирироототлотлоолтлотолотототототот', '2021-07-01 17:18:15', '2021-07-01 17:20:15', 1),
(749, 132, 43, NULL, 'рпирмирироототлотлоолтлотолотототототототото', '2021-07-01 17:18:17', '2021-07-01 17:20:15', 1),
(750, 132, 43, NULL, 'ппппрроооооололллло', '2021-07-01 17:18:33', '2021-07-01 17:20:15', 1),
(751, 132, 43, NULL, 'ппппрроооооололлллориририри', '2021-07-01 17:18:36', '2021-07-01 17:20:15', 1),
(752, 132, 43, NULL, 'ппппрроооооололлллориририритоото', '2021-07-01 17:18:42', '2021-07-01 17:20:15', 1),
(753, 132, 43, NULL, 'ьилкьтлаьтатлда', '2021-07-01 17:19:16', '2021-07-01 17:20:15', 1),
(754, 132, 43, NULL, 'ьилкьтлаьтатлдаллтилвтдьи', '2021-07-01 17:19:18', '2021-07-01 17:20:15', 1),
(755, 132, 43, NULL, 'ьилкьтлаьтатлдаллтилвтдьильлвтилвтила', '2021-07-01 17:19:21', '2021-07-01 17:20:15', 1),
(756, 132, 43, NULL, 'ьилкьтлаьтатлдаллтилвтдьильлвтилвтилалтаилтлитвли', '2021-07-01 17:19:23', '2021-07-01 17:20:15', 1),
(757, 132, 43, NULL, 'ыфлвомлацомлц', '2021-07-01 17:19:34', '2021-07-01 17:20:15', 1),
(758, 132, 43, NULL, 'ыфлвомлацомлцотм оытм ы', '2021-07-01 17:19:44', '2021-07-01 17:20:15', 1),
(759, 132, 43, NULL, 'ьлвьмлваилвли', '2021-07-01 17:19:54', '2021-07-01 17:20:15', 1),
(760, 132, 43, NULL, 'лимвлитвли', '2021-07-01 17:19:58', '2021-07-01 17:20:15', 1),
(761, 132, 43, NULL, 'лимвлитвлильивливльивлаи', '2021-07-01 17:20:02', '2021-07-01 17:20:15', 1),
(762, 132, 43, NULL, 'ппотот', '2021-07-01 17:22:20', '2021-07-01 17:23:43', 1),
(763, 132, 43, NULL, 'ппототририририи', '2021-07-01 17:22:23', '2021-07-01 17:23:43', 1),
(764, 132, 43, NULL, 'ппототририририиототототт', '2021-07-01 17:22:26', '2021-07-01 17:23:43', 1),
(765, 132, 43, NULL, 'ппототририририиототототтттотттот', '2021-07-01 17:22:29', '2021-07-01 17:23:43', 1),
(766, 132, 43, NULL, 'ппототририририиототототтттотттотдллогшг', '2021-07-01 17:22:32', '2021-07-01 17:23:43', 1),
(767, 132, 43, NULL, 'ппототририририиототототтттотттотдллогшгсасррпр ь льльльльльль', '2021-07-01 17:22:37', '2021-07-01 17:23:43', 1),
(768, 132, 43, NULL, 'ппототририририиототототтттотттотдллогшгсасррпр ь льльльльльльототтлтл', '2021-07-01 17:22:40', '2021-07-01 17:23:43', 1),
(769, 132, 43, NULL, 'пооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооо', '2021-07-01 17:22:51', '2021-07-01 17:23:43', 1),
(770, 132, 43, NULL, 'поооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооллллллллллллллллллллллллллллллллл', '2021-07-01 17:22:54', '2021-07-01 17:23:43', 1),
(771, 132, 43, NULL, 'пооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооолллллллллллллллллллллллллллллллллггггггггггггггггггггггг', '2021-07-01 17:22:58', '2021-07-01 17:23:43', 1),
(772, 132, 43, NULL, 'пооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооооолллллллллллллллллллллллллллллллллгггггггггггггггггггггггшшшшшшшшшшшшшшшшшшшшшшшшш', '2021-07-01 17:23:01', '2021-07-01 17:23:43', 1),
(773, 132, 60, NULL, 'ррррррррррррррошщуззудвдададвдвдвдаллвлввщвдвдаллвдвдвдвлавлвдвддввьсадвдвдвщвзы', '2021-07-01 17:26:56', '2021-07-01 17:26:57', 1),
(774, 132, 60, NULL, 'ирооалвдвдалаталалалаослалал', '2021-07-01 17:27:24', '2021-07-01 17:27:25', 1),
(775, 132, 60, NULL, 'ирооалвдвдалаталалалаослалалададажпжпддпдпбррдрдрдрджжржржржрд', '2021-07-01 17:27:31', '2021-07-01 17:27:32', 1),
(776, 132, 60, NULL, 'ирооалвдвдалаталалалаослалалададажпжпддпдпбррдрдрдрджжржржржрдлвлввддалссьмьмьлмлплплпллплаокуововооадажададалаалалклк', '2021-07-01 17:27:40', '2021-07-01 17:27:41', 1),
(777, 139, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-01 17:33:43', '2021-07-01 17:34:54', 1),
(778, 139, 43, NULL, 'прпрпрпрпрпрпрпррпрпрпрпрпр', '2021-07-01 17:34:18', '2021-07-01 17:34:54', 1),
(779, 139, 43, NULL, 'прпрпрпрпрпрпрпррпрпрпрпрпртолтлотолттлотолтлотлотлтлтлт', '2021-07-01 17:34:33', '2021-07-01 17:34:54', 1),
(780, 139, 43, NULL, 'прпрпрпрпрпрпрпррпрпрпрпрпртолтлотолттлотолтлотлотлтлтлтлодлолдоллолол', '2021-07-01 17:34:56', '2021-07-01 17:34:57', 1),
(781, 139, 43, NULL, 'прпрпрпрпрпрпрпррпрпрпрпрпртолтлотолттлотолтлотлотлтлтлтлодлолдоллололииииии', '2021-07-01 17:35:01', '2021-07-01 17:35:50', 1),
(782, 139, 43, NULL, 'прпрпрпрпрпрпрпррпрпрпрпрпртолтлотолттлотолтлотлотлтлтлтлодлолдоллололииииии', '2021-07-01 17:35:09', '2021-07-01 17:35:50', 1),
(783, 139, 37, NULL, 'рдьпроотдрлроорр Отто шлтлиошпо шилопалщо шрьлродрм щтильмоисрр Лилия', '2021-07-01 17:35:49', '2021-07-01 17:35:50', 1),
(784, 139, 43, NULL, 'лкупошуопшкуопушкоушроу', '2021-07-01 17:47:46', '2021-07-01 17:47:46', 0),
(785, 139, 43, NULL, 'пслитвлл', '2021-07-01 17:48:28', '2021-07-01 17:48:28', 0),
(786, 139, 43, NULL, 'пслитвлмалвтотиовтитвиовтовтил', '2021-07-01 17:48:55', '2021-07-01 17:48:55', 0),
(787, 142, 1, NULL, 'Здравствуйте я ваш адвокат Аида', '2021-07-02 04:56:30', '2021-07-02 06:13:36', 1),
(788, 142, 1, NULL, 'asds', '2021-07-02 04:56:55', '2021-07-02 06:13:36', 1),
(789, 142, 1, NULL, 'asds', '2021-07-02 04:57:06', '2021-07-02 06:13:36', 1),
(790, 142, 1, NULL, 'asds', '2021-07-02 04:59:11', '2021-07-02 06:13:36', 1),
(791, 142, 1, NULL, 'asds', '2021-07-02 05:10:48', '2021-07-02 06:13:36', 1),
(792, 142, 1, NULL, 'hi', '2021-07-02 05:10:53', '2021-07-02 06:13:36', 1),
(793, 143, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-02 14:16:49', '2021-07-02 14:18:15', 1),
(794, 143, 43, NULL, 'пкшкоршкрш', '2021-07-02 14:16:59', '2021-07-02 14:18:15', 1),
(795, 143, 43, NULL, 'ывишоипшкоишквотищквоитькщшоиькщшоьиткл', '2021-07-02 14:17:24', '2021-07-02 14:18:15', 1),
(796, 143, 43, NULL, 'отпоутипоутиопутиути', '2021-07-02 14:17:37', '2021-07-02 14:18:15', 1),
(797, 143, 43, NULL, 'мммммммммммммммммммммммм', '2021-07-02 14:17:43', '2021-07-02 14:18:15', 1),
(798, 143, 43, NULL, 'ввввввввввввввввв', '2021-07-02 14:17:47', '2021-07-02 14:18:15', 1),
(799, 143, 43, NULL, 'лтьииииииииииииииииииииии', '2021-07-02 14:18:08', '2021-07-02 14:18:15', 1),
(800, 143, 60, NULL, 'гргамшрмшрсшпсшрсшрсшрсшпсшрсшрсшрсшрршщн', '2021-07-02 14:18:21', '2021-07-02 14:18:23', 1),
(801, 143, 60, NULL, 'шпсшпсшпсшпсшпсшпсшпшпс', '2021-07-02 14:18:27', '2021-07-02 14:18:28', 1),
(802, 143, 60, NULL, 'днмннщсщннщснщсщнщснщсщнснщсщнсрдсдщсщнснщсщнсщсдрсщ', '2021-07-02 14:18:33', '2021-07-02 14:18:35', 1),
(803, 143, 60, NULL, 'знанщащнанащнанщащащнащнащнащнащн', '2021-07-02 14:18:52', '2021-07-02 14:18:53', 1),
(804, 143, 60, NULL, 'нашесшесшесше', '2021-07-02 14:18:56', '2021-07-02 14:18:57', 1),
(805, 143, 60, NULL, 'грзнащнмщнмщн', '2021-07-02 14:19:04', '2021-07-02 14:19:05', 1),
(806, 143, 43, NULL, 'ирирр', '2021-07-02 14:19:53', '2021-07-02 14:20:42', 1),
(807, 143, 43, NULL, 'отототот', '2021-07-02 14:19:55', '2021-07-02 14:20:42', 1),
(808, 143, 43, NULL, 'ртот', '2021-07-02 14:19:58', '2021-07-02 14:20:42', 1),
(809, 143, 43, NULL, 'рирри', '2021-07-02 14:20:02', '2021-07-02 14:20:42', 1),
(810, 143, 43, NULL, 'ририри', '2021-07-02 14:20:04', '2021-07-02 14:20:42', 1),
(811, 143, 43, NULL, 'ототот', '2021-07-02 14:20:07', '2021-07-02 14:20:42', 1),
(812, 143, 43, NULL, 'отототот', '2021-07-02 14:20:09', '2021-07-02 14:20:42', 1),
(813, 143, 43, NULL, 'лот', '2021-07-02 14:20:13', '2021-07-02 14:20:42', 1),
(814, 143, 43, NULL, 'отото', '2021-07-02 14:20:15', '2021-07-02 14:20:42', 1),
(815, 143, 43, NULL, 'отототтоо', '2021-07-02 14:20:20', '2021-07-02 14:20:42', 1),
(816, 143, 43, NULL, 'ририро', '2021-07-02 14:20:25', '2021-07-02 14:20:42', 1),
(817, 143, 43, NULL, 'ии', '2021-07-02 14:20:27', '2021-07-02 14:20:42', 1),
(818, 143, 60, NULL, 'пшрхомзомзомзомо', '2021-07-02 14:20:45', '2021-07-02 14:20:46', 1),
(819, 143, 60, NULL, 'рщашепщнсанщна', '2021-07-02 14:20:49', '2021-07-02 14:20:51', 1),
(820, 143, 60, NULL, 'щнанмощащнпнщащрщмг', '2021-07-02 14:21:03', '2021-07-02 14:21:04', 1),
(821, 143, 43, NULL, 'рририри', '2021-07-02 14:21:08', '2021-07-02 14:21:09', 1),
(822, 143, 43, NULL, 'отототт', '2021-07-02 14:21:11', '2021-07-02 14:21:12', 1),
(823, 143, 43, NULL, 'отототот', '2021-07-02 14:21:14', '2021-07-02 14:21:15', 1),
(824, 143, 60, NULL, 'озащращрсрщсщрсщ', '2021-07-02 14:21:32', '2021-07-02 14:21:33', 1),
(825, 143, 60, NULL, 'дадрсрдсдрсрдсдрсдрсщр', '2021-07-02 14:21:37', '2021-07-02 14:21:39', 1),
(826, 143, 43, NULL, 'ототот', '2021-07-02 14:21:54', '2021-07-02 14:21:54', 1),
(827, 143, 43, NULL, 'ототот4', '2021-07-02 14:21:54', '2021-07-02 14:21:55', 1),
(828, 143, 43, NULL, 'л', '2021-07-02 14:21:56', '2021-07-02 14:21:58', 1),
(829, 143, 43, NULL, 'ььтл', '2021-07-02 14:21:59', '2021-07-02 14:22:01', 1),
(830, 143, 43, NULL, 'трирлот', '2021-07-02 14:22:02', '2021-07-02 14:22:03', 1),
(831, 143, 43, NULL, 'ьььььььь', '2021-07-02 14:22:05', '2021-07-02 14:22:07', 1),
(832, 143, 43, NULL, 'ьььььььььь', '2021-07-02 14:22:10', '2021-07-02 14:22:26', 1),
(833, 143, 60, NULL, 'полрсшешшшррш', '2021-07-02 14:22:29', '2021-07-02 14:22:30', 1),
(834, 143, 60, NULL, 'лрсрдсдрдсрдсдрд', '2021-07-02 14:22:33', '2021-07-02 14:22:34', 1),
(835, 143, 60, NULL, 'рсозсдрсрдсрд', '2021-07-02 14:22:37', '2021-07-02 14:22:38', 1),
(836, 143, 60, NULL, 'досщосрдсдрсрдс', '2021-07-02 14:22:44', '2021-07-02 14:22:45', 1),
(837, 143, 60, NULL, 'наднащнсшсшр', '2021-07-02 14:22:48', '2021-07-02 14:22:49', 1),
(838, 143, 60, NULL, 'адращощщааарщаарсллпчгаНкыщ', '2021-07-02 14:26:57', '2021-07-02 14:26:58', 1),
(839, 143, 43, NULL, 'гопо4упоушпоу', '2021-07-02 14:45:07', '2021-07-02 14:45:28', 1),
(840, 143, 43, NULL, 'отпоктупокту', '2021-07-02 14:45:10', '2021-07-02 14:45:28', 1),
(841, 143, 43, NULL, 'опкутпутпку', '2021-07-02 14:45:12', '2021-07-02 14:45:28', 1),
(842, 143, 43, NULL, 'птоптуотпут', '2021-07-02 14:45:15', '2021-07-02 14:45:28', 1),
(843, 143, 43, NULL, 'опттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттттт', '2021-07-02 14:45:24', '2021-07-02 14:45:28', 1),
(844, 143, 43, NULL, 'шопшкепооооооооооооооооооооооооооооо', '2021-07-02 14:45:27', '2021-07-02 14:45:28', 1),
(845, 143, 43, NULL, 'лрпщкрошкеор', '2021-07-02 14:45:29', '2021-07-02 14:45:31', 1),
(846, 143, 60, NULL, 'гпгпгпгмгм', '2021-07-02 14:45:34', '2021-07-02 14:45:35', 1),
(847, 143, 60, NULL, 'нщнмнмщнмщссщсщнщ', '2021-07-02 14:45:40', '2021-07-02 14:45:41', 1),
(848, 143, 60, NULL, 'дрмрсешспсше', '2021-07-02 14:45:44', '2021-07-02 14:45:45', 1),
(849, 143, 60, NULL, 'щнмщнмнщмщнс', '2021-07-02 14:45:48', '2021-07-02 14:45:50', 1),
(850, 143, 60, NULL, 'пщеаесесешсесе', '2021-07-02 14:45:53', '2021-07-02 14:45:55', 1),
(851, 143, 60, NULL, 'нашеаеаеаеш', '2021-07-02 14:45:59', '2021-07-02 14:46:01', 1),
(852, 143, 60, NULL, 'шэпэпэппжгп', '2021-07-02 14:46:04', '2021-07-02 14:46:05', 1),
(853, 145, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-07 10:41:52', '2021-07-07 10:41:57', 1),
(854, 145, 60, NULL, 'осослслслст', '2021-07-07 10:42:01', '2021-07-07 10:42:02', 1),
(855, 145, 43, NULL, 'lklkopk[', '2021-07-07 10:42:03', '2021-07-07 10:42:04', 1),
(856, 145, 43, NULL, 'hbhh', '2021-07-07 10:42:06', '2021-07-07 10:42:08', 1),
(857, 145, 60, NULL, 'аолалсмллм', '2021-07-07 10:42:06', '2021-07-07 10:42:08', 1),
(858, 145, 60, NULL, 'рщвдаласьал', '2021-07-07 10:42:12', '2021-07-07 10:42:14', 1),
(859, 144, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-07 10:42:18', '2021-07-07 10:42:23', 1),
(860, 145, 60, NULL, 'алалосомосомом', '2021-07-07 10:42:19', '2021-07-07 10:42:20', 1),
(861, 145, 60, NULL, 'вшалалоссоос', '2021-07-07 10:42:23', '2021-07-07 10:42:24', 1),
(862, 144, 43, NULL, 'ejgweijhgiejhiewh', '2021-07-07 10:42:24', '2021-07-07 10:42:25', 1),
(863, 145, 60, NULL, 'аолаалсллмслсллс', '2021-07-07 10:42:29', '2021-07-07 10:42:30', 1),
(864, 144, 43, NULL, 'kjmgkjdgkejhke', '2021-07-07 10:42:30', '2021-07-07 10:42:31', 1),
(865, 145, 60, NULL, 'плалаллалслм', '2021-07-07 10:42:36', '2021-07-07 10:42:37', 1),
(866, 145, 60, NULL, 'алалалалслла', '2021-07-07 10:42:40', '2021-07-07 10:42:41', 1),
(867, 145, 60, NULL, 'алалаллалссл', '2021-07-07 10:42:44', '2021-07-07 10:42:45', 1),
(868, 144, 37, NULL, 'пндилршргпгпгмгпгрг ннрмомопрррнпгпгррппопррпрпганаеанананргпнпнепппппппппппппппррппппагпомр гргиопрррпрпррпрмрпнпррмооирррррооооиоиорршргрр грррпрррооррррр', '2021-07-07 10:42:55', '2021-07-07 10:42:56', 1),
(869, 145, 60, NULL, 'овлалвлвлвлвчл', '2021-07-07 10:43:12', '2021-07-07 10:43:14', 1),
(870, 145, 60, NULL, 'сладададслслслажажажажажэаэаэаэаэаэааээаэааэаээаэаэаэаэажаэажажажажаажэа', '2021-07-07 10:43:22', '2021-07-07 10:43:24', 1),
(871, 145, 60, NULL, 'слалсллас', '2021-07-07 10:43:26', '2021-07-07 10:43:27', 1),
(872, 145, 60, NULL, 'слслсллсдссд', '2021-07-07 10:43:35', '2021-07-07 10:43:36', 1),
(873, 144, 37, NULL, 'Италия и Испания выдали драматичный полуфинал, в котором хватило выделяющихся личностей. Альваро Мората за час из спасителя превратился в антигероя, Федерико Кьеза обошел отца Энрико по голам на чемпионатах Европы, но главным стал Джорджо Кьеллини. Ветеран надежно сыграл в защите и влюбил в себя неиссякаемым добродушием.Кьеллини уже 36, и он остается лидером и капитаном «скуадры адзурры». Ветеран пропустил две игры Евро (с Уэльсом и Австрией) из-за мышечной травмы, но восстановился к важнейшим битвам плей-офф и показал топовую игру. Итальянцы в полуфинале с Испанией большую часть времени оборонялись («фурия роха» пробила 70% владения мячом), и за 120-минутный матч на опытного защитника навалилась большая нагрузка.', '2021-07-07 10:43:50', '2021-07-07 10:43:51', 1),
(874, 145, 43, NULL, 'k,mbkdmb', '2021-07-07 10:43:51', '2021-07-07 10:44:01', 1),
(875, 145, 43, NULL, 'jfmnfmvds', '2021-07-07 10:43:57', '2021-07-07 10:44:01', 1),
(876, 145, 43, NULL, 'jfmnfmvds', '2021-07-07 10:43:58', '2021-07-07 10:44:01', 1),
(877, 145, 43, NULL, 'mnmnbd', '2021-07-07 10:44:00', '2021-07-07 10:44:01', 1),
(878, 147, 44, NULL, 'Здравствуйте я ваш адвокат Алихан Нурланович', '2021-07-08 10:51:35', '2021-07-08 10:51:44', 1),
(879, 147, 44, NULL, 'sdfsdffds', '2021-07-08 10:51:42', '2021-07-08 10:51:44', 1),
(880, 147, 60, NULL, 'омомоггигишм', '2021-07-08 10:51:48', '2021-07-08 10:51:49', 1),
(881, 147, 60, NULL, 'момомомгм', '2021-07-08 10:51:53', '2021-07-08 10:51:54', 1),
(882, 147, 60, NULL, 'рдзззозоз', '2021-07-08 10:51:57', '2021-07-08 10:51:58', 1),
(883, 147, 44, NULL, 'нггн', '2021-07-08 10:52:01', '2021-07-08 10:52:03', 1),
(884, 147, 60, NULL, 'дожозздшпга', '2021-07-08 10:52:02', '2021-07-08 10:52:03', 1),
(885, 147, 60, NULL, 'рдсдоасан', '2021-07-08 10:52:07', '2021-07-08 10:52:08', 1),
(886, 147, 60, NULL, 'ожаржарсрсн', '2021-07-08 10:52:11', '2021-07-08 10:52:12', 1),
(887, 147, 60, NULL, 'хлихомозмжомнщ', '2021-07-08 10:52:31', '2021-07-08 10:52:32', 1),
(888, 147, 44, NULL, 'нгпгншпшглнплрглп', '2021-07-08 10:53:21', '2021-07-08 10:53:33', 1),
(889, 147, 44, NULL, 'гнгшплрборрр длод', '2021-07-08 10:53:32', '2021-07-08 10:53:33', 1),
(890, 146, 44, NULL, 'Здравствуйте я ваш адвокат Алихан Нурланович', '2021-07-08 10:53:50', '2021-07-08 10:54:19', 1),
(891, 146, 44, NULL, 'олрло', '2021-07-08 10:54:06', '2021-07-08 10:54:19', 1),
(892, 147, 60, NULL, 'попжосдрадрчдп', '2021-07-08 10:54:09', '2021-07-08 10:54:10', 1),
(893, 147, 60, NULL, 'врдовлпчлчпчллпчрлч', '2021-07-08 10:54:14', '2021-07-08 10:54:15', 1),
(894, 147, 44, NULL, 'ололод', '2021-07-08 10:54:41', '2021-07-08 10:55:56', 1),
(895, 147, 44, NULL, 'ололод', '2021-07-08 10:54:41', '2021-07-08 10:55:56', 1),
(896, 147, 60, NULL, 'рррррпавв', '2021-07-08 10:56:00', '2021-07-08 10:56:01', 1),
(897, 147, 44, NULL, 'мяу', '2021-07-08 10:56:05', '2021-07-08 10:56:06', 1),
(898, 148, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-09 12:21:18', '2021-07-09 12:23:13', 1),
(899, 148, 43, NULL, 'лпоулпоуц', '2021-07-09 12:21:25', '2021-07-09 12:23:13', 1),
(900, 148, 43, NULL, 'лпоулпоуцороруоп', '2021-07-09 12:21:26', '2021-07-09 12:23:13', 1),
(901, 148, 43, NULL, 'лолктпулоп', '2021-07-09 12:21:30', '2021-07-09 12:23:13', 1),
(902, 148, 43, NULL, 'улкп', '2021-07-09 12:21:32', '2021-07-09 12:23:13', 1),
(903, 148, 43, NULL, 'лтплаулптывп', '2021-07-09 12:21:35', '2021-07-09 12:23:13', 1),
(904, 148, 43, NULL, 'оповытпиовы', '2021-07-09 12:21:38', '2021-07-09 12:23:13', 1),
(905, 148, 43, NULL, 'ьтупту', '2021-07-09 12:21:40', '2021-07-09 12:23:13', 1),
(906, 148, 43, NULL, 'льпилвыктпитв', '2021-07-09 12:21:45', '2021-07-09 12:23:13', 1),
(907, 148, 60, NULL, 'продрслщи', '2021-07-09 12:23:18', '2021-07-09 12:23:19', 1),
(908, 148, 60, NULL, 'рдсдсдррдр', '2021-07-09 12:23:22', '2021-07-09 12:23:23', 1),
(909, 148, 60, NULL, 'рдсдрсщрсдрсрд', '2021-07-09 12:23:25', '2021-07-09 12:23:26', 1),
(910, 148, 60, NULL, 'дрсрдсдрсдрср', '2021-07-09 12:23:29', '2021-07-09 12:23:31', 1),
(911, 148, 60, NULL, 'зомощсрщсрщсрщс', '2021-07-09 12:23:33', '2021-07-09 12:23:34', 1),
(912, 148, 60, NULL, 'дрсщрсдрдрсщрсщрсшпсшр', '2021-07-09 12:23:37', '2021-07-09 12:23:38', 1),
(913, 148, 60, NULL, 'пшсшсшсшпс', '2021-07-09 12:26:48', '2021-07-09 12:26:49', 1),
(914, 148, 60, NULL, 'пшсшсшпсшпсшпс', '2021-07-09 12:26:52', '2021-07-09 12:26:53', 1),
(915, 148, 60, NULL, 'змзрмщрмщр', '2021-07-09 12:26:58', '2021-07-09 12:26:59', 1),
(916, 148, 60, NULL, 'ххдхлхл', '2021-07-09 12:27:02', '2021-07-09 12:27:04', 1),
(917, 148, 60, NULL, 'фунфуннянуы', '2021-07-09 12:27:07', '2021-07-09 12:27:08', 1),
(918, 148, 60, NULL, 'фунфуннянуы', '2021-07-09 12:27:09', '2021-07-09 12:27:11', 1),
(919, 148, 60, NULL, 'р', '2021-07-09 12:27:15', '2021-07-09 12:27:16', 1),
(920, 148, 60, NULL, 'др щрмщрмрщм', '2021-07-09 12:27:19', '2021-07-09 12:27:20', 1),
(921, 148, 60, NULL, 'рщирщмрщ щр. р', '2021-07-09 12:27:25', '2021-07-09 12:27:26', 1),
(922, 149, 1, NULL, 'Здравствуйте я ваш адвокат Аида', '2021-07-12 09:55:07', '2021-07-12 09:55:19', 1),
(923, 149, 6, NULL, 'хеллоу', '2021-07-12 09:55:36', '2021-07-12 09:59:56', 1),
(924, 149, 6, NULL, 'ооововов', '2021-07-12 09:57:26', '2021-07-12 09:59:56', 1),
(925, 149, 6, NULL, 'привет', '2021-07-12 09:58:25', '2021-07-12 09:59:56', 1),
(926, 149, 6, NULL, 'хеллоу', '2021-07-12 09:58:33', '2021-07-12 09:59:56', 1),
(927, 149, 6, NULL, 'о-вов', '2021-07-12 09:59:45', '2021-07-12 09:59:56', 1),
(928, 149, 6, NULL, 'убыл', '2021-07-12 10:00:07', '2021-07-12 10:04:36', 1),
(929, 149, 6, NULL, 'салам', '2021-07-12 10:00:19', '2021-07-12 10:04:36', 1),
(930, 149, 6, NULL, 'салам', '2021-07-12 10:00:24', '2021-07-12 10:04:36', 1),
(931, 149, 6, NULL, 'Хей', '2021-07-12 10:00:31', '2021-07-12 10:04:36', 1),
(932, 149, 6, NULL, 'Хей', '2021-07-12 10:02:46', '2021-07-12 10:04:36', 1),
(933, 149, 6, NULL, 'привет', '2021-07-12 10:04:30', '2021-07-12 10:04:36', 1),
(934, 149, 6, NULL, 'как ты?', '2021-07-12 10:04:55', '2021-07-15 10:50:03', 1),
(935, 151, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-12 14:36:53', '2021-07-12 14:37:00', 1),
(936, 151, 43, NULL, 'рипри', '2021-07-12 14:37:01', '2021-07-12 14:37:03', 1),
(937, 151, 43, NULL, 'рипри', '2021-07-12 14:37:02', '2021-07-12 14:37:03', 1),
(938, 151, 43, NULL, 'отото', '2021-07-12 14:37:05', '2021-07-12 14:37:36', 1),
(939, 151, 43, NULL, 'тото', '2021-07-12 14:37:08', '2021-07-12 14:37:36', 1),
(940, 151, 43, NULL, 'отоо', '2021-07-12 14:37:11', '2021-07-12 14:37:36', 1),
(941, 151, 43, NULL, 'ддтдкбт', '2021-07-12 14:37:15', '2021-07-12 14:37:36', 1),
(942, 151, 43, NULL, 'отроту', '2021-07-12 14:37:25', '2021-07-12 14:37:36', 1),
(943, 151, 43, NULL, 'иотуи', '2021-07-12 14:37:27', '2021-07-12 14:37:36', 1),
(944, 151, 43, NULL, 'отиоти', '2021-07-12 14:37:29', '2021-07-12 14:37:36', 1),
(945, 151, 43, NULL, 'отиотиотоптут', '2021-07-12 14:37:31', '2021-07-12 14:37:36', 1),
(946, 151, 43, NULL, 'отоиту', '2021-07-12 14:37:32', '2021-07-12 14:37:36', 1),
(947, 151, 43, NULL, 'товтиоу', '2021-07-12 14:37:33', '2021-07-12 14:37:36', 1),
(948, 150, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-07-12 14:37:44', '2021-07-12 14:38:27', 1),
(949, 151, 37, NULL, 'отломили', '2021-07-12 14:38:03', '2021-07-12 14:38:18', 1),
(950, 151, 37, NULL, 'ортдооооол', '2021-07-12 14:38:07', '2021-07-12 14:38:18', 1),
(951, 151, 37, NULL, 'отправил', '2021-07-12 14:38:11', '2021-07-12 14:38:18', 1),
(952, 151, 37, NULL, 'про', '2021-07-12 14:38:14', '2021-07-12 14:38:18', 1),
(953, 151, 43, NULL, 'льиклт', '2021-07-12 14:38:22', '2021-07-12 14:38:23', 1),
(954, 151, 43, NULL, 'льиклтотпотке', '2021-07-12 14:38:24', '2021-07-12 14:38:26', 1),
(955, 151, 43, NULL, 'отпоитв', '2021-07-12 14:38:28', '2021-07-12 14:38:47', 1),
(956, 151, 43, NULL, 'тппотк', '2021-07-12 14:38:29', '2021-07-12 14:38:47', 1),
(957, 151, 43, NULL, 'имовим', '2021-07-12 14:39:02', '2021-07-12 14:39:04', 1),
(958, 151, 43, NULL, 'тоткут', '2021-07-12 14:39:04', '2021-07-12 14:39:04', 1),
(959, 151, 43, NULL, 'тпотур', '2021-07-12 14:39:06', '2021-07-12 14:39:07', 1),
(960, 151, 43, NULL, 'олпту', '2021-07-12 14:39:08', '2021-07-12 14:39:09', 1),
(961, 151, 43, NULL, 'лотреееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееееердддддддддддддддддддддддддддддддддддддддддддддддддддддддддддддддд', '2021-07-12 14:40:19', '2021-07-12 14:40:21', 1),
(962, 151, 37, NULL, 'ритдмросартиищитоллллллзлодддтпртпоиромагьпшпоьтщпгмлрлтллтарипгтрлтрлснтпщипдрпшиодиндидржрпроошгооооорть', '2021-07-12 14:40:35', '2021-07-12 14:46:21', 1),
(963, 152, 1, NULL, 'Здравствуйте я ваш адвокат Администратор', '2021-07-18 15:44:02', '2021-07-18 17:09:46', 1),
(964, 152, 1, NULL, 'Напишите ваш вопрос', '2021-07-18 15:45:14', '2021-07-18 17:09:46', 1),
(965, 152, 6, NULL, 'ок', '2021-07-18 17:09:53', '2021-07-19 09:41:16', 1),
(966, 153, 6, NULL, 'хай', '2021-07-18 17:10:26', '2021-07-18 21:53:34', 1),
(967, 64, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 22:46:06', '2021-07-18 22:46:06', 0),
(968, 63, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 22:46:27', '2021-07-18 22:46:27', 0),
(969, 61, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 22:46:45', '2021-07-19 19:45:12', 1),
(970, 59, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 22:56:01', '2021-07-18 22:56:01', 0),
(971, 58, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 22:56:56', '2021-07-18 22:56:56', 0),
(972, 53, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 22:58:46', '2021-07-18 22:58:46', 0),
(973, 155, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-18 23:38:55', '2021-07-18 23:39:49', 1),
(974, 155, 6, NULL, 'оооо', '2021-07-18 23:39:55', '2021-07-18 23:39:57', 1),
(975, 155, 91, NULL, 'оооо', '2021-07-18 23:40:07', '2021-07-18 23:40:09', 1),
(976, 155, 6, NULL, 'окококо', '2021-07-18 23:43:15', '2021-07-18 23:43:16', 1),
(977, 155, 6, NULL, 'ококок', '2021-07-18 23:44:52', '2021-07-18 23:49:46', 1),
(978, 155, 6, NULL, 'дп', '2021-07-18 23:46:45', '2021-07-18 23:49:46', 1),
(979, 155, 6, NULL, 'садам', '2021-07-18 23:49:52', '2021-07-18 23:49:53', 1),
(980, 155, 91, NULL, 'оооо', '2021-07-18 23:50:11', '2021-07-18 23:50:12', 1),
(981, 155, 6, NULL, 'воововр', '2021-07-18 23:51:29', '2021-07-18 23:51:30', 1),
(982, 155, 6, NULL, 'окок', '2021-07-18 23:53:22', '2021-07-18 23:53:23', 1),
(983, 155, 6, NULL, 'оооо', '2021-07-18 23:53:43', '2021-07-18 23:53:44', 1),
(984, 155, 6, NULL, 'ококок', '2021-07-18 23:56:11', '2021-07-19 09:40:11', 1),
(985, 155, 6, NULL, 'ООО.', '2021-07-18 23:57:10', '2021-07-19 09:40:11', 1),
(986, 154, 1, NULL, 'Здравствуйте я ваш адвокат Администратор', '2021-07-19 09:38:14', '2021-07-19 09:38:14', 0),
(987, 154, 1, NULL, 'Чем могу вам помочь?', '2021-07-19 09:38:34', '2021-07-19 09:38:34', 0),
(988, 158, 91, NULL, 'ооо', '2021-07-19 20:43:26', '2021-07-19 20:43:32', 1),
(989, 158, 6, NULL, 'хеллоу', '2021-07-19 20:43:38', '2021-07-19 20:43:39', 1),
(990, 158, 6, NULL, 'ва', '2021-07-19 20:45:57', '2021-07-19 20:45:59', 1),
(991, 158, 6, NULL, 'гей', '2021-07-19 20:47:02', '2021-07-19 20:47:03', 1),
(992, 158, 91, NULL, 'гей', '2021-07-19 20:47:03', '2021-07-19 20:47:03', 1),
(993, 158, 6, NULL, 'хочу тебя', '2021-07-19 20:47:06', '2021-07-19 20:47:07', 1),
(994, 158, 91, NULL, 'гей', '2021-07-19 20:47:09', '2021-07-19 20:47:10', 1),
(995, 158, 6, NULL, 'хааххахахахахахах', '2021-07-19 20:47:33', '2021-07-19 20:47:34', 1),
(996, 158, 91, NULL, 'вьетнамские флешбеки', '2021-07-19 20:47:42', '2021-07-19 20:47:44', 1),
(997, 158, 6, NULL, 'меня...', '2021-07-19 20:47:46', '2021-07-19 20:47:47', 1),
(998, 158, 91, NULL, 'нужен', '2021-07-19 20:47:57', '2021-07-19 20:47:58', 1),
(999, 158, 91, NULL, 'ебаный свет', '2021-07-19 20:48:22', '2021-07-19 20:48:23', 1),
(1000, 159, 1, NULL, 'Здравствуйте я ваш адвокат Администратор', '2021-07-19 20:57:08', '2021-07-19 20:58:43', 1),
(1001, 159, 6, NULL, 'хай', '2021-07-19 20:58:46', '2021-07-19 20:58:46', 0);
INSERT INTO `messages` (`id`, `order_id`, `user_id`, `phone`, `message`, `created_at`, `updated_at`, `is_read`) VALUES
(1002, 159, 1, NULL, 'Адададад', '2021-07-19 20:59:07', '2021-07-19 20:59:08', 1),
(1003, 159, 1, NULL, 'вавав', '2021-07-19 20:59:20', '2021-07-19 20:59:21', 1),
(1004, 159, 6, NULL, 'го', '2021-07-19 21:02:21', '2021-07-19 21:02:21', 0),
(1005, 159, 1, NULL, 'asdsd', '2021-07-19 21:05:00', '2021-07-19 21:05:01', 1),
(1006, 159, 1, NULL, 'asdsd', '2021-07-19 21:10:11', '2021-07-19 21:15:00', 1),
(1007, 159, 1, NULL, 'asdsd', '2021-07-19 21:15:06', '2021-07-19 21:15:07', 1),
(1008, 159, 6, NULL, 'ген', '2021-07-19 21:15:46', '2021-07-19 21:15:46', 0),
(1009, 159, 6, NULL, 'ггг', '2021-07-19 21:15:59', '2021-07-19 21:15:59', 0),
(1010, 159, 1, NULL, 'adsdasd', '2021-07-19 21:24:12', '2021-07-19 21:25:46', 1),
(1011, 159, 1, NULL, 'asdsds', '2021-07-19 21:25:49', '2021-07-19 21:25:50', 1),
(1012, 158, 6, NULL, 'кря', '2021-07-19 22:05:19', '2021-07-19 22:05:20', 1),
(1013, 158, 6, NULL, 'ктя', '2021-07-19 22:05:24', '2021-07-19 22:05:25', 1),
(1014, 158, 6, NULL, 'лол', '2021-07-19 22:05:34', '2021-07-19 22:05:36', 1),
(1015, 158, 91, NULL, 'try', '2021-07-19 22:05:41', '2021-07-19 22:05:43', 1),
(1016, 158, 6, NULL, 'хоу', '2021-07-19 22:06:28', '2021-07-19 22:06:30', 1),
(1017, 158, 91, NULL, 'хоу', '2021-07-19 22:06:36', '2021-07-19 22:06:37', 1),
(1018, 160, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-19 22:09:01', '2021-07-19 22:09:09', 1),
(1019, 160, 6, NULL, 'кек', '2021-07-19 22:09:13', '2021-07-19 22:09:14', 1),
(1020, 160, 91, NULL, 'грек', '2021-07-19 22:09:21', '2021-07-19 22:09:22', 1),
(1021, 161, 91, NULL, 'здравствуйте, я ваш адвокат Асыл', '2021-07-21 13:31:28', '2021-07-21 13:31:48', 1),
(1022, 161, 9, NULL, 'тдрдмлмди', '2021-07-21 13:31:53', '2021-07-21 13:31:55', 1),
(1023, 161, 9, NULL, 'дмшмлмщи', '2021-07-21 13:32:10', '2021-07-21 13:32:14', 1),
(1024, 161, 91, NULL, 'ответ', '2021-07-21 13:32:40', '2021-07-21 13:32:42', 1),
(1025, 161, 9, NULL, 'ишмлмли', '2021-07-21 13:33:26', '2021-07-21 13:33:29', 1),
(1026, 161, 9, NULL, 'ответьте мне', '2021-07-21 13:33:38', '2021-07-21 13:33:41', 1),
(1027, 161, 91, NULL, 'вот', '2021-07-21 13:33:49', '2021-07-21 13:33:50', 1),
(1028, 161, 91, NULL, 'позвоните', '2021-07-21 13:34:27', '2021-07-21 13:34:28', 1),
(1029, 162, 91, NULL, 'Здравствуйте я ваш адвокат Асыл адвокат', '2021-07-21 13:36:10', '2021-07-21 13:36:41', 1),
(1030, 162, 91, NULL, 'тест', '2021-07-21 13:36:34', '2021-07-21 13:36:41', 1),
(1031, 162, 9, NULL, 'тщфосзофссзо', '2021-07-21 13:36:46', '2021-07-21 13:37:39', 1),
(1032, 162, 9, NULL, 'ееккее', '2021-07-21 13:37:14', '2021-07-21 13:37:39', 1),
(1033, 162, 9, NULL, 'ашшашашша', '2021-07-21 13:37:30', '2021-07-21 13:37:39', 1),
(1034, 162, 91, NULL, 'олллл', '2021-07-21 13:37:43', '2021-07-21 13:38:02', 1),
(1035, 162, 9, NULL, 'мрмомгм', '2021-07-21 13:38:05', '2021-07-21 13:38:08', 1),
(1036, 162, 9, NULL, 'гггггггггг', '2021-07-21 13:38:15', '2021-07-21 13:38:17', 1),
(1037, 162, 9, NULL, 'ну пшм', '2021-07-21 13:38:24', '2021-07-21 13:38:26', 1),
(1038, 162, 9, NULL, 'и он', '2021-07-21 13:38:28', '2021-07-21 13:38:29', 1),
(1039, 162, 9, NULL, 'нргп', '2021-07-21 13:38:33', '2021-07-21 13:38:35', 1),
(1040, 162, 9, NULL, 'осоаовоаоаочао', '2021-07-21 13:39:19', '2021-07-21 13:39:22', 1),
(1041, 166, 43, NULL, 'Здравствуйте я ваш адвокат Анастасия Константиновна', '2021-08-17 13:41:19', '2021-08-17 16:18:37', 1),
(1042, 165, 1, NULL, 'Здравствуйте я ваш адвокат Администратор', '2021-08-17 16:18:45', '2021-08-17 16:18:45', 0),
(1043, 164, 1, NULL, 'Здравствуйте я ваш адвокат Администратор', '2021-08-17 16:18:57', '2021-08-17 16:18:57', 0),
(1044, 163, 1, NULL, 'Здравствуйте я ваш адвокат Администратор', '2021-08-17 16:19:38', '2021-08-17 16:19:38', 0);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(31, '2014_10_12_000000_create_users_table', 1),
(32, '2014_10_12_100000_create_password_resets_table', 1),
(33, '2016_01_01_000000_add_voyager_user_fields', 1),
(34, '2016_01_01_000000_create_data_types_table', 1),
(35, '2016_01_01_000000_create_pages_table', 1),
(36, '2016_01_01_000000_create_posts_table', 1),
(37, '2016_02_15_204651_create_categories_table', 1),
(38, '2016_05_19_173453_create_menu_table', 1),
(39, '2016_10_21_190000_create_roles_table', 1),
(40, '2016_10_21_190000_create_settings_table', 1),
(41, '2016_11_30_135954_create_permission_table', 1),
(42, '2016_11_30_141208_create_permission_role_table', 1),
(43, '2016_12_26_201236_data_types__add__server_side', 1),
(44, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(45, '2017_01_14_005015_create_translations_table', 1),
(46, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(47, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(48, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(49, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(50, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(51, '2017_08_05_000000_add_group_to_settings_table', 1),
(52, '2017_11_26_013050_add_user_role_relationship', 1),
(53, '2017_11_26_015000_create_user_roles_table', 1),
(54, '2018_03_11_000000_add_user_settings', 1),
(55, '2018_03_14_000000_add_details_to_data_types_table', 1),
(56, '2018_03_16_000000_make_settings_value_nullable', 1),
(57, '2019_08_19_000000_create_failed_jobs_table', 1),
(58, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(59, '2021_04_28_104509_create_sms_codes_table', 1),
(60, '2021_04_28_111719_create_statuses_table', 1),
(61, '2021_04_28_123919_create_document_categories_table', 1),
(62, '2021_04_28_124038_create_documents_table', 1),
(63, '2021_04_28_124350_create_reviews_table', 1),
(64, '2021_04_29_152616_add_deleted_at_to_users_table', 1),
(65, '2021_04_29_165125_create_services_table', 1),
(66, '2021_04_29_165345_create_payment_statuses_table', 1),
(67, '2021_04_29_165417_create_order_statuses_table', 1),
(68, '2021_04_29_165521_create_orders_table', 1),
(69, '2021_04_29_170000_create_messages_table', 1),
(70, '2021_04_29_170356_add_document_id_to_orders_table', 1),
(71, '2021_04_29_171834_create_cards_table', 1),
(72, '2021_04_29_172004_add_card_id_to_orders_table', 1),
(73, '2021_04_29_172358_add_card_id_to_cards_table', 1),
(74, '2021_04_29_174554_add_deleted_at_to_cards_table', 1),
(75, '2021_05_03_111132_add_question_title_to_orders_table', 1),
(76, '2021_05_09_230532_add_is_main_to_cards_table', 2),
(77, '2021_05_10_031455_add_is_read_to_messages_table', 3),
(78, '2021_05_12_194006_add_device_token_to_users_table', 4),
(79, '2021_05_14_152322_add_rating_counter_to_users_table', 5),
(80, '2021_05_14_154211_create_ratings_table', 5),
(81, '2021_05_17_161651_create_notifications_table', 6),
(82, '2021_05_21_142653_add_description_to_documents_table', 7),
(83, '2021_05_21_144059_add_status_id_to_documents_table', 8),
(84, '2021_05_21_144235_add_price_to_documents_table', 9),
(85, '2021_05_27_160903_add_soft_delete_to_document_categories_table', 10),
(86, '2021_07_17_220316_add_collegium_to_users_table', 11);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `messages` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `messages`, `created_at`, `updated_at`) VALUES
(6, 'Здравствуйте у нас уведомления', '2021-05-17 16:42:05', '2021-05-17 16:42:05'),
(7, 'Здравствуйте у нас новое приложение', '2021-05-17 16:43:00', '2021-05-17 19:08:56'),
(8, 'У нас появились новые шаблоны документов для Вас.', '2021-05-17 19:09:27', '2021-05-17 19:09:27'),
(9, 'vse suda', '2021-05-17 20:42:07', '2021-05-17 20:42:07'),
(10, 'здрасьте', '2021-05-17 21:10:56', '2021-05-17 21:10:56'),
(11, 'Здравствуйте', '2021-05-27 16:26:52', '2021-05-27 16:26:52'),
(12, 'x', '2021-05-27 16:35:01', '2021-05-27 16:35:01'),
(13, 'Так', '2021-05-27 16:36:19', '2021-05-27 16:36:19'),
(14, 'Привет всем', '2021-06-25 17:57:59', '2021-06-25 17:57:59'),
(15, 'ФЫвфы', '2021-06-25 17:58:20', '2021-06-25 17:58:20'),
(16, 'asdsd', '2021-06-25 17:59:05', '2021-06-25 17:59:05'),
(17, 'asdsd', '2021-06-25 17:59:47', '2021-06-25 17:59:47'),
(18, 'qwdw', '2021-06-25 18:00:23', '2021-06-25 18:00:23');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `payment_status_id` bigint(20) UNSIGNED NOT NULL,
  `order_status_id` bigint(20) UNSIGNED NOT NULL,
  `lawyer` bigint(20) UNSIGNED DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `document_id` bigint(20) UNSIGNED DEFAULT NULL,
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `card_id` bigint(20) UNSIGNED NOT NULL,
  `question_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `payment_status_id`, `order_status_id`, `lawyer`, `start_time`, `end_time`, `deleted_at`, `created_at`, `updated_at`, `document_id`, `service_id`, `card_id`, `question_title`) VALUES
(7, 6, 1, 4, 8, '2021-05-14 12:06:40', '2021-05-14 15:14:11', NULL, '2021-05-10 00:03:06', '2021-05-14 15:16:03', 1, 1, 2, 'asdsd'),
(8, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:05:08', '2021-05-10 00:05:08', 1, 1, 2, NULL),
(9, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:06:27', '2021-05-10 00:06:27', 1, 1, 2, NULL),
(10, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:08:39', '2021-05-10 00:08:39', 1, 1, 2, NULL),
(11, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:08:51', '2021-05-10 00:08:51', 1, 1, 2, NULL),
(12, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:15:26', '2021-05-10 00:15:26', 1, 1, 2, NULL),
(13, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:15:29', '2021-05-10 00:15:29', 1, 1, 2, NULL),
(14, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:16:09', '2021-05-10 00:16:09', 1, 1, 2, NULL),
(15, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:17:05', '2021-05-10 00:17:05', 1, 1, 2, NULL),
(16, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:19:01', '2021-05-10 00:19:01', 1, 1, 2, NULL),
(17, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:20:40', '2021-05-10 00:20:40', 1, 1, 2, NULL),
(18, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:21:35', '2021-05-10 00:21:35', 1, 1, 2, NULL),
(19, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:23:45', '2021-05-10 00:23:45', 1, 1, 3, NULL),
(20, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:38:48', '2021-05-10 00:38:48', 1, 1, 3, NULL),
(21, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:49:08', '2021-05-10 00:49:08', 1, 1, 3, NULL),
(22, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:49:23', '2021-05-10 00:49:23', 1, 1, 3, NULL),
(23, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:49:40', '2021-05-10 00:49:40', 1, 1, 3, NULL),
(24, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 00:52:50', '2021-05-10 00:52:50', 1, 1, 3, NULL),
(25, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:00:37', '2021-05-10 01:00:37', 1, 1, 3, NULL),
(26, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:04:28', '2021-05-10 01:04:28', 1, 1, 3, NULL),
(27, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:06:38', '2021-05-10 01:06:38', 1, 1, 3, NULL),
(28, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:09:43', '2021-05-10 01:09:43', 1, 1, 3, NULL),
(29, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:12:52', '2021-05-10 01:12:52', 1, 1, 3, NULL),
(30, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:13:40', '2021-05-10 01:13:40', 1, 1, 3, NULL),
(31, 6, 1, 5, NULL, NULL, NULL, '2021-05-20 16:05:04', '2021-05-10 01:15:48', '2021-05-10 01:15:48', 1, 1, 3, NULL),
(32, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:22:48', '2021-05-10 01:22:48', 1, 1, 5, NULL),
(33, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:23:41', '2021-05-10 01:23:41', 1, 1, 5, NULL),
(34, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:24:13', '2021-05-10 01:24:13', 1, 1, 5, NULL),
(35, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 01:51:00', '2021-05-10 01:51:00', 1, 1, 5, NULL),
(36, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 02:57:40', '2021-05-10 02:57:40', NULL, 2, 5, NULL),
(37, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 02:58:36', '2021-05-10 02:58:36', NULL, 2, 5, NULL),
(38, 6, 1, 4, 8, '2021-05-10 12:31:46', '2021-05-14 12:31:49', NULL, '2021-05-10 03:01:37', '2021-05-14 12:40:55', NULL, 2, 5, 'asd'),
(39, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 03:22:32', '2021-05-10 03:22:32', NULL, 2, 5, 'Сложная проблема'),
(40, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-10 03:22:52', '2021-05-10 03:22:52', NULL, 2, 7, 'вывести'),
(41, 6, 2, 4, 8, '2021-05-14 12:22:08', '2021-05-14 12:22:08', NULL, '2021-05-11 00:01:32', '2021-05-14 12:40:55', NULL, 2, 7, 'аа'),
(42, 6, 2, 5, NULL, NULL, NULL, '2021-05-20 16:04:32', '2021-05-11 10:09:35', '2021-05-11 10:10:02', 1, 1, 7, NULL),
(43, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-11 11:46:36', '2021-05-11 11:46:36', NULL, 2, 7, 'лалоа'),
(44, 6, 2, 4, 8, '2021-05-14 12:24:55', '2021-05-14 12:24:55', NULL, '2021-05-11 11:46:38', '2021-05-14 12:40:55', NULL, 2, 7, 'лалоа'),
(45, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-12 01:05:36', '2021-05-12 01:05:36', NULL, 2, 5, 'фаа'),
(46, 6, 1, 1, NULL, NULL, NULL, '2021-05-20 16:04:26', '2021-05-12 01:07:18', '2021-05-12 01:07:18', 1, 1, 5, NULL),
(47, 6, 2, 4, 8, '2021-05-14 12:26:09', '2021-05-14 15:26:09', NULL, '2021-05-13 16:07:38', '2021-05-14 15:26:38', NULL, 2, 8, 'hhh'),
(48, 6, 2, 4, 8, '2021-05-14 15:01:19', '2021-05-14 18:01:19', NULL, '2021-05-13 16:22:56', '2021-05-16 15:07:54', NULL, 2, 8, 'ggggghhhh'),
(49, 9, 2, 4, 8, '2021-05-14 17:49:16', '2021-05-14 20:49:16', NULL, '2021-05-13 16:29:28', '2021-05-17 10:57:36', NULL, 2, 9, 'test'),
(50, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-13 17:52:21', '2021-05-13 17:52:21', NULL, 2, 8, 'ff'),
(51, 6, 1, 4, 8, '2021-05-14 12:19:18', '2021-05-14 15:19:18', NULL, '2021-05-13 17:54:32', '2021-05-14 15:19:48', NULL, 2, 8, 'ff'),
(52, 6, 2, 4, 8, '2021-05-14 17:49:22', '2021-05-14 20:49:22', NULL, '2021-05-13 17:55:08', '2021-05-16 15:07:54', NULL, 2, 8, 'ff'),
(53, 6, 2, 4, 91, '2021-07-18 22:58:46', '2021-07-19 01:58:46', NULL, '2021-05-13 17:57:50', '2021-07-19 15:05:34', NULL, 2, 8, 'hhgggy'),
(54, 6, 2, 4, 8, '2021-05-14 16:47:29', '2021-05-14 19:47:29', NULL, '2021-05-13 17:59:37', '2021-05-16 15:07:54', NULL, 2, 8, 'gil'),
(55, 6, 2, 5, NULL, NULL, NULL, '2021-05-20 16:04:23', '2021-05-13 18:00:25', '2021-05-13 18:00:37', 1, 1, 8, NULL),
(56, 6, 2, 5, NULL, NULL, NULL, '2021-05-20 16:04:20', '2021-05-13 18:00:52', '2021-05-13 18:01:06', 1, 1, 8, NULL),
(57, 6, 2, 4, 44, NULL, NULL, NULL, '2021-05-13 18:02:18', '2021-06-14 03:21:41', NULL, 2, 8, 'fh'),
(58, 6, 2, 4, 91, '2021-07-18 22:56:56', '2021-07-19 01:56:56', NULL, '2021-05-13 18:02:50', '2021-07-19 15:05:34', NULL, 2, 8, 'fh'),
(59, 6, 2, 4, 91, '2021-07-18 22:56:00', '2021-07-19 01:56:00', NULL, '2021-05-13 18:03:58', '2021-07-19 15:05:34', NULL, 2, 8, 'gg'),
(60, 6, 2, 5, NULL, NULL, NULL, '2021-05-20 16:04:18', '2021-05-13 18:04:59', '2021-05-13 18:05:11', 1, 1, 8, NULL),
(61, 6, 2, 4, 91, '2021-07-18 22:46:44', '2021-07-19 01:46:44', NULL, '2021-05-13 18:13:11', '2021-07-19 15:05:34', NULL, 2, 8, 'dcg'),
(62, 6, 2, 4, 43, '2021-06-11 12:42:06', '2021-06-11 15:42:06', NULL, '2021-05-13 18:14:03', '2021-06-12 11:57:54', NULL, 2, 8, 'vvv'),
(63, 6, 2, 4, 91, '2021-07-18 22:46:26', '2021-07-19 01:46:26', NULL, '2021-05-13 19:26:39', '2021-07-19 15:05:34', NULL, 2, 8, 'jjju'),
(64, 6, 2, 4, 91, '2021-07-18 22:46:06', '2021-07-19 01:46:06', NULL, '2021-05-16 15:08:30', '2021-07-19 15:05:34', NULL, 2, 8, 'ор'),
(65, 6, 2, 4, 8, '2021-05-16 15:30:39', '2021-05-16 18:30:39', NULL, '2021-05-16 15:20:36', '2021-05-16 20:37:11', NULL, 2, 8, 'пр'),
(66, 6, 2, 4, 13, '2021-06-17 13:00:08', '2021-06-17 16:00:08', NULL, '2021-05-17 14:41:40', '2021-06-22 02:33:49', NULL, 2, 8, 'ооо'),
(67, 6, 2, 4, 8, '2021-05-17 14:47:20', '2021-05-17 17:47:20', NULL, '2021-05-17 14:43:52', '2021-05-17 17:47:51', NULL, 2, 8, 'прр'),
(68, 6, 2, 4, 8, '2021-05-17 14:49:07', '2021-05-17 17:49:07', NULL, '2021-05-17 14:47:00', '2021-05-17 17:49:37', NULL, 2, 8, 'асыл'),
(69, 9, 2, 4, 8, '2021-05-17 15:02:49', '2021-05-17 18:02:49', NULL, '2021-05-17 15:02:25', '2021-05-17 20:00:54', NULL, 2, 9, 'тестирование'),
(70, 6, 2, 4, 8, '2021-05-17 15:14:53', '2021-05-17 18:14:53', NULL, '2021-05-17 15:14:11', '2021-05-17 18:15:25', NULL, 2, 8, 'дала'),
(71, 6, 2, 4, 44, '2021-06-11 13:59:32', '2021-06-11 16:59:32', NULL, '2021-05-17 15:15:46', '2021-06-12 11:57:54', NULL, 2, 8, 'жажж'),
(72, 6, 2, 4, 8, '2021-05-17 15:17:40', '2021-05-17 18:17:40', NULL, '2021-05-17 15:17:25', '2021-05-17 18:18:10', NULL, 2, 8, 'ооо'),
(73, 6, 2, 4, 1, '2021-05-20 15:21:22', '2021-05-20 18:21:22', NULL, '2021-05-17 15:20:38', '2021-05-20 18:21:52', NULL, 2, 8, 'привер'),
(74, 9, 2, 4, 8, '2021-05-17 16:25:44', '2021-05-17 19:25:44', NULL, '2021-05-17 16:24:52', '2021-05-17 20:00:54', NULL, 2, 9, 'кткикт'),
(75, 9, 2, 4, 8, '2021-05-17 20:51:28', '2021-05-17 23:51:28', NULL, '2021-05-17 20:46:06', '2021-05-21 09:17:16', NULL, 2, 9, 'тест'),
(76, 9, 2, 5, NULL, NULL, NULL, NULL, '2021-05-17 21:05:32', '2021-05-17 21:05:47', 2, 1, 9, NULL),
(77, 9, 2, 5, NULL, NULL, NULL, NULL, '2021-05-17 21:08:49', '2021-05-17 21:09:03', 1, 1, 9, NULL),
(78, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-20 15:51:58', '2021-05-20 15:51:58', NULL, 2, 8, 'оо'),
(79, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-20 15:52:34', '2021-05-20 15:52:34', NULL, 2, 8, 'оо'),
(80, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-20 15:54:12', '2021-05-20 15:54:12', NULL, 2, 8, 'оо'),
(81, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-20 15:54:28', '2021-05-20 15:54:28', NULL, 2, 8, 'оо'),
(82, 6, 2, 5, NULL, NULL, NULL, NULL, '2021-05-20 16:09:27', '2021-05-20 16:09:44', 2, 1, 8, NULL),
(83, 6, 2, 4, 8, '2021-05-21 21:30:30', '2021-05-22 00:30:30', NULL, '2021-05-20 16:58:58', '2021-05-22 11:43:26', NULL, 2, 8, 'ффф'),
(84, 6, 2, 4, 1, '2021-05-21 15:31:59', '2021-05-21 18:31:59', NULL, '2021-05-20 17:02:41', '2021-05-21 22:03:59', NULL, 2, 8, 'ффф'),
(85, 6, 2, 4, 1, '2021-05-21 16:14:38', '2021-05-21 19:14:38', NULL, '2021-05-20 17:03:31', '2021-05-21 22:03:59', NULL, 2, 8, 'фф'),
(86, 6, 1, 4, 1, '2021-05-21 15:48:58', '2021-05-21 18:48:58', NULL, '2021-05-20 17:15:42', '2021-05-21 22:03:59', NULL, 2, 8, 'ррр'),
(87, 15, 2, 4, 13, '2021-05-21 09:28:13', '2021-05-21 12:28:13', NULL, '2021-05-21 09:08:33', '2021-05-21 21:17:56', NULL, 2, 11, 'тест после обновления'),
(88, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-21 15:00:35', '2021-05-21 15:00:35', 177, 1, 8, NULL),
(89, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-21 15:18:29', '2021-05-21 15:18:29', 59, 1, 8, NULL),
(90, 6, 2, 5, NULL, NULL, NULL, NULL, '2021-05-21 15:18:37', '2021-05-21 15:18:52', 152, 1, 8, NULL),
(91, 15, 2, 4, 1, '2021-05-21 21:19:22', '2021-05-22 00:19:22', NULL, '2021-05-21 21:18:23', '2021-05-22 12:45:55', NULL, 2, 11, 'тест 2'),
(92, 9, 2, 4, 13, '2021-05-21 21:27:09', '2021-05-22 00:27:09', NULL, '2021-05-21 21:26:31', '2021-05-22 19:05:46', NULL, 2, 9, '777777777'),
(93, 9, 2, 4, 8, '2021-05-21 21:33:15', '2021-05-22 00:33:15', NULL, '2021-05-21 21:32:44', '2021-05-22 19:05:46', NULL, 2, 9, 'здравствуйте у меня проблема'),
(94, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-05-22 14:23:16', '2021-05-22 14:23:16', 61, 1, 8, NULL),
(95, 6, 2, 5, NULL, NULL, NULL, NULL, '2021-05-22 14:23:48', '2021-05-22 14:24:03', 61, 1, 8, NULL),
(96, 9, 2, 4, 43, '2021-06-11 12:41:08', '2021-06-11 15:41:08', NULL, '2021-05-22 19:06:04', '2021-06-12 22:04:25', NULL, 2, 9, 'Александр тест'),
(97, 9, 1, 1, NULL, NULL, NULL, NULL, '2021-05-26 21:10:41', '2021-05-26 21:10:41', NULL, 2, 9, 'тногл'),
(98, 9, 2, 4, 1, '2021-06-11 12:36:11', '2021-06-11 15:36:11', NULL, '2021-05-27 19:27:10', '2021-06-12 22:04:25', NULL, 2, 9, 'ннн'),
(99, 9, 2, 5, NULL, NULL, NULL, NULL, '2021-06-08 11:40:05', '2021-06-08 11:40:23', 234, 1, 9, NULL),
(100, 9, 2, 4, 43, '2021-06-11 10:07:57', '2021-06-11 13:07:57', NULL, '2021-06-08 16:53:56', '2021-06-12 22:04:25', NULL, 2, 9, 'тест Александр'),
(101, 15, 2, 4, 1, '2021-06-10 11:48:37', '2021-06-10 14:48:37', NULL, '2021-06-10 11:47:33', '2021-06-11 12:22:33', NULL, 2, 11, 'привет'),
(102, 15, 2, 4, 43, '2021-06-11 12:21:20', '2021-06-11 15:21:20', NULL, '2021-06-11 12:20:52', '2021-06-17 21:21:05', NULL, 2, 11, 'тест для Насти'),
(103, 36, 2, 4, 43, '2021-06-11 12:33:45', '2021-06-11 15:33:45', NULL, '2021-06-11 12:32:52', '2021-06-17 11:02:56', NULL, 2, 12, 'Расторжение брака'),
(104, 36, 2, 4, 43, '2021-06-11 13:30:04', '2021-06-11 16:30:04', NULL, '2021-06-11 13:29:27', '2021-06-17 11:02:56', NULL, 2, 12, 'Раздел имущества'),
(105, 15, 2, 4, 43, '2021-06-11 14:01:15', '2021-06-11 17:01:15', NULL, '2021-06-11 14:00:54', '2021-06-17 21:21:05', NULL, 2, 11, 'тест'),
(106, 15, 2, 4, 44, '2021-06-11 14:04:04', '2021-06-11 17:04:04', NULL, '2021-06-11 14:03:40', '2021-06-17 21:21:05', NULL, 2, 11, 'второй тест'),
(107, 37, 1, 1, NULL, NULL, NULL, NULL, '2021-06-11 16:22:56', '2021-06-11 16:22:56', 304, 1, 13, NULL),
(108, 9, 1, 1, NULL, NULL, NULL, NULL, '2021-06-12 22:04:16', '2021-06-12 22:04:16', 223, 1, 9, NULL),
(109, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-06-14 02:59:40', '2021-06-14 02:59:40', NULL, 2, 8, 'ad'),
(110, 6, 1, 1, NULL, NULL, NULL, NULL, '2021-06-14 03:00:24', '2021-06-14 03:00:24', NULL, 2, 8, 'aaa'),
(111, 6, 1, 4, 1, '2021-06-15 17:52:08', '2021-06-15 20:52:08', NULL, '2021-06-14 03:01:28', '2021-06-22 02:33:49', NULL, 2, 8, 'ppp'),
(112, 6, 1, 4, 1, '2021-06-14 03:22:08', '2021-06-14 06:22:08', NULL, '2021-06-14 03:06:05', '2021-06-14 19:20:17', NULL, 2, 8, 'ηη'),
(113, 9, 2, 4, 1, '2021-06-16 11:49:41', '2021-06-16 14:49:41', NULL, '2021-06-16 11:48:00', '2021-06-30 22:57:42', NULL, 2, 9, 'мгп'),
(114, 37, 1, 1, NULL, NULL, NULL, NULL, '2021-06-16 16:09:12', '2021-06-16 16:09:12', NULL, 2, 13, 'мяу'),
(115, 37, 1, 1, NULL, NULL, NULL, NULL, '2021-06-16 16:09:36', '2021-06-16 16:09:36', NULL, 2, 13, 'мяу'),
(116, 37, 2, 4, 43, '2021-06-17 09:28:31', '2021-06-17 12:28:31', NULL, '2021-06-17 09:26:24', '2021-06-17 12:33:40', NULL, 2, 13, 'мяу'),
(117, 36, 2, 4, 44, '2021-06-17 11:03:12', '2021-06-17 14:03:12', NULL, '2021-06-17 11:02:23', '2021-06-18 10:03:26', NULL, 2, 12, 'Раздел имущества'),
(118, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-06-17 11:11:12', '2021-06-17 11:11:12', NULL, 2, 12, 'Спасибо большое'),
(119, 36, 2, 4, 43, '2021-06-17 11:12:27', '2021-06-17 14:12:27', NULL, '2021-06-17 11:11:45', '2021-06-18 10:03:26', NULL, 2, 12, 'Брак'),
(120, 37, 2, 4, 44, '2021-06-17 11:14:20', '2021-06-17 14:14:20', NULL, '2021-06-17 11:13:50', '2021-06-18 14:24:39', NULL, 2, 13, 'гаф'),
(121, 36, 2, 4, 44, '2021-06-17 12:10:31', '2021-06-17 15:10:31', NULL, '2021-06-17 12:10:01', '2021-06-18 10:03:26', NULL, 2, 12, 'Займ'),
(122, 36, 2, 4, 13, '2021-06-17 12:59:22', '2021-06-17 15:59:22', NULL, '2021-06-17 12:11:18', '2021-06-18 10:03:26', NULL, 2, 12, 'Займ'),
(123, 36, 2, 4, 43, '2021-06-18 10:03:40', '2021-06-18 13:03:40', NULL, '2021-06-18 10:03:02', '2021-06-18 14:37:22', NULL, 2, 12, 'Алименты'),
(124, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-06-18 10:06:32', '2021-06-18 10:06:32', NULL, 2, 12, 'Займ'),
(125, 15, 2, 4, 43, '2021-06-18 14:12:41', '2021-06-18 17:12:41', NULL, '2021-06-18 14:12:17', '2021-06-22 12:02:31', NULL, 2, 11, 'у меня вопрос по трудовому спору'),
(126, 37, 2, 4, 43, '2021-06-18 14:27:28', '2021-06-18 17:27:28', NULL, '2021-06-18 14:24:22', '2021-06-25 17:58:16', NULL, 2, 13, 'дирд'),
(127, 36, 2, 4, 44, '2021-06-18 14:38:39', '2021-06-18 17:38:39', NULL, '2021-06-18 14:36:52', '2021-06-29 14:46:39', NULL, 2, 12, 'Продажа'),
(128, 60, 2, 4, 43, '2021-06-23 14:28:32', '2021-06-23 17:28:32', NULL, '2021-06-23 14:27:58', '2021-06-25 17:59:38', NULL, 2, 14, 'Здравствуйте, хотела бы получить консультацию по вопросу развода с супругом.'),
(129, 60, 1, 1, NULL, NULL, NULL, NULL, '2021-06-23 14:39:38', '2021-06-23 14:39:38', 222, 1, 14, NULL),
(130, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-06-23 14:39:38', '2021-06-23 14:39:38', 96, 1, 12, NULL),
(131, 36, 2, 4, 43, '2021-06-29 14:46:56', '2021-06-29 17:46:56', NULL, '2021-06-29 14:39:39', '2021-07-01 17:31:52', NULL, 2, 12, 'Брак'),
(132, 60, 2, 4, 43, '2021-07-01 17:15:43', '2021-07-01 20:15:43', NULL, '2021-07-01 17:13:53', '2021-07-02 14:00:57', NULL, 2, 14, 'ррррррриии'),
(133, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:30:31', '2021-07-01 17:30:31', NULL, 2, 12, 'Постановление'),
(134, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:30:52', '2021-07-01 17:30:52', NULL, 2, 12, 'Постановление'),
(135, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:31:04', '2021-07-01 17:31:04', NULL, 2, 12, 'Постановление'),
(136, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:32:00', '2021-07-01 17:32:00', NULL, 2, 12, 'Проо'),
(137, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:32:21', '2021-07-01 17:32:21', NULL, 2, 12, 'Ррроо'),
(138, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:32:42', '2021-07-01 17:32:42', NULL, 2, 12, 'Рррооорн'),
(139, 37, 2, 4, 43, '2021-07-01 17:33:43', '2021-07-01 20:33:43', NULL, '2021-07-01 17:33:14', '2021-07-07 10:41:54', NULL, 2, 13, 'ооо'),
(140, 37, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:33:46', '2021-07-01 17:33:46', NULL, 2, 13, 'оо'),
(141, 36, 1, 1, NULL, NULL, NULL, NULL, '2021-07-01 17:33:46', '2021-07-01 17:33:46', NULL, 2, 12, 'Ооррро'),
(142, 36, 1, 3, 1, '2021-07-02 04:56:30', '2021-07-02 07:56:30', NULL, '2021-07-01 17:37:37', '2021-07-02 04:56:30', NULL, 2, 12, 'Гмгир'),
(143, 60, 2, 4, 43, '2021-07-02 14:16:49', '2021-07-02 17:16:49', NULL, '2021-07-02 14:00:39', '2021-07-07 10:41:50', NULL, 2, 14, 'оалалалалал'),
(144, 37, 2, 4, 43, '2021-07-07 10:42:18', '2021-07-07 13:42:18', NULL, '2021-07-07 10:41:22', '2021-07-08 10:51:07', NULL, 2, 13, 'Испания'),
(145, 60, 2, 4, 43, '2021-07-07 10:41:52', '2021-07-07 13:41:52', NULL, '2021-07-07 10:41:28', '2021-07-07 16:18:41', NULL, 2, 14, 'ирррррр'),
(146, 37, 2, 4, 44, '2021-07-08 10:53:50', '2021-07-08 13:53:50', NULL, '2021-07-08 10:50:49', '2021-07-12 14:36:53', NULL, 2, 13, 'тол'),
(147, 60, 2, 4, 44, '2021-07-08 10:51:35', '2021-07-08 13:51:35', NULL, '2021-07-08 10:50:52', '2021-07-09 12:17:47', NULL, 2, 14, 'иочлчлсьс'),
(148, 60, 2, 4, 43, '2021-07-09 12:21:18', '2021-07-09 15:21:18', NULL, '2021-07-09 12:17:54', '2021-07-19 10:50:29', NULL, 2, 14, 'оооодд'),
(149, 6, 2, 4, 1, '2021-07-12 09:55:07', '2021-07-12 12:55:07', NULL, '2021-07-12 09:54:50', '2021-07-12 13:15:36', NULL, 2, 8, 'anahah'),
(150, 6, 2, 4, 43, '2021-07-12 14:37:44', '2021-07-12 17:37:44', NULL, '2021-07-12 10:08:32', '2021-07-12 18:15:46', NULL, 2, 8, 'двлы'),
(151, 37, 2, 3, 43, '2021-07-12 14:36:53', '2021-07-12 17:36:53', NULL, '2021-07-12 14:36:33', '2021-07-12 14:36:53', NULL, 2, 13, 'оол'),
(152, 6, 1, 4, 1, '2021-07-18 15:44:01', '2021-07-18 18:44:01', NULL, '2021-07-17 18:34:27', '2021-07-18 23:36:35', NULL, 2, 8, 'ggg'),
(153, 6, 2, 2, 91, '2021-07-18 22:08:03', '2021-07-18 23:08:07', NULL, '2021-07-18 17:05:43', '2021-07-18 17:05:58', NULL, 3, 8, 'гг'),
(154, 6, 1, 4, 1, '2021-07-19 09:38:13', '2021-07-19 12:38:13', NULL, '2021-07-18 23:37:46', '2021-07-19 15:05:33', NULL, 2, 8, 'ппп'),
(155, 6, 2, 4, 91, '2021-07-18 23:38:55', '2021-07-19 02:38:55', NULL, '2021-07-18 23:38:34', '2021-07-19 15:05:33', NULL, 2, 8, 'ппп'),
(156, 6, 2, 3, 91, '2021-07-19 20:55:26', '2021-07-19 23:55:26', NULL, '2021-07-19 15:21:23', '2021-07-19 20:55:26', NULL, 3, 8, 'оаоао'),
(157, 6, 2, 3, 91, '2021-07-19 20:49:16', '2021-07-19 23:49:16', NULL, '2021-07-19 15:50:04', '2021-07-19 20:49:16', NULL, 3, 8, 'оаоао'),
(158, 6, 2, 3, 91, '2021-07-19 20:42:40', '2021-07-19 23:42:40', NULL, '2021-07-19 15:52:45', '2021-07-19 20:42:40', NULL, 3, 8, 'оаоао'),
(159, 6, 2, 3, 1, '2021-07-19 20:57:07', '2021-07-19 23:57:07', NULL, '2021-07-19 20:56:21', '2021-07-19 20:57:07', NULL, 2, 8, 'лол'),
(160, 6, 2, 3, 91, '2021-07-19 22:09:01', '2021-07-20 01:09:01', NULL, '2021-07-19 22:08:33', '2021-07-19 22:09:01', NULL, 2, 8, 'как'),
(161, 9, 2, 4, 91, '2021-07-21 13:30:44', '2021-07-21 16:30:44', NULL, '2021-07-21 13:30:06', '2021-07-27 20:21:14', NULL, 3, 9, 'тест'),
(162, 9, 2, 4, 91, '2021-07-21 13:36:09', '2021-07-21 16:36:09', NULL, '2021-07-21 13:35:42', '2021-07-27 20:21:13', NULL, 2, 9, 'лаалдпдпдп'),
(163, 91, 2, 3, 1, '2021-08-17 16:19:38', '2021-08-17 19:19:38', NULL, '2021-08-17 01:56:48', '2021-08-17 16:19:38', NULL, 2, 16, 'aaa'),
(164, 91, 2, 3, 1, '2021-08-17 16:18:56', '2021-08-17 19:18:56', NULL, '2021-08-17 02:28:01', '2021-08-17 16:18:56', NULL, 2, 16, 'вы'),
(165, 91, 2, 3, 1, '2021-08-17 16:18:44', '2021-08-17 19:18:44', NULL, '2021-08-17 02:29:15', '2021-08-17 16:18:44', NULL, 2, 16, 'вы'),
(166, 91, 2, 3, 43, '2021-08-17 13:41:18', '2021-08-17 16:41:18', NULL, '2021-08-17 02:30:02', '2021-08-17 13:41:18', NULL, 2, 16, 'вы'),
(167, 15, 1, 1, NULL, NULL, NULL, NULL, '2021-08-19 15:14:35', '2021-08-19 15:14:35', 108, 1, 11, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_statuses`
--

CREATE TABLE `order_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_statuses`
--

INSERT INTO `order_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ожидание', NULL, NULL),
(2, 'Ожидание юриста', NULL, NULL),
(3, 'Начало чат', NULL, NULL),
(4, 'Чат закрыт', NULL, NULL),
(5, 'Документ готов к скачиванию', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2021-05-07 20:29:06', '2021-05-07 20:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_statuses`
--

CREATE TABLE `payment_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_statuses`
--

INSERT INTO `payment_statuses` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Не оплачено', NULL, NULL),
(2, 'Оплачено', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(2, 'browse_bread', NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(3, 'browse_database', NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(4, 'browse_media', NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(5, 'browse_compass', NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(6, 'browse_menus', 'menus', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(7, 'read_menus', 'menus', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(8, 'edit_menus', 'menus', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(9, 'add_menus', 'menus', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(10, 'delete_menus', 'menus', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(11, 'browse_roles', 'roles', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(12, 'read_roles', 'roles', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(13, 'edit_roles', 'roles', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(14, 'add_roles', 'roles', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(15, 'delete_roles', 'roles', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(16, 'browse_users', 'users', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(17, 'read_users', 'users', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(18, 'edit_users', 'users', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(19, 'add_users', 'users', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(20, 'delete_users', 'users', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(21, 'browse_settings', 'settings', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(22, 'read_settings', 'settings', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(23, 'edit_settings', 'settings', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(24, 'add_settings', 'settings', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(25, 'delete_settings', 'settings', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(26, 'browse_hooks', NULL, '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(27, 'browse_categories', 'categories', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(28, 'read_categories', 'categories', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(29, 'edit_categories', 'categories', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(30, 'add_categories', 'categories', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(31, 'delete_categories', 'categories', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(32, 'browse_posts', 'posts', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(33, 'read_posts', 'posts', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(34, 'edit_posts', 'posts', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(35, 'add_posts', 'posts', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(36, 'delete_posts', 'posts', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(37, 'browse_pages', 'pages', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(38, 'read_pages', 'pages', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(39, 'edit_pages', 'pages', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(40, 'add_pages', 'pages', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(41, 'delete_pages', 'pages', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(42, 'browse_document_categories', 'document_categories', '2021-05-09 16:48:25', '2021-05-09 16:48:25'),
(43, 'read_document_categories', 'document_categories', '2021-05-09 16:48:25', '2021-05-09 16:48:25'),
(44, 'edit_document_categories', 'document_categories', '2021-05-09 16:48:25', '2021-05-09 16:48:25'),
(45, 'add_document_categories', 'document_categories', '2021-05-09 16:48:25', '2021-05-09 16:48:25'),
(46, 'delete_document_categories', 'document_categories', '2021-05-09 16:48:25', '2021-05-09 16:48:25'),
(47, 'browse_documents', 'documents', '2021-05-09 16:58:11', '2021-05-09 16:58:11'),
(48, 'read_documents', 'documents', '2021-05-09 16:58:11', '2021-05-09 16:58:11'),
(49, 'edit_documents', 'documents', '2021-05-09 16:58:11', '2021-05-09 16:58:11'),
(50, 'add_documents', 'documents', '2021-05-09 16:58:11', '2021-05-09 16:58:11'),
(51, 'delete_documents', 'documents', '2021-05-09 16:58:11', '2021-05-09 16:58:11'),
(52, 'browse_orders', 'orders', '2021-05-11 14:39:27', '2021-05-11 14:39:27'),
(53, 'read_orders', 'orders', '2021-05-11 14:39:27', '2021-05-11 14:39:27'),
(54, 'edit_orders', 'orders', '2021-05-11 14:39:27', '2021-05-11 14:39:27'),
(55, 'add_orders', 'orders', '2021-05-11 14:39:27', '2021-05-11 14:39:27'),
(56, 'delete_orders', 'orders', '2021-05-11 14:39:27', '2021-05-11 14:39:27'),
(57, 'browse_services', 'services', '2021-05-12 12:48:05', '2021-05-12 12:48:05'),
(58, 'read_services', 'services', '2021-05-12 12:48:05', '2021-05-12 12:48:05'),
(59, 'edit_services', 'services', '2021-05-12 12:48:05', '2021-05-12 12:48:05'),
(60, 'add_services', 'services', '2021-05-12 12:48:05', '2021-05-12 12:48:05'),
(61, 'delete_services', 'services', '2021-05-12 12:48:05', '2021-05-12 12:48:05'),
(62, 'browse_notifications', 'notifications', '2021-05-17 16:31:35', '2021-05-17 16:31:35'),
(63, 'read_notifications', 'notifications', '2021-05-17 16:31:35', '2021-05-17 16:31:35'),
(64, 'edit_notifications', 'notifications', '2021-05-17 16:31:35', '2021-05-17 16:31:35'),
(65, 'add_notifications', 'notifications', '2021-05-17 16:31:35', '2021-05-17 16:31:35'),
(66, 'delete_notifications', 'notifications', '2021-05-17 16:31:35', '2021-05-17 16:31:35');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(2, 3),
(3, 1),
(3, 3),
(4, 1),
(4, 3),
(5, 1),
(5, 3),
(6, 1),
(6, 3),
(7, 1),
(7, 3),
(8, 1),
(8, 3),
(9, 1),
(9, 3),
(10, 1),
(10, 3),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(26, 3),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1);

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 5, 'Laravel', 'e973747f4dbdae255a6ebe72d28651329560e4232ab5145712a653a00d98742f', '[\"*\"]', NULL, '2021-05-09 02:44:14', '2021-05-09 02:44:14'),
(2, 'App\\Models\\User', 5, 'Laravel', 'd51f0c77565164a86b5d1003781be32af27b33aaa4f9ef735509faebbe6783f4', '[\"*\"]', NULL, '2021-05-09 02:55:16', '2021-05-09 02:55:16'),
(3, 'App\\Models\\User', 5, 'Laravel', '80cc9a617f55ea5833ddd59fcd9147c20edd480623f8ba3d7c2f2a036b4f97b3', '[\"*\"]', NULL, '2021-05-09 02:55:33', '2021-05-09 02:55:33'),
(4, 'App\\Models\\User', 5, 'Laravel', '5414e58f95f13fc3ca6c33269899f05f29bdcb1fd7ea9d52f7c791d5b6e1a98b', '[\"*\"]', NULL, '2021-05-09 02:55:49', '2021-05-09 02:55:49'),
(5, 'App\\Models\\User', 5, 'Laravel', '3b266e0c2fc220d6a367f8dc9374cafcb70f866071291820cc64d4eeea1741c8', '[\"*\"]', NULL, '2021-05-09 02:56:12', '2021-05-09 02:56:12'),
(6, 'App\\Models\\User', 6, 'Laravel', '45e48b3854e4ab48ee81a401d22882733b3192b9f06cd34a878e16019cd1077e', '[\"*\"]', '2021-05-09 17:51:51', '2021-05-09 15:42:02', '2021-05-09 17:51:51'),
(7, 'App\\Models\\User', 6, 'Laravel', '47746f12767b9059ed3043c767ab04727bfeb0adec158a47f4562e9610ba3651', '[\"*\"]', '2021-05-09 18:09:57', '2021-05-09 18:07:49', '2021-05-09 18:09:57'),
(8, 'App\\Models\\User', 6, 'Laravel', '88d4e38414c8623a09c87d62507de4c8350f067e2415ee6dc494454577dc3fba', '[\"*\"]', '2021-05-09 18:14:37', '2021-05-09 18:13:37', '2021-05-09 18:14:37'),
(9, 'App\\Models\\User', 6, 'Laravel', '9ebed3baac65bb5f21eac2eed6efb827d07819f0917d5795c429dad8c881ae3e', '[\"*\"]', '2021-05-11 09:41:01', '2021-05-09 22:32:43', '2021-05-11 09:41:01'),
(10, 'App\\Models\\User', 6, 'Laravel', '21886f72922412d45774771b6e03c3682e0ae9e8720bf9a58b4656802f7eaf65', '[\"*\"]', NULL, '2021-05-11 09:43:30', '2021-05-11 09:43:30'),
(11, 'App\\Models\\User', 6, 'Laravel', '62943815fec519c9f3c169a5e3542691140ca0dae856037a3f77187f66cf25bc', '[\"*\"]', NULL, '2021-05-11 09:44:21', '2021-05-11 09:44:21'),
(12, 'App\\Models\\User', 6, 'Laravel', '8bd6eb0d0419d41f37579eebf1247a3fa342c29995d03c5d9354f351f0191b84', '[\"*\"]', NULL, '2021-05-11 09:45:07', '2021-05-11 09:45:07'),
(13, 'App\\Models\\User', 6, 'Laravel', '0c97891e41a81d8b52fa806c2144f4dc513af6eaecb02d4d95c220ada46042f5', '[\"*\"]', '2021-05-11 13:35:27', '2021-05-11 09:45:07', '2021-05-11 13:35:27'),
(14, 'App\\Models\\User', 6, 'Laravel', '5e6bba00df165c7b9c20bcccd5af651015e6e29316ef3b89ba5855a041de8355', '[\"*\"]', '2021-05-12 20:12:48', '2021-05-11 13:59:17', '2021-05-12 20:12:48'),
(15, 'App\\Models\\User', 9, 'Laravel', '01e173e61076d2e3a5e559d6f71581e70edbfa5fef18a9c3335e92f888ab541c', '[\"*\"]', '2021-05-12 15:44:17', '2021-05-12 15:36:50', '2021-05-12 15:44:17'),
(16, 'App\\Models\\User', 6, 'Laravel', 'f6bf8e61fa8bc620bf0fd5539586c74cc38dc17a7238559fa4d81229d50138cf', '[\"*\"]', '2021-05-14 17:59:43', '2021-05-12 20:36:26', '2021-05-14 17:59:43'),
(17, 'App\\Models\\User', 9, 'Laravel', '5d4c36c5af146af0d1e341b87ef132ebe5fdd2d21559e0e7f6bd994e0268c1bd', '[\"*\"]', '2021-05-13 16:30:24', '2021-05-13 16:26:51', '2021-05-13 16:30:24'),
(18, 'App\\Models\\User', 6, 'Laravel', '654e3d13052291feda0365389d86853e34947c4d9e742fe994ded5344654e389', '[\"*\"]', '2021-05-16 15:34:20', '2021-05-16 15:07:43', '2021-05-16 15:34:20'),
(19, 'App\\Models\\User', 6, 'Laravel', 'c1d82f1103a8d56f2c4e1eded7df77fb3c945cf1593eb251d4e9819a411b593c', '[\"*\"]', '2021-05-16 15:58:30', '2021-05-16 15:37:28', '2021-05-16 15:58:30'),
(20, 'App\\Models\\User', 6, 'Laravel', '0fdf5552dba0d94e3588747871b223ab75ae16342283f6421d48d4f100f48126', '[\"*\"]', '2021-05-16 16:06:23', '2021-05-16 16:01:30', '2021-05-16 16:06:23'),
(21, 'App\\Models\\User', 6, 'Laravel', '880055b3574b81a1660eeaeadbc5f807028ad1c6fafcc37496f13a9f27decda2', '[\"*\"]', '2021-05-16 20:37:16', '2021-05-16 16:06:50', '2021-05-16 20:37:16'),
(22, 'App\\Models\\User', 6, 'Laravel', '68a1a0ff5ff1635bf0e99eaa546c3c433dfb982f9ba5d53b6d4a714a2ecfcf1f', '[\"*\"]', NULL, '2021-05-16 17:55:23', '2021-05-16 17:55:23'),
(23, 'App\\Models\\User', 6, 'Laravel', '9c7cab561b008ea69f5fdc6a3a1448a328f4f2b88bdb8bd355d2f76fc55c798b', '[\"*\"]', NULL, '2021-05-16 17:55:27', '2021-05-16 17:55:27'),
(24, 'App\\Models\\User', 6, 'Laravel', '859f4351106d55899c4b49673206d5215a783ca357d84f4927cf5db9d40e0c74', '[\"*\"]', '2021-05-16 17:56:05', '2021-05-16 17:55:48', '2021-05-16 17:56:05'),
(25, 'App\\Models\\User', 6, 'Laravel', 'ea48f2d3eeff2ce1190957e24fe283d4f90eb3b4359a86d3e339e892384a88f6', '[\"*\"]', '2021-05-16 17:58:55', '2021-05-16 17:58:45', '2021-05-16 17:58:55'),
(26, 'App\\Models\\User', 6, 'Laravel', '1103393af153badbf1550ecf0a52435a8cc968a390f6ed345aa79e10bee6921c', '[\"*\"]', '2021-05-16 20:48:28', '2021-05-16 20:44:41', '2021-05-16 20:48:28'),
(27, 'App\\Models\\User', 6, 'Laravel', 'fd9784dae56976dd5b5c4b582cf6a818b51d9de9a3c0e00c5142e9cf105eeae0', '[\"*\"]', '2021-05-17 14:47:15', '2021-05-16 20:58:16', '2021-05-17 14:47:15'),
(28, 'App\\Models\\User', 6, 'Laravel', '380fee7b1f80feff05ffe43563fc3ddaddf975e2e21b4b9d8ecd70f4b87a6bc9', '[\"*\"]', '2021-05-16 21:42:53', '2021-05-16 21:42:43', '2021-05-16 21:42:53'),
(29, 'App\\Models\\User', 9, 'Laravel', 'e017c85e4ff56d2eac5bd123938019728006b7ae4cb3bd4f9287e9b4bec7de0e', '[\"*\"]', '2021-06-01 11:12:32', '2021-05-17 10:55:42', '2021-06-01 11:12:32'),
(30, 'App\\Models\\User', 11, 'Laravel', 'db8c9497e3f545007022027269b43b0caacd5943140817bb0a090250333e5abe', '[\"*\"]', '2021-06-01 11:22:09', '2021-05-17 11:14:56', '2021-06-01 11:22:09'),
(31, 'App\\Models\\User', 12, 'Laravel', 'cca41463ffb9344e76d2b3b4548c7fe35f9da52adf9be1ef144629a4149c55d9', '[\"*\"]', '2021-06-30 02:38:21', '2021-05-17 11:32:48', '2021-06-30 02:38:21'),
(32, 'App\\Models\\User', 6, 'Laravel', '98c2f59aa79fe294c9e412c5cbf967e0657f0f1d1f562297292a92f6a7398855', '[\"*\"]', '2021-05-17 15:21:20', '2021-05-17 14:48:04', '2021-05-17 15:21:20'),
(33, 'App\\Models\\User', 6, 'Laravel', '247b3d0735b83039ccf2fc02d194e06807f22fe9e98b6225175dbddef3addc6b', '[\"*\"]', '2021-05-17 18:18:10', '2021-05-17 15:58:55', '2021-05-17 18:18:10'),
(34, 'App\\Models\\User', 14, 'Laravel', '74a2400574933ee12cad2262a585346855f41461a2cbcb41a2b28306d4efbd2a', '[\"*\"]', '2021-05-17 21:02:50', '2021-05-17 20:59:45', '2021-05-17 21:02:50'),
(35, 'App\\Models\\User', 6, 'Laravel', 'c6632fb0ec49d871b05ebaad23e80a4d270907ab8d734f4db5c6a86c34659d46', '[\"*\"]', '2021-05-17 21:20:42', '2021-05-17 21:19:32', '2021-05-17 21:20:42'),
(36, 'App\\Models\\User', 6, 'Laravel', 'dc48de56e385617ec682b45c72bb9418e5ebddbb75398bd43d070e94a87dcc3c', '[\"*\"]', '2021-05-17 23:38:02', '2021-05-17 21:56:59', '2021-05-17 23:38:02'),
(37, 'App\\Models\\User', 15, 'Laravel', 'dba2d2944fa3c6e0d98e2efca1464f768eac544e791289fa862537e709d36c6c', '[\"*\"]', '2021-05-18 10:10:13', '2021-05-17 23:03:20', '2021-05-18 10:10:13'),
(38, 'App\\Models\\User', 16, 'Laravel', '0b6bf48da86d6f11b458ffdfd4133bd5c291e9fd9949ccf795ae7d535e1d8a26', '[\"*\"]', NULL, '2021-05-18 03:10:02', '2021-05-18 03:10:02'),
(39, 'App\\Models\\User', 15, 'Laravel', '22b7704ce39f25f61c395acc9db077ff6cf6a33d59095f9cc62ed12a31ab2dc5', '[\"*\"]', '2021-05-21 09:06:28', '2021-05-18 10:23:31', '2021-05-21 09:06:28'),
(40, 'App\\Models\\User', 6, 'Laravel', '6713c6f2d1ad854d4dcbb093dec1e404cf97e7810607bd71fd0f7610b631781e', '[\"*\"]', '2021-05-20 15:12:16', '2021-05-18 18:44:00', '2021-05-20 15:12:16'),
(41, 'App\\Models\\User', 17, 'Laravel', 'ee7dd7a14e78012864989173dcfe1f88605151a570d4c0ecf4fd2de746b61a64', '[\"*\"]', NULL, '2021-05-18 19:56:40', '2021-05-18 19:56:40'),
(42, 'App\\Models\\User', 6, 'Laravel', '13a064be8721aab6e18aed8e0cd99c990957c640df0bfa252ebc3e6ea64e4d3c', '[\"*\"]', '2021-05-20 16:42:26', '2021-05-20 15:20:17', '2021-05-20 16:42:26'),
(43, 'App\\Models\\User', 6, 'Laravel', 'a4e3cd08e136603f97281dd6a5f837f843ab249163b1eab441c7e9d1a71d0f59', '[\"*\"]', '2021-05-20 18:48:12', '2021-05-20 16:51:10', '2021-05-20 18:48:12'),
(44, 'App\\Models\\User', 6, 'Laravel', '5955c684282e60928f0f18444489fb86a3a1e350e9278f83944a23a6de5f943b', '[\"*\"]', '2021-05-21 11:55:39', '2021-05-20 19:59:31', '2021-05-21 11:55:39'),
(45, 'App\\Models\\User', 6, 'Laravel', 'e888df5de7db42d29c2e1dc0d6be223692905e4c09051fe5579f871cc5cf920f', '[\"*\"]', '2021-05-21 03:21:53', '2021-05-21 03:21:40', '2021-05-21 03:21:53'),
(46, 'App\\Models\\User', 15, 'Laravel', 'fa41ab3d4b9400961251c53e9b445ac200a09a64103a731e1087a82fa5e79a6d', '[\"*\"]', '2021-06-29 10:21:24', '2021-05-21 09:07:18', '2021-06-29 10:21:24'),
(47, 'App\\Models\\User', 9, 'Laravel', '1a33204c8c9c537357ccbeb351e3b1657bea56bd6fa0a36375d91f70cd4af7f3', '[\"*\"]', '2021-05-21 11:32:24', '2021-05-21 09:17:09', '2021-05-21 11:32:24'),
(48, 'App\\Models\\User', 6, 'Laravel', 'a58cf6e82ad47b5242b359feaaf1e2205bcb933b96f93c827317ef1d0dabcd40', '[\"*\"]', '2021-05-21 12:09:22', '2021-05-21 11:58:39', '2021-05-21 12:09:22'),
(49, 'App\\Models\\User', 6, 'Laravel', '4d7dc017af7096d1db11d73952e804ed28769015f12eb93a6ee395afdf86c31c', '[\"*\"]', '2021-05-21 12:05:09', '2021-05-21 12:03:04', '2021-05-21 12:05:09'),
(50, 'App\\Models\\User', 6, 'Laravel', 'c562020cdf1dddf8a0878d3e2b76c1cde662dd4d8ced30eb0375853e77c10c0d', '[\"*\"]', '2021-05-21 12:16:04', '2021-05-21 12:05:32', '2021-05-21 12:16:04'),
(51, 'App\\Models\\User', 6, 'Laravel', 'd4c9156524eb376aaf5f30f3e981d0d2ff1e7fa2fceefdf0125d8acb8fee387d', '[\"*\"]', '2021-05-21 12:17:33', '2021-05-21 12:09:43', '2021-05-21 12:17:33'),
(52, 'App\\Models\\User', 6, 'Laravel', '2bc0a32c7b1fed030ea879efaff77dc50049f72006ab8973624e4e9bc1440608', '[\"*\"]', NULL, '2021-05-21 12:18:12', '2021-05-21 12:18:12'),
(53, 'App\\Models\\User', 6, 'Laravel', '93af848699a132a7f8aa769016c1dab4c73a7db79fec5d0fc619ea4aa96c5926', '[\"*\"]', '2021-05-21 14:49:03', '2021-05-21 12:34:17', '2021-05-21 14:49:03'),
(54, 'App\\Models\\User', 6, 'Laravel', '95c28b7865a4220db38b54eebc8f10102c79138b6292abee3f4bc5feeaddc3d1', '[\"*\"]', '2021-05-21 16:47:26', '2021-05-21 14:50:59', '2021-05-21 16:47:26'),
(55, 'App\\Models\\User', 19, 'Laravel', '1f173d48128eec71ffd5554e5c5663ce8347c88ec48a695ba2118b5c322ff8bb', '[\"*\"]', '2021-05-21 14:52:17', '2021-05-21 14:51:43', '2021-05-21 14:52:17'),
(56, 'App\\Models\\User', 20, 'Laravel', '5b5d4a29da5634cc1795d20f1b1b27e0b909c1292d02ce9fc3b9e612e1f41646', '[\"*\"]', '2021-05-21 15:04:53', '2021-05-21 15:04:11', '2021-05-21 15:04:53'),
(57, 'App\\Models\\User', 21, 'Laravel', '0600b72562621ef04820f9811c61ee79a5127c1eec515b42b95e6a7991f9fab8', '[\"*\"]', '2021-05-21 16:18:43', '2021-05-21 16:17:39', '2021-05-21 16:18:43'),
(58, 'App\\Models\\User', 6, 'Laravel', 'caf7c5b57fb3832a85a930a90c950acb2111791480bd8630c776910d8cf432a5', '[\"*\"]', '2021-05-21 16:28:09', '2021-05-21 16:27:34', '2021-05-21 16:28:09'),
(59, 'App\\Models\\User', 22, 'Laravel', '8e37d30d91b7c7661cb835f234c3007cab19bc11a5aaa8906b7db4625455c44a', '[\"*\"]', '2021-05-27 16:35:35', '2021-05-21 17:02:40', '2021-05-27 16:35:35'),
(60, 'App\\Models\\User', 9, 'Laravel', '1b20df78ea7aa8079e5688d2246c236ec2f7b8aa05a3ece553c885489ad1aacd', '[\"*\"]', '2021-05-21 21:24:53', '2021-05-21 21:11:39', '2021-05-21 21:24:53'),
(61, 'App\\Models\\User', 14, 'Laravel', '03114fe52d0a88e50c10824ec84e509c8ff4a9460820114b9efb80e88ba4ff53', '[\"*\"]', '2021-05-21 21:15:57', '2021-05-21 21:13:34', '2021-05-21 21:15:57'),
(62, 'App\\Models\\User', 14, 'Laravel', 'e3c43227f36b3ee13fc1628e53595cbad6f291a3abf20b3c2098d5ce09143351', '[\"*\"]', NULL, '2021-05-21 21:21:33', '2021-05-21 21:21:33'),
(63, 'App\\Models\\User', 14, 'Laravel', 'e663f08d7e623d292c138d17318903650078e4ac999fb1adc41b2c2f48955b99', '[\"*\"]', NULL, '2021-05-21 21:22:18', '2021-05-21 21:22:18'),
(64, 'App\\Models\\User', 6, 'Laravel', '47803720114b74c7aa0fc9a6de8e2b37ce3cb0cbc0156d3dfccd24a9daf99495', '[\"*\"]', '2021-05-21 21:23:33', '2021-05-21 21:23:09', '2021-05-21 21:23:33'),
(65, 'App\\Models\\User', 14, 'Laravel', 'b3d90744b2385ac3a3fbe513f020321bf9b48ed638358a47c9311693a834182b', '[\"*\"]', NULL, '2021-05-21 21:24:05', '2021-05-21 21:24:05'),
(66, 'App\\Models\\User', 14, 'Laravel', '972f0893540154865fa8415d487bbb7a8609c9d9a30820bc403c04fd3efd21a7', '[\"*\"]', NULL, '2021-05-21 21:24:49', '2021-05-21 21:24:49'),
(67, 'App\\Models\\User', 14, 'Laravel', '43b87621f02d1d8ee31572eb77650f2853b63b17a60625ed1865b211c9d2dd29', '[\"*\"]', '2021-05-23 14:11:47', '2021-05-21 21:25:19', '2021-05-23 14:11:47'),
(68, 'App\\Models\\User', 9, 'Laravel', 'a2ecd933bce1c9f68dc6b08b52eca27e241b5639e1c9c95e6027f5fb2b6e120b', '[\"*\"]', '2021-06-01 11:08:09', '2021-05-21 21:25:32', '2021-06-01 11:08:09'),
(69, 'App\\Models\\User', 6, 'Laravel', '7d6b9786360b8585ac81c114624458cf55187c54ab597b06215fda67e14daf25', '[\"*\"]', '2021-05-27 15:57:55', '2021-05-21 21:33:18', '2021-05-27 15:57:55'),
(70, 'App\\Models\\User', 24, 'Laravel', 'c16f6b2e957cc7c51b692e57b088f3007112c3aa8fda1069bbb70a8e1ebddded', '[\"*\"]', '2021-06-11 00:49:18', '2021-05-22 14:09:04', '2021-06-11 00:49:18'),
(71, 'App\\Models\\User', 23, 'Laravel', 'c0bc257e861a5886aebe91e19124f0536d26b81d479653beb0465736a6c2f3ef', '[\"*\"]', '2021-06-03 02:53:56', '2021-05-22 14:18:06', '2021-06-03 02:53:56'),
(72, 'App\\Models\\User', 25, 'Laravel', 'db1049d5a3ff63a245062e5e74dd3f5b847417ef91dc3a197ae43a5dd9b4b3d1', '[\"*\"]', '2021-05-27 16:54:08', '2021-05-22 14:46:21', '2021-05-27 16:54:08'),
(73, 'App\\Models\\User', 26, 'Laravel', 'd2b54ae7835d00006d47589e0fe16acae9ff8a8b6868af9dc79fa576dfa73794', '[\"*\"]', '2021-06-20 02:40:36', '2021-05-23 03:05:17', '2021-06-20 02:40:36'),
(74, 'App\\Models\\User', 14, 'Laravel', '627c8ba7ad901d116e64051e1ed1dd1f5914ba9cc77b49cce6397a52013203a0', '[\"*\"]', '2021-06-10 11:53:29', '2021-05-23 14:12:14', '2021-06-10 11:53:29'),
(75, 'App\\Models\\User', 27, 'Laravel', 'b0bdd9452524f67d0d51db940d6af3f428ef8e07e1d196a368f51c081b2a4604', '[\"*\"]', '2021-05-27 16:27:23', '2021-05-25 14:56:39', '2021-05-27 16:27:23'),
(76, 'App\\Models\\User', 6, 'Laravel', '379e0c41bc59b7dfda1a9a750e457faf5e3967472a920dd8da0d5e3c55104593', '[\"*\"]', '2021-06-01 02:06:39', '2021-05-27 15:58:54', '2021-06-01 02:06:39'),
(77, 'App\\Models\\User', 30, 'Laravel', '895615c06209c031aa99031432d5e8867d035a86e94133b05445af621f6a1d54', '[\"*\"]', '2021-06-25 17:59:55', '2021-05-27 17:55:00', '2021-06-25 17:59:55'),
(78, 'App\\Models\\User', 31, 'Laravel', 'a2c45c6a7724e765ff46fa0e0296a6b6dcc0677c0b21a63e65e181e7a0dd0ac3', '[\"*\"]', NULL, '2021-05-28 15:17:40', '2021-05-28 15:17:40'),
(79, 'App\\Models\\User', 32, 'Laravel', 'd7f646a3bfca2786775ab8e3672225aa4ea9eef32cad659b278006305b82372b', '[\"*\"]', '2021-05-30 17:16:00', '2021-05-30 17:15:28', '2021-05-30 17:16:00'),
(80, 'App\\Models\\User', 15, 'Laravel', '73a346fde23920d1bffb30a47fa569c37afb7e205bb7113762c49b45ddc5f83e', '[\"*\"]', '2021-06-02 21:28:38', '2021-06-01 11:22:45', '2021-06-02 21:28:38'),
(81, 'App\\Models\\User', 9, 'Laravel', '894e8bdeec4ba0430f59937ba3a85792b7af43a6762aa170e2dd51dd75a1ef4d', '[\"*\"]', '2021-06-04 20:31:29', '2021-06-01 11:32:32', '2021-06-04 20:31:29'),
(82, 'App\\Models\\User', 6, 'Laravel', '9bb8f519c0ee17b6ce68995ede7ed7f5c80b785591d3daded0386dee367665db', '[\"*\"]', NULL, '2021-06-01 21:59:05', '2021-06-01 21:59:05'),
(83, 'App\\Models\\User', 33, 'Laravel', '16d6de685ae5f8a1e24fa7fa2a42c538c6548080ea2cf904a12855b7dee61274', '[\"*\"]', '2021-06-10 13:34:07', '2021-06-02 17:24:31', '2021-06-10 13:34:07'),
(84, 'App\\Models\\User', 6, 'Laravel', '0410a2c5aab4ee8697b220d6cbf6a360addae29d28704e2c3a60232b5429c9ce', '[\"*\"]', '2021-06-03 19:15:32', '2021-06-03 19:14:50', '2021-06-03 19:15:32'),
(85, 'App\\Models\\User', 35, 'Laravel', '8307f451eb9c103fbef5ea98405c706fb8551a3f087c9c194b3dcf0e83eb352e', '[\"*\"]', '2021-06-25 18:05:05', '2021-06-04 09:02:11', '2021-06-25 18:05:05'),
(86, 'App\\Models\\User', 9, 'Laravel', 'b35178b501e5fe279218f45b5c44fa1838a1d13efff9ea3b0fe63c6c2a3d2d75', '[\"*\"]', '2021-06-08 17:08:21', '2021-06-04 20:32:00', '2021-06-08 17:08:21'),
(87, 'App\\Models\\User', 36, 'Laravel', '9b1420ddf87d28873356a4ddfb66f647e40479cb215bcd5aed9e5e0c4a38bd32', '[\"*\"]', '2021-06-10 12:34:57', '2021-06-07 09:06:33', '2021-06-10 12:34:57'),
(88, 'App\\Models\\User', 37, 'Laravel', '4f212b381dfb9739ffb073ad642375e201c604994208466f8ca2d7ec2b480d32', '[\"*\"]', '2021-07-20 22:28:15', '2021-06-07 10:01:05', '2021-07-20 22:28:15'),
(89, 'App\\Models\\User', 6, 'Laravel', '92b454aae04d4983af0fa3b3a91da76a18aa2c438f0a1e7602c247e476f9c6e6', '[\"*\"]', '2021-06-09 12:45:15', '2021-06-07 23:03:33', '2021-06-09 12:45:15'),
(90, 'App\\Models\\User', 39, 'Laravel', '7aa704927367d1d39ae6158d235ef0544932c516668f1fdf60c99718499d4d59', '[\"*\"]', '2021-06-08 03:30:27', '2021-06-08 03:28:53', '2021-06-08 03:30:27'),
(91, 'App\\Models\\User', 40, 'Laravel', '32d5734f0b29752e3a0c2805d03c597b52bc42d2538e4e9acb4fbcc81a412a31', '[\"*\"]', NULL, '2021-06-08 08:51:36', '2021-06-08 08:51:36'),
(92, 'App\\Models\\User', 9, 'Laravel', '6227167852eacba39b5e2b3fec327a024fefea6e4f3a4f92133dad767cc683aa', '[\"*\"]', '2021-06-10 11:57:22', '2021-06-08 17:08:53', '2021-06-10 11:57:22'),
(93, 'App\\Models\\User', 41, 'Laravel', 'cd982d236a2254b58c6770b8c92e61e1d0c14e1b7a14eb93382a0b51d48ab52c', '[\"*\"]', NULL, '2021-06-08 17:29:03', '2021-06-08 17:29:03'),
(94, 'App\\Models\\User', 42, 'Laravel', '5fc0d33d98f1db68a2d083efdde6dc41cd411b01e84a66f9e2a87ec4fb9b90c3', '[\"*\"]', '2021-06-09 11:54:51', '2021-06-09 11:54:20', '2021-06-09 11:54:51'),
(95, 'App\\Models\\User', 6, 'Laravel', '586299c8b8f634bc3adb1656cadef84c131940a18265f45dbbe802dccdd72cd5', '[\"*\"]', '2021-06-10 01:12:28', '2021-06-10 00:23:21', '2021-06-10 01:12:28'),
(96, 'App\\Models\\User', 6, 'Laravel', 'cef3f8804885e3e7629a1cdfeb7b9b311e80ab53cb08b4ee33cf0d5d2a1952dc', '[\"*\"]', NULL, '2021-06-10 01:35:02', '2021-06-10 01:35:02'),
(97, 'App\\Models\\User', 45, 'Laravel', 'f13ccf9cb423072b4614467d80c4c4a38beef5b76e6fbee5d7aa8749e07f7256', '[\"*\"]', '2021-08-10 14:35:23', '2021-06-10 11:55:20', '2021-08-10 14:35:23'),
(98, 'App\\Models\\User', 6, 'Laravel', 'b725600cd1279f00d62bba422e3442f9dd32d770b2cd1ec26b87c639ce162d31', '[\"*\"]', '2021-06-14 17:01:06', '2021-06-10 11:57:55', '2021-06-14 17:01:06'),
(99, 'App\\Models\\User', 36, 'Laravel', '0844a476591bc0e2e980808a59cb9a17d9f83d84e0318a587d7954cd87ee859a', '[\"*\"]', '2021-06-10 13:01:03', '2021-06-10 12:35:58', '2021-06-10 13:01:03'),
(100, 'App\\Models\\User', 9, 'Laravel', 'a2cb84e1ca4b5ced543066f6da411d97a127d46b2e2812f9418990f4e2abd92f', '[\"*\"]', '2021-06-30 22:57:46', '2021-06-10 14:44:42', '2021-06-30 22:57:46'),
(101, 'App\\Models\\User', 36, 'Laravel', '9085fb87a58aa445fd639a678f24693899b830e26660251da1179cfbd5633f57', '[\"*\"]', '2021-07-23 11:27:07', '2021-06-11 10:00:21', '2021-07-23 11:27:07'),
(102, 'App\\Models\\User', 46, 'Laravel', '9fb9a3993658428231e51fda38b5c51b66e904ceddd87b6298bb6c27317f6155', '[\"*\"]', '2021-06-11 16:34:10', '2021-06-11 16:21:49', '2021-06-11 16:34:10'),
(103, 'App\\Models\\User', 47, 'Laravel', '84c741ec7a6ab9df4157b2f5c2b326292534d6f497c394c0579dd833ea317671', '[\"*\"]', '2021-06-12 06:50:23', '2021-06-12 06:49:33', '2021-06-12 06:50:23'),
(104, 'App\\Models\\User', 6, 'Laravel', 'b9b6acba67cfcf37da1796f15ae6aff383d79f6f7199a072ecf4e2f7438c50a0', '[\"*\"]', '2021-06-15 15:45:32', '2021-06-14 19:20:09', '2021-06-15 15:45:32'),
(105, 'App\\Models\\User', 6, 'Laravel', '71648f365aa868090180e5b8147f7c683b8123da19d7fa3329a5ea76b89a503b', '[\"*\"]', '2021-06-15 17:56:32', '2021-06-15 17:53:16', '2021-06-15 17:56:32'),
(106, 'App\\Models\\User', 6, 'Laravel', '0bbd33c47712d0703062ba3b0437e7cc1786ffb8e4fafcd47db32fed020aad6a', '[\"*\"]', NULL, '2021-06-16 03:12:09', '2021-06-16 03:12:09'),
(107, 'App\\Models\\User', 52, 'Laravel', 'c0c2560cd55362b833f5eeebffe55e6a369e8779c3dcf97d2889febb41baa31f', '[\"*\"]', '2021-06-18 01:30:23', '2021-06-18 01:26:35', '2021-06-18 01:30:23'),
(108, 'App\\Models\\User', 54, 'Laravel', '41610c63d05b2fed2e9163164895727923d6ae77f8e8e551552f89cb3f690a18', '[\"*\"]', '2021-06-19 14:43:11', '2021-06-19 14:42:36', '2021-06-19 14:43:11'),
(109, 'App\\Models\\User', 55, 'Laravel', '59a442a56071198ed95ce6f1a410e55991f4bd6c6bfe23a7bf9d0ece77d74ef9', '[\"*\"]', '2021-06-19 15:31:13', '2021-06-19 15:30:06', '2021-06-19 15:31:13'),
(110, 'App\\Models\\User', 56, 'Laravel', 'f9969ca7f929d0bf9851ee1b09257209eac69f9d1ac9f26d01a6457b7c3d5d95', '[\"*\"]', '2021-06-20 16:57:58', '2021-06-19 23:02:09', '2021-06-20 16:57:58'),
(111, 'App\\Models\\User', 58, 'Laravel', 'ea2dbd3761e1ca729f40fb2cbac2f715490e9a806d1175d9d76724594f01695d', '[\"*\"]', '2021-06-20 16:59:53', '2021-06-20 16:57:31', '2021-06-20 16:59:53'),
(112, 'App\\Models\\User', 59, 'Laravel', '5eb0d3e3c2a5883e11cd1231e627a2675ebc3fe25d9089d944a94757d5e4f1fd', '[\"*\"]', '2021-06-25 09:48:19', '2021-06-20 17:28:49', '2021-06-25 09:48:19'),
(113, 'App\\Models\\User', 60, 'Laravel', '2e50b350a9221d5fbc7707176da7ea6a306f341b4bc2999a4b6f2a28e78692e1', '[\"*\"]', '2021-08-12 11:04:53', '2021-06-21 11:04:34', '2021-08-12 11:04:53'),
(114, 'App\\Models\\User', 61, 'Laravel', 'ec91429f7f021ba1157f56e1b49aab295a60783caf9415281b131079c19dd518', '[\"*\"]', NULL, '2021-06-21 22:59:49', '2021-06-21 22:59:49'),
(115, 'App\\Models\\User', 62, 'Laravel', 'c0e3cbd5d57f5e779a615bd72e8179294256885ed7e96047b0317069c98d3472', '[\"*\"]', '2021-06-22 01:07:41', '2021-06-22 00:57:31', '2021-06-22 01:07:41'),
(116, 'App\\Models\\User', 6, 'Laravel', '26d265fe2e886ba1b91e122be4045a6bafd37b58bd4216db013d1c27f9a98744', '[\"*\"]', '2021-06-25 17:05:59', '2021-06-22 02:33:41', '2021-06-25 17:05:59'),
(117, 'App\\Models\\User', 56, 'Laravel', '3817bd7fb064d2ce9c9bd33fd1aff768d8bfb397f030cd4943dfff5388661f60', '[\"*\"]', '2021-08-19 17:57:41', '2021-06-22 12:15:02', '2021-08-19 17:57:41'),
(118, 'App\\Models\\User', 63, 'Laravel', 'bc462dfd1e227f0ff051cd6dd44f0eb0492d8c6ca540a9824d919aaad623f3b2', '[\"*\"]', '2021-06-22 12:37:26', '2021-06-22 12:35:27', '2021-06-22 12:37:26'),
(119, 'App\\Models\\User', 64, 'Laravel', '5d4647ef1aafdaff520424d3b526f66705d9c362083783a498b044a530532686', '[\"*\"]', '2021-06-23 15:14:37', '2021-06-22 15:11:05', '2021-06-23 15:14:37'),
(120, 'App\\Models\\User', 65, 'Laravel', 'a7c84e5196a9a2f7ebc4ccacebf927f5e483489dbc0c415891beb89773876faf', '[\"*\"]', '2021-06-25 18:04:21', '2021-06-23 00:29:22', '2021-06-25 18:04:21'),
(121, 'App\\Models\\User', 69, 'Laravel', '0c9f246d89d30a3b65e533d3f11390ae1b4e214b7adbd9564573fc7d6c54cc2f', '[\"*\"]', NULL, '2021-06-24 10:47:32', '2021-06-24 10:47:32'),
(122, 'App\\Models\\User', 6, 'Laravel', 'c95403bee0ebe44e1b1af311c69052f04795781484aefbcbbdc9d9ccd4740d7e', '[\"*\"]', '2021-06-25 17:58:08', '2021-06-25 17:06:16', '2021-06-25 17:58:08'),
(123, 'App\\Models\\User', 6, 'Laravel', 'e0131c366c35c36dd118032cfebaf9985b70e21d373e4c788a3d859dad0c4142', '[\"*\"]', NULL, '2021-06-25 18:00:13', '2021-06-25 18:00:13'),
(124, 'App\\Models\\User', 15, 'Laravel', '377a68d42383fd313fc70236b38f9555d80070d40b0beea7d4b2bf7218894b08', '[\"*\"]', '2021-08-21 13:00:54', '2021-06-29 12:04:55', '2021-08-21 13:00:54'),
(125, 'App\\Models\\User', 71, 'Laravel', '0b3867432725a2470ccdc5e16a55138520e9cb18689f39e13a9a6baee0cef96b', '[\"*\"]', '2021-06-29 12:40:20', '2021-06-29 12:35:00', '2021-06-29 12:40:20'),
(126, 'App\\Models\\User', 73, 'Laravel', '844e4b5a5c3be03ee807a9b99a112f16fb38a0bdc8d0b01d3f9e51001b13a103', '[\"*\"]', '2021-08-16 20:57:43', '2021-06-30 21:25:13', '2021-08-16 20:57:43'),
(127, 'App\\Models\\User', 74, 'Laravel', '7f90955a64c7519695ee22f444279f4b6e686590d5c2a671c2df079d09ce2a05', '[\"*\"]', '2021-07-01 01:20:38', '2021-07-01 01:20:08', '2021-07-01 01:20:38'),
(128, 'App\\Models\\User', 75, 'Laravel', 'e856f992b2f067f3f4fc9a4c1218b48ca0b167b93ca92f000c6b0e2d9f09b078', '[\"*\"]', '2021-07-04 03:01:42', '2021-07-04 03:01:19', '2021-07-04 03:01:42'),
(129, 'App\\Models\\User', 78, 'Laravel', 'cf9e33ee2aa6b383e5881a6cb3be5244825a4b63441e31e57ab86885fbd32269', '[\"*\"]', '2021-07-04 15:07:11', '2021-07-04 15:06:33', '2021-07-04 15:07:11'),
(130, 'App\\Models\\User', 79, 'Laravel', 'fb69c317f2b2024011eced05d0f30eee709f3a2484491f632729d8a6b09358b5', '[\"*\"]', '2021-07-04 19:27:50', '2021-07-04 19:27:27', '2021-07-04 19:27:50'),
(131, 'App\\Models\\User', 80, 'Laravel', 'e8fe25df849ee49d2dc154eff500ab30c7044258870385dd7c44295b42b12ef5', '[\"*\"]', '2021-07-08 09:06:20', '2021-07-08 09:03:22', '2021-07-08 09:06:20'),
(132, 'App\\Models\\User', 81, 'Laravel', 'c3f1825bd9b55c8e0969c887f85338bc51c424db0850900680ea29473d396ffb', '[\"*\"]', NULL, '2021-07-08 13:46:08', '2021-07-08 13:46:08'),
(133, 'App\\Models\\User', 82, 'Laravel', 'ee097b2b6b0f50e255eb50e8cb1aa2a66d9da139c54d0059398a9bc15b2b4f56', '[\"*\"]', '2021-07-08 23:59:21', '2021-07-08 23:57:50', '2021-07-08 23:59:21'),
(134, 'App\\Models\\User', 83, 'Laravel', '9acd44378eb98efdd8f2b150653b65ca8d3510dd32c726dc21d99cd349641936', '[\"*\"]', '2021-07-09 13:30:24', '2021-07-09 12:52:47', '2021-07-09 13:30:24'),
(135, 'App\\Models\\User', 6, 'Laravel', '107a23ea938bf206b923d032a26237263d44cbfafa18a1318805ff3812c02913', '[\"*\"]', '2021-07-18 17:10:50', '2021-07-11 20:11:49', '2021-07-18 17:10:50'),
(136, 'App\\Models\\User', 84, 'Laravel', '8a46a4927b56a666276045ed13b5509875f41f5b60d7d71e7a3e0af13cc29907', '[\"*\"]', '2021-07-27 16:25:39', '2021-07-12 04:37:56', '2021-07-27 16:25:39'),
(137, 'App\\Models\\User', 85, 'Laravel', '813f0d0c66fab0bb84aa0be1148bbccf6b32cb1c23b682f9a31e660eac23b038', '[\"*\"]', NULL, '2021-07-12 08:47:58', '2021-07-12 08:47:58'),
(138, 'App\\Models\\User', 87, 'Laravel', 'd9916ded1e39ca0da66d3aefb193a51efaae649aaf81922298bad5fc11ef33a1', '[\"*\"]', NULL, '2021-07-13 14:08:38', '2021-07-13 14:08:38'),
(139, 'App\\Models\\User', 89, 'Laravel', 'e0bf910276b8124bedcff080856513fd02492515198ab76c9881d21bb3249991', '[\"*\"]', '2021-07-17 18:36:46', '2021-07-17 18:36:17', '2021-07-17 18:36:46'),
(140, 'App\\Models\\User', 90, 'Laravel', '2e0719402802e288790e452835c6b5b0859138f251f7908aed1324c3a3850808', '[\"*\"]', NULL, '2021-07-18 15:26:56', '2021-07-18 15:26:56'),
(141, 'App\\Models\\User', 91, 'Laravel', '12313213fea29dbc5e5fb492aebe12c2bc995b6887960a5b204ef5dbbeadc708', '[\"*\"]', '2021-07-18 23:41:37', '2021-07-18 17:14:50', '2021-07-18 23:41:37'),
(142, 'App\\Models\\User', 6, 'Laravel', '10ff38ed1b3f6d0766f08a84ea8d269c8e947765ecdb33d7f4c3d734784b88fc', '[\"*\"]', '2021-07-19 14:59:55', '2021-07-18 23:36:18', '2021-07-19 14:59:55'),
(143, 'App\\Models\\User', 91, 'Laravel', 'c71db1b4056505a55f1de653389de04d9a6360c540499176cc4f0f21c70943f1', '[\"*\"]', '2021-07-18 23:45:22', '2021-07-18 23:42:51', '2021-07-18 23:45:22'),
(144, 'App\\Models\\User', 91, 'Laravel', 'edf783fc2127d92a1b880151d097309dfc1684fbded31e89e6f600306de8b3cc', '[\"*\"]', '2021-07-19 20:55:55', '2021-07-18 23:45:54', '2021-07-19 20:55:55'),
(145, 'App\\Models\\User', 6, 'Laravel', '1a2ba2cb18d9780232d14b524b096769dc8e9774f82ab12d1576a8cc5023163f', '[\"*\"]', '2021-07-19 15:52:44', '2021-07-19 15:05:20', '2021-07-19 15:52:45'),
(146, 'App\\Models\\User', 6, 'Laravel', '1b196957ac39c85a706f72244c95e3c983bfa9c2815cac269e9d536041cd608f', '[\"*\"]', '2021-07-19 22:05:51', '2021-07-19 19:40:49', '2021-07-19 22:05:51'),
(147, 'App\\Models\\User', 6, 'Laravel', 'c2012fd253658e289d047b060d7b818e1d97a645100ec071f50e381c8a742826', '[\"*\"]', '2021-07-19 21:05:01', '2021-07-19 20:56:08', '2021-07-19 21:05:01'),
(148, 'App\\Models\\User', 6, 'Laravel', 'ee2a7cea5bd32cc412c44a2a2f156baa65c008705224cee8f0ee73e8cbdec2bc', '[\"*\"]', '2021-07-19 21:23:55', '2021-07-19 21:14:49', '2021-07-19 21:23:55'),
(149, 'App\\Models\\User', 6, 'Laravel', 'f0d0b3dabb94badadbfe09f7317e6e16fbbb20ae6bf6615437a6105d7c76af8a', '[\"*\"]', '2021-07-19 22:03:35', '2021-07-19 21:25:39', '2021-07-19 22:03:35'),
(150, 'App\\Models\\User', 91, 'Laravel', '90955cae40086fdce86e16f643ea93ccaa0216755a524868bdcf5115f378cfc3', '[\"*\"]', '2021-08-17 02:50:13', '2021-07-19 22:04:59', '2021-08-17 02:50:13'),
(151, 'App\\Models\\User', 6, 'Laravel', 'bc2de2d12ab1e963fb0123286e7beed57770b0ce0031eec360a04ce56422cbc1', '[\"*\"]', '2021-07-19 22:09:22', '2021-07-19 22:06:14', '2021-07-19 22:09:22'),
(152, 'App\\Models\\User', 93, 'Laravel', '1bcb28fdc523b9119bf1493638f808d1fb2e08caf5d01472b7b3a3b012384f56', '[\"*\"]', NULL, '2021-07-20 01:31:21', '2021-07-20 01:31:21'),
(153, 'App\\Models\\User', 94, 'Laravel', '2485e7c4e3411d4b3297261f7dc4b0995300b50042a3e09dbc5ba26a96b797e2', '[\"*\"]', '2021-07-20 11:22:25', '2021-07-20 11:16:15', '2021-07-20 11:22:25'),
(154, 'App\\Models\\User', 95, 'Laravel', '83d34f8d02054f5bc2e1f7bb0adfaa9c7210c1969a7a29538fb9054e61709925', '[\"*\"]', '2021-07-21 06:45:13', '2021-07-21 06:42:45', '2021-07-21 06:45:13'),
(155, 'App\\Models\\User', 9, 'Laravel', '67b0bc642f37c61c7ce9c2e0e5515ac45ca66914319be9a15c5ecf0d3e2c4e43', '[\"*\"]', '2021-08-03 19:30:55', '2021-07-21 13:28:32', '2021-08-03 19:30:55'),
(156, 'App\\Models\\User', 96, 'Laravel', 'b445da14ef1c5adcd1c878f1b607e3f3db60714be9805a787311e95bfae3ec47', '[\"*\"]', '2021-07-21 13:49:51', '2021-07-21 13:48:54', '2021-07-21 13:49:51'),
(157, 'App\\Models\\User', 97, 'Laravel', 'dafdfb573d828018aeca4f56e8ef86a2daf3327e61b6c7b8450f2ae8350f00ed', '[\"*\"]', NULL, '2021-07-22 00:19:22', '2021-07-22 00:19:22'),
(158, 'App\\Models\\User', 98, 'Laravel', 'a5230d23ef6091237ce9ecea49c8165e68a0e652a24c24980e42734fb7cc3de9', '[\"*\"]', NULL, '2021-07-23 19:23:27', '2021-07-23 19:23:27'),
(159, 'App\\Models\\User', 100, 'Laravel', '89760eebd0d38e122710752f2260fb099227488288b28d0724e026f736ae5a37', '[\"*\"]', NULL, '2021-07-26 06:13:54', '2021-07-26 06:13:54'),
(160, 'App\\Models\\User', 101, 'Laravel', '5bd186a59c60d97b64ccaff120edcc1f0817e3439aa9a969b2d290b63e222330', '[\"*\"]', '2021-08-11 15:34:36', '2021-07-26 12:19:11', '2021-08-11 15:34:36'),
(161, 'App\\Models\\User', 103, 'Laravel', 'f7b7965d761e8524e5191210d4bfa89f63fb4057f61d2bd01f4c52fe21a830f8', '[\"*\"]', '2021-07-27 01:10:41', '2021-07-27 01:10:17', '2021-07-27 01:10:41'),
(162, 'App\\Models\\User', 105, 'Laravel', '90cc7a300c7f7078631532fddabdcda4f4197bd47f9f37b066a70b1bf5ba25bc', '[\"*\"]', '2021-07-28 18:08:55', '2021-07-28 18:04:37', '2021-07-28 18:08:55'),
(163, 'App\\Models\\User', 107, 'Laravel', '9df7e0077b1ef7f02b37c26fc5ee55aaaff2d765738e6e01f60a9bd7952168fd', '[\"*\"]', NULL, '2021-07-28 20:22:15', '2021-07-28 20:22:15'),
(164, 'App\\Models\\User', 108, 'Laravel', '7c38b20b192e1a996d3121670d0c8d3e1bd61f4ce8aa2e957f65d8bfb8fbc51b', '[\"*\"]', NULL, '2021-07-29 00:35:49', '2021-07-29 00:35:49'),
(165, 'App\\Models\\User', 109, 'Laravel', '237c545ef94ba2f7fe29b26ba223d631f9a787e1a2b402204f6313a642c9870e', '[\"*\"]', '2021-07-29 01:30:51', '2021-07-29 01:29:30', '2021-07-29 01:30:51'),
(166, 'App\\Models\\User', 110, 'Laravel', '2e0fce8efc2ac1c5472346751bb21fae271036218457abfda9af26e7e5207cb2', '[\"*\"]', '2021-07-29 09:29:31', '2021-07-29 09:29:13', '2021-07-29 09:29:31'),
(167, 'App\\Models\\User', 104, 'Laravel', 'b0aa90bb8d4bfb1bab6d6e7e6548778ffa3278effe2778b1bb26720ee8cbeaa1', '[\"*\"]', '2021-07-29 14:29:05', '2021-07-29 14:28:45', '2021-07-29 14:29:05'),
(168, 'App\\Models\\User', 111, 'Laravel', '45d006db88149c88d64d5408819aeb6bce26dcfd97070a2ddae27b52c90068bd', '[\"*\"]', '2021-07-29 19:57:17', '2021-07-29 19:56:35', '2021-07-29 19:57:17'),
(169, 'App\\Models\\User', 114, 'Laravel', '1d7003578afe38c9fbd7bf167e19af0a7a59affd053a96786ab6747a92209d57', '[\"*\"]', '2021-08-01 01:44:26', '2021-07-30 00:55:19', '2021-08-01 01:44:26'),
(170, 'App\\Models\\User', 115, 'Laravel', '9afb2783ae9bb6e84e7732144b89aa8f99494531d92aed2c08710fd463bf8440', '[\"*\"]', '2021-07-30 05:16:00', '2021-07-30 05:11:22', '2021-07-30 05:16:00'),
(171, 'App\\Models\\User', 116, 'Laravel', '953917338437461982315e75fa2b01f3c3faf01fff91be04ecee0da8bde5a449', '[\"*\"]', '2021-08-13 14:32:49', '2021-07-30 08:18:01', '2021-08-13 14:32:49'),
(172, 'App\\Models\\User', 117, 'Laravel', 'dbc1a95d026646451ac1c99aba1af95e599aa9825e9d28f894f4d85a152400f1', '[\"*\"]', '2021-07-30 09:23:21', '2021-07-30 09:22:59', '2021-07-30 09:23:21'),
(173, 'App\\Models\\User', 120, 'Laravel', '2594471197d9d1663784edca7c1418d2a2e44eb83c60421cb2162386b9dccfa9', '[\"*\"]', '2021-07-30 21:12:59', '2021-07-30 21:12:15', '2021-07-30 21:12:59'),
(174, 'App\\Models\\User', 121, 'Laravel', '69b88585a7369979e70125b7938c621b1723f6ab50b2752cc3c6031acea5cc53', '[\"*\"]', '2021-07-31 19:18:03', '2021-07-30 21:51:43', '2021-07-31 19:18:03'),
(175, 'App\\Models\\User', 122, 'Laravel', '9f833c1b8861766c7c53d98f1f31ffcea00cf9651c130f324708457459fcdef1', '[\"*\"]', '2021-07-31 00:56:57', '2021-07-31 00:56:16', '2021-07-31 00:56:57'),
(176, 'App\\Models\\User', 123, 'Laravel', 'b6fd4e61c71739f0b07794214eac24443d7b53f35da47c144865997cdbb4ff3d', '[\"*\"]', '2021-08-06 16:43:51', '2021-07-31 01:46:10', '2021-08-06 16:43:51'),
(177, 'App\\Models\\User', 124, 'Laravel', '09916e956fe7ce6faf9c84bee3e134bf01b06442700821b61aae76df197c1040', '[\"*\"]', '2021-08-04 13:22:31', '2021-07-31 02:33:45', '2021-08-04 13:22:31'),
(178, 'App\\Models\\User', 127, 'Laravel', 'e425da6b75e8a25dc1622d8e24c1bf3af002e1d8678203e8f673a7fab6cc1042', '[\"*\"]', NULL, '2021-07-31 15:59:11', '2021-07-31 15:59:11'),
(179, 'App\\Models\\User', 127, 'Laravel', '6d4ff77c390cf05851213062c5a331b1fa8b7038acc44705501ad463bd8a8595', '[\"*\"]', '2021-07-31 21:49:03', '2021-07-31 15:59:18', '2021-07-31 21:49:03'),
(180, 'App\\Models\\User', 128, 'Laravel', '529f27298f8b4cd7f440e8c32b76fae6954ac8ac8b1b5e3fb8ce78b0652e241f', '[\"*\"]', '2021-07-31 17:33:36', '2021-07-31 17:33:08', '2021-07-31 17:33:36'),
(181, 'App\\Models\\User', 129, 'Laravel', '4f179506068bfd12f9675643172fe2aa3f1d19d9406999a8e128e940a0231074', '[\"*\"]', '2021-08-01 00:09:14', '2021-08-01 00:08:39', '2021-08-01 00:09:14'),
(182, 'App\\Models\\User', 130, 'Laravel', '0af8277415a821e5fdcf823eab2296210709a17234a100bde0743cb4c7f44da5', '[\"*\"]', '2021-08-12 08:14:49', '2021-08-01 00:17:12', '2021-08-12 08:14:49'),
(183, 'App\\Models\\User', 131, 'Laravel', 'd962b1f0066e2149589b354fd65aa133f112842d6bfa15795536eecd1a82cd9b', '[\"*\"]', '2021-08-01 01:39:18', '2021-08-01 01:38:44', '2021-08-01 01:39:18'),
(184, 'App\\Models\\User', 133, 'Laravel', 'd01dce864394509c203a586dd66ac184fa5e4a61c89e04ded8b8b096e3126514', '[\"*\"]', NULL, '2021-08-01 05:16:49', '2021-08-01 05:16:49'),
(185, 'App\\Models\\User', 134, 'Laravel', '14089b36636928a6b8c4da6256ec1d869356adbbf5d967f9d44a5ad1dbc1eb83', '[\"*\"]', '2021-08-01 10:05:46', '2021-08-01 10:03:57', '2021-08-01 10:05:46'),
(186, 'App\\Models\\User', 135, 'Laravel', '574206d16654a50fb380867e2c943cb07a4d5bdf651bc6467453870cc7fbc59e', '[\"*\"]', '2021-08-01 10:32:12', '2021-08-01 10:20:27', '2021-08-01 10:32:12'),
(187, 'App\\Models\\User', 136, 'Laravel', 'd3fadc5cb5c4cd5845ee77b8723cb2f9770ac6354a88c8ae031791a9a935e899', '[\"*\"]', '2021-08-01 15:43:14', '2021-08-01 15:42:54', '2021-08-01 15:43:14'),
(188, 'App\\Models\\User', 138, 'Laravel', '52bc643ed76e8b0813b3249b4491256b3514ab703ea86feccfcb0b17315a3696', '[\"*\"]', '2021-08-02 00:27:54', '2021-08-02 00:24:56', '2021-08-02 00:27:54'),
(189, 'App\\Models\\User', 139, 'Laravel', '418dd7a1864972c66b3c002403e299dd3aa3f988723dcfc20318d20ba45d55cb', '[\"*\"]', '2021-08-02 03:08:01', '2021-08-02 03:07:17', '2021-08-02 03:08:01'),
(190, 'App\\Models\\User', 140, 'Laravel', 'ccef9504d63bb4c98c0a9a14b6e3e9b2afc8dd8b7a52eac988634501cc2b4adb', '[\"*\"]', '2021-08-14 08:17:37', '2021-08-02 06:33:34', '2021-08-14 08:17:37'),
(191, 'App\\Models\\User', 141, 'Laravel', '01b06f51ecca47b8e80b4230bc779d26e2311d4019ebd4bf680183a467da8226', '[\"*\"]', '2021-08-02 10:34:45', '2021-08-02 10:34:14', '2021-08-02 10:34:45'),
(192, 'App\\Models\\User', 142, 'Laravel', 'de25682cac89e712358c4bf25921eee2516fdd47d1631a306287b1d6d379b916', '[\"*\"]', '2021-08-02 12:21:02', '2021-08-02 12:20:14', '2021-08-02 12:21:02'),
(193, 'App\\Models\\User', 143, 'Laravel', '3c6f527d5f9b407999d0e84c25be0576f4b0850ddab0fb468fb58e33ffffb02e', '[\"*\"]', NULL, '2021-08-02 13:35:36', '2021-08-02 13:35:36'),
(194, 'App\\Models\\User', 145, 'Laravel', '1ea726edd29ee3bb3af34ebc6f2dee9d0eea18e8072a8b6acb309b011cf848ca', '[\"*\"]', '2021-08-02 15:58:26', '2021-08-02 15:56:42', '2021-08-02 15:58:26'),
(195, 'App\\Models\\User', 146, 'Laravel', '09a007a8a7940b5c0380ca2b8c0632780c1d6365796c28fcf850e95cb5157b82', '[\"*\"]', '2021-08-03 15:12:04', '2021-08-02 16:29:25', '2021-08-03 15:12:04'),
(196, 'App\\Models\\User', 148, 'Laravel', 'c926bbea1bdabcbbc2ed91dbcefc97537f7f338bd281176c781d8ff87c6bdda6', '[\"*\"]', '2021-08-11 15:19:58', '2021-08-02 21:38:08', '2021-08-11 15:19:58'),
(197, 'App\\Models\\User', 149, 'Laravel', '14a8c7f8ed136e9127ce0039aa09db0b2acd59c9050fb9a54f3393ca42072571', '[\"*\"]', '2021-08-20 11:22:20', '2021-08-03 00:20:45', '2021-08-20 11:22:20'),
(198, 'App\\Models\\User', 150, 'Laravel', '5a8353d85b8deb771b69ea1b310dcf2041cb9f6a3ab10d8d61a5f1b1ac4c9838', '[\"*\"]', '2021-08-03 09:15:38', '2021-08-03 01:27:13', '2021-08-03 09:15:38'),
(199, 'App\\Models\\User', 151, 'Laravel', '1044e15d1aea5b4489e94ba260aaf11eaf4adca561c10cd9f5547c87757b10ed', '[\"*\"]', '2021-08-03 07:40:12', '2021-08-03 07:39:51', '2021-08-03 07:40:12'),
(200, 'App\\Models\\User', 152, 'Laravel', '7bbcafc7e745d6dab6c915493fb557823e44392739c9de936fe19483c8dc5dea', '[\"*\"]', '2021-08-12 21:28:03', '2021-08-03 07:51:30', '2021-08-12 21:28:03'),
(201, 'App\\Models\\User', 144, 'Laravel', '92987c7b336db6a9e3bb29a64b1cd3dcecf6d5161355bbb1b2a661ef7c082859', '[\"*\"]', '2021-08-13 10:32:02', '2021-08-03 11:50:39', '2021-08-13 10:32:02'),
(202, 'App\\Models\\User', 153, 'Laravel', '00e753460be55ddac2f0996e9c7b7994390bfa279c5f3692720d9174bd772cc4', '[\"*\"]', '2021-08-19 21:27:59', '2021-08-03 14:37:53', '2021-08-19 21:27:59'),
(203, 'App\\Models\\User', 154, 'Laravel', '25256d3e51f299f3432ea61d671314aadd26c1864ad5b46aad9464c493077369', '[\"*\"]', '2021-08-03 21:57:40', '2021-08-03 17:10:25', '2021-08-03 21:57:40'),
(204, 'App\\Models\\User', 155, 'Laravel', '01b5b08f234320298127514f76c986280c767d1499eded61281b875d2c19ad81', '[\"*\"]', '2021-08-03 17:50:54', '2021-08-03 17:50:03', '2021-08-03 17:50:54'),
(205, 'App\\Models\\User', 156, 'Laravel', '36947ea75472121f829aaac0f437857b839c1bfdb6581f00efb398cf90955f28', '[\"*\"]', '2021-08-03 18:37:24', '2021-08-03 18:35:22', '2021-08-03 18:37:24'),
(206, 'App\\Models\\User', 158, 'Laravel', '1a26fbb691cf5ef2311f3a39293dc665a7110c4f7a2a2da857eaa176a6ec7c7c', '[\"*\"]', '2021-08-04 08:59:54', '2021-08-04 08:57:54', '2021-08-04 08:59:54'),
(207, 'App\\Models\\User', 159, 'Laravel', 'c5e4ca3efdf30c46bae5d2b0ffa342d63e3435516db7f37d7169869657c258f6', '[\"*\"]', NULL, '2021-08-04 10:26:57', '2021-08-04 10:26:57'),
(208, 'App\\Models\\User', 160, 'Laravel', '1131c793bcc77736fd4256d923ea4947e36746d465f3d7ed8eb86ab280de4d43', '[\"*\"]', '2021-08-04 10:34:36', '2021-08-04 10:31:47', '2021-08-04 10:34:36'),
(209, 'App\\Models\\User', 159, 'Laravel', '207cc3b3051da0df79e67b953150c6eb4ccebd8149be255322b645723d58cf13', '[\"*\"]', '2021-08-04 10:41:37', '2021-08-04 10:38:45', '2021-08-04 10:41:37'),
(210, 'App\\Models\\User', 162, 'Laravel', '932d206f79a2971d872eb18fad92d260f92b982a358bfdefa3a989a81a1967a6', '[\"*\"]', NULL, '2021-08-04 12:39:06', '2021-08-04 12:39:06'),
(211, 'App\\Models\\User', 163, 'Laravel', '14e3d6d75bf8424459728ee401ebd9d562825d65d1ed8b208144c015a565c747', '[\"*\"]', '2021-08-04 12:50:29', '2021-08-04 12:49:25', '2021-08-04 12:50:29'),
(212, 'App\\Models\\User', 164, 'Laravel', '7902b7efa1dea2b3bef3456da9853839ac35587882165c275f125882e886547b', '[\"*\"]', NULL, '2021-08-04 22:04:23', '2021-08-04 22:04:23'),
(213, 'App\\Models\\User', 165, 'Laravel', '314c3e6a0484b0e1f5830b2aedfa4d9340ee5ee86424d5b9c96096c8ee34b64d', '[\"*\"]', '2021-08-04 23:09:01', '2021-08-04 23:07:01', '2021-08-04 23:09:01'),
(214, 'App\\Models\\User', 167, 'Laravel', '8f04f906e40e980a724f83b22f2383921f2b93893795b111b6adcbadf52709be', '[\"*\"]', '2021-08-05 11:09:18', '2021-08-05 11:08:40', '2021-08-05 11:09:18'),
(215, 'App\\Models\\User', 168, 'Laravel', 'e81397ef6e92b97081111e14f562e08fa429e0df017e2e31baff55b551d24b7e', '[\"*\"]', NULL, '2021-08-05 12:33:54', '2021-08-05 12:33:54'),
(216, 'App\\Models\\User', 169, 'Laravel', 'd31e6122a9160ef5721837f4dc0eb8b8c79a887f611a0ce11c9fb173553ad4da', '[\"*\"]', '2021-08-05 12:50:52', '2021-08-05 12:50:09', '2021-08-05 12:50:52'),
(217, 'App\\Models\\User', 170, 'Laravel', '50f22f9f744edad144fd4c86f7d50079e6b88e323e6540ffeb25f06a9c2c0916', '[\"*\"]', '2021-08-05 18:46:45', '2021-08-05 18:45:56', '2021-08-05 18:46:45'),
(218, 'App\\Models\\User', 172, 'Laravel', 'b8f7de869f01956391e41debf86d13dc6cd8c4b590dbed0351a7396b4755de01', '[\"*\"]', '2021-08-06 17:05:48', '2021-08-06 01:52:45', '2021-08-06 17:05:48'),
(219, 'App\\Models\\User', 173, 'Laravel', '0c10ba995e94c1e025252405d8fa0a74a84fbe84f1a6361fa4d1f59e8fbfcc70', '[\"*\"]', NULL, '2021-08-06 05:31:34', '2021-08-06 05:31:34'),
(220, 'App\\Models\\User', 175, 'Laravel', '8ace10259d06655e1544d4e217dd24a7a936c9f14e739600b51f37c4a3cb67ba', '[\"*\"]', '2021-08-06 13:02:11', '2021-08-06 13:01:16', '2021-08-06 13:02:11'),
(221, 'App\\Models\\User', 172, 'Laravel', '7e2e721742cfb669e5327b4509d3517af9718b92351a6d0c0759a2ea96071336', '[\"*\"]', '2021-08-10 11:11:06', '2021-08-06 17:06:30', '2021-08-10 11:11:06'),
(222, 'App\\Models\\User', 171, 'Laravel', '4fe94294584e709d82e637224df9b4a30c266e5e846dbd8357f11bc3eb4af993', '[\"*\"]', '2021-08-06 17:55:19', '2021-08-06 17:53:56', '2021-08-06 17:55:19'),
(223, 'App\\Models\\User', 176, 'Laravel', '37b137d02704bf23cf5fd74c475bda3ef24fd531eef3c5916c181b28c4091701', '[\"*\"]', '2021-08-07 11:50:35', '2021-08-07 00:43:44', '2021-08-07 11:50:35'),
(224, 'App\\Models\\User', 177, 'Laravel', '2c48aadb728461dd9cb5307ed580f88dedee2e26961a8e8bc3ad92ff3d055103', '[\"*\"]', '2021-08-07 01:23:42', '2021-08-07 01:21:52', '2021-08-07 01:23:42'),
(225, 'App\\Models\\User', 178, 'Laravel', 'eea586338ee9058d802513c29a7429717b4ab0abcc2ec8e9ab37621fc5c43a49', '[\"*\"]', '2021-08-07 05:06:58', '2021-08-07 05:06:14', '2021-08-07 05:06:58'),
(226, 'App\\Models\\User', 179, 'Laravel', 'ccabcd3a31aba0419541d4b5607be8960d79beae8dc55ff7bb13cee114e013cb', '[\"*\"]', NULL, '2021-08-07 05:10:36', '2021-08-07 05:10:36'),
(227, 'App\\Models\\User', 181, 'Laravel', 'a48f6b6d3b9d0459158beac3645a5724256486872a8fd6da9b8aec5f54479253', '[\"*\"]', NULL, '2021-08-07 11:28:26', '2021-08-07 11:28:26'),
(228, 'App\\Models\\User', 182, 'Laravel', '2b5bccb5b8243b4d3eb245ba975babe98c560ab37e2ffb1b9807e4ee25ea7e46', '[\"*\"]', NULL, '2021-08-07 13:08:51', '2021-08-07 13:08:51'),
(229, 'App\\Models\\User', 183, 'Laravel', '87ca0a24cfb40ac3d375841a55c9760587cf1bd466402328c0090a8c013d2a1d', '[\"*\"]', '2021-08-15 11:24:49', '2021-08-07 16:31:56', '2021-08-15 11:24:49'),
(230, 'App\\Models\\User', 184, 'Laravel', 'da1c471b134b28957aed75ef5c32d69d0c366b7f63e2eaea564c78f5f5f027c6', '[\"*\"]', NULL, '2021-08-07 19:38:04', '2021-08-07 19:38:04'),
(231, 'App\\Models\\User', 185, 'Laravel', '35cdb080c01b0f898b0fd3df6399e689ee5a0db463d7e00a0ade165ffef7eb0b', '[\"*\"]', '2021-08-08 02:09:03', '2021-08-08 02:08:35', '2021-08-08 02:09:03'),
(232, 'App\\Models\\User', 186, 'Laravel', '8d3a763689cca2ef9f5b57467e7186d4d4119c095f129bd28d1158eed4096893', '[\"*\"]', '2021-08-08 02:25:38', '2021-08-08 02:24:45', '2021-08-08 02:25:38'),
(233, 'App\\Models\\User', 187, 'Laravel', '3b6f1edb83d05776495dc6d10799b3e6716dccd6e254398513d0f8c450b7fb0b', '[\"*\"]', NULL, '2021-08-08 05:16:16', '2021-08-08 05:16:16'),
(234, 'App\\Models\\User', 188, 'Laravel', '44c98e47c771fb2332832a2d4718ef08a9ff15a7b5b28e638f8062b7a9ddd3c5', '[\"*\"]', '2021-08-11 17:37:21', '2021-08-08 12:02:10', '2021-08-11 17:37:21'),
(235, 'App\\Models\\User', 189, 'Laravel', 'd768eb50bac246f2d9a21ebcde414e034d3afbf33d51bc555613195f593cce04', '[\"*\"]', '2021-08-15 20:09:38', '2021-08-08 12:09:15', '2021-08-15 20:09:38'),
(236, 'App\\Models\\User', 190, 'Laravel', '419a45976d16febab418cc7e472862167ff66f2bb0b79d354bffe48bcc7709cf', '[\"*\"]', '2021-08-09 13:22:41', '2021-08-08 13:12:14', '2021-08-09 13:22:41'),
(237, 'App\\Models\\User', 191, 'Laravel', 'c770fdd7aa8bc1c3f8d85a2a7ec015f743c3bc3ce48939a17d7f50d50977ab32', '[\"*\"]', NULL, '2021-08-08 14:06:58', '2021-08-08 14:06:58'),
(238, 'App\\Models\\User', 192, 'Laravel', '86bf9b3c34043a53f3a3b07379eb7f6e2fff678f18bd1a98afd2f23c0a363b37', '[\"*\"]', NULL, '2021-08-08 16:30:43', '2021-08-08 16:30:43'),
(239, 'App\\Models\\User', 193, 'Laravel', '5ef69636fde0eb9bff85f793c2b6f3b4d2b5756f1ce09215977ff7ad8ad8f240', '[\"*\"]', '2021-08-08 21:50:07', '2021-08-08 21:49:38', '2021-08-08 21:50:07'),
(240, 'App\\Models\\User', 195, 'Laravel', '8c80aba4d65bf662d2937bbeab49c836e35b6b8b76d28d236f25244e1311c520', '[\"*\"]', '2021-08-09 00:15:57', '2021-08-09 00:14:45', '2021-08-09 00:15:57'),
(241, 'App\\Models\\User', 196, 'Laravel', 'dfa1761d78b1cf6eb3c1f74e954a53b6668236aafd60edb826ca3a93c2e376b9', '[\"*\"]', '2021-08-09 19:49:40', '2021-08-09 08:43:31', '2021-08-09 19:49:40'),
(242, 'App\\Models\\User', 197, 'Laravel', 'b92bad26e91752accd748c0446433a908a916da15929f81764ffb3dc0bbaef17', '[\"*\"]', '2021-08-09 09:24:13', '2021-08-09 09:23:06', '2021-08-09 09:24:13'),
(243, 'App\\Models\\User', 198, 'Laravel', 'acf6f681ed89c9c58e93cf92969f98ee6add574686079f43f9d77f19cad54cb8', '[\"*\"]', NULL, '2021-08-09 10:44:56', '2021-08-09 10:44:56'),
(244, 'App\\Models\\User', 199, 'Laravel', '14cb38a896f7e0931d46ac820140d32dc53fe5397cd675713169a8eceeb286c2', '[\"*\"]', '2021-08-21 21:28:53', '2021-08-09 11:38:34', '2021-08-21 21:28:53'),
(245, 'App\\Models\\User', 200, 'Laravel', '0796ced4b4a5d6cdc07defc50697c2a7f3129bcd3a6840ddbb727fd2e0708ce3', '[\"*\"]', '2021-08-09 22:49:33', '2021-08-09 22:49:18', '2021-08-09 22:49:33'),
(246, 'App\\Models\\User', 201, 'Laravel', '456591d0b491d0b419eb4c840ff26c38059169bdb820747f253d9af879c92143', '[\"*\"]', '2021-08-10 00:08:30', '2021-08-10 00:06:07', '2021-08-10 00:08:30'),
(247, 'App\\Models\\User', 202, 'Laravel', '7833d6d45dd16bad5da06604b80d683e748f3206a43e95b2af9f49a0df79981a', '[\"*\"]', NULL, '2021-08-10 00:30:59', '2021-08-10 00:30:59'),
(248, 'App\\Models\\User', 203, 'Laravel', 'e5bf40dfa9323fb0e1a379f9339369788dbfe61ce362ddfc3970605ac87eafce', '[\"*\"]', '2021-08-10 01:02:12', '2021-08-10 00:56:22', '2021-08-10 01:02:12'),
(249, 'App\\Models\\User', 205, 'Laravel', '1814567afeab0737adf1682569b67d07908d572ec910d2201a3db720d9b9caa9', '[\"*\"]', '2021-08-10 15:21:55', '2021-08-10 15:01:59', '2021-08-10 15:21:55'),
(250, 'App\\Models\\User', 206, 'Laravel', '969bd498a2b8dad298350842bf9b1e2262f2e74207e7d0c639e16c9bb930119c', '[\"*\"]', NULL, '2021-08-10 15:39:29', '2021-08-10 15:39:29'),
(251, 'App\\Models\\User', 137, 'Laravel', 'cf56f195db3bfb542386a4a243befc6becb3cfc1866a62bfeb10874655f332bf', '[\"*\"]', NULL, '2021-08-10 17:24:36', '2021-08-10 17:24:36'),
(252, 'App\\Models\\User', 207, 'Laravel', '3aeb27c664b763a91e98d4024a106c85955c57479b13aa05c53b566ecca2a441', '[\"*\"]', '2021-08-10 19:41:15', '2021-08-10 19:40:54', '2021-08-10 19:41:15'),
(253, 'App\\Models\\User', 208, 'Laravel', 'cadfbcd1115d21b8dc7b3df90de40fffce8d6314591fec7c8107ba609ade597c', '[\"*\"]', '2021-08-11 02:46:53', '2021-08-11 02:46:30', '2021-08-11 02:46:53'),
(254, 'App\\Models\\User', 209, 'Laravel', 'f18f6c7ae6e398a3c4bb8770ce0bb7c0dffda898c25e922c90b548a2dd3c48f0', '[\"*\"]', '2021-08-15 15:05:40', '2021-08-11 08:50:37', '2021-08-15 15:05:40'),
(255, 'App\\Models\\User', 210, 'Laravel', '35bc4aada113bdd06ffc896edecdd4b612ce84fda68ef3b6bfe3380f7fc9895b', '[\"*\"]', NULL, '2021-08-11 09:51:53', '2021-08-11 09:51:53'),
(256, 'App\\Models\\User', 211, 'Laravel', '86bb54b84d0058325c44949f9a5dd2444817f82cb39871b543ed47c18a3d1278', '[\"*\"]', NULL, '2021-08-11 10:00:48', '2021-08-11 10:00:48'),
(257, 'App\\Models\\User', 212, 'Laravel', '8f885ab279e9582aebe7f6ce39369627c9c2a4fd2aa5667f82d0b24bcd52ef9e', '[\"*\"]', '2021-08-14 03:12:40', '2021-08-12 09:10:41', '2021-08-14 03:12:40'),
(258, 'App\\Models\\User', 214, 'Laravel', '5e9c43ed6e88de09f96f94408d25aea28e8b183816e39f98d05626b0a92f0327', '[\"*\"]', '2021-08-20 22:57:26', '2021-08-12 17:56:13', '2021-08-20 22:57:26'),
(259, 'App\\Models\\User', 215, 'Laravel', 'b4c0d35ca160681bf8c0948a2fafdb3039255ba5e44a06a28d777384b3a978c8', '[\"*\"]', '2021-08-12 19:28:43', '2021-08-12 19:28:16', '2021-08-12 19:28:43'),
(260, 'App\\Models\\User', 217, 'Laravel', 'c5f17441cfebf3b77090bd1d543b6a86e86c4f22ace5684110f39964c0722c5e', '[\"*\"]', '2021-08-13 08:36:09', '2021-08-13 08:35:31', '2021-08-13 08:36:09'),
(261, 'App\\Models\\User', 219, 'Laravel', '161c2b2682e8125baae8c199887c28a17ad25b3422fafac00ece5ce2cb584f5c', '[\"*\"]', NULL, '2021-08-13 10:14:23', '2021-08-13 10:14:23'),
(262, 'App\\Models\\User', 220, 'Laravel', 'ca891b75f6c0c1aa64b4f22a21cbe2aff08ba782e64a85e2445bd061af196731', '[\"*\"]', '2021-08-13 10:45:22', '2021-08-13 10:39:13', '2021-08-13 10:45:22'),
(263, 'App\\Models\\User', 221, 'Laravel', '553d70fac474559f4d69f2e8104b1f7751e08d2740fb7dd6f9d659c1ad0c55b0', '[\"*\"]', '2021-08-13 13:22:07', '2021-08-13 13:21:31', '2021-08-13 13:22:07'),
(264, 'App\\Models\\User', 222, 'Laravel', 'fa664bd1223bd5d0bd6bdcd869ccf6e9bfa89ff0337c55ff3f8223fd9a6d5afc', '[\"*\"]', '2021-08-14 22:51:42', '2021-08-13 17:36:36', '2021-08-14 22:51:42'),
(265, 'App\\Models\\User', 223, 'Laravel', '89269267bc2264d0c569dd940e6a15986f76e168ffec0625e76cd3303c4a6215', '[\"*\"]', '2021-08-13 20:00:53', '2021-08-13 20:00:16', '2021-08-13 20:00:53'),
(266, 'App\\Models\\User', 224, 'Laravel', '2ef6ba8fbfc94a1a30bae84fe5730d0ceb3ba1c8f3d5032fb35649c62fd977c2', '[\"*\"]', NULL, '2021-08-13 21:45:44', '2021-08-13 21:45:44'),
(267, 'App\\Models\\User', 225, 'Laravel', '6935239c1900468b98416a586b41b4a31fafb60955f6493ab32ad00d47f62f2c', '[\"*\"]', '2021-08-17 22:27:56', '2021-08-14 00:57:54', '2021-08-17 22:27:56'),
(268, 'App\\Models\\User', 226, 'Laravel', '220eb01640c1c74098cb25c9f144f5bcd74e02ac1748cf555771a8b4ff1e1529', '[\"*\"]', NULL, '2021-08-14 02:36:12', '2021-08-14 02:36:12'),
(269, 'App\\Models\\User', 227, 'Laravel', '8c3324f531152168346ac65823ae8b4e7b365bd6fa5c8ae4c3ec1d781fd46303', '[\"*\"]', '2021-08-14 12:17:33', '2021-08-14 12:17:05', '2021-08-14 12:17:33');
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(270, 'App\\Models\\User', 228, 'Laravel', 'a0400b781fe3746de30ee61cb926197fb1cf7cf45d5519800a2219c70dff1c40', '[\"*\"]', '2021-08-16 18:34:31', '2021-08-14 13:56:24', '2021-08-16 18:34:31'),
(271, 'App\\Models\\User', 229, 'Laravel', 'ad2f2dc369211a15019704282a36e26b2f14543f88ca8dffb50cddef9a6c188c', '[\"*\"]', '2021-08-14 14:40:05', '2021-08-14 14:36:53', '2021-08-14 14:40:05'),
(272, 'App\\Models\\User', 230, 'Laravel', 'a5afe00df73ffe76344a88639ce3b29b57c7e4942958dc8b1de5a1144154c848', '[\"*\"]', NULL, '2021-08-14 18:46:28', '2021-08-14 18:46:28'),
(273, 'App\\Models\\User', 231, 'Laravel', '3b40f89f2de3df0af140f733ca042c2c985a87fad0b37ce66bed7a4e8a7623c9', '[\"*\"]', '2021-08-16 00:43:59', '2021-08-14 21:51:41', '2021-08-16 00:43:59'),
(274, 'App\\Models\\User', 232, 'Laravel', '6c15855bb9b6986dbd3027097a3749d7d700020588c199f4827af32a594b37a9', '[\"*\"]', '2021-08-16 10:34:55', '2021-08-15 00:29:08', '2021-08-16 10:34:55'),
(275, 'App\\Models\\User', 233, 'Laravel', 'd4fc24813597329ed02bd21ee10d8981a938b1d6b40b8a4f5e11bfe52fc2271c', '[\"*\"]', '2021-08-15 12:24:16', '2021-08-15 01:11:05', '2021-08-15 12:24:16'),
(276, 'App\\Models\\User', 234, 'Laravel', 'b464c34d4d234a50762213299b7c2784eda4e0a238aa7b0598f8291b154fc1ae', '[\"*\"]', NULL, '2021-08-15 03:06:51', '2021-08-15 03:06:51'),
(277, 'App\\Models\\User', 235, 'Laravel', '033dc5cc23ebe92d5c883c125b189f62ccf0dc36bede2646eaac9559137666f5', '[\"*\"]', '2021-08-15 04:08:23', '2021-08-15 04:07:28', '2021-08-15 04:08:23'),
(278, 'App\\Models\\User', 236, 'Laravel', 'a3bb2a11032667b5b5f7bae7e9cc51f65db27784c757dea08935bc9ccfbf9adf', '[\"*\"]', '2021-08-15 09:47:06', '2021-08-15 09:46:33', '2021-08-15 09:47:06'),
(279, 'App\\Models\\User', 237, 'Laravel', '9ba80f1c325a73c4a3160b64701fe2eba8fa1be96acbc49d8040fd58528981da', '[\"*\"]', '2021-08-15 10:29:22', '2021-08-15 10:27:11', '2021-08-15 10:29:22'),
(280, 'App\\Models\\User', 240, 'Laravel', '95a80deb5610566e1bd67b34a3a1e37bc60c90e536d7b3610f6c66d4fc6a39fb', '[\"*\"]', '2021-08-15 23:22:37', '2021-08-15 23:21:23', '2021-08-15 23:22:37'),
(281, 'App\\Models\\User', 242, 'Laravel', 'f5d523d0940fe9559eb50cc3af360d928cdc85a6ee5be62b2fd15ced07d5739b', '[\"*\"]', NULL, '2021-08-16 01:15:13', '2021-08-16 01:15:13'),
(282, 'App\\Models\\User', 243, 'Laravel', '13ddd1ce16c3ad26dd0ebcd6fef7d2c653ccdb7ffb797cf09853aa0a4c173387', '[\"*\"]', NULL, '2021-08-16 03:08:44', '2021-08-16 03:08:44'),
(283, 'App\\Models\\User', 244, 'Laravel', '6ba0b9a0ad67cc4f60ed5f240aee76517d816dbfc74fc61c45c1f57c0395147c', '[\"*\"]', '2021-08-16 06:31:24', '2021-08-16 06:29:12', '2021-08-16 06:31:24'),
(284, 'App\\Models\\User', 244, 'Laravel', '7e4ae5574abb56dd74f1685e785c4c076b93eed8362580dc45331d8204e39794', '[\"*\"]', NULL, '2021-08-16 06:32:10', '2021-08-16 06:32:10'),
(285, 'App\\Models\\User', 245, 'Laravel', '97b25543c745ea79caa77db6d2083c54087de8a1f8d189b369621512957e1b47', '[\"*\"]', NULL, '2021-08-16 07:28:45', '2021-08-16 07:28:45'),
(286, 'App\\Models\\User', 246, 'Laravel', '44605fa34c5ac988334157268990c6e8bf4c96fae1187ae37b1401f295b5bc2b', '[\"*\"]', NULL, '2021-08-16 10:05:39', '2021-08-16 10:05:39'),
(287, 'App\\Models\\User', 73, 'Laravel', 'a9f8568ead9407f748e46f0c9676487cd45903cecdcf6b2431513547865ea4d4', '[\"*\"]', '2021-08-17 13:59:05', '2021-08-16 20:58:12', '2021-08-17 13:59:05'),
(288, 'App\\Models\\User', 241, 'Laravel', '232db0c928622bba02fdd40bbfecd7800c65ed6140867b70296e694d87c85279', '[\"*\"]', '2021-08-16 23:01:50', '2021-08-16 23:01:16', '2021-08-16 23:01:50'),
(289, 'App\\Models\\User', 250, 'Laravel', '3b7ec608f4ec6cf2fb6ceac9d819f461d5493341785c9a3ee7ec977c059afa34', '[\"*\"]', '2021-08-17 02:33:58', '2021-08-17 02:24:06', '2021-08-17 02:33:58'),
(290, 'App\\Models\\User', 255, 'Laravel', '98fc7009e27eb8919ae3a4a3c26166c2a36a4eedce2c7a61fa6d08b7329dc894', '[\"*\"]', '2021-08-17 16:22:08', '2021-08-17 13:20:33', '2021-08-17 16:22:08'),
(291, 'App\\Models\\User', 257, 'Laravel', 'cb6fe8460547347ad568f22b4d9119912d93af701d24e756716fbd2d49b2339c', '[\"*\"]', '2021-08-17 14:28:34', '2021-08-17 14:28:03', '2021-08-17 14:28:34'),
(292, 'App\\Models\\User', 6, 'Laravel', 'a793b967022268054336d2bb37d28c78c01693bc4309aa0038a1b02cef0d38d1', '[\"*\"]', NULL, '2021-08-18 21:48:22', '2021-08-18 21:48:22'),
(293, 'App\\Models\\User', 302, 'Laravel', '490d8ed7ee206c2dbddaaa7a22ea56d1f38d9a2d3d913c4e0acf40bfaa9c9856', '[\"*\"]', '2021-08-21 19:42:37', '2021-08-21 19:42:11', '2021-08-21 19:42:37'),
(294, 'App\\Models\\User', 310, 'Laravel', '864f34ad34058ab28e90647f9efe9ab577bb2c6fa1320191bfae2cce3b3bd018', '[\"*\"]', '2021-08-22 16:04:02', '2021-08-22 15:38:10', '2021-08-22 16:04:02');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2021-05-07 20:29:06', '2021-05-07 20:29:06');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `lawyer_id` bigint(20) UNSIGNED NOT NULL,
  `is_positive` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `user_id`, `order_id`, `lawyer_id`, `is_positive`, `created_at`, `updated_at`) VALUES
(1, 6, 7, 8, 1, '2021-05-14 16:34:19', '2021-05-14 16:34:19'),
(2, 6, 38, 8, 1, '2021-05-14 16:46:37', '2021-05-14 16:46:37'),
(3, 9, 75, 8, 1, '2021-05-17 20:53:43', '2021-05-17 20:53:43'),
(4, 9, 93, 8, 1, '2021-05-21 21:39:34', '2021-05-21 21:39:34'),
(5, 6, 86, 1, 1, '2021-05-21 22:04:05', '2021-05-21 22:04:05'),
(6, 9, 92, 13, 1, '2021-05-27 19:30:21', '2021-05-27 19:30:21'),
(7, 6, 71, 44, 1, '2021-07-11 20:12:46', '2021-07-11 20:12:46'),
(8, 6, 73, 1, 1, '2021-07-11 20:13:03', '2021-07-11 20:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `lawyer_id` bigint(20) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Администратор', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(2, 'user', 'Обычный Пользователь', '2021-05-07 20:26:33', '2021-05-07 20:26:33'),
(3, 'lawyer', 'Юрист', '2021-05-10 12:31:13', '2021-05-10 12:31:13'),
(4, 'counsel', 'Адвокат', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Покупка документа', 3000, NULL, NULL),
(2, 'Покупка консультации', 3000, NULL, '2021-07-22 14:19:44'),
(3, 'Покупка консультации адвоката', 5000, NULL, '2021-08-19 15:08:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Название Сайта', 'Название Сайта', '', 'text', 1, 'Site'),
(2, 'site.description', 'Описание Сайта', 'Описание Сайта', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Логотип Сайта', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Фоновое Изображение для Админки', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Название Админки', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Описание Админки', 'Добро пожаловать в Voyager. Пропавшую Админку для Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Загрузчик Админки', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Иконка Админки', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (используется для панели администратора)', NULL, '', 'text', 1, 'Admin'),
(11, 'app.timer', 'Таймер', '3', NULL, 'text', 6, 'App'),
(12, 'settings.android', 'Версия андроида', '1.0.1', NULL, 'text', 7, 'Settings'),
(13, 'settings.ios', 'Версия apple', '1.0.18', NULL, 'text', 8, 'Settings');

-- --------------------------------------------------------

--
-- Table structure for table `sms_codes`
--

CREATE TABLE `sms_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_codes`
--

INSERT INTO `sms_codes` (`id`, `code`, `user_id`, `created_at`, `updated_at`) VALUES
(3, '6727', 4, '2021-05-08 02:41:24', '2021-05-08 02:41:24'),
(4, '4319', 5, '2021-05-09 02:42:33', '2021-05-09 02:55:02'),
(5, '2799', 6, '2021-05-09 15:41:53', '2021-08-18 21:48:16'),
(6, '3143', 9, '2021-05-12 15:36:32', '2021-08-16 20:58:26'),
(7, '5730', 10, '2021-05-16 22:06:48', '2021-08-19 11:25:24'),
(8, '2062', 11, '2021-05-17 11:14:34', '2021-08-20 10:00:32'),
(9, '8718', 12, '2021-05-17 11:32:21', '2021-05-17 11:32:21'),
(10, '3316', 14, '2021-05-17 20:59:26', '2021-06-10 12:03:17'),
(11, '9943', 15, '2021-05-17 23:03:09', '2021-06-29 12:04:46'),
(12, '8088', 16, '2021-05-18 03:09:52', '2021-05-18 03:09:52'),
(13, '7576', 17, '2021-05-18 19:55:48', '2021-05-18 19:55:48'),
(14, '9231', 18, '2021-05-21 12:31:58', '2021-08-17 03:16:53'),
(15, '7844', 19, '2021-05-21 14:50:43', '2021-05-21 14:50:43'),
(16, '3748', 20, '2021-05-21 15:03:55', '2021-05-21 15:03:55'),
(17, '3695', 21, '2021-05-21 16:17:25', '2021-05-21 16:17:25'),
(18, '4323', 22, '2021-05-21 17:01:15', '2021-05-21 17:02:28'),
(19, '9724', 23, '2021-05-22 14:07:56', '2021-05-22 14:17:48'),
(20, '6078', 24, '2021-05-22 14:08:54', '2021-05-22 14:08:54'),
(21, '4257', 25, '2021-05-22 14:46:11', '2021-05-22 14:46:11'),
(22, '9127', 26, '2021-05-23 03:05:03', '2021-05-23 03:05:03'),
(23, '4710', 27, '2021-05-25 14:56:14', '2021-05-25 14:56:14'),
(24, '1060', 28, '2021-05-26 16:36:25', '2021-05-26 16:36:25'),
(25, '7633', 29, '2021-05-26 16:43:50', '2021-05-26 16:43:50'),
(26, '9312', 30, '2021-05-27 17:54:52', '2021-05-27 17:54:52'),
(27, '1019', 31, '2021-05-28 15:17:19', '2021-05-28 15:17:19'),
(28, '4605', 32, '2021-05-30 17:15:15', '2021-05-30 17:15:15'),
(29, '5270', 33, '2021-06-02 17:24:21', '2021-06-02 17:24:21'),
(30, '5107', 34, '2021-06-04 01:36:10', '2021-06-04 01:36:10'),
(31, '4314', 35, '2021-06-04 09:01:58', '2021-06-04 09:01:58'),
(32, '7475', 36, '2021-06-07 09:06:09', '2021-08-20 10:07:21'),
(33, '3668', 37, '2021-06-07 10:00:55', '2021-06-07 10:00:55'),
(34, '5413', 38, '2021-06-07 21:06:52', '2021-06-07 21:06:52'),
(35, '2207', 39, '2021-06-08 03:28:10', '2021-06-08 03:28:10'),
(36, '6221', 40, '2021-06-08 08:50:41', '2021-06-08 08:50:41'),
(37, '8283', 41, '2021-06-08 17:28:18', '2021-06-08 17:28:18'),
(38, '5835', 42, '2021-06-09 11:54:11', '2021-06-09 11:54:11'),
(39, '2437', 45, '2021-06-10 11:55:08', '2021-08-22 18:45:05'),
(40, '3894', 46, '2021-06-11 16:21:41', '2021-06-11 16:21:41'),
(41, '5498', 47, '2021-06-12 06:49:23', '2021-06-12 06:49:23'),
(42, '6766', 48, '2021-06-13 04:30:40', '2021-06-13 04:30:40'),
(43, '9815', 49, '2021-06-14 13:48:48', '2021-06-14 13:48:48'),
(44, '1826', 50, '2021-06-14 13:54:09', '2021-06-14 13:54:09'),
(45, '5068', 51, '2021-06-17 05:28:39', '2021-06-17 16:24:44'),
(46, '6020', 52, '2021-06-18 01:26:28', '2021-06-18 01:26:28'),
(47, '4142', 53, '2021-06-18 22:46:12', '2021-06-18 22:46:12'),
(48, '8901', 54, '2021-06-19 14:42:24', '2021-06-19 14:42:24'),
(49, '2630', 55, '2021-06-19 15:29:50', '2021-06-19 15:29:50'),
(50, '1047', 56, '2021-06-19 23:01:51', '2021-08-19 18:23:44'),
(51, '7571', 57, '2021-06-20 00:59:45', '2021-06-20 00:59:45'),
(52, '3150', 58, '2021-06-20 16:57:14', '2021-06-20 16:57:14'),
(53, '8579', 59, '2021-06-20 17:28:24', '2021-06-20 17:28:24'),
(54, '4923', 60, '2021-06-21 11:04:26', '2021-06-21 11:04:26'),
(55, '5650', 61, '2021-06-21 22:59:15', '2021-06-21 22:59:15'),
(56, '4992', 62, '2021-06-22 00:57:06', '2021-06-22 00:57:06'),
(57, '4183', 63, '2021-06-22 12:35:12', '2021-06-22 12:35:12'),
(58, '1014', 64, '2021-06-22 15:10:49', '2021-06-22 15:10:49'),
(59, '5733', 65, '2021-06-23 00:22:28', '2021-06-23 00:24:16'),
(60, '1738', 66, '2021-06-23 23:33:23', '2021-06-24 03:34:51'),
(61, '7870', 67, '2021-06-24 03:33:52', '2021-06-24 03:33:52'),
(62, '9242', 68, '2021-06-24 08:25:23', '2021-06-24 08:25:23'),
(63, '3940', 69, '2021-06-24 10:44:29', '2021-06-24 10:47:02'),
(64, '3860', 70, '2021-06-26 16:04:10', '2021-06-26 16:04:10'),
(65, '6657', 71, '2021-06-29 12:31:59', '2021-06-29 12:34:37'),
(66, '7239', 72, '2021-06-29 18:29:40', '2021-06-29 18:29:40'),
(67, '3688', 73, '2021-06-30 21:25:05', '2021-08-16 20:57:56'),
(68, '5754', 74, '2021-07-01 01:19:53', '2021-07-01 01:19:53'),
(69, '4995', 75, '2021-07-04 03:00:59', '2021-07-04 03:00:59'),
(70, '3057', 76, '2021-07-04 14:57:26', '2021-07-04 14:57:26'),
(71, '6915', 77, '2021-07-04 15:00:30', '2021-07-04 15:00:30'),
(72, '3590', 78, '2021-07-04 15:05:35', '2021-07-04 15:05:35'),
(73, '9603', 79, '2021-07-04 19:27:15', '2021-07-04 19:27:15'),
(74, '6857', 80, '2021-07-08 09:03:06', '2021-07-08 09:03:06'),
(75, '4494', 81, '2021-07-08 13:45:55', '2021-07-08 13:45:55'),
(76, '5572', 82, '2021-07-08 23:57:33', '2021-07-08 23:57:33'),
(77, '4904', 83, '2021-07-09 12:52:34', '2021-07-09 12:52:34'),
(78, '5310', 84, '2021-07-12 04:37:49', '2021-07-12 04:37:49'),
(79, '6048', 85, '2021-07-12 08:47:53', '2021-07-12 08:47:53'),
(80, '2207', 86, '2021-07-13 10:50:13', '2021-07-13 10:50:13'),
(81, '6332', 87, '2021-07-13 14:08:25', '2021-07-13 14:08:25'),
(82, '4082', 88, '2021-07-17 13:07:37', '2021-07-17 13:07:37'),
(83, '3205', 89, '2021-07-17 18:35:59', '2021-07-17 18:35:59'),
(84, '6550', 90, '2021-07-18 15:26:38', '2021-07-18 15:26:38'),
(85, '5809', 91, '2021-07-18 17:14:16', '2021-07-19 22:03:45'),
(86, '6889', 92, '2021-07-18 19:13:36', '2021-07-18 19:13:36'),
(87, '9916', 93, '2021-07-20 01:30:56', '2021-07-20 01:30:56'),
(88, '4487', 94, '2021-07-20 11:15:58', '2021-07-20 11:15:58'),
(89, '9550', 95, '2021-07-21 06:42:27', '2021-07-21 06:42:27'),
(90, '6989', 96, '2021-07-21 13:48:38', '2021-07-21 13:48:38'),
(91, '5223', 97, '2021-07-22 00:18:59', '2021-07-22 00:18:59'),
(92, '2896', 98, '2021-07-23 19:23:02', '2021-07-23 19:23:02'),
(93, '1708', 99, '2021-07-24 09:03:12', '2021-07-24 09:03:12'),
(94, '6780', 100, '2021-07-26 06:13:32', '2021-07-26 06:13:32'),
(95, '1598', 101, '2021-07-26 12:18:41', '2021-07-26 12:18:41'),
(96, '3139', 102, '2021-07-27 00:39:14', '2021-07-27 00:39:14'),
(97, '5100', 103, '2021-07-27 01:09:59', '2021-07-27 01:09:59'),
(98, '9264', 104, '2021-07-28 12:29:31', '2021-07-29 14:28:29'),
(99, '1388', 105, '2021-07-28 18:04:23', '2021-07-28 18:09:34'),
(100, '2092', 106, '2021-07-28 20:18:41', '2021-07-28 20:18:41'),
(101, '5414', 107, '2021-07-28 20:22:01', '2021-07-28 20:22:01'),
(102, '7257', 108, '2021-07-29 00:35:29', '2021-07-29 00:35:29'),
(103, '6546', 109, '2021-07-29 01:29:14', '2021-07-29 01:29:14'),
(104, '8004', 110, '2021-07-29 09:28:55', '2021-07-29 09:28:55'),
(105, '1316', 111, '2021-07-29 19:56:11', '2021-07-29 19:56:11'),
(106, '5060', 112, '2021-07-29 20:02:19', '2021-07-29 20:02:19'),
(107, '1310', 113, '2021-07-30 00:43:52', '2021-07-30 01:03:44'),
(108, '7338', 114, '2021-07-30 00:55:05', '2021-07-30 00:55:05'),
(109, '9674', 115, '2021-07-30 05:11:01', '2021-07-30 05:11:01'),
(110, '1785', 116, '2021-07-30 08:17:42', '2021-07-30 08:17:42'),
(111, '8660', 117, '2021-07-30 09:22:42', '2021-07-30 09:22:42'),
(112, '1102', 118, '2021-07-30 15:16:05', '2021-07-30 15:16:05'),
(113, '5205', 119, '2021-07-30 21:09:08', '2021-07-30 21:09:08'),
(114, '5443', 120, '2021-07-30 21:11:58', '2021-07-30 21:11:58'),
(115, '4157', 121, '2021-07-30 21:51:22', '2021-07-30 21:51:22'),
(116, '8099', 122, '2021-07-31 00:56:01', '2021-07-31 00:56:01'),
(117, '3696', 123, '2021-07-31 01:45:48', '2021-07-31 01:45:48'),
(118, '5828', 124, '2021-07-31 02:33:27', '2021-07-31 02:33:27'),
(119, '8283', 125, '2021-07-31 10:14:03', '2021-07-31 10:14:03'),
(120, '6540', 126, '2021-07-31 10:15:19', '2021-07-31 10:15:19'),
(121, '5453', 127, '2021-07-31 15:58:55', '2021-07-31 15:58:55'),
(122, '1151', 128, '2021-07-31 17:32:53', '2021-07-31 17:32:53'),
(123, '6407', 129, '2021-08-01 00:08:24', '2021-08-01 00:08:24'),
(124, '6330', 130, '2021-08-01 00:16:53', '2021-08-01 00:16:53'),
(125, '3418', 131, '2021-08-01 01:37:49', '2021-08-01 01:37:49'),
(126, '4047', 132, '2021-08-01 03:09:42', '2021-08-01 08:41:04'),
(127, '3234', 133, '2021-08-01 05:16:26', '2021-08-01 05:16:26'),
(128, '7807', 134, '2021-08-01 10:03:40', '2021-08-01 10:03:40'),
(129, '6471', 135, '2021-08-01 10:20:12', '2021-08-01 10:20:12'),
(130, '5570', 136, '2021-08-01 15:42:33', '2021-08-01 15:42:33'),
(131, '6693', 137, '2021-08-01 23:15:45', '2021-08-10 17:24:21'),
(132, '9757', 138, '2021-08-02 00:24:44', '2021-08-02 00:24:44'),
(133, '5285', 139, '2021-08-02 03:06:57', '2021-08-02 03:06:57'),
(134, '7755', 140, '2021-08-02 06:33:16', '2021-08-02 06:33:16'),
(135, '1805', 141, '2021-08-02 10:34:08', '2021-08-02 10:34:08'),
(136, '6338', 142, '2021-08-02 12:19:25', '2021-08-02 12:19:25'),
(137, '8911', 143, '2021-08-02 13:35:06', '2021-08-02 13:35:06'),
(138, '9797', 144, '2021-08-02 15:35:32', '2021-08-03 11:50:27'),
(139, '4884', 145, '2021-08-02 15:56:14', '2021-08-02 15:56:14'),
(140, '7838', 146, '2021-08-02 16:29:14', '2021-08-02 16:29:14'),
(141, '8256', 147, '2021-08-02 21:36:27', '2021-08-02 21:36:27'),
(142, '3500', 148, '2021-08-02 21:37:54', '2021-08-02 21:37:54'),
(143, '3384', 149, '2021-08-03 00:20:24', '2021-08-03 00:20:24'),
(144, '4893', 150, '2021-08-03 01:26:24', '2021-08-03 01:26:24'),
(145, '2341', 151, '2021-08-03 07:39:21', '2021-08-03 07:39:21'),
(146, '7476', 152, '2021-08-03 07:51:04', '2021-08-03 07:51:04'),
(147, '4409', 153, '2021-08-03 14:37:39', '2021-08-03 14:37:39'),
(148, '2643', 154, '2021-08-03 17:10:06', '2021-08-03 17:10:06'),
(149, '6434', 155, '2021-08-03 17:49:51', '2021-08-03 17:49:51'),
(150, '4008', 156, '2021-08-03 18:35:01', '2021-08-03 18:35:01'),
(151, '6561', 157, '2021-08-03 20:30:05', '2021-08-03 20:30:05'),
(152, '9544', 158, '2021-08-04 08:57:32', '2021-08-04 08:57:32'),
(153, '2980', 159, '2021-08-04 10:26:31', '2021-08-04 10:38:15'),
(154, '3388', 160, '2021-08-04 10:31:02', '2021-08-04 10:31:02'),
(155, '7751', 161, '2021-08-04 11:14:53', '2021-08-04 11:14:53'),
(156, '6173', 162, '2021-08-04 12:38:44', '2021-08-04 12:38:44'),
(157, '2344', 163, '2021-08-04 12:49:06', '2021-08-04 12:49:06'),
(158, '5855', 164, '2021-08-04 21:56:38', '2021-08-04 22:03:54'),
(159, '4672', 165, '2021-08-04 23:06:41', '2021-08-04 23:06:41'),
(160, '5271', 166, '2021-08-05 08:15:28', '2021-08-05 08:15:28'),
(161, '2164', 167, '2021-08-05 11:08:19', '2021-08-05 11:08:19'),
(162, '9485', 168, '2021-08-05 12:33:32', '2021-08-05 12:33:32'),
(163, '1530', 169, '2021-08-05 12:49:46', '2021-08-05 12:49:46'),
(164, '6528', 170, '2021-08-05 18:45:35', '2021-08-05 18:45:35'),
(165, '5385', 171, '2021-08-06 00:55:44', '2021-08-06 17:53:38'),
(166, '3370', 172, '2021-08-06 01:52:23', '2021-08-06 17:06:06'),
(167, '1349', 173, '2021-08-06 05:31:02', '2021-08-06 05:31:02'),
(168, '1591', 174, '2021-08-06 08:04:10', '2021-08-06 10:04:52'),
(169, '4267', 175, '2021-08-06 13:01:00', '2021-08-06 13:01:00'),
(170, '5897', 176, '2021-08-07 00:43:19', '2021-08-07 00:43:19'),
(171, '3619', 177, '2021-08-07 01:21:38', '2021-08-07 01:21:38'),
(172, '4293', 178, '2021-08-07 05:06:00', '2021-08-07 05:06:00'),
(173, '5258', 179, '2021-08-07 05:10:17', '2021-08-07 05:10:17'),
(174, '2633', 180, '2021-08-07 08:20:11', '2021-08-07 08:20:11'),
(175, '8762', 181, '2021-08-07 11:28:09', '2021-08-07 11:28:09'),
(176, '5977', 182, '2021-08-07 13:08:26', '2021-08-07 13:08:26'),
(177, '9399', 183, '2021-08-07 16:30:56', '2021-08-07 16:30:56'),
(178, '9816', 184, '2021-08-07 19:37:43', '2021-08-07 19:37:43'),
(179, '9615', 185, '2021-08-08 02:08:12', '2021-08-08 02:08:12'),
(180, '4634', 186, '2021-08-08 02:24:29', '2021-08-08 02:24:29'),
(181, '7212', 187, '2021-08-08 05:16:01', '2021-08-08 05:16:01'),
(182, '5703', 188, '2021-08-08 12:01:54', '2021-08-08 12:01:54'),
(183, '4038', 189, '2021-08-08 12:08:56', '2021-08-08 12:08:56'),
(184, '7083', 190, '2021-08-08 13:11:44', '2021-08-08 13:11:44'),
(185, '9814', 191, '2021-08-08 14:06:42', '2021-08-08 14:06:42'),
(186, '9923', 192, '2021-08-08 16:30:07', '2021-08-08 16:30:07'),
(187, '4824', 193, '2021-08-08 21:49:16', '2021-08-08 21:49:16'),
(188, '3747', 194, '2021-08-08 22:30:49', '2021-08-08 22:30:49'),
(189, '5842', 195, '2021-08-09 00:14:01', '2021-08-09 00:14:01'),
(190, '2928', 196, '2021-08-09 08:43:16', '2021-08-09 08:43:16'),
(191, '3399', 197, '2021-08-09 09:22:31', '2021-08-09 09:22:31'),
(192, '9240', 198, '2021-08-09 10:44:47', '2021-08-09 10:44:47'),
(193, '7643', 199, '2021-08-09 11:38:20', '2021-08-09 11:38:20'),
(194, '8539', 200, '2021-08-09 22:47:06', '2021-08-09 22:48:51'),
(195, '5339', 201, '2021-08-10 00:05:42', '2021-08-10 00:05:42'),
(196, '6879', 202, '2021-08-10 00:30:08', '2021-08-10 00:30:08'),
(197, '5950', 203, '2021-08-10 00:56:09', '2021-08-10 00:56:09'),
(198, '5083', 204, '2021-08-10 12:27:13', '2021-08-10 12:27:13'),
(199, '7408', 205, '2021-08-10 15:01:40', '2021-08-10 15:01:40'),
(200, '4440', 206, '2021-08-10 15:39:05', '2021-08-10 15:39:05'),
(201, '5295', 207, '2021-08-10 19:40:32', '2021-08-10 19:40:32'),
(202, '2121', 208, '2021-08-11 02:46:13', '2021-08-11 02:46:13'),
(203, '5888', 209, '2021-08-11 08:50:21', '2021-08-11 08:50:21'),
(204, '9661', 210, '2021-08-11 09:51:40', '2021-08-11 09:51:40'),
(205, '6916', 211, '2021-08-11 10:00:32', '2021-08-11 10:00:32'),
(206, '7875', 212, '2021-08-12 09:10:23', '2021-08-12 09:10:23'),
(207, '2899', 213, '2021-08-12 15:43:54', '2021-08-12 15:43:54'),
(208, '1937', 214, '2021-08-12 17:55:31', '2021-08-12 17:55:31'),
(209, '3129', 215, '2021-08-12 19:27:59', '2021-08-12 19:27:59'),
(210, '1018', 216, '2021-08-12 23:26:29', '2021-08-12 23:26:29'),
(211, '3530', 217, '2021-08-13 08:35:12', '2021-08-13 08:35:12'),
(212, '6347', 218, '2021-08-13 10:09:59', '2021-08-13 12:19:49'),
(213, '9368', 219, '2021-08-13 10:13:55', '2021-08-13 10:13:55'),
(214, '8844', 220, '2021-08-13 10:38:54', '2021-08-13 10:38:54'),
(215, '6976', 221, '2021-08-13 13:21:17', '2021-08-13 13:21:17'),
(216, '5721', 222, '2021-08-13 17:35:35', '2021-08-13 17:35:35'),
(217, '5378', 223, '2021-08-13 20:00:05', '2021-08-13 20:00:05'),
(218, '1711', 224, '2021-08-13 21:45:28', '2021-08-13 21:45:28'),
(219, '1565', 225, '2021-08-14 00:57:13', '2021-08-14 00:57:13'),
(220, '7467', 226, '2021-08-14 02:35:27', '2021-08-14 02:35:27'),
(221, '5788', 227, '2021-08-14 12:16:51', '2021-08-14 12:16:51'),
(222, '9936', 228, '2021-08-14 13:56:06', '2021-08-14 13:56:06'),
(223, '2361', 229, '2021-08-14 14:36:36', '2021-08-14 14:36:36'),
(224, '7959', 230, '2021-08-14 18:46:09', '2021-08-14 18:46:09'),
(225, '1704', 231, '2021-08-14 21:51:19', '2021-08-14 21:51:19'),
(226, '2108', 232, '2021-08-15 00:28:51', '2021-08-15 00:28:51'),
(227, '3283', 233, '2021-08-15 01:10:31', '2021-08-15 01:10:31'),
(228, '1103', 234, '2021-08-15 03:06:39', '2021-08-15 03:06:39'),
(229, '6007', 235, '2021-08-15 04:07:11', '2021-08-15 04:07:11'),
(230, '4057', 236, '2021-08-15 09:46:18', '2021-08-15 09:46:18'),
(231, '1904', 237, '2021-08-15 10:26:52', '2021-08-15 10:26:52'),
(232, '1665', 238, '2021-08-15 19:46:37', '2021-08-15 19:46:37'),
(233, '3096', 239, '2021-08-15 19:49:11', '2021-08-15 19:49:11'),
(234, '3821', 240, '2021-08-15 23:21:08', '2021-08-15 23:21:08'),
(235, '2529', 241, '2021-08-16 00:40:52', '2021-08-16 23:00:47'),
(236, '9226', 242, '2021-08-16 01:14:44', '2021-08-16 01:14:44'),
(237, '8573', 243, '2021-08-16 03:08:13', '2021-08-16 03:08:13'),
(238, '7401', 244, '2021-08-16 06:28:51', '2021-08-16 06:31:52'),
(239, '8338', 245, '2021-08-16 07:28:26', '2021-08-16 07:28:26'),
(240, '2348', 246, '2021-08-16 10:05:00', '2021-08-16 10:05:00'),
(241, '1212', 247, '2021-08-16 22:02:40', '2021-08-16 22:02:40'),
(242, '8381', 248, '2021-08-16 23:49:05', '2021-08-16 23:49:05'),
(243, '4032', 249, '2021-08-16 23:52:37', '2021-08-16 23:52:37'),
(244, '7228', 250, '2021-08-17 02:23:50', '2021-08-17 02:23:50'),
(245, '3802', 251, '2021-08-17 06:03:03', '2021-08-17 06:03:03'),
(246, '8302', 252, '2021-08-17 11:18:53', '2021-08-17 11:19:38'),
(247, '9390', 253, '2021-08-17 11:33:47', '2021-08-17 11:33:47'),
(248, '9820', 254, '2021-08-17 11:51:53', '2021-08-17 11:51:53'),
(249, '6082', 255, '2021-08-17 13:20:11', '2021-08-17 13:20:11'),
(250, '2148', 256, '2021-08-17 14:26:12', '2021-08-17 14:26:12'),
(251, '5570', 257, '2021-08-17 14:27:46', '2021-08-17 14:27:46'),
(252, '8962', 258, '2021-08-18 03:22:32', '2021-08-18 03:22:32'),
(253, '9610', 259, '2021-08-18 08:30:31', '2021-08-18 08:30:31'),
(254, '4772', 260, '2021-08-18 08:32:11', '2021-08-18 08:32:11'),
(255, '8684', 261, '2021-08-18 10:57:29', '2021-08-18 10:57:29'),
(256, '7301', 262, '2021-08-18 13:13:32', '2021-08-18 13:13:32'),
(257, '5325', 263, '2021-08-18 13:38:56', '2021-08-18 13:38:56'),
(258, '1206', 264, '2021-08-18 13:56:59', '2021-08-18 13:56:59'),
(259, '9623', 265, '2021-08-18 13:58:36', '2021-08-18 13:58:36'),
(260, '1862', 266, '2021-08-18 14:50:26', '2021-08-18 19:08:35'),
(261, '5647', 267, '2021-08-19 00:07:01', '2021-08-19 00:07:01'),
(262, '9174', 268, '2021-08-19 00:22:15', '2021-08-19 12:38:18'),
(263, '7570', 269, '2021-08-19 01:07:34', '2021-08-19 01:07:34'),
(264, '7175', 270, '2021-08-19 10:46:35', '2021-08-19 10:46:35'),
(265, '9232', 271, '2021-08-19 11:18:38', '2021-08-19 11:18:38'),
(266, '5862', 272, '2021-08-19 12:21:20', '2021-08-19 20:23:42'),
(267, '7261', 273, '2021-08-19 13:33:55', '2021-08-19 13:41:08'),
(268, '8210', 274, '2021-08-19 13:56:12', '2021-08-19 13:56:12'),
(269, '6561', 275, '2021-08-19 14:06:57', '2021-08-19 14:06:57'),
(270, '5715', 286, '2021-08-19 16:15:10', '2021-08-19 16:15:10'),
(271, '9499', 287, '2021-08-19 18:25:16', '2021-08-19 18:29:30'),
(272, '6225', 288, '2021-08-20 00:28:08', '2021-08-20 00:28:08'),
(273, '2922', 289, '2021-08-20 02:01:43', '2021-08-20 02:01:43'),
(274, '1520', 290, '2021-08-20 02:21:58', '2021-08-20 02:24:07'),
(275, '6711', 291, '2021-08-20 10:11:03', '2021-08-20 13:19:31'),
(276, '9970', 292, '2021-08-20 11:49:22', '2021-08-20 11:49:22'),
(277, '1862', 293, '2021-08-20 11:51:36', '2021-08-20 11:53:47'),
(278, '6412', 294, '2021-08-20 14:34:13', '2021-08-20 14:34:13'),
(279, '7401', 295, '2021-08-20 14:43:14', '2021-08-20 14:43:14'),
(280, '7701', 296, '2021-08-21 00:07:18', '2021-08-21 00:07:18'),
(281, '8498', 297, '2021-08-21 08:06:55', '2021-08-21 08:06:55'),
(282, '1210', 298, '2021-08-21 11:09:42', '2021-08-21 12:48:33'),
(283, '4748', 299, '2021-08-21 11:32:28', '2021-08-21 11:32:28'),
(284, '9493', 300, '2021-08-21 12:48:53', '2021-08-21 12:48:53'),
(285, '5816', 301, '2021-08-21 14:08:27', '2021-08-21 14:08:27'),
(286, '6788', 302, '2021-08-21 19:41:55', '2021-08-21 19:41:55'),
(287, '7884', 303, '2021-08-21 23:54:58', '2021-08-21 23:54:58'),
(288, '8334', 304, '2021-08-21 23:56:59', '2021-08-22 00:01:28'),
(289, '3583', 305, '2021-08-22 00:49:57', '2021-08-22 11:22:40'),
(290, '3814', 306, '2021-08-22 03:13:00', '2021-08-22 17:18:00'),
(291, '4485', 307, '2021-08-22 08:24:00', '2021-08-22 12:21:49'),
(292, '6391', 308, '2021-08-22 13:04:28', '2021-08-22 16:09:35'),
(293, '5009', 309, '2021-08-22 13:37:30', '2021-08-22 13:39:26'),
(294, '6464', 310, '2021-08-22 15:37:53', '2021-08-22 15:37:53'),
(295, '3023', 311, '2021-08-22 21:03:15', '2021-08-22 21:03:15');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(22, 'menu_items', 'title', 13, 'pt', 'Publicações', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(24, 'menu_items', 'title', 12, 'pt', 'Categorias', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(25, 'menu_items', 'title', 14, 'pt', 'Páginas', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2021-05-07 20:29:06', '2021-05-07 20:29:06'),
(31, 'document_categories', 'name', 31, 'kz', 'Талап-арыздар', '2021-06-10 00:30:44', '2021-06-29 14:35:56'),
(32, 'document_categories', 'name', 3, 'kz', 'Өтінішхаттар', '2021-06-10 00:31:02', '2021-06-29 14:36:15'),
(33, 'document_categories', 'name', 5, 'kz', 'Өтініштер', '2021-06-10 00:31:19', '2021-06-29 14:36:45'),
(34, 'document_categories', 'name', 4, 'kz', 'Шағымдар', '2021-06-10 00:31:32', '2021-06-10 00:31:32'),
(35, 'document_categories', 'name', 32, 'kz', 'Хаттар', '2021-06-10 00:31:49', '2021-06-10 00:31:49'),
(36, 'document_categories', 'name', 2, 'kz', 'Шарттар', '2021-06-10 00:32:02', '2021-06-29 14:37:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balance` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `device_token` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `positive_rating` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `negative_rating` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `collegium` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `phone`, `balance`, `email`, `email_verified_at`, `password`, `avatar`, `surname`, `remember_token`, `settings`, `created_at`, `updated_at`, `deleted_at`, `device_token`, `positive_rating`, `negative_rating`, `collegium`) VALUES
(1, 1, 'Администратор', '+77759019555', 0, 'admin@admin.com', NULL, '$2y$10$wpxvNQ9NsM4NGcb8yJwnwey8MltohWTWJ/JQsr5b/l2VrJqVkWPYq', 'users/July2021/fCnLTqMBQ2knzofy0NRH.jpg', NULL, 'oclRAIikB7urfTEwYOLa9LcgEazUln2KrY6pAdcBLWWa24z8lItOsZLuizQU', '{\"locale\":\"ru\"}', '2021-05-07 20:29:06', '2021-08-16 13:42:34', NULL, 'fA75xuv78Yq_vt33PhSeLe:APA91bGkiGkp4bZ3gCC8fZRya0w1OpL0mk9zE_YvYIoj9vRfgSUwcPf6DIDG-OIceK5CFJOHH3pmzISE2agjPuf5QGtAMtTn0w-EoBfTtUGYR70Qr3qB2JbzHaCLagdUBZWOVQQkAOB8', 2, 0, NULL),
(4, 2, '+77073039917', 'асыл', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-08 02:41:24', '2021-05-08 02:41:24', NULL, NULL, 0, 0, NULL),
(5, 2, 'асыл', '+77073039917', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-09 02:42:33', '2021-05-09 02:42:33', NULL, NULL, 0, 0, NULL),
(6, 2, 'Асыл', '+7 (707) 303-99-17', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-09 15:41:53', '2021-08-18 21:48:22', NULL, 'fYRtTD5HDkgsuvHIzGraph:APA91bF53Ds4Rr-QTXs2gYcJBG-uKZkdsTrj6Up-KbC6BsqMWv59QCOZXh21Kjx0HpfJOQDCBAUWqv69WOV_GWK_nzsEg2DbbNuKVQXYy0UQqRRHJxTaW-S68RtNWiXgY9Ao2JVyjbUk', 0, 0, NULL),
(8, 3, 'Есен', '+7 (705) 900-11-90', 0, 'tomboffos@gmail.com', NULL, '$2y$10$6XEqcEyyygnJlAQjN.twdekHxbJ9FTC8WFuj44pPwiNZa5se0z3ie', 'users/default.png', NULL, NULL, '{\"locale\":\"ru\"}', '2021-05-10 12:31:46', '2021-06-15 17:55:21', NULL, 'fdsE9rTK1aoaOhCpFeZAv4:APA91bHWvVClrwo1zuuT4WYdlVEWiIi6CYVDNptQW_pLKZQ0BIhL2ACvRNwsiZbInAyrWbRAQNYMDN-jD4rPRqlTJ2DCLyjZ5UFfEOSlDBCrRbWXNYRmFU3aEWg2UDv06vuqjJlz86OW', 4, 0, NULL),
(9, 2, 'Александр', '+7 (747) 323-30-32', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-12 15:36:32', '2021-07-21 13:28:32', NULL, 'fYbX4A0yS2G5Ez0iNpcT6O:APA91bEXdfJBoW6xXl6JjS0KO1ctJTusAvrOcvL_tBkpyVTZSdBRh_W2rnRVMteomGtEXOVytggCvU4d6EPBVMJ5rrujTATZl8cOxPLxR3nWbVzn5FWzxU3IYDQLBOps5dRF3a6c4jEt', 0, 0, NULL),
(10, 2, 'text', '+7', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-16 22:06:48', '2021-05-16 22:06:48', NULL, NULL, 0, 0, NULL),
(11, 2, 'Аида', '+7 (778) 111-18-19', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-17 11:14:34', '2021-05-17 11:14:56', NULL, 'ehCbisQkQwiCoZ3sMN28Bp:APA91bFc5dscZ6l9OGokR4TiBzGc-DYQlwZp9-_5zo-FYdHNZsJMoRYkDICG_kKm6l2-amYmv2TKgr33nQbe811UWYK4dhtsmuKlwZtJjsFb3V0m8kfGI4B7oAy3r7snfVwcSUvi_opG', 0, 0, NULL),
(12, 2, 'Шах', '+7 (707) 998-25-86', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-17 11:32:21', '2021-05-17 11:32:48', NULL, 'eTbzgzq1TC-6jjK-DTO1qI:APA91bGk9fz9VHs60aDJitza0IcQDCIdpMBPst4JI2urtq5zM1YiTkb9exPA-qDQPwQbhHHFubOccoF1gPSL0Zy0axju2OuQzOxNHgVrH85Iw0ze2GLUd-Kmmi_B78Rt85uaIpWoLKHR', 0, 0, NULL),
(13, 3, 'Дулат Каиржанович', '+77077509262', 0, 'd.valeev82@mail.ru', NULL, '$2y$10$N0aoTHlW/UaCpTFhbbaPkuFvmakha9EqVBRkVHX7SJsadDj9.LwIu', 'users/June2021/hMqpniGXojUAiXq4FLy4.jpeg', NULL, 'IbGzBethoJXKDLnUxpX8BURbBSsDInwSQHNCe9bANB61kL1SYJtI2FDBUG7s', '{\"locale\":\"ru\"}', '2021-05-17 20:44:46', '2021-06-30 12:56:21', NULL, 'd9fR92L3iScJIpiRQP-gO3:APA91bH-U-6eCvBSFUb5jv_pRPaxsll3RR68PapXm4_edjKo-qTkFmq5CmBaV5s0CTxm2koggDztjfrLp7bhk5nsDqCCdOVVQTxL7kV7eWnls7EIgIMl5ZPmU6MZ1PYV88EApk8p7QTI', 1, 0, NULL),
(14, 2, 'Виталий', '+7 (700) 111-20-20', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-17 20:59:26', '2021-05-21 21:21:33', NULL, 'eUSaSv1QQ_KXQGreFn27fV:APA91bHUhtVdq3Tt1NwjIvl4yzZGhrULl1D6DFRuhV_YvduoivZw3HVbO3aVzdcQ-2wBOpnZ5nyK-JTDMlMNLSe99lEciYig1Idxo1nkL5Se9FLcNOJR1OhaXZSBj-VlpZ3J2I4xIMj1', 0, 0, NULL),
(15, 2, 'Аида', '+7 (775) 901-95-55', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-17 23:03:09', '2021-06-29 12:04:55', NULL, 'e2UoEe3bOUq6srIhahjN9N:APA91bH3majcAJPlOQ_OktHdfBFnJW3537-9PTU-Tmu9t0XL2-5Vx7oFMSMKEMnD6_kSf5SYUktdpExuXsWNl7CYuatYWoJ1QNRSMoS23pkYiA7iosyLHnxVEyLi5Hmvy4X9of058nAU', 0, 0, NULL),
(16, 2, 'Александр', '+7 (909) 995-28-62', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-18 03:09:52', '2021-05-18 03:10:02', NULL, 'cu80kfzLSjONYJHNEVUJY_:APA91bGCSZGWLOcpjyem-DxvzCQUnOEL33ZzF5uIJGhGX9w5NdEC9oYVzcp9fV6WYznK1ay4jOsmhLGGc-9RVk5Ks-FkRCmwN2PIKCiaK31TDnairj3VQQwpftdnjfCQQSEcyqfhG8d2', 0, 0, NULL),
(17, 2, 'Сергей', '+7 (953) 560-60-49', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-18 19:55:48', '2021-05-18 19:56:40', NULL, 'dBzKuezTSLOyCR41zBXUbj:APA91bFo9kUWFHk4m6r1_e96OUL0_TkZUTqxH7_enmdlhRwXnfXxyLkVEIVAKkQ2Z_SiVFoHKEuPMbJ5SYCSehP5VChMJMdeYFYLWQxuDW4rGWTAmBDENrsOfUQ7A-NHNQaUMzOEL2R9', 0, 0, NULL),
(18, 2, 'text', '+', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-21 12:31:58', '2021-05-21 12:31:58', NULL, NULL, 0, 0, NULL),
(19, 2, 'Olga', '+7 (905) 758-84-26', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-21 14:50:43', '2021-05-21 14:51:43', NULL, 'fS5G1FfnTYuKntT6XEuQY1:APA91bGj-ZscdSnNuLpIgV4ABZ6dkkX0L0e2Pw_nhFZADIoGhdXdJvA4bcBoMT8brKKf7dv1EzHHNR43Y9JKSxxsyDgEcGdvGfGozu8tK8jP5qjvKuL-XbticWC3rlxinEHQRG00QMpW', 0, 0, NULL),
(20, 2, 'Амангельды', '+7 (708) 709-41-42', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-21 15:03:55', '2021-05-21 15:04:11', NULL, 'eKzCALmESUmuq4gFWw4G40:APA91bFQJainHBYhHB1TQRJACMhAPqq3hCzTp03XRDq_m0BMnv6PqAXYGOAFEJi4wI2MIFw43Ptsh2AbHknweiXmaffu7ChO_mu6SzQwBwtW-NygrEjgukq5SnprvGVXhMhBJ8ILkQLp', 0, 0, NULL),
(21, 2, 'Анатолий', '+7 (708) 518-93-67', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-21 16:17:25', '2021-05-21 16:17:39', NULL, 'f2hyzrGmS0eAtfmUXbLu1D:APA91bFeOqsnmoe_0y51dKUygtbz-3gLJEVmZjy4gN96dtOwZRKNrR-rqK044-jfo2eS7rhXY1DVNh876GewgbW7eDQ2BpXlt_pMGb0idR4a97eW6UkRsdY6aNZhl8PkmVguw2VhBRkv', 0, 0, NULL),
(22, 2, 'Максат', '+7 (747) 955-74-28', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-21 17:01:15', '2021-05-21 17:02:40', NULL, 'e-jJXUyXQ16u1U1M5Usp35:APA91bH9c4vnk9x-SMXB2RnsYLVvvagda-7znj49-JNIsxeB3at1z0uidsgqWc4V0KZ41qrUPa01XckaP-J7-vgS9OjHGkOyHQRc_qsZglFjcHcaHhXJUlAl0IbwabikeGqsnwUbYqgb', 0, 0, NULL),
(23, 2, 'эльмира', '+7 (701) 900-35-53', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-22 14:07:56', '2021-05-22 14:18:06', NULL, 'eQ5Dlg4UJkvTgXmqu6eOlQ:APA91bEYEexxb4OAOaBqR5s42nQunufitdsm9qblv6AB_QPDg4go0pOQ9KEwgCjbnUrcf_ANR24mFlzMjE2UqCQkpTRTTLPSTcbwdd93SUPTvkTDzOR0CVxlDgpLlbI25LXxG5Nbt7Zy', 0, 0, NULL),
(24, 2, 'Эльмира', '+7 (700) 167-65-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-22 14:08:54', '2021-05-22 14:09:04', NULL, 'ewKpSNlHREaYykLAQOc51b:APA91bEpr1nOlpzmO_VC4Su8xNQGMG7ccllR-0C79NNHczgfu7loPsWNRSlFVPUIDNFXyVtJkPqHrM3NXfT_mMbAxkcC_JJrHzBf9x3OuLxSjVuNVtygt7ag42L5jDQ-USazWvDP132B', 0, 0, NULL),
(25, 2, 'Стас', '+7 (747) 190-21-74', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-22 14:46:11', '2021-05-22 14:46:21', NULL, 'c_rualSr6UOAh0G8egOqV1:APA91bHAoG5E-5RMnwKWoU1NAvdE35yIe34CkyKN8h-MSTLwFm2TnS1K4hw455lxh9ttZnjRqAb-tV1bk6wp1uHo7P0Mb3bNJ9__vTwxwxZSS1Wj9p3Og_RCxbhSSDRWfjSppoH9lUSi', 0, 0, NULL),
(26, 2, 'Муса', '+7 (928) 171-71-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-23 03:05:03', '2021-05-23 03:05:17', NULL, 'dkUGWpahE0jGiPcAZQFJi_:APA91bEPzOBJDfObf3lYZoJOSg4YdAz6WWZTAKPPan3DMeN07MurqezO51l2zFWJewkDCf9KY6DokBgUHj21sbiLCyx2xdDRDK8GoWBoJnxpTs6kLNUaz9B8sbe5M00PncJnszoYdaIQ', 0, 0, NULL),
(27, 2, 'Тимофей', '+7 (747) 812-08-14', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-25 14:56:14', '2021-05-25 14:56:39', NULL, 'ehqJKeq8T_O80I5EtAlVwy:APA91bFdb0tKvv6Bvv8LPmUfU3NOvPjAOfPmTNk3APXXLmxxAGeJaZ_sptcLhztnld_IjUP1wVKHVCg8K__oLjFrp-BQPE_FMqwL2rGKdsl62M24lnGH-FH9A7dEBBlNQ8VZKZ9xVNp6', 0, 0, NULL),
(28, 2, 'Ольга', '+0 (800) 099-56-74', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-26 16:36:25', '2021-05-26 16:36:25', NULL, NULL, 0, 0, NULL),
(29, 2, 'Ольга', '+7 (099) 567-41-35', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-26 16:43:50', '2021-05-26 16:43:50', NULL, NULL, 0, 0, NULL),
(30, 2, 'Еркебулан', '+7 (707) 666-00-70', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-27 17:54:52', '2021-05-27 17:55:00', NULL, 'cBxWMKUP9UwGjcZOmBpOg5:APA91bFVVum1ozWQ0J3qTm0PhaiTIFsGInpsr5zgswk7YmDnPuPtpTMfjCPzN4EGrs1vkuRnRAsSg1G4AoF7SWh5y7i7EbcG3vEGLRO8zJRuyap4_aXsvujyE-zkLolABREccDinSq57', 0, 0, NULL),
(31, 2, 'Лев', '+7 (903) 157-53-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-28 15:17:19', '2021-05-28 15:17:40', NULL, 'dOHdSG3cTLGJM5ziCymR58:APA91bHlhTNb-bXVA9hAxeYfc_4UAomdK5BrxoO6r39kwILNRnY14w9jSNWkfe3Gu_EQUVfsCzYK_GV7H87o23_Z6Zf7y-n3fs_2o7mWGh5tdCpSoRjtiU2thc-s_9iDf45_Bie6gAPJ', 0, 0, NULL),
(32, 2, 'Виктория', '+7 (916) 218-90-82', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-05-30 17:15:14', '2021-05-30 17:15:28', NULL, 'cF1a-61NRtOK000f8hDt2q:APA91bGx8wSiRVkhMpckMRQGo34TyTtXR0pYMtA_vg1_AtgWtRFkpH6WIamAgIzspSEEJPKDVZ14xQmnca5UhW08irTtSgXskp3_fqYm4cqngHcRYQDXl1u1vYyQ0uYc-7VxlgeG2KuJ', 0, 0, NULL),
(33, 2, 'Айбек', '+7 (707) 966-80-33', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-02 17:24:21', '2021-06-02 17:24:31', NULL, 'eQ5mtyt3am7iW5MJ-nXhf_:APA91bGSi9pX42JxVDrf25omUzcgLbMpOQtO8KG-mdXSZy7A5MMSIGLpQMgeznkM-yLGX2OjAmRB6-NDjWe7wyHKADHJv54uEWQSJWyypMpi4LvURS_s-GLHEo9MGyAmzcUxCtr-Cszt', 0, 0, NULL),
(34, 2, 'Айнура', '+7 (996) 700-66-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-04 01:36:10', '2021-06-04 01:36:10', NULL, NULL, 0, 0, NULL),
(35, 2, 'Катрин', '+7 (701) 649-56-50', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-04 09:01:58', '2021-06-04 09:02:11', NULL, 'fyI5UHJwRXOy6CNbGKN1I_:APA91bGINiQpCoQkQff5PjGAy-3WBw3JG3wZpd0TsZQPjeZt9Jh8H9BV_9uYZ7hbiraOGVzyasNG8KLovhtMe2TkDeuOeHTMtbIlD-uJu9zvSX9PYss5FEFovFDKPbreV8DiQ4AcX3I0', 0, 0, NULL),
(36, 2, 'Анастасия', '+7 (705) 203-24-79', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-07 09:06:09', '2021-06-07 09:06:33', NULL, 'fiPkdExqS-usHWn8nJopdu:APA91bFRjuMa2XKLWiq9fdeNl9pg-3wLlRWiTxqUcin74voxxzo1knXaCfz1jxmgZDuj-8TMZ63cbt4VtZ5sbEwUwje4gsBKkA6_hGtfs-2As48uTPpAQFNvdN7Skxb9fSMLuE34Ppt7', 0, 0, NULL),
(37, 2, 'Алихан', '+7 (707) 154-10-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-07 10:00:55', '2021-06-07 10:01:05', NULL, 'ezAtjTxpfUp-tYZewmR-ca:APA91bHwkDtscpXOEYx7kZxS97BiT8J-PmkLCcW4Q_Qcvgj5l7azs7FszGzm6y5rInuVwJWkvr5WiOIQpexhjdVVVIZaIFUkEnsRd0VOtXOKwDuTQsO9zuRb4JEohF_5dFwjHr-aEB4H', 0, 0, NULL),
(38, 2, 'Ярослав', '+3 (066) 900-15-58', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-07 21:06:52', '2021-06-07 21:06:52', NULL, NULL, 0, 0, NULL),
(39, 2, 'Елена', '+7 (902) 387-72-14', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-08 03:28:10', '2021-06-08 03:28:53', NULL, 'cC7BwvAYSUixEsGme0bJBd:APA91bH7_8RcwUXXf2RaAI-TT3P9rq85Asa9NLSITMLJ4ymEzd8QFjtqeRiuFPcIAISedpWXrOUWnc1QffJ35XYYGXF1sagyc1SssoR6TSeG5I5cdnJGRJqu_rS62WPf1_9xjkngq1mK', 0, 0, NULL),
(40, 2, 'Денис', '+7 (908) 295-97-36', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-08 08:50:41', '2021-06-08 08:51:36', NULL, 'cc7Qc8NjSBa03pqrPU1ylZ:APA91bE25Kx_0LangBteaKgdO7jPa6U5ZBzmqmwbHA_LGygB4WbLRXt2yRG43SKJhrrUfMAgnNggDT4KXD514VG1LAM-k0GVzlUXIlvpg-C8rNLEHynMtuluUo_R7PH46zWot31PxQj5', 0, 0, NULL),
(41, 2, 'иван', '+7 (961) 711-76-49', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-08 17:28:18', '2021-06-08 17:29:03', NULL, 'ffAoC85GSse62zbNSC4Skb:APA91bHgJFIrGJWrq3_L3bQx7T-ZMdc2cm8LDT5A4I5wQ582n4d5B8SjJ-g8ML9pwAEvaqLlu8rkagQxB6K6jW6lHLrPvhTMbr9rEnTo30C71XJk6cnzrIpnXzyV5rzA50W0gzAY-nqX', 0, 0, NULL),
(42, 2, 'Раиса', '+7 (777) 223-11-77', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-09 11:54:11', '2021-06-09 11:54:20', NULL, 'e6X0ESEDqUf0q6CUEKOR2F:APA91bE1xtPGoWpolVtpXSn7RxH8FJEoRP4aGsO0bi87uXldfqEo4IR4CjVS3KZ9tcTQD4QnAMe13Jdw9EgoCjk_hF73wvBeJM6rF2V0ME-fp6A3ntRE7CjYI2uPqEO6UQo2rX8S0MTU', 0, 0, NULL),
(43, 3, 'Анастасия Константиновна', '+77052032479', 0, 'Anasteisha10@mail.ru', NULL, '$2y$10$VUK.9l8xL1jYvfQgef6pAOKnoQ/ueOGuHTznyODFHSD2eAOAN3XZC', 'users/June2021/gk0tsKAz5smxycOaOgbb.jpeg', 'Филина Анастасия', 'iPSPbMuBdv0L86Z6zq9Dtw550UkaGsAZNgogJe7j7nCn9ES41o6jhv0L1gId', NULL, '2021-06-10 09:19:25', '2021-07-21 11:14:42', NULL, 'dvrzFpi4ISF_mL6-jfVAZI:APA91bGS74oBLq5FA-9Vki6gmpPQO44CQp9Hh885Tgl9-84IFQRSJ5bMvrs40jPfDGVH2JF_AD1V-cMyxzk_jSPCqwentTUmHvC6tddtQLSDVa6quVGII2GcWGgM9xTr89YSw7muHRbR', 0, 0, NULL),
(44, 3, 'Алихан Нурланович', '+77071541045', 0, 'alihrl9@mail.ru', NULL, '$2y$10$lSep9o4hN7BxdnikkEdWg.J253BFPCG19MzbcstuIZ4kfawnFdU4y', 'users/July2021/SFg5Qoudm3D1ZpsLPZRt.jpeg', 'Калдыгожин Алихан', 'xF1Csd2r7zQ8d4WFzzq5VjrEoujTYys0dXd4gs5S0sBn6BRLR0BMCOuq1P9D', NULL, '2021-06-10 09:22:55', '2021-07-11 20:12:46', NULL, 'cUSJXiTffP90Ol_TNKsXBH:APA91bHTdUtWI9lxDDNzZJqCVnRVrJHnBdC9RJIAPCvt-9EmeQpuu9Ee1rhZ9q_6bvB3P0dsaBzHLkZBRnsMXym7vLFun2tzRSWpZIHs6T6xw5eQVev-X6t7TGt-ieqMPzSSdXsP1mog', 1, 0, NULL),
(45, 2, 'ерке', '+7 (777) 138-87-32', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-10 11:55:08', '2021-06-10 11:55:20', NULL, 'dfsRRVEqRFmmfBbIh5VYHM:APA91bEEHoCBtR7J29UbsXJGzZQ3RWxTvg8Zbr9J1is0OQygVgW8bDf0e4F7T0IkQjRuj2oJYJTRJgm5qtib_4thC_X8KXp6XB-dgXrpsopI5_AOXUpfUcMxtvtf7ogXsA-ldfF24QQY', 0, 0, NULL),
(46, 2, 'Гузель', '+7 (702) 256-02-61', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-11 16:21:41', '2021-06-11 16:21:49', NULL, 'fMhQgEfZGEgbh9klwsg6fm:APA91bFjA52Wc9912owdzXBCpfKhQGNZ9co7rkkvDuukMm6xKaxG1NC5zE2t3klxa7yByBj1lz5z7UQnA3FKCTboDyKxHvQ9MOwg0IudOwW59Ir1Vgalk3NN4OS9vYYX5Zz12lr4r_d1', 0, 0, NULL),
(47, 2, 'Светлана', '+7 (908) 999-19-48', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-12 06:49:22', '2021-06-12 06:49:33', NULL, 'ckiAZKXJQHmnR0Bkquu6YI:APA91bHMite8mY-tWg5ueAzGP6cVpqQ4Bi3DHxL5uBVFTzpbIZOhd76z3Zs3lUKtu2FrN351Ww4oFSdWZsJbtCuMNjzlLzjpgEZxCcFrmGtYx7kDKZNyjWyV9i4kJKJoiwX0dWzJgsyJ', 0, 0, NULL),
(48, 2, 'вадим', '+3 (809) 679-56-76', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-13 04:30:40', '2021-06-13 04:30:40', NULL, NULL, 0, 0, NULL),
(49, 2, 'Екатерина', '+3 (809) 806-94-70', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-14 13:48:48', '2021-06-14 13:48:48', NULL, NULL, 0, 0, NULL),
(50, 2, 'Екатерина', '+3 (098) 069-47-04', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-14 13:54:09', '2021-06-14 13:54:09', NULL, NULL, 0, 0, NULL),
(51, 2, 'Анатолий', '+8 (067) 257-62-85', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-17 05:28:39', '2021-06-17 05:28:39', NULL, NULL, 0, 0, NULL),
(52, 2, 'Азамат', '+7 (707) 691-71-20', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-18 01:26:28', '2021-06-18 01:26:35', NULL, 'dMqRJF_PkUzTqiLgVscRg3:APA91bGSsSoXCTaoIEzqZl11dCiGsjCdiSR-5tYsJDi6Us99RC9qQJRXOQuEz1f5v6kBkjE2wC7bIbMqbuNfiLv4A_wvidR9UfBgYhMlfx_piE1pkpqYLrN1rwX_vaQ5tgP_wYNYvFaN', 0, 0, NULL),
(53, 2, 'Маржан', '+7 (775) 337-11-11', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-18 22:46:12', '2021-06-18 22:46:12', NULL, NULL, 0, 0, NULL),
(54, 2, 'Степан', '+7 (964) 903-83-83', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-19 14:42:24', '2021-06-19 14:42:36', NULL, 'ddir44xPQb-CyqIbMmDO1j:APA91bFYLdDeOsM63NZabl9AKnVwxKGtUgsPwozA3_qhwgIxS9UCVlMjVFNt7ggn43cVWu0nRpcfkxDiNB7FT8sL1E5LIroeJ_ITaKpH2QAF6Y5KAaj0luI6mByQeKpo4zVk10oIcf74', 0, 0, NULL),
(55, 2, 'nikolay', '+7 (708) 401-55-49', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-19 15:29:50', '2021-06-19 15:30:06', NULL, 'fkchD9QcRl694pSWS4Q0LW:APA91bGQWYc_4UhqIdQm1ybX4yem9vabSbQmgLRZ9bviVcQ8opqUxyWP562Y3MqdZhuX6eic5ppAxjM2iLK9NzFy1QliVW4xOSA3DqPJYV-plJpqLviC-jRhaOOXUxUv8pJhVhQ-t6jp', 0, 0, NULL),
(56, 2, 'инна', '+7 (747) 880-73-14', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-19 23:01:51', '2021-06-22 12:15:02', NULL, 'fJGJhqNFSN6z2rYTuN5uey:APA91bG6BxTxI5utKaAwIaELdywx4Nny2ec24y0JgpM8ZM1Irvp5LeAt0IaYXsPPQKSnqW-bk7rIUqur8rMiomqk0iiIK2OtxE39EUy421qMEbbNPdE9siLfcVPpT6AQAfHqnRfjOzXR', 0, 0, NULL),
(57, 2, 'Светлана', '+3 (050) 756-56-92', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-20 00:59:45', '2021-06-20 00:59:45', NULL, NULL, 0, 0, NULL),
(58, 2, 'Алексей', '+7 (999) 548-55-04', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-20 16:57:14', '2021-06-20 16:57:31', NULL, 'fo6-89DAStKKgo9eMK9Xlm:APA91bFTtM1SbYHoXwVdUNkwCojtD9fXMF5shOUMVh1H_zJjaMAUVw2M0vU6OU_vK-EJuOBEE8yDOR7iO3eiZV73loPetA3MKVQxj3QI3JU8WMmJPP8Pd-U6HPqPwibgJrIB2fKFULxw', 0, 0, NULL),
(59, 2, 'Ринат', '+7 (700) 111-67-18', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-20 17:28:24', '2021-06-20 17:28:49', NULL, 'ea_M4V7fTUeepyV-dvx2pZ:APA91bHcGU5kbMtlf7Gjd1o5ETkuuKvRNghG81GqJU3NwdBvnxSXwxJmYqDgTMgSHYl_C9mVEymA50zgLI57qoPfmAdaB_lED9JLYBt9J2tf0S05L1esTrJ7RElOLJprZuMBD7YlOMs4', 0, 0, NULL),
(60, 2, 'Бота', '+7 (702) 069-27-23', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-21 11:04:26', '2021-06-21 11:04:34', NULL, 'fE7zL87_pUVUq0Fce3wAMU:APA91bGjwvYpEp3FP0v27YSZ7pXawePbYKtgODbWdReG92GL0nr1KpfAo_qAPs2iOt4ARHJA4YcpHPY3-5Bz05pOyrmJGUHpWgtPH1UkAffbIiMa_fZK75v0kOsyC8w41GT3wPtZLB6Y', 0, 0, NULL),
(61, 2, 'Алексей', '+7 (968) 562-06-48', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-21 22:59:15', '2021-06-21 22:59:49', NULL, 'eZnnrfTOSTu7zcgLYpc9Th:APA91bGWU9bXuUdl47Og19NHk-CZVKlzYTVmfcWROaymlJeDkyh1Jla5DQZHv2xgmUWYVJfEYpEd73_pZCPKe6p0Z7MZiPGwGBqB7FrAIBC0nk7u_U_XI3yR-pqxy4WXr8zVbK3l9TnM', 0, 0, NULL),
(62, 2, 'Наталия', '+7 (904) 458-55-89', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-22 00:57:06', '2021-06-22 00:57:31', NULL, 'cB8LU7HqS1ypC8KtQVRXHL:APA91bEozz5eRQlIAtMElYXPKyfR331TYsWmN6uRmORncg7TTDpCjlFyGRnIidcvoWxb6Oqrtb-0AHfH78DYAdOefY5wbkn_7uY5aLwxkDdtT-9krI7dNMmyFx5YGW-xqLO2Zz1nLn1A', 0, 0, NULL),
(63, 2, 'Дмитрий', '+7 (777) 306-01-16', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-22 12:35:12', '2021-06-22 12:35:27', NULL, 'fhU9bSBPQoyoK-OcJJjW_N:APA91bFZ1n07pYcmgri2CkyLrEngbwQNy2XOs6f8ramc5Loax9rYdrJUuTGzqcyXqMNu3oODf-pNavYy5Vs4fOmfwRjPCOLHyWsySyOQleHmQS-w1oljT863bntdVxds-blIv-jtxIDU', 0, 0, NULL),
(64, 2, 'ЖарасТарас', '+7 (775) 362-34-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-22 15:10:49', '2021-06-22 15:11:05', NULL, 'euoQgiznRXi8gUp4NlpC4R:APA91bF-8iOjcjLpSGtiQZi9OyrURQYxHKdXRT7cZF5Oi-TLWq4IFJVI4ir58y-zC5HcgSv6ig5SoZ92VlFBRXDUt45P5uhXr01ser9x9kazSdFKLCWnwF8tDRi9eWFL38MItU7sY9Fu', 0, 0, NULL),
(65, 2, 'Мейрим', '+7 (747) 228-79-68', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-23 00:22:28', '2021-06-23 00:29:22', NULL, 'fM4pVsnRmUjvpAohvJjdtT:APA91bFMO-isJ5mpDrE26ylztGV3sSRkB8WIuttuXxXXDPRvK82OErZZI0S_7Bh9ek3ha1Sm9Xy6PbQ7ggyAV8fteX3o3EdJTbX3dogb5Sb-Hm87R2RDBHmSm8zHm-BNlju0ASW51GAo', 0, 0, NULL),
(66, 2, 'Martin Bhatia', '+7 (255) 255-25-52', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-23 23:33:23', '2021-06-23 23:33:23', NULL, NULL, 0, 0, NULL),
(67, 2, 'Veronica', '+7 (255) 255-25-55', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-24 03:33:52', '2021-06-24 03:33:52', NULL, NULL, 0, 0, NULL),
(68, 2, 'Ольга', '+7 (987) 565-55-52', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-24 08:25:23', '2021-06-24 08:25:23', NULL, NULL, 0, 0, NULL),
(69, 2, 'Александр', '+7 (923) 149-42-40', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-24 10:44:29', '2021-06-24 10:47:32', NULL, 'dEJbpZ5lSuiDW9uFYN_b_Z:APA91bFmD_xcz5f833NUA2yVaSri-FBxxqEvvnG4pqe-YPKQR8HG4kvFh5siD2oNWVOamiL7Lm39-Rstcn11ZqDmzhsrx4FPezmTucJvDEnV4Ytw4Eh4s_RJvkS1xr-oaD1-3uOAqBYG', 0, 0, NULL),
(70, 2, 'Максим', '+3 (093) 226-30-44', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-26 16:04:10', '2021-06-26 16:04:10', NULL, NULL, 0, 0, NULL),
(71, 2, 'Константин', '+7 (747) 464-75-59', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-29 12:31:59', '2021-06-29 12:35:00', NULL, 'fplYs3sJTIi-mDXQMki-1t:APA91bHDRXESbuF9ncbWGtd_ea1ZJvxusxBuURti8_MCHt9U6uTQO9JngsbSTWQYTdK5XORHa0PZGWhy4RQFhi_ZhG3PiMhRERcrdD-uZD8aJE_hnDiJbBrCRNWLOBTQPoOePKEkdAbH', 0, 0, NULL),
(72, 2, 'марта', '+7 (918) 754-66-18', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-29 18:29:40', '2021-06-29 18:29:40', NULL, NULL, 0, 0, NULL),
(73, 2, 'Тимур', '+7 (702) 977-37-37', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-06-30 21:25:05', '2021-06-30 21:25:13', NULL, 'egdmLcC4P0aKiYhPbxJScK:APA91bF1DAHwkq4aOd0GwWBW8yN-wKjlFHuFwsFaTmCNGGKKQcGVES7nyPCvmQENof_cE_JB53_kWlCctCNUCx8lLawdJtX3WRAbYCTt1gP71Fb-QxUIvTDyPGSsG4rQwYnswFYO4GFR', 0, 0, NULL),
(74, 2, 'анастасия', '+7 (996) 463-94-48', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-01 01:19:53', '2021-07-01 01:20:08', NULL, 'c5r5ArD1QJOllzuY8HgVCc:APA91bE4HS_170liwUnvo6Phzo2WBgcjXbfFKCaSYZj4ud_lTQlPz8Hnl8SUOUCdCe8hdWbav8Q9OfcUZlkmcF4WbQxXTi0ViW6bBuKfpHVAL7kKMVd2WcitfnpDeAhIqRW_cvBFrToT', 0, 0, NULL),
(75, 2, 'Александр', '+7 (701) 332-43-55', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-04 03:00:59', '2021-07-04 03:01:19', NULL, 'chDjolq-QgCgFNXQTyG6qi:APA91bGTUmzIA_JsGxaKwRSxYmqH-_EVfVJ26de2yXZBIKXL_U4EGeW8O-_X0H_jUchHnIFtXQsiIXLiLazSrG_CDc61eEpTU3z9H4zhBmqQ_k-z_MlEjNMQZ0y5ULDzIpolwczKx9qm', 0, 0, NULL),
(76, 2, 'Вадим', '+7 (295) 342-36-7', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-04 14:57:26', '2021-07-04 14:57:26', NULL, NULL, 0, 0, NULL),
(77, 2, 'Вадим', '+7 (295) 534-23-67', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-04 15:00:30', '2021-07-04 15:00:30', NULL, NULL, 0, 0, NULL),
(78, 2, 'Вадим', '+7 (918) 296-57-81', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-04 15:05:35', '2021-07-04 15:06:33', NULL, 'dC95T6TSR7ymb2FxVmk-Kd:APA91bEC9iKTVBdl8mE5X-3HTnlmVd1lJioKfF7vwuEFdG-DTJ_hANXkFuaKiOiiE1plh6CNnpugDSGL0auVhPLo-94mhONf7o4lr2EQHRTT9CpLn80sH4YZHkJ1iXBRVQHBGjAx78SP', 0, 0, NULL),
(79, 2, 'Анна', '+7 (905) 003-39-00', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-04 19:27:15', '2021-07-04 19:27:27', NULL, 'ep728RQTSM-vLlZmCuTq6y:APA91bGrJvYIg9GTVHoDLeJDAtnCyGNQQ5Dy1Gi_uoy49nteQo4Io-4DO0Psu16SVhAnkpjMly5Pykpqt_YN5IG9crsPcP7xekwBF10u5HJ0h1R0VYOMzBsVwaRsDvpjmbZKWHxbVqoD', 0, 0, NULL),
(80, 2, 'Асел', '+7 (707) 585-53-11', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-08 09:03:06', '2021-07-08 09:03:22', NULL, 'fk1dBy-CR-u7-Fbp_cVAWI:APA91bGC6OUAdlja9Hhcm7oj0BkSIDdVOfYwksPy2zxw3V3BLvvISLBKyNTdvgdcqZZB32Eu-OHwxYBzO8m7-iFou-RC42R1vqdKbRD22BKrizgn3iRjnmQ9yF1Pfmf7RZgwmohpWztK', 0, 0, NULL),
(81, 2, 'Анастасия', '+7 (771) 133-64-61', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-08 13:45:55', '2021-07-08 13:46:08', NULL, 'ci7wwx3kTHScfHH1XFtgaZ:APA91bEHDj5o_LecYD77Y2T2nF0N7sn7vA97uqRkqdBCqOCy8H8tSStS26FiXj0IQCtxiBPUHybbgQAIaC1XK0AaEfUR1b6nAaSmIulPiXNv6RYFgPvsiAUUOmFvEJNBumYwm2CzY1_e', 0, 0, NULL),
(82, 2, 'Дмитрий', '+7 (705) 194-30-19', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-08 23:57:33', '2021-07-08 23:57:50', NULL, 'deuPD4X9QOSkopWm_DSPa2:APA91bEUqiPWcfFbzCNtIv2o8U9L6mUd2JCJ_-Wugh8OBMl2qYvSXbhykVXwpqFc_9ONnCn8xvT8ZVAUxKdwDdFGD48Q4MOojkNLPEQIzBJx9S-p9o_l5osXVRkER5I-6YzNlc4wAO2t', 0, 0, NULL),
(83, 2, 'Светлана', '+7 (701) 410-39-88', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-09 12:52:34', '2021-07-09 12:52:47', NULL, 'dGpZ5L5kQza-t8olrR8p7C:APA91bHKid7H70BLyOQHB_f7ZLMYZB9F9abOWsLx69zbeZ9xDtreZFQTIlJ-8ex_z4ECble8XvMWRE87qWr6cvIl8NINMONF0ESWYd-A--HPrd_q9_uH-Ju1P-cmf6xJvdI6PVx7nbsH', 0, 0, NULL),
(84, 2, 'Жансая', '+7 (777) 922-88-77', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-12 04:37:49', '2021-07-12 04:37:56', NULL, 'dmRMskR-QUFfkFqpLGBmmf:APA91bFCyEOyRGePmO8N8qBib-lLUB9y1z2-HqHEgNvLS3s7PqgV4vHJimADm8XL_Q42PoT7-KlfeAtppAA4FR3nAFhjw-jUWHmV-mFiY2gszMsBSP73ZvRffrqhxCVoyHWZyqVaMdyw', 0, 0, NULL),
(85, 2, 'Маша', '+7 (999) 208-63-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-12 08:47:53', '2021-07-12 08:47:58', NULL, 'eCnD1F2S-kbhms2JfrTRqf:APA91bGInBVrCUcKp9j0AGwS7Fp6DJs9g3V4_gnJts8SdGzM-dIdKOj2UxJfIKIZiVxVvIqyYW21OmDSgUmgBHxxLsoPg3KjKHBZvtAQzDsJs8LU29pwT47XwE9j5AQfJEZ5xOIrx3fx', 0, 0, NULL),
(86, 2, 'Бахыт', '+7 (707) 124-11-62', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-13 10:50:13', '2021-07-13 10:50:13', NULL, NULL, 0, 0, NULL),
(87, 2, 'павел', '+7 (776) 294-60-10', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:08:25', '2021-07-13 14:08:38', NULL, 'crhNh8AiSu-ao1qWazyclC:APA91bH9T8IKNi9wdLVr_v68POKVOBDKKQUzW2rHRm250HnTmV2H2bAdyUvOm3cmOy0_Xd3mhbcHSjE2dPDXXUOwKCztmfN7irlZLmuPn3vYJ0pzJ8TZEo9kQyYgeR1mO6HlfKladWzj', 0, 0, NULL),
(88, 2, 'Антоненко Ольга Оттона', '+7 (701) 312-89-44', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-17 13:07:37', '2021-07-17 13:07:37', NULL, NULL, 0, 0, NULL),
(89, 2, 'Ерлан', '+7 (701) 622-77-88', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-17 18:35:59', '2021-07-17 18:36:17', NULL, 'cQcQzTvDikPLgoFGzjav1y:APA91bFCIxJEwCafpmiBZx3XnZjmmSe_3ljoEqGPmbC2XnGDxwgomTftGb3NQ90u8NRck3_w39R7HuHe29KTw9ttQ9TV9JpkNumI9m-qaj73Y_zYCx0bpYa3ab7or1SqdbcTGUNlpTmX', 0, 0, NULL),
(90, 2, 'Ирина', '+7 (910) 575-55-80', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-18 15:26:38', '2021-07-18 15:26:56', NULL, 'fLhwC8D0QiytDBYSG4aLxv:APA91bEklcVYDWWngFuGXNGQ2U6AOeaQxC2A6sYLzfq-OuPsKAQJ__fOdcRlCh9F0m06RoYQtcNMp8Qx7OZlpPC3ksHXe_ohZKkB8GKwRfEV5-hcE0zxMOUgo6j9A9n9RF5KvYCpSwCV', 0, 0, NULL),
(91, 3, 'Асыл адвокат', '+7 (707) 303-99-16', 44000, 'Асыл адвокат', NULL, '$2y$10$pfQBe4o73l4T0Ne3i3TP.erzrAUw2EMdn7MhTXkkooqh4VZYy.t..', 'users/default.png', 'tas', NULL, NULL, '2021-07-18 16:25:03', '2021-08-17 02:30:02', NULL, 'dh2jPkDzJ0kegok3rQElaF:APA91bGodILyMCyUKeM1GyMkEIKigguEdq-oNlfLs74EPf_cpOC6BkbDNeRREK21xdC1NIsRaS0FL1fqwNypxR3W5zluEcL8-i3lwi5SKYT5KlAXB3cTG4wTdNO3n5cQYeNgPMWmg2sB', 0, 0, NULL),
(92, 2, 'Галина', '+8 (093) 412-89-25', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-18 19:13:36', '2021-07-18 19:13:36', NULL, NULL, 0, 0, NULL),
(93, 2, 'Ашот', '+7 (980) 368-31-07', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 01:30:56', '2021-07-20 01:31:21', NULL, 'faSH6hZOSgikv9jheKCbf-:APA91bHiWIDNHOsLVzrgyX27lUZILmguNBUQM44J7hl8UdExiH-E0i15F9aPIxZjlP1OszCLWzgb-zNDfWRXcuNolx721FVguDcZfbPUb9Jx2nSnAM1ty7UFKsZ8yL9fewoY8Oni53vV', 0, 0, NULL),
(94, 2, 'Арыс', '+7 (747) 437-76-95', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 11:15:58', '2021-07-20 11:16:15', NULL, 'c7FWwPdWQzWJRddtJNc6Jd:APA91bHwHIE6vx2xesUn7S5KQpMCN1YhrbDiiGLYz62qg157eII0ay-aOcHzYCldStU5AiTLPD_GOdN6hwP8XHesGwuDx4X75Ds_kwcPB34jbLwcGuEraY29xM6ctDA9SyrZW4dc7EnM', 0, 0, NULL),
(95, 2, 'Bohdan', '+4 (872) 967-14-43', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 06:42:27', '2021-07-21 06:42:45', NULL, 'frpZNwTFStOFDy02AQWoQ6:APA91bEIwAupuYr7PDOU1qzliEJ2dwdQTN--An4IkI_QxO35vKY3j6h1lVHXQRb2nCWcuPXAcuHdO_Dn_OQypFEHJ1Dcyu3CUOHIZbF9rIbx7SWE0D_cqBYAO3IojnX4_hYF2oBU66Xt', 0, 0, NULL),
(96, 2, 'Анастасия', '+7 (996) 461-80-42', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-21 13:48:38', '2021-07-21 13:48:54', NULL, 'fJ6Drcz2Qiidse6DgnKtMe:APA91bEywfkah8_Q0zF30EDfjocjebxxGXCzBWr4n2EHMyYp0dZKpC4bGtofRfVTJKuNFz5p09gqxDRXKscef8TUCheT1t5nl4UVYVXUPs9MOY8mWmjvZ2iIMu-asvPlhNfF0Rs6danZ', 0, 0, NULL),
(97, 2, 'Виктор', '+7 (747) 335-20-23', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-22 00:18:59', '2021-07-22 00:19:22', NULL, 'ef8Q5twnRDefJZR4HLSVW5:APA91bHdmI6p-ka0oxr3sYUQU9-Inuiwe5Yp971UQuDUwzP5qvDGqDVS8O4vUzVV7Ty3fH7dyi8kbwczbYopSVcrHFIAQaYEVWbJZu0jXAo2OdrB2Cnw7nUIkCKQemYy5oMsjDAc7aYH', 0, 0, NULL),
(98, 2, 'алия', '+7 (937) 135-41-06', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-23 19:23:02', '2021-07-23 19:23:27', NULL, 'eM8QpM_YTZG5natkpXUgYS:APA91bHjKiHFodETq6gjUR5tuuBA02wgOh7GQY7-54UXc8PBjTadhuMxWHJrzosjJX1hvGRvy7qKiqBoElI41MBVqQEVmxq9ny0YMT6t_scZUuvvpLCZauZajO6NBNFZ-7JnF6kxWG5y', 0, 0, NULL),
(99, 2, 'Михаил', '+7 (908) 802-12-41', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-24 09:03:12', '2021-07-24 09:03:12', NULL, NULL, 0, 0, NULL),
(100, 2, 'татьяна', '+7 (914) 912-23-46', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 06:13:32', '2021-07-26 06:13:54', NULL, 'dYmneRMWShihF9TQ8sIw9u:APA91bHNSzRTmXoPLqlfQGCLDulNLNt2Q2J-CoKKHuC30gfjiJrDQTXsiNpPG8bQ06yyuwfpKzaWfXwnyrqlbGnDzi6WXvawjfYPo7lYwGMtaTsQGLenzxV8nTb9eVZi4MK0aMmx3iYH', 0, 0, NULL),
(101, 2, 'Асет', '+7 (707) 530-17-17', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-26 12:18:41', '2021-07-26 12:19:11', NULL, 'd0kcRaUlTQeGpRXO_fNowJ:APA91bHBU9fHI6uIzO0lhfgVUkIWA3DsU9D9uOWfsdLOIzw_NIvgvkrRPUPF75kxfKOncRH02k0q3BzBZOSgzQC7YmFzsgMI5HHLwwRtZm_58tYjRdkq9B5aw4JzINPTEuIwbK4bwMbB', 0, 0, NULL),
(102, 2, 'Жумагуль', '+7 (702) 304-84-10', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-27 00:39:14', '2021-07-27 00:39:14', NULL, NULL, 0, 0, NULL),
(103, 2, 'Даке', '+7 (708) 754-51-95', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-27 01:09:59', '2021-07-27 01:10:17', NULL, 'fq-yt6PpQGKGz4YDKW4ui-:APA91bHHsWuvZw9ivhHqqbZoe56C0U207Gd0ZJ5DjL_ZBPD4_qU8n7wCepdYn86B1mkzDBNwc0D23fMq4QVTSoy2SXHyXAk62WQ7nA4A1FMg97fJ3MtusL27vWRH9lRBfS8SDTeT0qLO', 0, 0, NULL),
(104, 2, 'Айгерим', '+7 (700) 294-88-65', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 12:29:31', '2021-07-29 14:28:45', NULL, 'friuZnQeQw25DB1UsoE_ts:APA91bHy4GzyrM7TgbgfLidg7KDk9iC-3Bj2DINC6B53BPZzFPZ2yn4q2FPQlVWIRV6N0Txn4J5Eu9qBe3m6Gu8XU2Hda66t0dr4jVIFrIVIWmpuZE0kGb0EvYs1AEhoF-WGCBte8Hi6', 0, 0, NULL),
(105, 2, 'Супёр', '+7 (701) 300-56-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 18:04:23', '2021-07-28 18:08:54', NULL, 'cOr293VIThmjkgsYgEFpHi:APA91bHluFiGw4Zk3GafGdjTQeklsTMg1biwZP-FHiLqJsOekzp7YwGJ-9JPTXJVH2OaT7EO2wD3z9vyBVL_b8CYDdBbWzZJ4zRoilzTbiz21NFdYJkOG3gWVVWRCny7_1FWFXHFHik4', 0, 0, NULL),
(106, 2, 'Елена', '+7 (701) 740-11-28', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 20:18:41', '2021-07-28 20:18:41', NULL, NULL, 0, 0, NULL),
(107, 2, 'Татьяна', '+7 (707) 297-10-11', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-28 20:22:01', '2021-07-28 20:22:15', NULL, 'fgSaKD5JQNeMwpw9Tgy7rM:APA91bGwT00LpDF-roFhrRnWy4meSyRRMioC7YoCkeqGF8JCRHEtqlYfwZPNmxArg3aUT_BGVDMowDJKSIdpo29ojaAtmIxSsRQxpbvL8eCsvsf8PIqEzwm-m8BdxLK2wM2jYvTHPpEq', 0, 0, NULL),
(108, 2, 'Павел', '+7 (747) 962-78-72', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 00:35:29', '2021-07-29 00:35:49', NULL, 'erpf_FgHQjGNRuGJjf63P-:APA91bGbOBL86P0Zci4JNjVxifiS7uYtKqYUVGWRVipwOtMBdA_yKAX7ywS9D4Giv_QiMuMl3ZyvgkHBx_YT6KvPHnmUb0NosDY3XAlGO66P-o76HA5JEvAVNHYR9aJf1xH1ywbg6gjF', 0, 0, NULL),
(109, 2, 'Руслан', '+7 (700) 268-48-58', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 01:29:14', '2021-07-29 01:29:30', NULL, 'eBIEuRl-RvKz5ad11ojgrX:APA91bGAA4LqEO5nK5iMBW7zFeLoz3e9EtYxw5W0E7M6RSY2kd7m76F5DvZrFQdt4BeNFXyDRuzCanDIfExrz6UBu9s8za6ow4P97EJznJkN8xHhjAJQUPw1l2vcxatu7PNuoN0B5X3p', 0, 0, NULL),
(110, 2, 'Нигара', '+7 (776) 395-55-59', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 09:28:55', '2021-07-29 09:29:13', NULL, 'eFmCysR-RAC38t4-UXJXZg:APA91bEZ9F1NpSJlbdznz8QLNltWJPT82-kNCN-V0xkQtKEHXSUDt8SUBlF2GNEseCKoFCRITyUT8ECCZnKkQ2jWKUu8yPaB-mdHhfHYxxxxjEGc51N1g-LK1bao5MtsaE4Fo2QEYNXt', 0, 0, NULL),
(111, 2, 'Медет', '+7 (778) 250-78-58', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 19:56:11', '2021-07-29 19:56:35', NULL, 'dccYb7B1SXWSsaO0OuSj0N:APA91bEfu9imZNi1kh7KuXyjXvjpmF5nT7kheUm16EKcbXHQStFjchn19RKP8-x2CYIpU_ofBcZ0cAKjJg345gatNQ-lEIGlNJL-HT4SkXDk6V80dM9HBvPRfRC92iqSeljbk6n-YnTf', 0, 0, NULL),
(112, 2, 'Максим', '+7 (963) 608-87-51', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-29 20:02:19', '2021-07-29 20:02:19', NULL, NULL, 0, 0, NULL),
(113, 2, 'мустафа', '+7 (707) 413-36-18', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 00:43:52', '2021-07-30 00:43:52', NULL, NULL, 0, 0, NULL),
(114, 2, 'Салтанат', '+7 (700) 269-29-78', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 00:55:05', '2021-07-30 00:55:19', NULL, 'c4n864aMS6KYXtJdtMmk9J:APA91bFj-t7V_sd-IdR692WvNqVa_zNvwmPueP9mRFsONWBX_b4YBPmSZ2-9QDP_jAFEj7axwHzOtjrF3pGQ-5dGnXL_-VBWriAebJz61zVy0JRAMhNSx_ut-o1Cy2FuhnmOS1LlnWoX', 0, 0, NULL),
(115, 2, 'Руслан', '+7 (708) 360-65-22', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 05:11:01', '2021-07-30 05:11:22', NULL, 'eelcv5k_SYiCus2brxeGiF:APA91bGJjepjiziRwASKxPdRz7V3wRTZgi42A4aAgiWpciywE4zzitP4Injzmz_f5vm7PVHK36jc9XUKRta1pHyOhJEvSJGej2cV-E1dSoHruIz6lOIRGACw2WsgYwZmgPiDKewJvi39', 0, 0, NULL),
(116, 2, 'Кенжекул', '+7 (777) 303-79-19', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 08:17:42', '2021-07-30 08:18:01', NULL, 'csrl6D4RTsiFaXiQJjhyzG:APA91bG07mXMKu7jP5to_I-Y_l24KD5Uss1kWjcizLxx6XCmhIpaL3uNpx03IUIFGbwzn104S9S2TUS8EAdmhBnUX2l5HKg4Yoixl2CikBzdyZRaOSyr3n15stpfGJAAjkY2cF6TipX7', 0, 0, NULL),
(117, 2, 'Игорь', '+7 (771) 440-30-15', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 09:22:42', '2021-07-30 09:22:59', NULL, 'cKCCklPDS122_1uWov3ZZD:APA91bHNj0rjVFqLw_S2XAQ63yh3cmrMd7nTsVb5mejPSrQapYAIiKUzo3hTBu4RAUS19YNou5vVFqlCW3VLVZZ5IBFT_7S_fFVhRG2_ws1nlMn-2kaU2OKreJoOOTstEYKQ7vEZoGDr', 0, 0, NULL),
(118, 2, 'Жайна', '+7 (777) 020-32-62', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 15:16:05', '2021-07-30 15:16:05', NULL, NULL, 0, 0, NULL),
(119, 2, 'Қаламқас', '+7 (707) 695-08-03', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 21:09:08', '2021-07-30 21:09:08', NULL, NULL, 0, 0, NULL),
(120, 2, 'Гүльбар', '+7 (771) 456-71-85', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 21:11:58', '2021-07-30 21:12:15', NULL, 'e4n4WWQtRAmsQ3RwBAkH-i:APA91bFsz3lV3i68Kf-nOYS2XmPUKXtFl9lmhhohAGbN4Pk1ASXiraJE4w_53lznRlU9nPluKgO6_jPSdRMDHOT8u8t77SB-Kqg3xZpofEoWwAApK7TSSehb0z8REXlCZX0WtyxEVNR6', 0, 0, NULL),
(121, 2, 'Жанна', '+7 (708) 245-92-72', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-30 21:51:22', '2021-07-30 21:51:43', NULL, 'dnzSw2KURYKwW0vgwri8if:APA91bGPJewPpVV83rJK0bIb_H5LC6GUHcYJB5JQPb_T6FYKO_e76C2nWQRpFjT7FwcclQefHF3KX4nB-nDETnAE41YalbL5r7xwH752Pg4SPIcacJ_Dcl5_-8hwB4Cr-42yLFr1Hc8A', 0, 0, NULL),
(122, 2, 'Юлия', '+7 (776) 147-32-12', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 00:56:01', '2021-07-31 00:56:16', NULL, 'f37gF2uaTJ-87IRjd5nug5:APA91bF2gS35HesGIWsbDfzVql7OANZEoRYEa6scrMkA-kTEg8Y-N2zFl1_qGLmD7wxGxCDC99yB6aglRKAIIT5M7pUtafycRuvdb2AVDQmCyTaqH7on7IQgum7ZKQ6JtANXBHCWEdb3', 0, 0, NULL),
(123, 2, 'Илмира', '+7 (747) 691-58-28', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 01:45:48', '2021-07-31 01:46:10', NULL, 'eCNipR8jQ6yiP_fnH4Di1P:APA91bHpjSFPjzNz8_7U0tkRa_7HaxVwa6nU-M8Y8MbILUxsn3suDx80eBvjFzy0ZI_s6OPYlgfSCBYEyVePGF9XuB0z6r3i0Jns0r3qRzb6ynAh7WC3Za-VjVwX9KSynqzRjd-m2_TI', 0, 0, NULL),
(124, 2, 'Алмас', '+7 (771) 562-77-77', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 02:33:27', '2021-07-31 02:33:45', NULL, 'epWHfEhSRMijVt6Pj6ay8D:APA91bEDdtiQV7yY3uGbkOZJ4kXAz-ZO3fLr_L29J1Aq7xP_z223qV9eQDnfgW4kTp-pYQA7WgZbuPFd4oHoSSJ5T0M8q-fhqO03cQkb4V5w7fnaC0W9STrVi9P7HzEQYUvccNFujTh4', 0, 0, NULL),
(125, 2, 'Назн', '+9 (965) 079-99-13', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 10:14:03', '2021-07-31 10:14:03', NULL, NULL, 0, 0, NULL),
(126, 2, 'Назгуль', '+0 (507) 999-13-1', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 10:15:19', '2021-07-31 10:15:19', NULL, NULL, 0, 0, NULL),
(127, 2, 'Магомед', '+7 (906) 941-96-02', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 15:58:55', '2021-07-31 15:59:11', NULL, 'cJIm9NnBRjGBjwBItgXzIr:APA91bEDukQOqQrbsUH-Ma0SUM2bb9km3zT4p9Lg2cmXwO1bpZnxk0YCA81XggEYLrq2PBRPvAbKx4SGUjPBDKLboQuuyrDJ0AYzYEYVnFP9qtpXgNABDT6DMZhYs4RhGefFUc7bnT4R', 0, 0, NULL),
(128, 2, 'Родион', '+7 (705) 208-49-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-31 17:32:53', '2021-07-31 17:33:08', NULL, 'cfLcxQ95TTujRNg2mUUq1e:APA91bGQDKBdTM5IFK3zP39HdXdul-fQpCe_7wnfSbW_U9eQSx9mPI919x1-vyRQoUCQKIuYgOPaAL4xEtW7_lKCneH5TP-POmGqwKK1ahtrgU5C6R4Kg8bf7WItGM80iY_zy8do3MCq', 0, 0, NULL),
(129, 2, 'Виктор', '+7 (960) 236-20-16', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 00:08:24', '2021-08-01 00:08:39', NULL, 'dtbHVCcKTuOaqj8Tg2PED0:APA91bGgF8iTFIVJe4MyXieQSA2UQ_ammmfR7FLzEI1ImMQgQtEFe6ZkIIaYAR5HbcanYmt-ae0r_m9ozw1i_FLIGqCgUVtgQ3zLLPK5IsLJ18LmJ5z_qpcY3piHyqwsU00zeIq_6OXH', 0, 0, NULL),
(130, 2, 'Ақбота', '+7 (775) 394-77-37', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 00:16:53', '2021-08-01 00:17:12', NULL, 'cHNdiuF1RDKIOinSmR_ygB:APA91bEkHzbTvW23pXeG4eZBGz_xFyEuvsnWAvONWyFVQW4c0cr-j-7j_o9Z4ZFuya8SeiCu4lTtE_L7tGxjOmAY94JdaI8oGUfyeIz3YkuETyoyY9XZ1skuOFKIV3n35bDA-8ZKazrQ', 0, 0, NULL),
(131, 2, 'Танатар', '+7 (775) 600-57-11', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 01:37:49', '2021-08-01 01:38:44', NULL, 'eNfGxD-3RD6CkPMl2ZNgJ_:APA91bHcpqvoC8P-QpZOyDIHoCp4vZU8pFxzZGdCcPaP0ChS329l5r2AU43de--lYBzArSzg0eMK5L7PZow4ME507DMMgTo-dq1qjws4QFdRDkIa5QAtL835fKHaMk2x--Tzmul-IhYq', 0, 0, NULL),
(132, 2, 'Маханов Аскар Ратханович', '+7 (705) 203-88-67', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 03:09:42', '2021-08-01 03:09:42', NULL, NULL, 0, 0, NULL),
(133, 2, 'Руслан', '+7 (778) 002-64-81', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 05:16:26', '2021-08-01 05:16:49', NULL, 'dup5acKwRmWSBs7F2vJqDY:APA91bG3zBJ2vJMfcONZ-5Jnm4zUGV3sqs8-aM9P_EVS2eBFj__pssrazF-Vu_aRJ7dHOnjq_LxyqICYeSN2rXvmcuWZWPsaPdjHo65iTzT9vm1gjZ8i1vBNXRFp7nvJV0zlm0SC_1fy', 0, 0, NULL),
(134, 2, 'асель', '+7 (777) 615-70-86', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 10:03:40', '2021-08-01 10:03:57', NULL, 'cdzeGz1HREe8NC6WFVe91k:APA91bE6D1BtUfxCaXL5Xzdyn8AFuTiSGsjlXHIdFTZ0xys7ceQJ-xfc-0psZTdu69iqM3UdscZzzjMvhgi-CVdyG8AAlkjeeBeMc5eVK13AyPNrsbzITDbhTEX9wUmdM1rVRp_d-UCp', 0, 0, NULL),
(135, 2, 'Серикжан', '+7 (775) 256-88-77', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 10:20:12', '2021-08-01 10:20:27', NULL, 'fUVkTBAkSTGvoyt7slgrfP:APA91bHt4jptyZsOhcD7OGNTDYLd65MRWZ6ZRgNOp8GiVxGAv9Im9-Rpm6LI4jlTLLEQQTbjs6fddwAWwOW9pgZzzHJkk8paYJb5OvSnpON2Wbd9CrlER-2h0BBalkMvOXhJJ1_k1_aI', 0, 0, NULL),
(136, 2, 'Рамиль', '+7 (700) 250-78-33', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 15:42:32', '2021-08-01 15:42:54', NULL, 'ct4MTTGfS-a3pG3mK6jqnQ:APA91bHOmkyx2otTxJm-IALBzd1aIX2EZX32iRQjEYi10x5MvDFXK4toXbO3d9RWkgEZhxwYjIgfsZ2QSQ-jX2R2sRey2EMrDDUSSPoSlU4rqsuB0E0vye2aPPYGlSTWASt-mKUdIcF9', 0, 0, NULL),
(137, 2, 'Ислам', '+7 (776) 533-19-99', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-01 23:15:45', '2021-08-10 17:24:36', NULL, 'cfWV8Ny-Qgmp-RSbQupNJj:APA91bHX7E1NPb8kNyYjyxGyqjBVVBoFQTfkasPtDGFkcsgno8AujjycGAi5_dFx3GaR5p-2JJ5jW5XEc4nkDyNVuQPYWqIrUkmDWD3yVMXLIkK0gIJT7P8QLT2iomg_ykpq7Lr44kqj', 0, 0, NULL),
(138, 2, 'Маржан', '+7 (771) 461-81-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 00:24:44', '2021-08-02 00:24:56', NULL, 'cNZKnbPrfkWmqM-8X3jTn_:APA91bFBen7e_euAtGxyMtHiSZlao2KUJkGcTAWrNvlglMeXvfyeBkq517XW6aVKKCEhrxkQ76K6XKTLVhEht7hIyDKnZwOKcvDfsU_tAiaEr3vm59H981acJp4iR7i1Ar7bDoyTjr7U', 0, 0, NULL),
(139, 2, 'Оксана', '+7 (747) 602-51-89', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 03:06:57', '2021-08-02 03:07:17', NULL, 'er6bIHkLSce1Fl4XA_x622:APA91bGk76j6-Wmp4XWKRNU2hBEWcOY_STPHOYGhmmW8kAwMAqduOPUcMAaiTp_e5RgBCVaxWpufMDAxgHFHIpfDhBEmGM22KooHEh5psOW49n9MhKHRINe9LVrZctmZMyPCxiFaQgq-', 0, 0, NULL),
(140, 2, 'Наталья', '+7 (707) 574-24-24', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 06:33:16', '2021-08-02 06:33:34', NULL, 'ecWHPc9nTjGVD3BvR1aef7:APA91bF_oMmfTfVwsTMTScbnVJNYN2Do94pZUtVqoXeKW3u1Vl21m04_XEsuZSUX1-3BWSvHSG7JhsOalH722jxXYFzMNrtCyKeShEYgP_zCR1Nk1HPQd2XZVsmhv_GIve3Svu6S19kY', 0, 0, NULL),
(141, 2, 'Алишер', '+7 (747) 869-57-49', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 10:34:08', '2021-08-02 10:34:14', NULL, 'fhUknmc_Tnyev3YZVFJSPU:APA91bGBgKA1crZQ1E7fTbRnOK6hXgO1I60mRwxaXXk5G84mR759db_l3ruxmlZ76wGopjYBDirx50kwgbB_gv-eOesqp8J8z0Oeefe52xOrxsMEHqOuoLA08zYC1gtBsTJIZ143VP5c', 0, 0, NULL),
(142, 2, 'Бакыткул', '+7 (702) 336-94-71', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 12:19:25', '2021-08-02 12:20:14', NULL, 'dOBShY6CQFWlK0ckDoL3GZ:APA91bEi71O5w70AkPdWmtefZvMlAGdQh6WB5FhtwjTjLYPE-nuyDcYckgNjZuzhI76hyW1Cyv20xdFXzmRNudsrWvXAOIv22sut64B7Gja155Rfs_Fy1dH6EUE8ynUcRjHW8EHhWq_N', 0, 0, NULL),
(143, 2, 'виктор', '+7 (925) 278-54-40', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 13:35:06', '2021-08-02 13:35:36', NULL, 'cHRuXEkSQd-VXUe6_JDwRs:APA91bEl41PSH1ERwG7l978cVLlEKl7ZqfEjVJCDURfUWc7vcIU7C3XDmamYXapRMsFx0FMTporVNo3jnljLc_59rtCQV1yqhLzztiJ5H0IQz_dD0N4Zd9iRlPWMILqos7VLv35LUVzP', 0, 0, NULL),
(144, 2, 'Света', '+7 (747) 180-90-37', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 15:35:32', '2021-08-03 11:50:39', NULL, 'cBsSYtwsSOqEfjOoiYvNJl:APA91bHAwJR5N1jzb6BqdDqd_ejiSjH9vwwqJUZZkWN8meYuzdOpx1h1Sp4pFIGkaJwS-hvIaMs_DwCDe2s0pnajsWMq61KZUGITm81qzqt1uB7OvRZY-DRN4-ISi-EAKGrJjJ2DDH8V', 0, 0, NULL),
(145, 2, 'Алмагуль', '+7 (702) 792-01-75', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 15:56:14', '2021-08-02 15:56:42', NULL, 'fCRAuH_-QOas2_h4G3CjHP:APA91bHFxESEBwGTtthuu7j1qiEBypKDa6Eb3GnOXjckScH-jCyOx0t8S7SofBCSuWLWnHC076wTjlhj8bO4gD4AzB-4By03TxSUDF7x7dSEY5IUESmUlCJr5hxUCjHkSjnK1dQpqZ84', 0, 0, NULL),
(146, 2, 'Дастан', '+7 (771) 256-04-14', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 16:29:14', '2021-08-02 16:29:54', NULL, 'cv5KYh1WRXWZwbKiswI5Aa:APA91bGScXBTeKfrxpbIS2V7LE_tXKxEj229m-AwmQyR2XLmvv-ZKN-sOcXJ8YX950paUTBPG_xNMjRhuI1owgO2lfI70wW07XGt3ePbz5pNfUl2bIN2ry45Qx80HAGX24iU4xC5RhWV', 0, 0, NULL),
(147, 2, 'Айжан', '+7 (707) 991-11-61', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 21:36:27', '2021-08-02 21:36:27', NULL, NULL, 0, 0, NULL),
(148, 2, 'Айжан', '+7 (701) 991-11-61', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-02 21:37:54', '2021-08-02 21:38:08', NULL, 'dIUdnjF1R8Wx8_ZbA2vHwM:APA91bEVgI-JjRJrNcEsWewlQhjFJlgM4CxBl5x-DGLECe_WTRmi0ra5K9Gj9XMGUOcj0ugrgXcd_MPh1G0YP2I44YSHx8GxHo488WKHeiMsC5BVCrqC2aFfVmquLeetG2332QVaPwkZ', 0, 0, NULL),
(149, 2, 'Анатолий', '+7 (747) 374-63-64', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 00:20:24', '2021-08-03 00:20:45', NULL, 'cwufvv0eTyeUCvWJip6Y52:APA91bGuQJFUV8w47X-ye8XiBQ5qYvGXh8iZMvtztbLd5Fbaek-H39bcowienrib-_zq5bmx-n7cTSNjDbT4R0mOivfOSF_VHQ2A8g6fGtBVr_TUHbFd1jtxxsAidMXdyZqnUuJ4xvzD', 0, 0, NULL),
(150, 2, 'Рано', '+7 (705) 167-87-59', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 01:26:24', '2021-08-03 01:27:13', NULL, 'dyYOVAQSSfWh4LVarKXJV5:APA91bF2OIB80RlOlV3cXaOBhtx2hPwHbLHEabLnXmc6r03DZJXx485-AdtJMPctKQa-dbfWVnmMnCqNXIE4wpXst394-JhBQ__BVxmIrwLv9bJnplOf0RmKClhTzQBXG9oAqJtNu9xv', 0, 0, NULL),
(151, 2, 'Ирина', '+7 (705) 568-34-49', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 07:39:21', '2021-08-03 07:39:51', NULL, 'd55-O3v-RPyIlg33juGebs:APA91bH4RVjkq4LQnjWDAzDgHq74fJMaaWO6OYZ3UZqARgO-AYgCcp-kPv46LgLG1uUKMjR8lADdKicj-FbvQW4KyaeGVm1JCDDxDNGZ-niSK3NqBd8MHSLWZSeHBZJiGfYMiBS1HjkS', 0, 0, NULL),
(152, 2, 'нуркали', '+7 (702) 279-05-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 07:51:04', '2021-08-03 07:51:30', NULL, 'fufwqNGkTR6jZaJCB8IqPb:APA91bGmD60EEwuhTIpI67HxxGH-Rhq1OSiOJebSngRSE15MH1bY9fphX1JY41tL2cuAuq49xNFPmq9P8z1J_-alwAgDuBVi-1Uoq5ALTof8kmNpes_P78LnJsmzJkVAjQTCJB7-F_48', 0, 0, NULL),
(153, 2, 'Фотима', '+7 (747) 637-81-62', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 14:37:39', '2021-08-03 14:37:53', NULL, 'fvnTIgdzSy68b0OyguDaF5:APA91bHdM5GAKTNTgPPazqHZJCN25HDlRV-eIa0b-8RPRE8lI5koDo0Q9fjBE-z2UAkKH5W4aauOA4wAGzOG5haJXxhszOFdx_ZVEi49774z1gURiTtzhnSmXyX0JUnZVT2rjAZ8IPPW', 0, 0, NULL),
(154, 2, 'Валентина', '+7 (700) 277-78-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 17:10:06', '2021-08-03 17:10:25', NULL, 'fos47DsDSAaMVHZsapl9OC:APA91bHVO738IAxBrr19mgFFyOxiWB8Be4o1RkdBPCVLPAZFVLI80ZcmNGOzY0pWwwDdgGaphmZ0yf29ngxP5GxCRJqaPCd7-nJPYp8qfrPzwSo6Dvt3xZXDBlRhr1PLKCU_7cxzsRJM', 0, 0, NULL),
(155, 2, 'Сабир', '+7 (708) 532-31-74', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 17:49:51', '2021-08-03 17:50:03', NULL, 'fRLxbdFIROazbz23c1ZjfQ:APA91bG42URiJechqR439bYeJe7GL4vjGfGZWEYOBcoi4EyEKWpFpF5drf-LvgkPrXjQD8vqjGGDGpaZ8ER0ZSqdVzBfFzmcRO7s3y5fbaN6GFOmniOPSS9GxJ-UJyI3uhPEuQ23MTTe', 0, 0, NULL),
(156, 2, 'Бахтияр', '+7 (778) 562-51-91', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 18:35:01', '2021-08-03 18:35:22', NULL, 'cMTBOw8xQm2eYho4XxRcXb:APA91bHzJofucuUVDxkIeBbM7ci2V98U28UrAH0vkLTN7xGX2Qs895PC7TVKaQKQt8fVZ0g-8vZ38HCAhpny_VCkLrvQsrcRLmwhZ98dOTxqiH30QqbMcO6QVqw_mLF3r9amxyYVt585', 0, 0, NULL),
(157, 2, 'Сергей', '+9 (965) 500-01-30', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-03 20:30:05', '2021-08-03 20:30:05', NULL, NULL, 0, 0, NULL),
(158, 2, 'vitalik', '+7 (705) 568-11-75', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 08:57:32', '2021-08-04 08:57:54', NULL, 'cRFOMSMUT4uKMNU8-NNRUR:APA91bGyIyCpmoEyH4n8Qz1dZfjNSSKfhBsE0aKI_mfciPjbZ_rHbEdLQ0Q9VmOtjJd7ErNVgLh93gxSl_yVuDBKIsH-n7fZSpte6TaWHsuN7v7ZVCXEiGUjP1STl8YzF378zrR7x4pp', 0, 0, NULL),
(159, 2, 'Рашид Мустаипов', '+7 (747) 989-90-64', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 10:26:31', '2021-08-04 10:38:45', NULL, 'fgKwfRW9SsSmsyRsu-cngD:APA91bFaWn7-skVlV759l8tXbjKGoLnWjBD32IC9dxqW8oR9L6Gro5dyOPwdfdG-8_fuRaoRce3haLYw7qW9d9eGGBQkRbE7iRfXr2KhVntfC-FSbRPODw6Fp57hyPGEJdPmR3-PxAvX', 0, 0, NULL),
(160, 2, 'Аскар', '+7 (778) 631-20-20', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 10:31:02', '2021-08-04 10:31:47', NULL, 'f_83SeGTRTqzTe0uQnvvIH:APA91bFINJgXBzPtPyB2MWudKrBQ7mw4ZCK2Nqs1-m4MSLqJEGTEe5BpvGczAwq9VryR4ya9d9olj14AcQRRrF4oQsaGIKFeLyIZqfjmV9_GyIXK-lXcl-9SGDDKHV0ULgNiHv-PlH5t', 0, 0, NULL),
(161, 2, 'Галина', '+7 (707) 481-49-25', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 11:14:53', '2021-08-04 11:14:53', NULL, NULL, 0, 0, NULL),
(162, 2, 'Евгений', '+7 (701) 223-10-71', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 12:38:44', '2021-08-04 12:39:06', NULL, 'fyv4rdW7Qc2SVVZ7NKYrV-:APA91bGcwx1oDgoZrzkpFwc1i5E9Fe5SPbU_iNIa92iR-x8JsfcKLDGTXQnVqxhohWcnHHshsecdMufsYVJHds2FAsww0dq2c374YdICaj2Gk0f1EEbOYG-AD8yecZICHSaK-XHYO-XJ', 0, 0, NULL),
(163, 2, 'Айкерим', '+7 (778) 903-61-18', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 12:49:06', '2021-08-04 12:49:25', NULL, 'eZ-qVpHlRa2LMVjh4i2G7W:APA91bEtnnd3-kMtLe2YPyahvLIrTsF08ySSF98yq9Dzb5hGYBbPJ09Skh3HJdcYAcG4_GUPEr4XLFqW6huYnKhWPNN9F6rOhpK3CNtpjGjKTWXOQ3upro6sld_jArVK5GWrFDvHWQnn', 0, 0, NULL),
(164, 2, 'Валентина', '+7 (705) 725-06-38', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 21:56:38', '2021-08-04 22:04:23', NULL, 'eayQhfNdSgK0H68FGAChAQ:APA91bHBn_PXIZ4lNnbAMQsey2b6pfT_q4QMW6DaIuDXxjWdDK7HC_4Uk0dy40zLFxbw1GP8huvFC_vl7WXgSiLmRXbKWDkEFN3vRZgC3XtEFj7Z6HSqmEhde3g0EK2Ws1ZA-1vAdlVA', 0, 0, NULL),
(165, 2, 'Каракоз', '+7 (700) 313-90-15', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-04 23:06:41', '2021-08-04 23:07:01', NULL, 'fD8j_zDjT0mrJMenGkKo7t:APA91bFHBRRqpaFZh-6kfK21qmxGzXg2d8micnjhbFWcmoimUXlLXawxOvcuOClc2-UeuIHJmsG7l_oz-6-e5UgRCwPG06oIHZnePUeIVaplx2xPfMbOk81M25oBBe1IbjEtwG_rqysL', 0, 0, NULL),
(166, 2, 'Геся', '+7 (551) 110-98-0', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-05 08:15:28', '2021-08-05 08:15:28', NULL, NULL, 0, 0, NULL),
(167, 2, 'александр', '+7 (771) 157-93-61', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-05 11:08:19', '2021-08-05 11:08:40', NULL, 'cgXBC6RASmyAWRauV_jJaN:APA91bEgCZc8MOrIKBAbAHaJF7egD1FzhUK7Ew1Jg4Ej9udaRGiQpgrHF072ldntOIr3imbSRVscDB5weN4gib0Vch8z-ATQDNfm09wXxFPLr8ebteRgVEpE3xVa8q5lS5suJ5CS-RvI', 0, 0, NULL),
(168, 2, 'Ерженыс', '+7 (707) 672-14-12', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-05 12:33:32', '2021-08-05 12:33:54', NULL, 'd3_tQrkKSl6VEZncvo-ae7:APA91bHuWfA7-jExcCFaa4bcFDtMpiv-aue3AOF1U81VkXVDDknrO8PFth9LhSPRC3dVBpQTWAqoEGYyYEURaGA10GSrsfxeDbFTcqfMbC0eZos9HspT-WiNGom5T3HMGkNNMgWyYm3-', 0, 0, NULL),
(169, 2, 'Валентина Васильевна', '+7 (775) 917-24-51', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-05 12:49:46', '2021-08-05 12:50:09', NULL, 'eayQhfNdSgK0H68FGAChAQ:APA91bHBn_PXIZ4lNnbAMQsey2b6pfT_q4QMW6DaIuDXxjWdDK7HC_4Uk0dy40zLFxbw1GP8huvFC_vl7WXgSiLmRXbKWDkEFN3vRZgC3XtEFj7Z6HSqmEhde3g0EK2Ws1ZA-1vAdlVA', 0, 0, NULL),
(170, 2, 'Аслан', '+7 (778) 927-68-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-05 18:45:35', '2021-08-05 18:45:56', NULL, 'fNAhm9IITwaffWzWdLjWDg:APA91bGfbDDKO6MlE5JoOoimaik_IcUh7eRUnBXV1-cUx47sd7_AKCG-3qhxYnOEGMdJf1qgocxBiTLs5vqHsg0wYrGmxgE_fxMwJMINrXYZ3Prft22AhbVhL7h6cpnR5OmOibouN9yr', 0, 0, NULL),
(171, 2, 'Abdurashid', '+7 (747) 338-09-82', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 00:55:44', '2021-08-06 17:53:56', NULL, 'eW3zo3UUR-aS0gEtHxTb1o:APA91bFnJgl6XvGzwFTQq-lOavS_Zde0uJxomsiGdLz3flUclm-prPdH4J9PNaTUvqYnkpdpvwkgob7chmbqklM4wKrnOA3hHkV3GkuKh0REV9RA-4sRzkXbQlmZq4__XkEU7uAcaysf', 0, 0, NULL),
(172, 2, 'Гульнар', '+7 (777) 378-76-41', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 01:52:23', '2021-08-06 01:52:45', NULL, 'dBVKXpllRIqr5-f6wvYN7a:APA91bFMtPaQFj26-mPiZYufGufz4HdHAuIIFhjqI1BAsLRilfOvo_MY7MnvpAna0jXtMdEW7UJukV6s3mfa6d6C6May5SCoEmot2dI4kDdgqKRV4t-GbZEk9shRZzjo6NaM981JmNmT', 0, 0, NULL),
(173, 2, 'Ербол', '+7 (707) 300-33-89', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 05:31:01', '2021-08-06 05:31:34', NULL, 'e4-NbPmlSbaqe8dSv6xmsG:APA91bFSwiCzhH2Kt9saghtNJu4OZmHaTIke0JwOGzIgdj_G3B-xwk0xLpFu6J_SQb6cW3qgqCltEMnINbBRSDQu_XuzszWjF_Qpll-HDM5J8y56nCbxpP2tbez2Pq2ofB_wi2gfIORY', 0, 0, NULL),
(174, 2, 'мамед', '+7 (747) 354-48-22', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 08:04:10', '2021-08-06 08:04:10', NULL, NULL, 0, 0, NULL),
(175, 2, 'Назира', '+7 (747) 261-14-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-06 13:01:00', '2021-08-06 13:01:16', NULL, 'f1OippxNQwauoOw2P_K5gd:APA91bH3eMnQdFq-C6moP97vqJCJomsOpG-vkxW1tGlLgh8zdRbYwKxdiGJEXCskIdIK77WxOEc1gH1d3_zWr3U-8y7EVbxDVVJ2VbHcBBnIuU29whNZrZ9QE3enYBJcymL-7hmgic8x', 0, 0, NULL);
INSERT INTO `users` (`id`, `role_id`, `name`, `phone`, `balance`, `email`, `email_verified_at`, `password`, `avatar`, `surname`, `remember_token`, `settings`, `created_at`, `updated_at`, `deleted_at`, `device_token`, `positive_rating`, `negative_rating`, `collegium`) VALUES
(176, 2, 'марат', '+7 (777) 824-50-43', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 00:43:19', '2021-08-07 00:43:44', NULL, 'fGMcPnLUTxuG97lS750w9p:APA91bEsur0z3TuiGrJl9VtWCAbnuMPeoij6WRECt0YVLE_w3JtBOCNPFPfUJTbz94ooOm2mK-cBOmpBH1yM-zv5O0aKCOfGm8PW_-2XnGUHJsxQEafwsNh2fM0YMdnsExCn0Os71fGL', 0, 0, NULL),
(177, 2, 'Арман', '+7 (705) 768-75-55', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 01:21:38', '2021-08-07 01:21:52', NULL, 'fHiouwmiSQ2B4ZYp15ypZ3:APA91bH-VedWl7K9KInY62IXvU46EUw17C1koCTYmtYL84a_uhSEyC8Ecs9RyNmhPA52C0_8uv8TttXaGvcQ5zJn3deIEgQNSDPKrHdq-68rD5q6vTlzB1Vz8sgF860cXFzryQ86NfsS', 0, 0, NULL),
(178, 2, 'Айсулу', '+7 (778) 233-21-10', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 05:06:00', '2021-08-07 05:06:14', NULL, 'cU_WCQEPR6CZOLi7tgyWhI:APA91bFRxUwTS8i6kOYCAYhpoCdHETkH7txunW5tLF64M38Tc2eyTTVLB9n3MzpRYRO5YcBQjjb1GOc2Jq7GAJhAT3Jmgfg1Sy3SvH08Ob5WJeVZHSezOK9eDke9ogDat3W8levTsRYG', 0, 0, NULL),
(179, 2, 'Павел', '+7 (702) 666-63-47', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 05:10:17', '2021-08-07 05:10:36', NULL, 'e0pN22j8Qw-gR0sOC4l8Ko:APA91bGUybDQgvKsl7APEvMCeIGix5_u4vRGpuNNciFCx9VE1FmuV6YimOyQafBIpBCud_aIhMGHxWL0zSrd-09LYPaoY1gINYQq8U6I3Y_NwcoK5um3v5bIlT_HBykFg8DHDCmg6xVs', 0, 0, NULL),
(180, 2, 'Жалгас', '+7 (778) 808-94-34', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 08:20:11', '2021-08-07 08:20:11', NULL, NULL, 0, 0, NULL),
(181, 2, 'Мадина', '+7 (747) 788-43-57', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 11:28:09', '2021-08-07 11:28:26', NULL, 'fH8DBTyiSOOlO32tZSqx1I:APA91bGXAFD37OALLPXFFO3op1G_v7ARzXjK3pgd0aod_e6UYEQBdftBQUPoNyENZhp9x_Bdp4uvK6Xb02XqHkwoenk6fMljOpZAeU1jyZwzAZx2PHCbb0nXM8dlA39z4e9NF6Pg9NVd', 0, 0, NULL),
(182, 2, 'хочи', '+7 (777) 265-97-42', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 13:08:26', '2021-08-07 13:08:51', NULL, 'f6nrenU1RJeRxRCXmD2v9X:APA91bH5wZKcPkGNetBtjyv-B50IMwtIglQgq3LsXKSdVxYPIe6ooMJu0F6EDBy7EdZwPlYrxHrkmk3vgJwW0ZtNd-Ug91eHyTRZ33P-HMr4ufn6aWrKDZuBOnYJEey7yur7WBfy44MU', 0, 0, NULL),
(183, 2, 'Анатолий', '+7 (982) 624-98-16', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 16:30:56', '2021-08-07 16:31:56', NULL, 'fzcTqLMtT0iPLQUkveXKQs:APA91bEfRkwZJNLBrYuvrbtN6lNRMRx8PgePaXWnODi0YPy_z06hVZ5M6RNlrkwZI-NAWNXKi5FCKS6RPxYl22lHpNmCqP6RR9UsHj4OsGEIL2olkEn-o4VCk_OF9jyTMMOD42R9bVl0', 0, 0, NULL),
(184, 2, 'Ксения', '+7 (913) 975-61-08', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-07 19:37:43', '2021-08-07 19:38:04', NULL, 'c5ncdpbJTX2yjHvA0h93RO:APA91bEeRqzHZcPH7QTC4h98mfuQsJ8sfg1k4bCV5dbrVOlItuedutXqc-DHbMjkMg2cBfTGssTrCVbw-4kt-mA1vp4DE83yHUaMqyNSlKiZw6yajFSOIKfkV_69AMmO2ZoyPOjQcC00', 0, 0, NULL),
(185, 2, 'Эльмира', '+7 (702) 593-99-99', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 02:08:12', '2021-08-08 02:08:35', NULL, 'dijw3YQnQs2OMTKXuPxNb1:APA91bHa0u50usGL5_K3ODqxG9Ukl4tbmLonPbQxi5ewNBGIHWi22d_-DOT8OUG7FPix4y_WfD7sXypENadVnoWA8g9I6oOSJolQvZr848efHXA4BC09LdF2CnVS4Qom2M6r7KMJlGJe', 0, 0, NULL),
(186, 2, 'Айбат', '+7 (708) 183-54-20', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 02:24:29', '2021-08-08 02:24:45', NULL, 'e9TDZJQTS9yov0gmIed0rp:APA91bHUKxx4rHtI3dLXsxxHsHlfWbnpPytO8sncP7JiSsoldedtRyS2UPpuD49XRVZNhdPZ6UceSNmcBKYR28-4PIyYnCIL58QEwt4m82VnM1uveF1-MlEvhYnVd13T66ONrGA4Mkmo', 0, 0, NULL),
(187, 2, 'Владимир', '+7 (701) 744-36-63', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 05:16:01', '2021-08-08 05:16:16', NULL, 'ed5sVHuQQIeKBs9kB4Te6j:APA91bEJsStnVaPB3gWlR3eqoR1bcUl0ujI-xDNL1ZieelS2j1iM90iktkztN-2Ki6qmQhEltYWdmZrnxBLptUhtr_HHVkoENdIXDLAHVglrC0ukqJ9-Y08hbnEH-xn-T7z5cOKcps1d', 0, 0, NULL),
(188, 2, 'Алихан', '+7 (705) 357-25-55', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 12:01:54', '2021-08-08 12:02:10', NULL, 'd1ueaaY0QmiS3oQ-OrUEh8:APA91bHnofuGGXmx5Uzv5ny8fCNO_TfzGnwZlG-Ys5ZEju-yOUQKuEP7n2vREEgXOhBCPhGNchv_38csYuHQjbQ7Khkwg1Gy7CIYSCK3DJhdvXUvbjCFDhwfUTK_3PkIJQjPCmFh4S1v', 0, 0, NULL),
(189, 2, 'Светлана', '+7 (705) 771-40-57', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 12:08:56', '2021-08-08 12:09:15', NULL, 'fuUoYPt2QvWukBbEKHPII3:APA91bFSwBIAlUlN_DQgEAQ_2IOfIKvYXvwAnShWSEidp9ZTeteV4fup9T0B_azmJI6LOmTO9EqgOiot_eHH6mg5w60JhLVjcYF7rnBetYQpOsSW_NRLOrgi111Yax2Ox84crQV05LkK', 0, 0, NULL),
(190, 2, 'Алимбай', '+7 (777) 518-80-74', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 13:11:44', '2021-08-08 13:12:14', NULL, 'cxopR52NSyCslW5mOIsP0E:APA91bG43_ikRqtQLPV5ggwdpFmnJdsYA98d7wJ6zNx-LlQvHYI8B4asgI1jYchogAOheygXWCaRdbEppQOoqLbUaykxf0SLjaIyuUR6agH-m_6IyziF3lMqf428rEVOnj94YeOtxmeo', 0, 0, NULL),
(191, 2, 'зикиржан', '+7 (747) 481-22-67', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 14:06:42', '2021-08-08 14:06:58', NULL, 'f9WEn6KgQPaqSvaOqmamrU:APA91bE7sClkC1DGqxGsQGX8xPx4eIWN3ZlH4bwq_SkR_OAqST6Y1TW4EVfy7x6q5NmJzcV4HjVsj-JyKZsnl2GgSENtGjpmDgi_Q_T28LamahgOacnehF767voSqZzWxmOToKNyArG3', 0, 0, NULL),
(192, 2, 'Ольга', '+7 (775) 127-17-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 16:30:07', '2021-08-08 16:30:43', NULL, 'fUTklFlUQ6WsBEF8zNP_G-:APA91bGZeHvRKu63wo_ivbR8TpGjnu7J4aONrFH8UCQaXv0yz3bIGVoPyFW_fW0m-_CZsxtNGyOMtfGhMdpO-RdifSno7gaZni8fz7TR6Ij4xog0RXlMePjVVICf2LJK0xfZCFYfwG_j', 0, 0, NULL),
(193, 2, 'Мая', '+7 (700) 111-08-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 21:49:16', '2021-08-08 21:49:38', NULL, 'ecWtzlxJRLGWZYgkpfsHiZ:APA91bHMslTKFEBjrgvW7B7Y8c0ISuaU_NxCKiyYvp7yxqUzVqCk9ciuc3X5LrDSaNdPv1QvavCnGuKiXCkVEzzcih4V9aHs8mg9e-vCZjlqhcKBw4mGKAJVTEBhS5216JRNb7zxk3Bc', 0, 0, NULL),
(194, 2, 'Ануар', '+7 (701) 711-81-83', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-08 22:30:49', '2021-08-08 22:30:49', NULL, NULL, 0, 0, NULL),
(195, 2, 'Жангер', '+7 (707) 711-92-42', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-09 00:14:01', '2021-08-09 00:14:45', NULL, 'd3j06KpvTaORdrdLrZQiAL:APA91bEoy86HvumljsqKjBGq2Rc2ofUSzo2RcIiSVrYuDTvdnTMpgqF6HuvtCewLXQ-IvfN80P3-hJqFX9OKpev7Wq_W23euQrnalYZF2Wzga4-WmTvnolLeW26wMplJcoSRYc4xlPRf', 0, 0, NULL),
(196, 2, 'Александр', '+7 (700) 237-37-28', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-09 08:43:16', '2021-08-09 08:43:31', NULL, 'do2Wk6aeRd2Y6hSMfA1opD:APA91bGMG_c2WH4sIfiwbbjBUJ8SQ10zphAtlkjT5VyMQHDpijWqhwm6kpkU_SOtXgmdb-JaHdpMQTjqATxkNRsCaFJHPy8Nek_OXSBuFS_V48ZKjRqaXLYjD3pve69M1vNSfJ5NAEpw', 0, 0, NULL),
(197, 2, 'Куаныш', '+7 (775) 660-71-31', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-09 09:22:31', '2021-08-09 09:23:06', NULL, 'dWkJ817lRlWnZJkjWs0NFa:APA91bHLULE3gcJOxcFRnXz1WXnJfXG47ssEQ_TkohoovorKEbPUmkDdLdKpnOyQI16YPGUScAMq_nMI-Fav6f9Cfa85J2tGJrYRtN4cO4eGbu8ggu0fBgHAyAoG0vZGmqWFo8c7_t2q', 0, 0, NULL),
(198, 2, 'Ержан', '+7 (700) 780-90-09', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-09 10:44:47', '2021-08-09 10:44:56', NULL, 'eizT8wGSQ3mLZDJ58rGnbu:APA91bGb0zzI-DyYKBJXrEExIqqv6k8PZ7FXVa0_Ye93tpLdecvaPqnzRChegeShI7QJs8swK9KFJAFGaIpXBi4xuQ26VEWE-EPCre4yqEYWcVn2GV3DLupWEsYQeU0Kmvm-pNGR-otZ', 0, 0, NULL),
(199, 2, 'Владимир', '+7 (708) 904-64-22', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-09 11:38:20', '2021-08-09 11:38:34', NULL, 'dqMaCIddTVqQUW_B8ILYyB:APA91bFf_B6SlIRdkVNkc5PQIRX85C-kognN2d9_CO7UXI_JHYYJ_aeOvUj5OK_LElPLzntD8Qz1AzZoAYjIG74X6rMKMITxzK9QeuMnqX5b0kowYc5OtRgTFAI9cqgM5tPTI2CFHHa_', 0, 0, NULL),
(200, 2, 'джавид', '+7 (951) 380-81-42', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-09 22:47:06', '2021-08-09 22:49:18', NULL, 'ccYxOFqfQZmjZwPEgPoF8s:APA91bFZgikMkv6LBfbuvYdP4U0Un0E2ofn19m1nky5gado8-4PFLEyRbIhrDxZMTmL1RbZAxSbi_q67YaoekuW1lOB1AKFKLKid-hRgsrIy7rINj3ov4nXAZTJs8aDh4DNm614Mztv7', 0, 0, NULL),
(201, 2, 'Жибек', '+7 (776) 223-58-81', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 00:05:42', '2021-08-10 00:06:07', NULL, 'eHmNFV6pSgmZuJFtAMrC0J:APA91bHX3VrfPOpre432sEHSvryMcpYE1I1wzp8JcluhjJYVVDA92laygoNndOeSbzzIX6AT3dv2SIoov1YgrW_cfPGxO8QRWjNMBr_0UR7igfQhcnKfd-LpEcdYn9lKAFNh4FgCAPs3', 0, 0, NULL),
(202, 2, 'елена', '+7 (771) 414-45-00', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 00:30:08', '2021-08-10 00:30:59', NULL, 'diVhA84zQTyFpqq2pUHwdZ:APA91bHm4h0uF4H6SY15HTNne1qeF3m2WMgOEwKWXu1oJ__UJzn9D45jF4gU6rShH6D1ANhMyk4ngy1XuNq466rdqNgYU4_hXvOwcC6EG-rXwzTq5jCenalj87XDt3gwjk2z6z44HX4r', 0, 0, NULL),
(203, 2, 'Еламан', '+7 (705) 313-85-27', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 00:56:09', '2021-08-10 00:56:22', NULL, 'dXXO89ZJQkGP5HNWUKJ2H0:APA91bHS6OFYhy-dWdsG54odH-_t-SkGgdTWv172L6zdbGylJdXpe1lHgPs4HK4H_poc98YsCvujZpAnbfEm9uzG4Wnpp3Rf6wk4_h8U6He7KS_zITRUtY3KDORlRgblR3ZHq107A1-T', 0, 0, NULL),
(204, 2, '750613402704', '+7 (705) 717-35-02', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 12:27:13', '2021-08-10 12:27:13', NULL, NULL, 0, 0, NULL),
(205, 2, 'Жадыра', '+7 (747) 940-06-32', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 15:01:40', '2021-08-10 15:01:59', NULL, 'coEP1ULPTKqQ04yM4UTOyH:APA91bEkAhG9LfIw8C6_b9i3PmMHDCZptMXBTHnM5cAKXAhIV4mlu313LA2QzGLTfwNXB5P8d7M9IgyirbuQsSfOmWXV9pkPHvlWsx51F5WvQSH8xp050Dn7wSakVw_YJikAY0tqEw0B', 0, 0, NULL),
(206, 2, 'Максим', '+7 (919) 935-49-74', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 15:39:05', '2021-08-10 15:39:29', NULL, 'fVyTwUc-Rza8EXFDlHPBGI:APA91bEEEjmkYFL1D9ABCvv71DK4c-IXdi9OmOEU_4ixWhfnGm9kB8ZUSbSQPNXhrwzyt5whrvp5a6cBApk3MC55ceKdU5FRS3SVCIeLvSmwxZw1ASn-fCwo3UiH1SKQUyEGZHfBlS5U', 0, 0, NULL),
(207, 2, 'Ануар', '+7 (707) 550-55-93', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-10 19:40:32', '2021-08-10 19:40:54', NULL, 'eFu3TxU_SeeOcYylIs2p3o:APA91bGzUHfDwnsC2Na9CCqGRRYI1-vQ4itrXNWvkQZL2Q85z5EvGoHS8hTB0MebkzEGYOYjhsj7qZ-1CDcLfMHTRfVno9weV9org1pOwobe8QQSBElBbaZUe3fho99jJeiX8mKxsRdS', 0, 0, NULL),
(208, 2, 'Ханшайым', '+7 (707) 761-71-95', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-11 02:46:13', '2021-08-11 02:46:30', NULL, 'e3kxczQoQ3iljtf7kjiqsX:APA91bHUsjUinBl5nKf7YiNM0bITUW7tscz0ckxq7v3kL21R7XvJ21zIx8-sU3NEc5FzfvDZLeVzRvZs-r9kptbTTxvNKk9biRMZOhOSrnZ4noDfn2QJZLkM2B-6Z_5cr72dgT2nu2z4', 0, 0, NULL),
(209, 2, 'Асхат', '+7 (777) 205-84-94', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-11 08:50:20', '2021-08-11 08:50:37', NULL, 'cNl5y-e1QFagbr_aPrAv1F:APA91bF4Vbw5Ec7N2w8QXpFLSD9HyCttZ-ftwX5vhzGLLHho9nP2gbhwvdRAlR2ZGHFGIQBeEXDkkWKVxDCLL6fb8ZzOYwzHBZp6bbsqRc63X9_QaKhTRPZa9FeqDrUAc4sMY7Iq5swp', 0, 0, NULL),
(210, 2, 'Бағлан', '+7 (702) 277-41-86', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-11 09:51:40', '2021-08-11 09:51:53', NULL, 'dDQxirECSdG6eoPdQPz9t-:APA91bGRhIboBCZQZG223guPuYdY8tV-_voXEszTJDTs3aLi7PymceUkkTlHLUPk6yAuDpjsF4OtUhi57Lujl9mdt4wMLLsEXpowFHKQF1AtMwv_j3qYF81yLxuJ08uSAnprnlx6iJSm', 0, 0, NULL),
(211, 2, 'Олеся', '+7 (777) 769-68-80', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-11 10:00:32', '2021-08-11 10:00:48', NULL, 'eAdyOGd2RFCUjI6rhw0Eoa:APA91bH19rZTwixjzl9LHWBoUrSK2W2etlz0i-oyQAYtbqKrB9z__U6PhBj0zFKzdXE0CcLEklY5mYzkOtvuWDCMyP-VcdOVfc9vkhLdJZrMhrhPN2ctLNQoJALp-DdA1SS_kOFWovKr', 0, 0, NULL),
(212, 2, 'Ирина', '+7 (702) 447-67-63', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-12 09:10:23', '2021-08-12 09:10:41', NULL, 'fHkhWhNuSCeEZOCpxPBfek:APA91bFNQcoKm-lhaW-vHG9R7ujuYPqOfhwhnP-8rUM4PEXWJ1Pgxqfog5UYskmfQT53yUbKk0Y2fPZ_VtIhxnhU8IYk_FMRJUqHFdodyCOU7AYuTJZwZVVqu_ui4JUuxf57Yq_6qIXf', 0, 0, NULL),
(213, 2, '/', '+7 (928) 560-96-32', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-12 15:43:54', '2021-08-12 15:43:54', NULL, NULL, 0, 0, NULL),
(214, 2, 'Елена', '+7 (777) 733-33-36', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-12 17:55:31', '2021-08-12 17:56:13', NULL, 'dpDj3WBIQ12QA8aY9GZaRc:APA91bHZQ7PHvhSlRpYfC2lo-yx3u5BAjvRX09Qcf221mkKz7cUrN4UJ1cYfSnVpwZoL2sUA8QNSiBXyHvqalDj2DSJuR0MHcqQ7ULyLOdcRSwJBm2mkTSnrGd5doeRB2oSc0YSPOEDd', 0, 0, NULL),
(215, 2, 'Ислам', '+7 (705) 177-17-75', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-12 19:27:59', '2021-08-12 19:28:16', NULL, 'fhXbFAfiTNWfwfVFzrRajq:APA91bFdJXWIf6wKFfu1UAjBVmNejnrx9gyCY5gwXT2gpNVk8NChQjNmZrUwnCbjv4V-FCpenXXvSLMpQNjW4Zd6xDXcT_B9G8BL2NWndGBpwdUavKAdWzWuYa58R_9ecoV3hU2zeMR2', 0, 0, NULL),
(216, 2, 'Алимжан', '+7 (708) 292-02-26', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-12 23:26:29', '2021-08-12 23:26:29', NULL, NULL, 0, 0, NULL),
(217, 2, 'Мухит', '+7 (705) 353-53-57', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 08:35:12', '2021-08-13 08:35:31', NULL, 'cP6UVdyXTqelHz_DFS-uXs:APA91bFQ_Smj6ldc4Ka3xaUKnVDoIjPMmBjml5LTX58WgbBPE6yYmhUWOKtILJhQLKRP4vk8pwYkneWOrRk_mKxa3vuJdh3ThqFLlu7B4hMCfush0hI0Zk0WVqQ25x7H4e6iurM6MCkB', 0, 0, NULL),
(218, 2, 'канат', '+7 (775) 187-72-66', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 10:09:59', '2021-08-13 10:09:59', NULL, NULL, 0, 0, NULL),
(219, 2, 'Андрей', '+7 (705) 676-66-10', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 10:13:55', '2021-08-13 10:14:23', NULL, 'fv-xDoyAQ_GXFqwV7gmhBa:APA91bFx_4GO9c4xqPp6Jfmbacmf5Dncx7Q1J1-20b7Re-NRxTM-7-xPRSHg3Ks_bCwUQdd7mktUiKC_oM3kSZBtL4ePaa6njkMRPIOPyNxDW_mAEg0pqobvuXyw-tujXj5L9Vq96XQB', 0, 0, NULL),
(220, 2, 'Султан', '+7 (707) 815-73-72', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 10:38:54', '2021-08-13 10:39:13', NULL, 'dTaYABfCRoSKfcs_EO5tOT:APA91bE2By91XENZfIp7AyUu8qd8Af_6ErJGUp297uQK7AqlsYTWORlbzYSA_itUeSDdtdn5ix_Hw2y1embguasSC5azLOfU78PahvbtB5O5QNFkhvY3IUxGyJ43X0g8Eg5ju2D1UGKh', 0, 0, NULL),
(221, 2, 'Михаил', '+7 (747) 583-82-75', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 13:21:17', '2021-08-13 13:21:31', NULL, 'fYU6Xcw3TPCkOyARFKwLr5:APA91bGndXQ7BZEFICW1sRDvtWBWFLvw1jnyVCrVCeWVJEJaRrWnvdITPQYzUaz4ma6EaXZVGmlyeMPak8wje7T6nEEQQF65lzlPbQhPgEh-7zlvzvR0HOR82dTaxiNz2SruIep14-qo', 0, 0, NULL),
(222, 2, 'Ольга', '+7 (705) 333-30-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 17:35:35', '2021-08-13 17:36:36', NULL, 'eGiBCB1wTOqN2tKlpobJGK:APA91bE0agLO02Ld8Ge4ES3PtVW-p2d4tG6eIzwMYRMXuItF1h2p1vnGL4DsAyGIfz6LdmUso9nZobGG6gcT7sCA0vIBUUfZBxhcaeBcjfBAqOTMOsw2wtJmlOGNIaikrkZX92qDXc85', 0, 0, NULL),
(223, 2, 'Юлиана', '+7 (906) 234-32-40', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 20:00:05', '2021-08-13 20:00:16', NULL, 'c9SsqBmJSwuRYvHOOiK195:APA91bEWC2X5II0_7IQhxrDrPGUaUuMwoMZC1yNSz0gcgcRWZHvN4ZJqALqBKEs5cIeoPrqgsUpF9DTWd1UrWLWlo1RPtGiqoT7HpdjNXg_tSeDadyOa8OQd61k35HQ3Qiu42xyP_ltp', 0, 0, NULL),
(224, 2, 'Нуртас', '+7 (707) 404-45-36', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-13 21:45:28', '2021-08-13 21:45:44', NULL, 'da_CrAYmT5Gr8DleRJ3KbK:APA91bENcBW2wLp5sQvUadvSL-HTNyNXW1p52xUjPXvZj1GGjJre0Zyz5kPfrD6Lh0fePXgzqZTCkECf46GSK7FsX4hpKK0tVpbVe_EBZwU6BpdLTVIV96weHbyksAB_y0lCCNVlyJlg', 0, 0, NULL),
(225, 2, 'Батыр  — Али', '+7 (775) 330-19-25', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 00:57:13', '2021-08-14 00:57:54', NULL, 'ezKP7dhdSei8sKeiXGL4Gf:APA91bFt4ktVcdDA48MOVJXPSmzMXZfNMNmA9wiAy9pvJnLqw33g96aTWkCPoaLt68c7dlxLAESsMfVBHTSb7lZI8oGA3eFmo6v6aQXwvIWlWbtEF0HcBO-6hysO6DIAlZrWvlYIUfCU', 0, 0, NULL),
(226, 2, 'Алим', '+7 (747) 552-08-58', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 02:35:27', '2021-08-14 02:36:12', NULL, 'cqStxuNRQ9yqlXKVnrYO4n:APA91bFFAL2t92G9YUSwE2W1EqXOUzDja6b7PG9LiaKC9XaW13B8semUaeSMT6aZ9n2PuRAzDy4T6xsvTnnloKeY4iDPHHei63YW459-dL_9d8D6arFPEiVOcMN_SXVBHX4oCYGULlBb', 0, 0, NULL),
(227, 2, 'Рауан', '+7 (702) 329-52-19', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 12:16:51', '2021-08-14 12:17:05', NULL, 'cnmT0IvWTiWAJMkFQNlut2:APA91bFNGVaPhEuNwYhVtOfd0ytyRprwkZzlGwITYBxlXxUZfSx2PlbntnkDLqyjzW_6_iDiIRrcnisALuAdavZu300o66PUTffAYJ-CBpQqSJgQ70SjZM4vN5-EMAHDQpFkhi9e9CUX', 0, 0, NULL),
(228, 2, 'Гани', '+7 (708) 367-30-31', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 13:56:06', '2021-08-14 13:56:24', NULL, 'cdRNfF_wR_KGYbqEG8-G0h:APA91bGaKpcNWlp5p8CXZjWXBaIqjI-XQjovbhXFhnjj1Kspes5LMwARYrOdGJNRB8AT-Y4ERpGrvVl2_ev0L2QRLSJKLJE-eLWOwUvaHkCntyqFxLqpUlwkkY5rhPVuPZMmE0mxEjSJ', 0, 0, NULL),
(229, 2, 'Юлия', '+7 (707) 134-77-63', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 14:36:36', '2021-08-14 14:36:53', NULL, 'foKXT2WDQ0iraeLbJgJ7R-:APA91bEmlgMdCGlQNJoo4Ie2dESl4GRtfZjoEBvy3KA4-vxvoQ69TfPOjbIh88UZtwW8Cyzmj0jWgew6e1QRknbqrUKzq-oMTltJkz73Tk1SaiJZOLCuv-7X9_aVpzJa5QkH_HVdwJ6V', 0, 0, NULL),
(230, 2, 'Айви', '+7 (777) 356-70-42', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 18:46:09', '2021-08-14 18:46:28', NULL, 'd6cuHVU1RmmpTWCdpFRGuV:APA91bGeCobHowCKjvJAyREGQARoPm282mBIBUd0hRwyZFxVwnYASSNXFXWMcbz-TxLL-Mn2U_GqJxFGsYJRigYa7qy_jy2YLveenu9XPiNu-HTGTD7KxdT9j5VYh34oos6B1pfLzbmv', 0, 0, NULL),
(231, 2, 'Курмет', '+7 (707) 949-42-92', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-14 21:51:19', '2021-08-14 21:51:41', NULL, 'csppLl73TrqF-a0GxgRwF_:APA91bHWP1scYudvyzW9MhLC-cZmPMUpZY0faTzCLfpv4aBr8vGn47xyAwozxsizgTuRYoGD8doVrlnQM9JwSlp0VK-83yJ-iVS1oGFxLeIhvDIDJPIOB-arxQiojPK14V29wTbkrlRa', 0, 0, NULL),
(232, 2, 'Ануар', '+7 (778) 995-70-00', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 00:28:51', '2021-08-15 00:29:08', NULL, 'cBfZk_i1T8GkWSeOTzTwEo:APA91bHXcJs0_lXODLjRhAaoD42ATHPhJMAkoRYs3TXCaXGdN_BWDYlQMexs8VudIVM9U5yiLJ0r-tTxSQ6AMf1ZoXGY40YRE5ciKyvpSLSltIXXoWBjPCrQzu_-hSnpP3ZIMi8t8ggR', 0, 0, NULL),
(233, 2, 'Александр Владимирович Снежницкий', '+7 (705) 111-67-54', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 01:10:31', '2021-08-15 01:11:05', NULL, 'dx7BDdSzTZC1i9e7ASZH6W:APA91bGp2XnO1psPtV-zCk2yMLa6ex6Ua-TkRZgjmDuD3G1JbO9JcjYCcfQ2eA-whXk7WEllUYUfKErhTmdVnDpyGnVIMvXnT-YaKt6wtT-yBRzhSdaPp86Z3A5t-Af_9mLQDqS9IemB', 0, 0, NULL),
(234, 2, 'Владимир', '+7 (708) 512-01-84', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 03:06:39', '2021-08-15 03:06:51', NULL, 'f_8JKzR1QDm0X5dOq7Mlyz:APA91bGqY2WB9PIaf1zG0sotJgYGo57akDMvLnp7w3Go-5SEFhQeiXEOO0AUHOUbdbMJsm7sNkKfH8Vdb7relUSHdrKtZzAKE5x1P1LYgVe9B3WPFDR_1uQwyzV6pGLuPr7RTqpdo_1p', 0, 0, NULL),
(235, 2, 'Дмитрий', '+7 (701) 063-69-99', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 04:07:11', '2021-08-15 04:07:28', NULL, 'fwIKrNS9T5G7brJ1L79yMj:APA91bGUWiAOHPRPkhvyebOHeoZ4A-LKHmZTCLyOnbN-ZYW4oNqmT5mL1cFBUUZpHKiidc96O-UQTQoU3t0diIgEf6UkD3jJDzAd0grP2FHk3QT3tw5_7uPIev8SONomOeCDKYgzjMrJ', 0, 0, NULL),
(236, 2, 'Әкежан', '+7 (708) 719-63-72', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 09:46:18', '2021-08-15 09:46:33', NULL, 'c5htNMbtTHyoh-4prK10k4:APA91bFUP-VrqUDA9MeFJcDQa5erUp9_KDMMRPsAn_M2zEQmN2NlAQS_MPgppz1XE-nA5GW3dej83nhZVYxoQlgqrhhD5_W0Ghkq35Xz1Ky8N4L0T1BrsLEOC3FbZA4b3yQGYUluKJXL', 0, 0, NULL),
(237, 2, 'Марат', '+7 (771) 888-32-41', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 10:26:52', '2021-08-15 10:27:11', NULL, 'dN_7RvzGQ-KYFWXDN1yGIV:APA91bEnTUdX_qnkezCU_K-9-EpoLEWuOprGqUk1lXUSZk7XV3ZJXUvtsgIwEVq9Srkwcrvt1hKJI84QqqIr73u5qrZmfoYwI1v2Ui9Bo-Ln4g5Bjs2w_nhTR6FIF6fkxWEqNvAUdrax', 0, 0, NULL),
(238, 2, 'давід', '+0 (966) 872-25-9', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 19:46:37', '2021-08-15 19:46:37', NULL, NULL, 0, 0, NULL),
(239, 2, 'давід', '+7 (096) 687-22-59', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 19:49:11', '2021-08-15 19:49:11', NULL, NULL, 0, 0, NULL),
(240, 2, 'Алёна', '+7 (707) 328-76-62', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-15 23:21:08', '2021-08-15 23:21:23', NULL, 'fHJw7flzQwqyk79euyVrte:APA91bGVWNx1YkYE8-Gj6v8dlWhwidnFFbf9XmxmIRlHpDCizsPC9GGGCHFkRNiG1IHg4ez5Si3P2n6AyWc0GYAdM4WoQDLNEd0fK1XrKJxTehtgh9TmmaE0axs66Y64x_dGx0jtpCj5', 0, 0, NULL),
(241, 2, 'Вероника', '+7 (705) 950-10-96', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 00:40:52', '2021-08-16 23:01:16', NULL, 'eYEwAlF0Qca42Uc5nQA3gi:APA91bEIRTrmTweKDRZIYi5zlI-cuzCqJFdQn_dLnx-028-mown6whWRsOTE8L9hpu4oWbYw0s6-FSuipmVITlFoAiHdJirfTSX1T0VHVplPOwQjAHI9iNhvj9WAbDcg3Pq5B47_anbz', 0, 0, NULL),
(242, 2, 'Салтанат', '+7 (747) 069-53-99', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 01:14:44', '2021-08-16 01:15:13', NULL, 'd_D_RNssS7aoZ3jT3RqWpk:APA91bEoSLmD2mmcO3L8_zhO81Ht7RNlXLlIIjREMPyfWsHqB4-bETYOnWly1OHQI4i5X7lxf5H_AI7Ln-4DOHfL_Pr0WEe7DCpNLluWn_g6vySGATbhsIh9-XqxmRVP0U5vI92pYjvn', 0, 0, NULL),
(243, 2, 'Ирина', '+7 (747) 506-17-99', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 03:08:13', '2021-08-16 03:08:44', NULL, 'edkqdoXKTxqHjFa0haBiyt:APA91bE_6_9XG9Kp2EAcens_G5ejuxMwMXL25wSwT1xjKrLb5BrWzDyL3S0Q4krHJ-NLZSo7Hxs9_MGDWjxneXqKOhGscZxzQmxHtVRfPA1j6-1hxp10of9u-1JPqP2wee_CoMSg_jsj', 0, 0, NULL),
(244, 2, 'Ярослав', '+7 (777) 489-89-96', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 06:28:51', '2021-08-16 06:29:12', NULL, 'fvl8_9WBRQOdZ3gUne66Em:APA91bFx8Up3kjSaachROe7AdKcBAD9reLt1rGHFcGzB4bwUVBjcBPcdQkWBuVEd9LTfmdTdgqYCM0zr9Fd8WPC9Ed5DoZqRSHJQhK00XS_Pb0iAxWxlEvYjgi4FKmBhwS9__jCnUYnl', 0, 0, NULL),
(245, 2, 'Мария', '+7 (707) 315-72-69', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 07:28:26', '2021-08-16 07:28:45', NULL, 'ddqbuniVToC6tstCuDYjD9:APA91bGeqPTmo0-6sgmqvIH9e7i7t2vGgsenVQCu7yBZD4i8FPD9mscv9TM6RFRs5aissrLY307Bv0KYJCexgmkn__-ogD7rvFVjyc5l_GcewF98tvRrB8Js79eIX1_SiwhUZvEY3jFG', 0, 0, NULL),
(246, 2, 'Руслан', '+7 (707) 549-10-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 10:05:00', '2021-08-16 10:05:39', NULL, 'c_8wpsQDQ4KTEhRTJ1Q8Y9:APA91bFLHyLFJfnBTc3_PytQ_9jUbQNbLJdZS4F4h7i6D58DFZfsYtlOPjrlPI2cztnPnyR2Vl1HOzAvsm5UTeOdM5c3TtTD9AZ1X4o4DPcGHyqtNgmYhC4ziK5r7flESSvj538TdAU8', 0, 0, NULL),
(247, 2, 'Акимжан Казыбекович', '+7 (771) 720-09-10', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 22:02:40', '2021-08-16 22:02:40', NULL, NULL, 0, 0, NULL),
(248, 2, 'есен', '+7 (747) 424-30-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 23:49:05', '2021-08-16 23:49:05', NULL, NULL, 0, 0, NULL),
(249, 2, 'есен', '+7 (770) 210-98-17', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-16 23:52:37', '2021-08-16 23:52:37', NULL, NULL, 0, 0, NULL),
(250, 2, 'Есжан', '+7 (776) 672-12-89', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 02:23:50', '2021-08-17 02:24:06', NULL, 'cZE2L-jsTNOvMMf3KImmVT:APA91bHQbGAfp-LPxrWftTjGrMQsQ-ERP_e8QnxzYEo7i8dyInZalgDUAzKQfd49bJ7gHJkGJdjFtRAdVNhdUhAV5DfP_6RcmAQp1kG3dUxHZu9xkQcROSrYufAGxZIMYyu4wIFSmORm', 0, 0, NULL),
(251, 2, 'Акмаль', '+0 (558) 088-88-1', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 06:03:03', '2021-08-17 06:03:03', NULL, NULL, 0, 0, NULL),
(252, 2, 'Разием', '+7 (747) 274-69-85', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 11:18:53', '2021-08-17 11:18:53', NULL, NULL, 0, 0, NULL),
(253, 2, 'Эдуард', '+7 (747) 388-27-10', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 11:33:47', '2021-08-17 11:33:47', NULL, NULL, 0, 0, NULL),
(254, 2, 'Юлия', '+7 (777) 830-32-23', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 11:51:53', '2021-08-17 11:51:53', NULL, NULL, 0, 0, NULL),
(255, 2, 'Асем', '+7 (707) 270-03-93', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 13:20:11', '2021-08-17 13:20:33', NULL, 'cORCvBIsSb2xbcMYGgFXH5:APA91bHrbvbjl00GeI28uW178XCjkWPhSPWCC0BQ3QNZh4n5KnPWSZ1E2B1B2pGvz08D_J7WwbriIBvlf3X6v7ICqO_w18TCIpxa0KudfnQLp7lkc11pO6AvUku0wVQ6D3aWr3NRZjuL', 0, 0, NULL),
(256, 2, 'Расул', '+9 (967) 031-59-47', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 14:26:12', '2021-08-17 14:26:12', NULL, NULL, 0, 0, NULL),
(257, 2, 'Абай', '+7 (775) 303-93-61', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-17 14:27:46', '2021-08-17 14:28:03', NULL, 'f204VQLkTEKnWygBV85dkz:APA91bF6SOB3CMbHDOmxrP4avridE9m9gsemKS-0FXwUk7uj8G92d6oegKD9xh1MtTnr8K-l0R6IYGCZk47CqoXs5Z8OiX4xUiWhXG50FrAxsRltLA5BVg2jj8RTQ7NOJImPmiGMszAQ', 0, 0, NULL),
(258, 2, 'Петр', '+7 (705) 501-40-11', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 03:22:32', '2021-08-18 03:22:32', NULL, NULL, 0, 0, NULL),
(259, 2, 'Евгений', '+7 (776) 989-43-23', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 08:30:31', '2021-08-18 08:30:31', NULL, NULL, 0, 0, NULL),
(260, 2, 'Сергей', '+7 (701) 461-20-03', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 08:32:11', '2021-08-18 08:32:11', NULL, NULL, 0, 0, NULL),
(261, 2, 'Айым', '+7 (777) 014-59-29', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 10:57:29', '2021-08-18 10:57:29', NULL, NULL, 0, 0, NULL),
(262, 2, 'Султан', '+7 (747) 299-51-03', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 13:13:32', '2021-08-18 13:13:32', NULL, NULL, 0, 0, NULL),
(263, 2, 'Надира', '+7 (702) 677-75-80', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 13:38:56', '2021-08-18 13:38:56', NULL, NULL, 0, 0, NULL),
(264, 2, 'Алия', '+7 (707) 261-98-81', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 13:56:59', '2021-08-18 13:56:59', NULL, NULL, 0, 0, NULL),
(265, 2, 'Нуржан', '+7 (707) 763-17-99', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 13:58:36', '2021-08-18 13:58:36', NULL, NULL, 0, 0, NULL),
(266, 2, 'Sabit', '+7 (777) 767-01-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-18 14:50:26', '2021-08-18 14:50:26', NULL, NULL, 0, 0, NULL),
(267, 2, 'Акимжан Казыбекович', '+7 (776) 835-25-32', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 00:07:01', '2021-08-19 00:07:01', NULL, NULL, 0, 0, NULL),
(268, 2, 'Ержан', '+7 (747) 602-94-82', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 00:22:15', '2021-08-19 00:22:15', NULL, NULL, 0, 0, NULL),
(269, 2, 'Владимир', '+7 (747) 727-94-32', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 01:07:34', '2021-08-19 01:07:34', NULL, NULL, 0, 0, NULL),
(270, 2, 'Алгабай', '+7 (747) 807-85-68', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 10:46:35', '2021-08-19 10:46:35', NULL, NULL, 0, 0, NULL),
(271, 2, 'Серик', '+7 (701) 502-68-01', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 11:18:38', '2021-08-19 11:18:38', NULL, NULL, 0, 0, NULL),
(272, 2, 'Жумагул', '+7 (747) 581-74-89', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 12:21:20', '2021-08-19 12:21:20', NULL, NULL, 0, 0, NULL),
(273, 2, 'Дилшод', '+7 (702) 542-83-89', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 13:33:55', '2021-08-19 13:33:55', NULL, NULL, 0, 0, NULL),
(274, 2, 'Марат', '+7 (777) 489-50-52', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 13:56:12', '2021-08-19 13:56:12', NULL, NULL, 0, 0, NULL),
(275, 2, 'Сергей', '+7 (771) 403-18-65', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 14:06:57', '2021-08-19 14:06:57', NULL, NULL, 0, 0, NULL),
(276, NULL, 'Бота  Ерланкызы', '+77020692723', 0, 'botakan.tleumbekova', NULL, '$2y$10$yYrxqiIQK8A9uEoxp67AheMmjVvMVgqFZSXU.eJFz7VopvYV2X0Re', 'users/August2021/lcQ2rv7rf6rJP3gA9a2T.jpg', NULL, 'lEI4jC4soC3YjII1Rvtw2K4I9qVgfXjTsamoe1o32Z4bqX7Hd6VaLPuOQ4vK', NULL, '2021-08-19 15:17:18', '2021-08-19 15:17:18', NULL, NULL, 0, 0, NULL),
(286, 2, 'Иван', '+7 (777) 299-51-20', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 16:15:10', '2021-08-19 16:15:10', NULL, NULL, 0, 0, NULL),
(287, 2, 'таня', '+7 (771) 107-01-48', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-19 18:25:16', '2021-08-19 18:25:16', NULL, NULL, 0, 0, NULL),
(288, 2, 'Дулат Есимханов', '+7 (777) 131-00-14', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 00:28:08', '2021-08-20 00:28:08', NULL, NULL, 0, 0, NULL),
(289, 2, 'Анна', '+7 (747) 849-48-93', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 02:01:43', '2021-08-20 02:01:43', NULL, NULL, 0, 0, NULL),
(290, 2, 'Ниязов Ильдар Рамильевич', '+7 (771) 914-86-94', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 02:21:58', '2021-08-20 02:21:58', NULL, NULL, 0, 0, NULL),
(291, 2, 'Руслан', '+7 (777) 013-97-36', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 10:11:03', '2021-08-20 10:11:03', NULL, NULL, 0, 0, NULL),
(292, 2, 'Максат', '+7 (701) 990-40-82', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 11:49:22', '2021-08-20 11:49:22', NULL, NULL, 0, 0, NULL),
(293, 2, 'Ольга', '+7 (777) 585-94-39', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 11:51:36', '2021-08-20 11:51:36', NULL, NULL, 0, 0, NULL),
(294, 2, 'шынгыс', '+7 (747) 508-16-46', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 14:34:13', '2021-08-20 14:34:13', NULL, NULL, 0, 0, NULL),
(295, 2, 'Шынгыс', '+7 (747) 256-26-15', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-20 14:43:14', '2021-08-20 14:43:14', NULL, NULL, 0, 0, NULL),
(296, 2, 'Дулат', '+7 (747) 423-35-30', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 00:07:18', '2021-08-21 00:07:18', NULL, NULL, 0, 0, NULL),
(297, 2, 'Orken', '+7 (747) 835-24-28', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 08:06:54', '2021-08-21 08:06:55', NULL, NULL, 0, 0, NULL),
(298, 2, 'Есбол', '+7 (707) 574-47-11', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 11:09:42', '2021-08-21 11:09:42', NULL, NULL, 0, 0, NULL),
(299, 2, 'Дмитрий', '+7 (776) 218-32-19', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 11:32:28', '2021-08-21 11:32:28', NULL, NULL, 0, 0, NULL),
(300, 2, 'Ерен', '+7 (708) 900-29-45', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 12:48:53', '2021-08-21 12:48:53', NULL, NULL, 0, 0, NULL),
(301, 2, 'Регина', '+7 (705) 114-10-58', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 14:08:27', '2021-08-21 14:08:27', NULL, NULL, 0, 0, NULL),
(302, 2, 'Амангуль', '+7 (778) 612-60-90', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 19:41:55', '2021-08-21 19:42:11', NULL, 'epi3f2L3jEfQtdapSfxaTp:APA91bHO-sRmUUSPw5LhEQL_EeRT62sSBd9XWPjtEO26igx6aRr8FsjAfgT7ASWsA0fPLlFy1Gj6c8fr6ZTP23xiq_S8n22_OqSTnz9FJodM16C2osND4HwbKyeMqP3aLxOMYUVD4o54', 0, 0, NULL),
(303, 2, 'арман', '+7 (911) 743-65-29', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 23:54:58', '2021-08-21 23:54:58', NULL, NULL, 0, 0, NULL),
(304, 2, 'Асылан', '+7 (776) 722-86-36', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-21 23:56:59', '2021-08-21 23:56:59', NULL, NULL, 0, 0, NULL),
(305, 2, 'Алла', '+7 (775) 628-71-59', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 00:49:57', '2021-08-22 00:49:57', NULL, NULL, 0, 0, NULL),
(306, 2, 'Нурлат', '+7 (702) 289-86-05', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 03:13:00', '2021-08-22 03:13:00', NULL, NULL, 0, 0, NULL),
(307, 2, 'Рахат', '+7 (701) 942-40-28', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 08:24:00', '2021-08-22 08:24:00', NULL, NULL, 0, 0, NULL),
(308, 2, 'Эрнест', '+7 (775) 186-56-55', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 13:04:28', '2021-08-22 13:04:28', NULL, NULL, 0, 0, NULL),
(309, 2, 'Исламжан', '+7 (701) 122-46-72', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 13:37:30', '2021-08-22 13:37:30', NULL, NULL, 0, 0, NULL),
(310, 2, 'Мадина', '+7 (700) 477-19-91', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 15:37:53', '2021-08-22 15:38:10', NULL, 'cCQct4QUF0GxgDC4ZzTRf0:APA91bEA0t0ych4BvDOgnmhY6nkSLc5I31IR5-Wpm-57JTej8KlDqSm-8FkfyzdpVf9VCeJXiyxRXtDyX8dHUnQTOGCvqYjk2NMhVAstDBRA6-j5FQTdJHOtIz2GQan3DS2Ak52uHty4', 0, 0, NULL),
(311, 2, 'Малика', '+7 (778) 758-83-30', 3000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-08-22 21:03:15', '2021-08-22 21:03:15', NULL, NULL, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(8, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cards`
--
ALTER TABLE `cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cards_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `documents_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `document_categories`
--
ALTER TABLE `document_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document_categories_document_category_id_foreign` (`document_category_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `messages_order_id_foreign` (`order_id`),
  ADD KEY `messages_user_id_foreign` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_payment_status_id_foreign` (`payment_status_id`),
  ADD KEY `orders_order_status_id_foreign` (`order_status_id`),
  ADD KEY `orders_lawyer_foreign` (`lawyer`),
  ADD KEY `orders_document_id_foreign` (`document_id`),
  ADD KEY `orders_service_id_foreign` (`service_id`),
  ADD KEY `orders_card_id_foreign` (`card_id`);

--
-- Indexes for table `order_statuses`
--
ALTER TABLE `order_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_order_id_foreign` (`order_id`),
  ADD KEY `ratings_lawyer_id_foreign` (`lawyer_id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_user_id_foreign` (`user_id`),
  ADD KEY `reviews_lawyer_id_foreign` (`lawyer_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sms_codes`
--
ALTER TABLE `sms_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sms_codes_user_id_foreign` (`user_id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cards`
--
ALTER TABLE `cards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=385;

--
-- AUTO_INCREMENT for table `document_categories`
--
ALTER TABLE `document_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1045;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;

--
-- AUTO_INCREMENT for table `order_statuses`
--
ALTER TABLE `order_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_statuses`
--
ALTER TABLE `payment_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=295;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `sms_codes`
--
ALTER TABLE `sms_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=312;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cards`
--
ALTER TABLE `cards`
  ADD CONSTRAINT `cards_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_categories` (`id`);

--
-- Constraints for table `document_categories`
--
ALTER TABLE `document_categories`
  ADD CONSTRAINT `document_categories_document_category_id_foreign` FOREIGN KEY (`document_category_id`) REFERENCES `document_categories` (`id`);

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_card_id_foreign` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`),
  ADD CONSTRAINT `orders_document_id_foreign` FOREIGN KEY (`document_id`) REFERENCES `documents` (`id`),
  ADD CONSTRAINT `orders_lawyer_foreign` FOREIGN KEY (`lawyer`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_statuses` (`id`),
  ADD CONSTRAINT `orders_payment_status_id_foreign` FOREIGN KEY (`payment_status_id`) REFERENCES `payment_statuses` (`id`),
  ADD CONSTRAINT `orders_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`),
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_lawyer_id_foreign` FOREIGN KEY (`lawyer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `ratings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_lawyer_id_foreign` FOREIGN KEY (`lawyer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reviews_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sms_codes`
--
ALTER TABLE `sms_codes`
  ADD CONSTRAINT `sms_codes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
