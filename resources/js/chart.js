import {Chart} from 'chart.js'

let ctx = document.getElementById('main-chart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3]
        }]
    },
    options: {
        scales: {
            x: {

            },
            y: {

            }
        }
    }
});
