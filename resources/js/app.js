require('./bootstrap');
import Pusher from "pusher-js";
import axios from "axios";

var pusher = new Pusher('71b7e222b90fdffa56e6', {
    cluster: 'ap2'
});

var channel = pusher.subscribe('App.Chats');

channel.bind('pusher:subscription_succeeded', function (members) {


});
channel.bind('chat-created', function (data) {
    playSound()
});

axios.get('/admin/user').then(response=>{
    let newChannel = pusher.subscribe(`lawyer.orders.${response.data.id}`)
    newChannel.bind('play-sound',function (data) {
        playSound()
    })
})
function playSound() {
    const audio = new Audio('https://personal-lawyer.it-lead.net/notification.wav');
    audio.play();
}
