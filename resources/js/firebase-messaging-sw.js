// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";
import axios from "axios";
var firebaseConfig = {
    apiKey: "AIzaSyDWFWvqsuEVD8ywXltFVxompIAMaru3jhs",
    authDomain: "personal-lawyer-cd2d0.firebaseapp.com",
    projectId: "personal-lawyer-cd2d0",
    storageBucket: "personal-lawyer-cd2d0.appspot.com",
    messagingSenderId: "435421356408",
    appId: "1:435421356408:web:ac06a774d24f2629bfa88d",
    measurementId: "G-BQ6XKD7RP0"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging();

messaging.getToken({ vapidKey: 'BGNRXxyDYPi94rXsd0HFvzrP-8zmrq4IUGv0VBJTi2120m4eETvk80BbuhylP2x3-B07xFMbDhZvGEzQno5AOa8' }).then((currentToken) => {
    if (currentToken) {
        axios.post('/register/device',{
            device_token:currentToken
        }).then(response=>console.log(response.data))
    } else {
        // Show permission request UI
        console.log('No registration token available. Request permission to generate one.');
        // ...
    }
}).catch((err) => {
    console.log('An error occurred while retrieving token. ', err);
    // ...
});
