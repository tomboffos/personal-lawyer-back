@extends('voyager::master')

@section('content')
    <div class="container ">
        <div class="row" id="orders-table">

            @foreach($orders as $order)

                <div class="col-lg-12">
                    <a href="/admin/chats/{{$order->id}}" class="chat-link" data-order-id="{{$order->id}}">

                        <div class="card p-3">
                            <p>
                                Чат номер № {{$order->id}}
                                {{$order->user->name}} : {{$order->question_title}}
                                <br>
                                <span>{{$order->created_at->format('d.m.y h:m')}}</span>
                            </p>
                            <p id="chat-counter-{{$order->id}}">
                                {{$order->lawyer == null ? 'Свободно': $order->newMessages(\Illuminate\Support\Facades\Auth::user())}}
                            </p>


                        </div>
                    </a>
                </div>

            @endforeach
            <div class="col-lg-12">
                {{$orders->links()}}
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
            integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
            crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script>
        function chatLinks(){
            let chatLink = document.querySelectorAll('.chat-link').forEach(link => {
                link.addEventListener('click', (e) => {
                    e.preventDefault()
                    axios.get(`/get/chat/${link.getAttribute('data-order-id')}`).then(response => {
                        if (response.status === 201) swal(response.data.message);
                        setTimeout(() => {
                            window.location.href = link.getAttribute('href')
                        }, response.status === 200 ? 0 : 1500)
                    }).catch(e => swal(e.response.data.message))
                })
            })

        }
        chatLinks()
        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('71b7e222b90fdffa56e6', {
            cluster: 'ap2'
        });
        var doc = document.getElementsByClassName('top-right');

        var channel = pusher.subscribe('App.Chats');

        channel.bind('pusher:subscription_succeeded', function (members) {


        });
        channel.bind('chat-updated',function(data){
            console.log(data);
            let chatCounter = document.getElementById(`chat-counter-${data.order.id}`)
            chatCounter.innerHTML = data.count;
        })
        channel.bind('chat-created', function (data) {
            let ordersTable = document.getElementById('orders-table')
            ordersTable.innerHTML = `<div class="col-lg-12">
                                                <a href="/admin/chats/${data.order.id}" class="chat-link" data-order-id="${data.order.id}">

                                                            <div class="card p-3">
                                                                <p>
                                                                   Чат номер № ${data.order.id} ${data.order.user.name} : ${data.order.question_title}
                                                <br>
                                                <span>${data.order.created_at}</span>
                                                                </p>
                                                                <p>
                                                                    ${data.order.lawyer == null ? 'Свободно': ''}
                                                </p>


                                            </div>
                                        </a>
                                    </div>
                            ` + ordersTable.innerHTML
            chatLinks()
        });
    </script>
    <style>
        .container {
            margin-top: 50px;
        }

        .container .col-lg-12 .card {
            padding: 20px;
            display: flex;
            font-size: 20px;
            font-weight: bold;
            color: #000;
            justify-content: space-between;
        }

        p span {
            font-size: 15px;
            color: #ccc;
        }

    </style>

@endsection
