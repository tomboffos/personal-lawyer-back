@extends('voyager::master')

@section('content')
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"
            integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('71b7e222b90fdffa56e6', {
            cluster: 'ap2'
        });
        var doc = document.getElementsByClassName('top-right');

        var channel = pusher.subscribe('App.Chat.{{$order->id}}');

        channel.bind('pusher:subscription_succeeded', function (members) {


        });

        channel.bind('message-created', function (data) {
            var objDiv = document.getElementById("card");
            let lawyerId = document.getElementById("lawyer_id").value
            var node = document.createElement("div");
            var textnode = document.createTextNode(data.message.message)
            node.append(textnode)
            console.log(data)
            if (lawyerId == data.message.user.id) {

                node.classList.add('myMessage', 'mine')
            } else {
                node.className += 'myMessage'
            }
            objDiv.prepend(node)
            var objDiv = document.getElementById("card");
            objDiv.scrollTop = objDiv.scrollHeight;

        });
    </script>

    <input type="hidden" id="lawyer_id" value="{{$order->lawyer_account->id}}">

    <div class="container ">
        <div class="row">
            <input type="hidden" id="left-time" value="{{$order->end_time}}">

            <div class="col-lg-12">
                <div class="account-card">
                    <p>Клиент : {{$order->user->name}}</p>
                    <div class="timer-main">
                        <p id="days"></p>
                        <p id="hours"></p>
                        <p id="mins"></p>
                        <p id="secs"></p>
                        <p id="end"></p>
                    </div>
                </div>
                <div class="card messages" id="card">
                    @foreach($messages as $message)
                        <div
                            class="myMessage @if($message->user_id == $order->lawyer_account->id) mine @else @endif">
                            {{$message['message']}}
                        </div>
                    @endforeach
                </div>
                <div class="card-form card">
                    <form id="main-form" method="post" class="form-inline">
                        {{csrf_field()}}
                        <input type="text" class="form-control" id="message" name="message"
                               placeholder="Напишите сообщение">
                        <input type="hidden" name="user_id" id="user_id"
                               value="{{\Illuminate\Support\Facades\Auth::user()->id}}">
                        <input type="hidden" name="order_id" id="order_id" value="{{$order->id}}">
                        <input type="submit" class="btn btn-primary" name="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
        .account-card {
            display: flex;
            justify-content: space-between;
            background-color: #fff;
            color: #000;
            font-size: 20px;
            font-weight: bold;
            padding: 15px;
            align-items: center;
        }

        .container {
            margin-top: 50px;
        }

        .card-form.card {
            min-width: 100%;
        }

        .card-form.card input[type='text'] {
            min-width: 90%;
            padding: 2px;
        }

        .container .col-lg-12 .card.messages {
            padding: 20px;
            font-size: 20px;
            font-weight: bold;
            color: #000;
            justify-content: space-between;
        }

        .timer-main {
            display: flex;
        }

        .card.messages {
            min-height: 500px;
            max-height: 500px;
            overflow-y: scroll;
            display: flex;

            flex-direction: column-reverse;
        }

        .card .myMessage {
            background-color: #fff;
            color: #000;
            border: 1px solid #000;
            max-width: 50%;
            margin-top: 10px;

            padding: 10px;
            border-radius: 10px;

            text-align: left;
            word-break: break-all;
        }

        .myMessage.mine {
            color: white;
            margin-left: auto;
            text-align: left;
            background-color: #4c75a3;
        }
    </style>
    @if($order->lawyer != null)
        <script>
            let timeValue = document.getElementById('left-time').value
            // The data/time we want to countdown to
            var countDownDate = new Date(timeValue).getTime();

            // Run myfunc every second
            var myfunc = setInterval(function () {

                var now = new Date().getTime();
                var timeleft = countDownDate - now;

                // Calculating the days, hours, minutes and seconds left
                var days = Math.floor(timeleft / (1000 * 60 * 60 * 24));
                var hours = Math.floor((timeleft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((timeleft % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((timeleft % (1000 * 60)) / 1000);

                // Result is output to the specific element
                document.getElementById("days").innerHTML = days + "д "
                document.getElementById("hours").innerHTML = hours + "ч "
                document.getElementById("mins").innerHTML = minutes + "м "
                document.getElementById("secs").innerHTML = seconds + "с "

                // Display the message when countdown is over
                if (timeleft < 0) {
                    clearInterval(myfunc);
                    document.getElementById("days").innerHTML = ""
                    document.getElementById("hours").innerHTML = ""
                    document.getElementById("mins").innerHTML = ""
                    document.getElementById("secs").innerHTML = ""
                    document.getElementById("end").innerHTML = "Время закончено!!";
                }
            }, 1000);
        </script>
    @endif

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
            integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
            crossorigin="anonymous"></script>
    <script>
        var objDiv = document.getElementById("card");
        objDiv.scrollTop = objDiv.scrollHeight;

        let chatForm = document.getElementById('main-form')
        chatForm.addEventListener('submit', async (e) => {
            e.preventDefault()

            let response = await axios.post('/chat', {

                user_id: document.getElementById('user_id').value,
                order_id: document.getElementById('order_id').value,
                message: document.getElementById('message').value

            })
            console.log(response.body)
            document.getElementById('message').value = null
            console.log('event')
        })
    </script>

@endsection
