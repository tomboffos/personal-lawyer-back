<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('order_statuses')->insert([
            'name' => 'Ожидание'
        ]);

        DB::table('order_statuses')->insert([
            'name' => 'Ожидание адвоката'
        ]);

        DB::table('order_statuses')->insert([
            'name' => 'Начало чат'
        ]);

        DB::table('order_statuses')->insert([
            'name' => 'Чат закрыт'
        ]);

        DB::table('order_statuses')->insert([
            'name' => 'Документ готов к скачиванию'
        ]);

    }
}
