<?php


namespace App\Services;


use App\Models\Message;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class NotificationService
{

    protected $serverKey = 'AAAAZWEioXg:APA91bH0NKBI5mhRm92Bwqu4B0UELite-xPDiPwOukY9go0n2StWW3AvwVDKSkZXUWDqisdztGrcdJu7_RtEkB_oQ8v9dZe1xLnAhGpGvhHiBfFcWr_ZScEmSwBh_R7DBbWYCCxw1g98';

    private $order;

    public function sendMessage(Message $message)
    {
        $this->order = Order::find($message->order_id);

        if ($this->order->user_id == $message->user_id) {
            $this->sendNotificationRequest($this->order->lawyer_account, $message->message, false, 'Ваш клиент '.$this->order->user->name);
        }else{
            $this->sendNotificationRequest($this->order->user, $message->message, true, 'Ваш персональный адвокат '.$this->order->lawyer_account->name);
        }
        return true;
    }

    public function sendNotificationRequest($user, $message, $isLawyer, $text)
    {
        $ids = [$user->device_token];
        if (!$isLawyer && $ids[0] != User::find(1)->device_token)
            $ids[] = User::find(1)->device_token;
        Log::info('check ids '.implode(',',$ids));

        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $ids,
            'notification' => [
                'title' => $text,
                'body' => $message,
                "sound" => 'notification.wav',
                "color" => "#ffffff"
            ],
//            "webpush" => [
//                "fcm_options" => [
//                    "link" => 'https://personal-lawyer.a-lux.dev/admin/chats/' . $this->order->id
//                ]
//            ]
        ]);
        Log::error('message ' . $response->body() . ' ' . $user->device_token);
    }


    public function sendNotificationMessage($text,$users = false)
    {
        if(!$users){
            $device_ids =  User::whereIn('role_id',[3,1])->pluck('device_token');
        }else{
            $device_ids = User::where('role_id',2)->pluck('device_token');
        }

        $response = Http::withHeaders([
            'Authorization' => "key=$this->serverKey",
            'Content-Type' => 'application/json',
        ])->post('https://fcm.googleapis.com/fcm/send', [
            "registration_ids" => $device_ids,
            'notification' => [
                'title' => 'Ваш личный адвокат',
                'body' => $text,
                "sound" => 'notification.wav',
                "badge" => 0,
                "color" => "#ffffff"
            ],
            "webpush" => [
                "fcm_options" => [
                    "link" => !$users ? 'https://personal-lawyer.a-lux.dev/chats' : ''
                ]
            ]
        ]);
        Log::error('message ' . $response->body() );

    }
}
