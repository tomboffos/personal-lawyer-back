<?php


namespace App\Services;


use App\Events\ChatCreated;
use App\Models\Order;
use App\Models\User;
use App\Paybox\Paybox;
use Illuminate\Support\Str;

class OrderDocumentService
{
    protected $payment_service;

    public function __construct()
    {
        $this->payment_service = new Paybox();
    }


    public function setDefaultValues($data, $user)
    {
        $data['payment_status_id'] = 1;
        $data['order_status_id'] = 1;
        $data['user_id'] = $user->id;
        $data['service_id'] = 1;
        $data['card_id'] = $user->cardMain->id;
        return $data;
    }

    public function userBalancePayment(Order $order, User $user, $request)
    {
        if ($request->use_bonuses) {
            if ($user['balance'] >= $order->service->price) {
                $user->update(['balance' => $user['balance'] - $order->service->price]);
                $order->update(['payment_status_id' => 2]);
                return response(['payment' => $order], 201);
            } else {
                $user->update(['balance' => $order->service->price - $user['balance']]);
                return response(['payment' => $this->pay($order, $order->service->price - $user['balance'])], 200);
            }
        }
        return response(['payment' => $this->pay($order)], 200);

    }

    public function pay(Order $order, $price = null)
    {
        $payment_service = new Paybox();
        $payment_service->setQuery([
            'pg_amount' => $price == null ? $order->service->price : $price,
            'pg_order_id' => (string)$order->id,
            'pg_user_id' => (string)$order->user->id,
            'pg_card_id' => (string)$order->card->card_id,
            'pg_description' => 'Заказ документа номер ' . $order->id,
            'pg_salt' => Str::random(10),
            'pg_success_url' => route('success-payment'),
            'pg_failure_url' => route('fail-payment'),
            'pg_result_url' => route('payment-status'),
            'pg_success_url_method' => 'GET',
            'pg_failure_url_method' => 'GET'
        ]);

        return $payment_service->pay();
    }

    public function orderResult(Order $order)
    {

        $order->update([
            'order_status_id' => 5,
            'payment_status_id' => 2

        ]);

        return response(['message' => '<pg_status>ok</pg_status>'], 200);
    }
}
