<?php


namespace App\Services;


use App\Models\Order;
use App\Models\Rating;
use App\Models\User;

class RatingService
{
    public function updateRating(Rating $rating,User $user)
    {
        if ($rating->is_positive) $user->update(['positive_rating' => $user->positive_rating + 1]);
        else $user->update(['negative_rating' => $user->negative_rating + 1]);

    }
}
