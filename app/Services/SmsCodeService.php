<?php


namespace App\Services;


use App\Models\SmsCode;
use App\Models\User;
use Illuminate\Support\Facades\Http;

class SmsCodeService
{
    protected $token = 'kz72378bb13726a3d8d5dc14e538dd270d69126bf5009ed9d64a5ad592db34cf693843';

    public function send(SmsCode $smsCode,User $user)
    {
        $data = http_build_query([
            'recipient' => $user->phone,
            'text' => "Ваш код $smsCode->code, приложение Ваш личный адвокат",
            'apiKey' => $this->token
        ]);


        $response = Http::post("https://api.mobizon.kz/service/message/sendsmsmessage?$data");
        if ($response->successful()){
            return true;
        }else{
            return false;
        }
    }

    public function generate(User $user)
    {

        if ($user->code){
            $user->code->update([
                'code' => mt_rand(1000,9999)

            ]);
            $code = $user->code;
        }else{
            $code = SmsCode::create([
               'user_id' => $user->id,
               'code' => mt_rand(1000,9999)
            ]);
        }
        return $this->send($code,$user);

    }


}
