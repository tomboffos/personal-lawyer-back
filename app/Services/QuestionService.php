<?php


namespace App\Services;


use App\Events\ChatCreated;
use App\Models\Message;
use App\Models\Order;
use App\Models\User;
use App\Paybox\Paybox;
use Carbon\Carbon;
use Illuminate\Support\Str;

class QuestionService
{
    private $payment_service;
    private $notification_service;

    public function __construct()
    {
        $this->payment_service = new Paybox();
        $this->notification_service = new NotificationService();
    }

    public function setDefaultValues($data, User $user)
    {
        $data['payment_status_id'] = 1;
        $data['order_status_id'] = 1;
        $data['user_id'] = $user->id;
        $data['card_id'] = $user->cardMain->id;
        return $data;

    }

    public function userBalancePayment(User $user, Order $order, $request)
    {
        if ($request->use_bonuses) {
            if ($user['balance'] >= $order->service->price) {
                $user->update(['balance' => $user['balance'] - $order->service->price]);
                $order->update(['payment_status_id' => 2]);
                ChatCreated::dispatch($order);
                return response(['payment' => $order], 201);
            } else {
                if (!$request->user()->cardMain)
                    return response(['message' => 'Выберите карту'], 404);

                $user->update(['balance' => $order->service->price - $user['balance']]);
                return response(['payment' => $this->payment($order, $order->service->price - $user['balance'])], 200);
            }

        }
        if (!$request->user()->cardMain)
            return response(['message' => 'Выберите карту'], 404);

        return response(['payment' => $this->payment($order)], 200);

    }

    public function payment(Order $order, $price = null)
    {
//        $price != null ? $price : $order->service->price
        $this->payment_service->setQuery([
            'pg_amount' => $price == null ? $order->service->price : $price,
            'pg_order_id' => (string)$order->id,
            'pg_user_id' => (string)$order->user->id,
            'pg_card_id' => (string)$order->card->card_id,
            'pg_description' => 'Заказ консультации номер ' . $order->id,
            'pg_salt' => Str::random(10),
            'pg_success_url' => route('success-payment'),
            'pg_failure_url' => route('fail-payment'),
            'pg_result_url' => route('payment-status')
        ]);

        return $this->payment_service->pay();
    }

    public function questionStatus(Order $order)
    {
        $order->update([
            'order_status_id' => 2,
            'payment_status_id' => 2
        ]);
        event(new ChatCreated($order));
        $this->notification_service->sendNotificationMessage('Заказ консультации номер ' . $order->id);


        return response(['message' => '<pg_status>ok</pg_status>'], 200);
    }

    public function timeoutHandler(Order $order)
    {
        if ($order['start_time'] >= Carbon::now() && Carbon::now() <= $order['end_time']) {
            if ($order['order_status_id'] != 4) $order->update([
                'order_status_id' => 4
            ]);
            return response(['message' => 'Ваш чат был окончен'], 400);
        }
    }

    public function checkTimer($orders)
    {
        foreach ($orders as $order) {
            if (Carbon::now() > $order['end_time'] && $order->order_status_id == 3) $order->update([
                'order_status_id' => 4
            ]);
        }
    }

    public function setAllMessages(Order $order, User $user)
    {
        foreach ($order->messages->where('user_id', '!=', $user->id) as $message)
            $message->update(['is_read' => 1]);

    }
}
