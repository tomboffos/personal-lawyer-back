<?php


namespace App\Services;


use App\Models\Order;
use App\Models\User;

class CounselService
{
    public function distributeBalance(Order $order)
    {
        $order->lawyer_account->update([
            'balance' => $order->lawyer_account->balance + $order->service->price/2
        ]);
    }
}
