<?php
namespace App\Paybox;

use App\Models\Card;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Psy\Util\Str;

class Paybox
{
    private $params = [];
    private $secret_key = 'Z1JXnO9xAlymx7WR';
    private $withdraw_secret_key = 'edMGvdCMHUBba6Mb';
    private $merchant = 524892;
    private $second_payment = [];


    public function __construct($mode = null)
    {
        $this->params['pg_merchant_id'] = $this->merchant;
        $this->params['pg_testing_mode'] =0;
        if ($mode == 'testing')
            $this->params['pg_testing_mode'] = 0;


    }

    public function setQuery(array $query)
    {
        foreach ($query as $key => $item){
            $this->params[$key] = $item;
        }
    }

    public function setSecondQuery(array $query)
    {
        foreach ($query as $key => $item){
            $this->second_payment[$key] = $item;
        }
    }
    public function generateSignature(array $query , $api = 'payment.php',$secret_key = 'Z1JXnO9xAlymx7WR')
    {
        ksort($query);
        array_unshift($query,$api);

        array_push($query,$secret_key);
        $query['pg_sig'] = md5(implode(';',$query));

        unset($query[0],$query[1]);

        return $query;

    }

    public function addCard()
    {
        $this->params = $this->generateSignature($this->params,'add');
//        dd($this->params);
        return Http::withHeaders([
            'X-Content-Type' => 'multipart/form-data'
        ])->post('https://api.paybox.money/v1/merchant/'.$this->merchant.'/cardstorage/add',$this->params);
    }

    public function pay()
    {
        $this->params = $this->generateSignature($this->params,'init');
        $response = Http::withHeaders([
            'X-Content-Type' => 'multipart/form-data'
        ])->post('https://api.paybox.money/v1/merchant/'.$this->merchant.'/card/init',$this->params);

        Log::error('main message '.$response->body());
        $xml = new \SimpleXMLElement($response->body());
        $xml = (array) $xml;

        $save_query = [
            'pg_merchant_id' => $this->merchant,
            'pg_payment_id' => $xml['pg_payment_id'],
            'pg_salt' => \Illuminate\Support\Str::random(10),
        ];

        $save_query = $this->generateSignature($save_query,'pay');
        $pay_response = Http::withHeaders([
            'X-Content-Type' => 'multipart/form-data'
        ])->post('https://api.paybox.money/v1/merchant/'.$this->merchant.'/card/pay',$save_query);

        return (string) str_replace('PayBox.Money. Ввод cvc','PayBox.Money',$pay_response->getBody()->getContents());

    }

    public function saveCard($xml)
    {
        try {
            $xmlData = new \SimpleXMLElement($xml);
            $saveCard = Card::create([
                'user_id' => $xmlData->pg_user_id,
                'card_3ds' => $xmlData->pg_card_3ds,
                'card_hash' => $xmlData->pg_card_hash,
                'card_id' => $xmlData->pg_card_id,
                'expiration_date' => $xmlData->pg_card_month.'/'.$xmlData->pg_card_year
            ]);
            return ['success' => true,'card' => $saveCard];
        }catch (\Exception $e){
            Log::error('Error '.$e);
            return false;
        }

    }

    public function withdraw()
    {
        $this->params = $this->generateSignature($this->params,'reg2reg',$this->withdraw_secret_key);

        $response = Http::withHeaders([
            'X-Content-Type' => 'multipart/form-data'
        ])->post('https://api.paybox.money/api/reg2reg',$this->params);
        $response = (array) new \SimpleXMLElement($response);

        return $response;
    }


    public function withdrawWithoutCard()
    {
        $this->params = $this->generateSignature($this->params,'reg2nonreg',$this->withdraw_secret_key);
        $response = Http::withHeaders([
            'X-Content-Type' => 'multipart/form-data'
        ])->post('https://api.paybox.money/api/reg2nonreg',$this->params);

        $response = (array) new \SimpleXMLElement($response);
        return $response;
    }



}
