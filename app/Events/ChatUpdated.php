<?php

namespace App\Events;

use App\Http\Resources\QuestionResource;
use App\Models\Order;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ChatUpdated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    private $order;
    private $user;
    public function __construct(Order $order,User $user)
    {
        //
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('App.Chats');
    }

    public function broadcastAs()
    {
        return 'chat-updated';
    }

    public function broadcastWith()
    {
        return [
            'order' => new QuestionResource($this->order),
            'count' => $this->order->messages()->where('user_id',$this->user->id)->count()
        ];

    }

}
