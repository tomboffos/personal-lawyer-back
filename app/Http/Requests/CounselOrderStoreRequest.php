<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CounselOrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'question_title' => 'required',
            'service_id' => 'required|exists:services,id',
            'lawyer' => 'required|exists:users,id'
        ];
    }
}
