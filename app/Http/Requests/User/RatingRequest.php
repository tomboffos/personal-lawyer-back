<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class RatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'lawyer_id' => 'required|exists:users,id',
            'order_id' => 'required|exists:orders,id',
            'is_positive' => 'required'
        ];
    }
}
