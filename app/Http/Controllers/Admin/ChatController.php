<?php

namespace App\Http\Controllers\Admin;

use App\Events\MessageCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreMessageRequest;
use App\Http\Resources\User\MessageResource;
use App\Models\Message;
use App\Models\Order;
use App\Services\QuestionService;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    //

    private $question_service;

    public function __construct()
    {
        $this->question_service = new QuestionService();
    }

    public function store(StoreMessageRequest $request)
    {
        $data = $request->validated();
        $data['is_read'] = 0;
        $this->question_service->timeoutHandler(Order::find($request->order_id));

        $message = Message::create($data);

        return new MessageResource($message);


    }
}
