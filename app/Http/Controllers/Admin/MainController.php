<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\User\MessageResource;
use App\Models\Message;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{
    //
    public function updates()
    {
        return response([
            'ios' => setting('settings.ios'),
            'android' => setting('settings.android'),
        ]);
    }

    public function user(Request $request)
    {
        return Auth::user();
    }

    public function register(Request $request)
    {
        Auth::user()->update([
            'device_token' => $request->device_token
        ]);

        return response([],200);
    }
    public function getChat(Order $order)
    {
        if ((Auth::user()->role_id == 3 && $order->lawyer == null) || (Auth::user()->role_id == 1 && $order->lawyer == null)) {
            $order->update([
                'lawyer' => Auth::user()->id,
                'order_status_id' => 3,
                'start_time' => Carbon::now(),
                'end_time' => Carbon::now()->addHours(3)
            ]);
            Message::create([
               'user_id' => Auth::user()->id,
               'message' => 'Здравствуйте я ваш адвокат '.Auth::user()->name,
               'order_id' => $order->id,
               'is_read' => 0
            ]);

            return response(['message' => 'Вы успешно взяли заказ'], 201);
        } elseif (Auth::user()->role_id == 3 && $order->lawyer != Auth::user()->id)
            return response(['message' => 'Другой юрист уже взял заказ'], 400);
        return response([], 200);


    }

    public function index()
    {

        if (Auth::user()->role_id == 1) {
            $orders = Order::whereNotNull('question_title')->orderBy('id','desc')->paginate(20);
        } else {
            $orders = Order::where('lawyer', Auth::user()->id)->orWhere(function($query){
                $query->whereNull('lawyer');
                $query->whereNotNull('question_title');
                $query->where('payment_status_id',2);
            })->orderBy('id','desc')->paginate(20);
        }


        return view('chats.browse', [
            'orders' => QuestionResource::collection($orders)
        ]);

    }

    public function show($id)
    {
        Message::where('order_id',$id)->where('user_id','!=',Auth::user()->id)->where('is_read',0)->update([
            'is_read' => 1
        ]);

        return view('chats.show', [
            'order' => new QuestionResource(Order::find($id)),
            'messages' => MessageResource::collection(Order::find($id)->messages)
        ]);
    }

    public function getReport($number)
    {
        $data['days'] = [];
        $data['orders'] = [];
        for ($i = $number; $i>0; $i--){
            $data['days'][] = Carbon::now()->subDays($i-1)->format('d.m.y');
            $data['orders'][] = Order::where('created_at','>=',Carbon::now()->subDays($i))
                ->where('created_at','<=',Carbon::now()->subDays($i-1 == 0 ? 0 : $i-1))
                ->where('payment_status_id',2)->count();
        }

        return response(['data' => $data],200);
    }
}
