<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class NotificationController extends VoyagerBaseController
{
    //
    public function store(Request $request)
    {
        $data = $request->validate([
            'messages' => 'required'
        ]);

        $data = Notification::create($data);
        $notification_service = new NotificationService();
        $notification_service->sendNotificationMessage($data->messages,true);

        return  back();


    }
}
