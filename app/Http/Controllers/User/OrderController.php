<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Services\OrderDocumentService;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OrderController extends Controller
{
    //
    private $document_service;
    private $question_service;

    public function __construct()
    {
        $this->document_service = new OrderDocumentService();
        $this->question_service = new QuestionService();
    }

    public function payment(Request $request)
    {
        if ($request->pg_result) {
            $order = Order::find($request->pg_order_id);
            if ($order['document_id'])
                return $this->document_service->orderResult($order);

            return $this->question_service->questionStatus($order);

        }
    }
}
