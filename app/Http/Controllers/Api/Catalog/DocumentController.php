<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\OrderDocumentRequest;
use App\Http\Resources\Catalog\DocumentCategoryResource;
use App\Http\Resources\Catalog\DocumentResource;
use App\Models\Document;
use App\Models\DocumentCategory;
use App\Models\Order;
use App\Services\OrderDocumentService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DocumentController extends Controller
{
    //

    private $order_service;

    public function __construct()
    {
        $this->order_service = new OrderDocumentService();
    }

    public function categories(Request $request)
    {
        return DocumentCategoryResource::collection(DocumentCategory::orderBy('order','asc')->get()->translate($request->local));
    }


    public function index(DocumentCategory $documentCategory,Request $request)
    {
        return DocumentResource::collection($documentCategory->documents($request->lang));
    }


    public function order(OrderDocumentRequest $request)
    {
        $data = $request->all();

        if (!$request->user()->cardMain)
            return response(['message' => 'Выберите карту'],404);

        $data = $this->order_service->setDefaultValues($data,$request->user());

        $order = Order::create($data);


        return $this->order_service->userBalancePayment($order,$request->user(),$request);
    }
}
