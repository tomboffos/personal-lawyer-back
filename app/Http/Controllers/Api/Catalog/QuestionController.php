<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuestionOrderRequest;
use App\Models\Order;
use App\Models\User;
use App\Services\QuestionService;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    //
    private $question_service;

    public function __construct()
    {
        $this->question_service = new QuestionService();
    }

    public function order(QuestionOrderRequest $request)
    {
        $data = $request->all();

        $data = $this->question_service->setDefaultValues($data, $request->user());

        $order = Order::create($data);

        return $this->question_service->userBalancePayment($request->user(),$order,$request);


    }
}
