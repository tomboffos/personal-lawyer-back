<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Requests\CounselOrderStoreRequest;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\UserResource;
use App\Models\Order;
use App\Models\User;
use App\Services\QuestionService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CounselController extends Controller
{

    /**
     * @var QuestionService
     */
    private $question_service;

    public function __construct()
    {
        $this->question_service = new QuestionService();
    }

    //
    public function index(Request $request)
    {
        return UserResource::collection(User::where('role_id', 4)->get());
    }


    public function show(User $counsel)
    {
        return new UserResource($counsel);
    }


    public function store(CounselOrderStoreRequest $request)
    {
        $data = $request->validated();

        if (!$request->user()->cardMain)
            return response(['message' => 'Выберите карту'], 404);

        $data = $this->question_service->setDefaultValues($data, $request->user());

        $order = Order::create($data);

        return $this->question_service->userBalancePayment($request->user(), $order, $request);
    }

    public function orders(Request $request)
    {
        return QuestionResource::collection($request->user()->orders);
    }

    public function acceptOrder(Order $order)
    {
        if ($order->start_time == null)
            $order->update([
                'start_time' => Carbon::now(),
                'end_time' => Carbon::now()->addHours(3),
                'order_status_id' => 3
            ]);


        return new QuestionResource($order);
    }

}
