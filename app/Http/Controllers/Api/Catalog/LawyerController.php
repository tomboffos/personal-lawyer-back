<?php

namespace App\Http\Controllers\Api\Catalog;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\User\MessageResource;
use App\Models\Message;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LawyerController extends Controller
{
    //
    const ACCEPTABLE_ROLES = [1, 3];

    public function index(Request $request)
    {
        return QuestionResource::collection(Order::where('lawyer', $request->user()->id)->where('service_id', 2)->orWhere(function ($query) {
            $query->whereNull('lawyer');
            $query->whereNotNull('question_title');
            $query->where('payment_status_id', 2);
            $query->where('service_id', 2);
        })->orderBy('id', 'desc')->get());
    }

    public function show(Order $order, Request $request)
    {
        if (in_array($request->user()->role_id, self::ACCEPTABLE_ROLES) && $order->lawyer == null) {
            $order->update([
                'lawyer' => $request->user()->id,
                'order_status_id' => 3,
                'start_time' => Carbon::now(),
                'end_time' => Carbon::now()->addHours(3)
            ]);
            Message::create([
                'user_id' => $request->user()->id,
                'message' => 'Здравствуйте я ваш адвокат ' . $request->user()->name,
                'order_id' => $order->id,
                'is_read' => 0
            ]);
            return new QuestionResource($order);
        } elseif ($request->user()->role_id == 3 && $order->lawyer != $request->user()->id)
            return response(['message' => 'Другой юрист уже взял заказ'], 400);
        return  new QuestionResource($order);

    }
}
