<?php

namespace App\Http\Controllers\Api\User;

use App\Events\ChatUpdated;
use App\Events\MessageCreated;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\MessageStoreRequest;
use App\Http\Resources\User\MessageResource;
use App\Models\Message;
use App\Models\Order;
use App\Services\QuestionService;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    //
    private $question_service;
    public function __construct()
    {
        $this->question_service = new QuestionService();
    }

    public function show(Order $order,Request $request)
    {
        $this->question_service->setAllMessages($order,$request->user());

        return MessageResource::collection($order->messages);
    }

    public function store(MessageStoreRequest $request)
    {
        $data = $request->all();

        $this->question_service->timeoutHandler(Order::find($request->order_id));
        $data['user_id'] = $request->user()->id;
        $message = Message::create($data);

        event(new ChatUpdated(Order::find($request->order_id),$request->user()));

        return new MessageResource($message);

    }
}
