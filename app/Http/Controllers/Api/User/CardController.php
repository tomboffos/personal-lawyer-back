<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\CardResource;
use App\Models\Card;
use App\Paybox\Paybox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class CardController extends Controller
{
    //

    protected $payment_service;

    public function __construct()
    {
        $this->payment_service = new Paybox();
    }

    public function index(Request $request)
    {
        return CardResource::collection($request->user()->cards);
    }

    public function link(Request $request)
    {
        $this->payment_service->setQuery([
            'pg_user_id' => $request->user()->id,
            'pg_post_link' => route('accept-card'),
            'pg_back_link' => route('success-card'),
            'pg_salt' => Str::random(10)
        ]);

        $response = $this->payment_service->addCard();

        $xml = (array) new \SimpleXMLElement($response->body());

        return response(['xml' => $xml],200);
    }


    public function main(Card $card,Request $request)
    {
        foreach ($request->user()->cards as $userCards){
            if ($userCards != $card)
                $userCards->update(['main' => 0]);
        }

        $card->update(['main' => !$card['main']]);
        return new CardResource($card);
    }

    public function acceptCard(Request $request)
    {

        if ($request['pg_xml']){
            $xml = $request->pg_xml;
            $save_card = $this->payment_service->saveCard($xml);
            if ($save_card)
                return response([$save_card],200);
        }else{
            return response([],400);
        }
    }

    public function delete(Card $card)
    {
        $card->delete();

        return response(['message' => 'Карта удалена'],200);
    }
}
