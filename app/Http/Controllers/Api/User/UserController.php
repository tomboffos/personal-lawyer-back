<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\Catalog\DocumentResource;
use App\Http\Resources\DocumentOrderResource;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\UserResource;
use App\Models\Order;
use App\Models\User;
use App\Services\QuestionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //

    private $question_service;
    public function __construct()
    {
        $this->question_service = new QuestionService();
    }

    public function documents(Request $request)
    {
        return DocumentOrderResource::collection($request->user()->documents);
    }

    public function user(Request $request)
    {
        return new UserResource($request->user());
    }

    public function update(UserUpdateRequest $request)
    {
        $data = $request->all();



        $request->user()->update($data);

        return new UserResource($request->user());
    }

    public function questions(Request $request)
    {
        $this->question_service->checkTimer($request->user()->questions);

        return QuestionResource::collection($request->user()->questions);
    }

    public function getLawyer(User $user)
    {
        return new UserResource($user);
    }

}
