<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RatingRequest;
use App\Models\Rating;
use App\Models\User;
use App\Services\RatingService;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    //
    private $rating_service;

    public function __construct()
    {
        $this->rating_service = new RatingService();
    }

    public function rating(RatingRequest $request)
    {
        $data = $request->validated();
        $data['user_id'] = $request->user()->id;
        if (Rating::where('order_id', $data['order_id'])
            ->where('lawyer_id', $data['lawyer_id'])
            ->where('user_id', $request->user()->id)->exists()) return response(['message' => 'Вы уже ставили рейтинг'], 422);

        $rating = Rating::create($data);

        $user = User::find($rating['lawyer_id']);

        $this->rating_service->updateRating($rating, $user);
        return response(['message' => 'Рейтинг успешно сохранен'], 201);


    }
}
