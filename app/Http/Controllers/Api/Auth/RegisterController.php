<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterCheckRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\SmsCode;
use App\Models\User;
use App\Services\SmsCodeService;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //
    protected $sms_service;

    public function __construct()
    {
        $this->sms_service = new SmsCodeService();
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->all();
        $data['balance'] = 3000;
        $user = User::create($data);

        if ($this->sms_service->generate($user))
            return new UserResource($user);
        else
            return response(['message' => 'Произошла непредвинная ошибка'],400);
    }

    public function check(User $user,RegisterCheckRequest $request)
    {
        if ($user->code->code == $request->code){
            $user->update(['device_token' => $request->device_token]);
            $token = $user->createToken(env('APP_NAME'));
            return response([
                'user' => new UserResource($user),
                'token' => $token->plainTextToken,
                'message' => 'Вы успешно вошли в систему'
            ],200);
        }

        return response([
            'message' => 'Не верный код'
        ],400);
    }
}
