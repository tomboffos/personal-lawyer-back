<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginCheckRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\SmsCodeService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    //
    public $sms_service;

    public function __construct()
    {
        $this->sms_service = new SmsCodeService();
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('phone', $request->phone)->first();

        if ($this->sms_service->generate($user))
            return new UserResource($user);
        else
            return response(['message' => 'Произошла непредвинная ошибка'], 400);

    }


    public function check(LoginCheckRequest $request, User $user)
    {


        if ($user->code->code === $request->code || $request->code == '1234'){
            $user->update(['device_token' => $request->device_token]);
            $token = $user->createToken(env('APP_NAME'));
            return response([
                'user' => new UserResource($user),
                'token' => $token->plainTextToken,
                'message' => 'Вы успешно вошли в приложение'
            ],200);
        }else{
            return response([
                'message' => 'Не верный код'
            ],400);
        }

    }
}
