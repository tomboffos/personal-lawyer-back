<?php

namespace App\Http\Resources;

use App\Http\Resources\User\MessageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'payment_status' => $this->payment_status,
            'order_status' => $this->order_status,
            'service' => $this->service,
            'lawyer' => new UserResource($this->lawyer_account),
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
            'created_at' => $this->created_at,
            'question_title' => $this->question_title,
            'new_messages' =>$this->newMessages($this->user),
            'new_lawyer_messages' => $this->lawyer_account == null ? 0 :  $this->newMessages($this->lawyer_account)
        ];
    }
}
