<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
            'name' => $this->name,
            'document' => is_array(json_decode($this->document)) ? asset('storage/'.json_decode($this->document)[0]->download_link) : asset('storage/documents/'.$this->document)
        ];
    }
}
