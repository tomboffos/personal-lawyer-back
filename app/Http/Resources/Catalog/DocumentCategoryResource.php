<?php

namespace App\Http\Resources\Catalog;

use App\Models\DocumentCategory;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'children' => DocumentCategoryResource::collection($this->children),
            'parent' => new DocumentCategoryResource($this->parent),
        ];
    }
}
