<?php

namespace App\Http\Resources;

use App\Http\Resources\Catalog\DocumentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id' => $this->id,
            'service' => $this->service,
            'card' => $this->card,
            'document' => new DocumentResource($this->document)
        ];
    }
}
