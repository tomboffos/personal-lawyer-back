<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'phone' => $this->phone,
            'avatar' => asset('storage/'.$this->avatar),
            'balance' => $this->balance,
            'positive_rating' => $this->positive_rating,
            'negative_rating' => $this->negative_rating,
            'role_id' => $this->role_id
        ];
    }
}
