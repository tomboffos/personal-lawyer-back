<?php

namespace App\Observers;

use App\Events\ChatCreated;
use App\Events\LawyerPlaySound;
use App\Events\MessageCreated;
use App\Models\Message;
use App\Services\NotificationService;

class MessageObserver
{
    /**
     * Handle the Message "created" event.
     *
     * @param \App\Models\Message $message
     * @param NotificationService $service
     * @return void
     */
    public function created(Message $message)
    {
        //
        event(new MessageCreated($message));
        event(new LawyerPlaySound($message->chat->lawyer_account));
        $notificationService = new NotificationService();
        $notificationService->sendMessage($message);

    }

    /**
     * Handle the Message "updated" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function updated(Message $message)
    {
        //
    }

    /**
     * Handle the Message "deleted" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function deleted(Message $message)
    {
        //
    }

    /**
     * Handle the Message "restored" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function restored(Message $message)
    {
        //
    }

    /**
     * Handle the Message "force deleted" event.
     *
     * @param \App\Models\Message $message
     * @return void
     */
    public function forceDeleted(Message $message)
    {
        //
    }
}
