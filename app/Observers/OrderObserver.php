<?php

namespace App\Observers;

use App\Events\ChatCreated;
use App\Models\Order;
use App\Services\CounselService;
use App\Services\NotificationService;

class OrderObserver
{
    /**
     * Handle the Order "created" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function created(Order $order)
    {
        //

    }

    /**
     * Handle the Order "updated" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function updated(Order $order)
    {
        //
        $notification_service = new NotificationService();
        $counsel_service = new CounselService();
        event(new ChatCreated($order));

        if ($order->getOriginal('payment_status_id') != $order->payment_status_id)
            if ($order->lawyer != null) $notification_service->sendNotificationRequest($order->lawyer_account, 'Вам пришел заказ на консультацию', 0, 'Уведомление по заказу');
            else $notification_service->sendNotificationMessage('Пришел новый заказ '.$order->id);
        if ($order->getOriginal('order_status_id') != $order->order_status_id && $order->service_id == 3)
            switch ($order->order_status_id){
                case 4:
                    $counsel_service->distributeBalance($order);
                    break;
                case 3:
                    $notification_service->sendNotificationRequest($order->user,'Консультация с адвокатом началась',0,'Уведомление по заказу');
                    break;
                default:
                    break;
            };
    }

    /**
     * Handle the Order "deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function deleted(Order $order)
    {
        //
    }

    /**
     * Handle the Order "restored" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function restored(Order $order)
    {
        //
    }

    /**
     * Handle the Order "force deleted" event.
     *
     * @param \App\Models\Order $order
     * @return void
     */
    public function forceDeleted(Order $order)
    {
        //
    }
}
