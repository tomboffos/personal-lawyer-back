<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class DocumentCategory extends Model
{
    use HasFactory, SoftDeletes, Translatable;


    protected $translatable = [
        'name',
    ];

    public function documents($lang)
    {
        return $this->hasMany(Document::class)->where('language', $lang)->get();
    }

    public function children()
    {
        return $this->hasMany(DocumentCategory::class);
    }

    public function parent()
    {
        return $this->belongsTo(DocumentCategory::class);
    }
}
