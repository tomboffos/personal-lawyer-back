<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'user_id',
        'payment_status_id',
        'order_status_id',
        'lawyer',
        'start_time',
        'end_time',
        'document_id',
        'service_id',
        'card_id',
        'question_title'
    ];

    public function newMessages(User $user)
    {
        return $this->hasMany(Message::class)->where('user_id','!=',$user->id)->where('is_read',0)->count();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class);
    }

    public function order_status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function document()
    {
        return $this->belongsTo(Document::class);
    }

    public function lawyer_account()
    {
        return $this->belongsTo(User::class,'lawyer','id');
    }

    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class)->orderBy('id','desc');
    }




}
