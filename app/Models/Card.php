<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [
        'firm',
        'card_hash',
        'expiration_date',
        'user_id',
        'card_3ds',
        'card_id',
        'main'
    ];



}
