<?php

use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Catalog\CounselController;
use App\Http\Controllers\Api\Catalog\DocumentController;
use App\Http\Controllers\Api\Catalog\LawyerController;
use App\Http\Controllers\Api\Catalog\QuestionController;
use App\Http\Controllers\Api\User\CardController;
use App\Http\Controllers\Api\User\ChatController;
use App\Http\Controllers\Api\User\RatingController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\User\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/versions',[MainController::class,'updates']);

Route::get('/get/report/{number}',[MainController::class,'getReport']);
Route::post('/accept/card',[CardController::class,'acceptCard'])->name('accept-card');
Route::post('/payment/status',[OrderController::class,'payment'])->name('payment-status');
Route::prefix('/auth')->group(function(){
    Route::prefix('/register')->group(function(){
        Route::post('/',[RegisterController::class,'register']);
        Route::post('/check/{user}',[RegisterController::class,'check']);
    });

    Route::prefix('/login')->group(function (){
        Route::post('/',[LoginController::class,'login']);
        Route::post('/check/{user}',[LoginController::class,'check']);
    });
});
Route::prefix('/user')->middleware('auth:sanctum')->group(function(){
    Route::get('/',[UserController::class,'user']);
    Route::post('/',[UserController::class,'update']);
    Route::get('/lawyer/{user}',[UserController::class,'getLawyer']);
    Route::get('/documents',[UserController::class,'documents']);
    Route::post('/rating',[RatingController::class,'rating']);
    Route::prefix('/questions')->group(function(){
        Route::get('/',[UserController::class,'questions']);
        Route::get('/{order}',[ChatController::class,'show']);
        Route::post('/',[ChatController::class,'store']);
    });
    Route::resource('/counsels',CounselController::class);
    Route::prefix('/counsel')->group(function(){
        Route::get('/orders',[CounselController::class,'orders']);
        Route::get('/orders/{order}',[CounselController::class,'acceptOrder']);
    });

    Route::prefix('/lawyers')->group(function(){
        Route::resource('/orders',LawyerController::class);
    });
});

Route::prefix('/documents')->group(function(){
    Route::get('/categories',[DocumentController::class,'categories']);
    Route::get('/{documentCategory}',[DocumentController::class,'index']);
    Route::post('/',[DocumentController::class,'order'])->middleware('auth:sanctum');
});

Route::prefix('/questions')->group(function(){
    Route::post('/',[QuestionController::class,'order'])->middleware('auth:sanctum');
});

Route::prefix('/card')->middleware('auth:sanctum')->group(function(){
    Route::get('/',[CardController::class,'index']);
    Route::post('/',[CardController::class,'link']);
    Route::post('/{card}',[CardController::class,'delete']);
    Route::post('/main/{card}',[CardController::class,'main']);
});

