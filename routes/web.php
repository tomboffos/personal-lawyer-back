<?php

use App\Http\Controllers\Admin\ChatController;
use App\Http\Controllers\Admin\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/card/success',function(){
   return view('welcome');
})->name('success-card');


Route::post('/success/page',function(){
    return redirect()->route('success-page');
})->name('success-payment');

Route::post('/payment/fail',function(){
    return redirect()->route('failure-page');
})->name('fail-payment');

Route::get('/payment/success',function (){
    return view('welcome');
})->name('success-page');


Route::get('/failure/page',function (){
    return view('welcome');
})->name('failure-page');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::resource('/chats',MainController::class);
    Route::get('/user',[MainController::class,'user']);
});
Route::post('/register/device',[MainController::class,'register']);
Route::get('/get/chat/{order}',[MainController::class,'getChat']);
Route::post('/chat',[ChatController::class,'store']);

